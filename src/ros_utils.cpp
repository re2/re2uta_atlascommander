/*
 * ros_utils.cpp
 *
 *  Created on: Apr 22, 2013
 *      Author: andrew.somerville
 */


#include "ros_utils.hpp"
#include <re2/eigen/eigen_util.h>

namespace re2uta // fixme this should be a completely separate package
{

geometry_msgs::Vector3StampedPtr buildVec3Stamped( const std::string & frameId, const Eigen::Vector3d & vec )
{
    geometry_msgs::Vector3StampedPtr message( new geometry_msgs::Vector3Stamped );

    message->header.frame_id = frameId;
    Eigen::Vector3dMap( & message->vector.x ) = vec;

    return message;
}



std_msgs::UInt8Ptr
buildUint8( unsigned int value )
{
    std_msgs::UInt8::Ptr msg( new std_msgs::UInt8 );
    msg->data = value;
    return msg;
}

std_msgs::Int8Ptr
buildInt8( unsigned int value )
{
    std_msgs::Int8::Ptr msg( new std_msgs::Int8 );
    msg->data = value;
    return msg;
}


std_msgs::UInt16Ptr
buildUint16( unsigned int value )
{
    std_msgs::UInt16::Ptr msg( new std_msgs::UInt16 );
    msg->data = value;
    return msg;
}


std_msgs::Int16Ptr
buildInt16( unsigned int value )
{
    std_msgs::Int16::Ptr msg( new std_msgs::Int16 );
    msg->data = value;
    return msg;
}



}

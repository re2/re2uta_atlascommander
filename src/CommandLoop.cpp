/*
 * CommandLoop.cpp
 *
 *  Created on: May 3, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/AtlasCommander/CommandLoop.hpp>
#include <osrf_msgs/JointCommands.h>
#include <ros/ros.h>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>


namespace re2uta
{
namespace atlascommander
{


CommandLoop::
CommandLoop( const ros::Duration & commandLoopPeriod )
{
    m_shutdown          = false;
    m_iteration         = 0;
    m_commandLoopPeriod = commandLoopPeriod;
    m_commandLoopThread = boost::thread( &CommandLoop::commandLoop, this );
    m_jointCommandPub   = m_node.advertise<osrf_msgs::JointCommands>( "/atlas/joint_commands", 1 );
}


osrf_msgs::JointCommands::Ptr
CommandLoop::
getLatestCommand()
{
//                currentCommand.reset();
//                boost::atomic_exchange( currentCommand, m_currentCommand );
}



void
CommandLoop::
commandLoop()
{
    ROS_INFO( "Starting command loop, period is: %2.3f", m_commandLoopPeriod.toSec() );
    using boost::format;
    osrf_msgs::JointCommands::Ptr currentCommand;

    int           overruns = 0;
    ros::Duration totalOverrun;
    m_nextStart = ros::Time::now() + m_commandLoopPeriod;
    while( ros::ok() && !m_shutdown )
    {
        m_nextStart = m_nextStart + m_commandLoopPeriod;

        currentCommand = getLatestCommand();

        if( currentCommand )
        {
            ROS_INFO_STREAM_THROTTLE( 5, "Incomplete AtlasCommander::lowLevelCommandLoop()" );

            m_jointCommandPub.publish( currentCommand );
        }

        ros::Duration delayUntilNext;
        delayUntilNext = m_nextStart - ros::Time::now();
        if( delayUntilNext.toSec() < 0 )
        {
            ++overruns;
            totalOverrun += -delayUntilNext;
            //Play catchup... will cause phase shift : drift
            delayUntilNext.fromSec(0);
            m_nextStart = ros::Time::now();

            ROS_WARN_STREAM_THROTTLE( 2, format("Over-runs so far %1% for a total overrun time of %2%") % overruns % totalOverrun  );
        }
        ++m_iteration;
    }
}


void
CommandLoop::
commandLoop2()
{
    ROS_INFO( "Starting command loop, period is: %2.3f", m_commandLoopPeriod.toSec() );
    using boost::format;
    osrf_msgs::JointCommands::Ptr currentCommand;
    boost::condition_variable cond;

    int           overruns = 0;
    ros::Duration totalOverrun;
    m_nextStart = ros::Time::now() + m_commandLoopPeriod;
    while( ros::ok() && !m_shutdown )
    {
//        if( cond.timed_wait(
    }
}


} /* namespace re2uta */
}

/*
 * JointController.cpp
 *
 *  Created on: May 13, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/AtlasCommander/JointController.hpp>


namespace re2uta
{
namespace atlascommander
{

JointController::
JointController( boost::condition_variable & syncVar )
    : m_syncVar( syncVar )
{}

JointController::
~JointController()
{}

void
JointController::
setLatestStateMsg( const atlas_msgs::AtlasStateConstPtr & msg )
{
    boost::atomic_store( &m_latestStateMsg, boost::atomic_load( &msg ) );
}


}
}

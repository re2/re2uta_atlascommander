/*
 * CompliantBodyController.cpp
 *
 *  Created on: June 14, 2013
 *      Author: andrew.somerville
 *                 Isura Ranatunga
 */

#include <re2uta/AtlasCommander/CompliantBodyController.hpp>
#include <re2uta/AtlasCommander/JointController.hpp>
#include <re2uta/constraints/CartesianConstraint.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <re2uta/gradient_descent.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <re2/eigen/eigen_util.h>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <atlas_msgs/AtlasState.h>
#include <tf_conversions/tf_eigen.h>
#include <ros/ros.h>
#include <boost/thread/condition_variable.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

namespace
{
    Eigen::Vector4d colorFromHash( int hash )
    {
        double red   = ((hash >> 16) & 0x0000FF) /255.0;
        double green = ((hash >>  8) & 0x0000FF) /255.0;
        double blue  = ((hash >>  0) & 0x0000FF) /255.0;

        return Eigen::Vector4d( red, green, blue, 1 );
    }
}



namespace re2uta
{
namespace atlascommander
{



atlas_msgs::AtlasCommand::Ptr
buildAtlasVelocityCommand( const Eigen::VectorXd & velocities,
                           const atlas_msgs::AtlasCommand::ConstPtr & defaultMsg = atlas_msgs::AtlasCommand::ConstPtr() )
{
    atlas_msgs::AtlasCommand::Ptr atlasCommand( new atlas_msgs::AtlasCommand );
    if( defaultMsg )
        *atlasCommand = *defaultMsg;

    atlasCommand->position    = std::vector<double>( velocities.size(), 0 );
    atlasCommand->velocity    = std::vector<double>( velocities.size(), 0 );
    atlasCommand->kp_velocity = re2::toStd( Eigen::VectorXf(re2::toEigen( atlasCommand->kp_position ) * 2) );
    atlasCommand->kp_position = std::vector<float>( velocities.size(), 0 );
    atlasCommand->kd_position = std::vector<float>( velocities.size(), 0 );

    std::copy( velocities.data(), velocities.data() + velocities.size(), atlasCommand->velocity.begin() );

    return atlasCommand;
}

atlas_msgs::AtlasCommand::Ptr
buildAtlasPositionCommand( const Eigen::VectorXd & positions,
                           const atlas_msgs::AtlasCommand::ConstPtr & defaultMsg = atlas_msgs::AtlasCommand::ConstPtr() )
{
    atlas_msgs::AtlasCommand::Ptr atlasCommand( new atlas_msgs::AtlasCommand );
    if( defaultMsg )
        *atlasCommand = *defaultMsg;

    atlasCommand->position.resize( positions.size(), 0 );
    atlasCommand->velocity.resize( positions.size(), 0 );

    std::copy( positions.data(), positions.data() + positions.size(), atlasCommand->position.begin() );

    return atlasCommand;
}




CompliantBodyController::
CompliantBodyController( const urdf::Model & model,
                         boost::condition_variable & syncVar,
                         const JointController::Ptr & jointController  )
    : m_syncVar( syncVar )
{
    m_iteration        = 0;
    m_dt               = ros::Duration( 0.05 );
    m_jointController  = jointController;
    m_shutdown         = false;
    m_constraintsDirty = true;
//    m_commandType      = POSITION_CMD;
    m_commandType      = VELOCITY_CMD;
//    m_jointControlType = POSITION_CONTROL;
    m_jointControlType = VELOCITY_CONTROL;

    std::string controlTypeString = "VELOCITY_CONTROL";
    ros::NodeHandle("~").getParam( "control_type", controlTypeString );

    if( boost::to_upper_copy( controlTypeString ) == "POSITION_CONTROL" )
        m_jointControlType = POSITION_CONTROL;
    else
    if( boost::to_upper_copy( controlTypeString ) == "VELOCITY_CONTROL" )
        m_jointControlType = VELOCITY_CONTROL;


    m_defaultTransform = Eigen::Affine3d::Identity();
    m_atlasLookup.reset( new AtlasLookup(        model ) );
    m_poseSolver.reset( new  FullBodyPoseSolver( model, m_atlasLookup->lookupString( "l_foot" ),
                                                        m_atlasLookup->lookupString( "r_foot" ),
                                                        m_atlasLookup->lookupString( "utorso" ) ) );


//    m_baseFootFrameId = "r_foot";
    m_baseFootFrameId = "l_foot";

    m_inputWeights              = Eigen::MatrixXd::Identity( m_atlasLookup->getNumTreeJoints(), m_atlasLookup->getNumTreeJoints() );
    m_defaultTreeJointPositions = m_atlasLookup->calcDefaultTreeJointAngles( 0.4 );
    m_treeJointPositions        = m_defaultTreeJointPositions;

    const KDL::TreeElement * baseElement;
    const KDL::TreeElement * oppositeFootElement;

    baseElement         = &getTreeElement( *m_poseSolver->tree(), m_baseFootFrameId     );
    oppositeFootElement = &getTreeElement( *m_poseSolver->tree(), oppositeFootFrameId() );

    m_poseSolver->comConstraint()->    setBaseElement( baseElement );
    m_poseSolver->postureConstraint()->setBaseElement( baseElement );
    m_poseSolver->postureConstraint()->setTipElement(    "utorso"  );
    m_poseSolver->oppFootConstraint()->setBaseElement( baseElement );
    m_poseSolver->oppFootConstraint()->setTipElement( oppositeFootElement );

    m_poseSolver->postureConstraint()->goalWeightVector()       = Eigen::VectorXd::Zero(6);
    m_poseSolver->postureConstraint()->goalWeightVector()(3)    = 0.0001; // rot x
    m_poseSolver->postureConstraint()->goalWeightVector()(4)    = 0.001;  // rot y
    m_poseSolver->postureConstraint()->goalWeightVector()(5)    = 0.0001; // rot z
    m_poseSolver->comConstraint()->    goalWeightVector()(0)    = 1;
    m_poseSolver->comConstraint()->    goalWeightVector()(1)    = 1;
    m_poseSolver->comConstraint()->    goalWeightVector()(2)    = 0.0001;


    m_defaultFrameGains.reset( new FrameGoalGains );
    m_defaultFrameGains->posKp()  = Eigen::VectorXd::Ones(6) * 100;// * m_dt.toSec();
    m_defaultFrameGains->posKd()  = Eigen::VectorXd::Ones(6) * 100;// * m_dt.toSec();
    m_defaultFrameGains->velKp()  = Eigen::VectorXd::Ones(6) * 100;// * m_dt.toSec();
    m_defaultFrameGains->velMax() = Eigen::VectorXd::Ones(6) * 100;// * m_dt.toSec();

    BOOST_FOREACH( const SolverConstraint::Ptr & constraint,  m_poseSolver->constraints() )
    {
        constraint->setGoal( constraint->getCurrentOutput( m_defaultTreeJointPositions ) );

        CartesianConstraint::Ptr cartesianConstraint;
        cartesianConstraint = boost::dynamic_pointer_cast<CartesianConstraint>( constraint );
        if( cartesianConstraint )
        {
            cartesianConstraint->posKp()  = m_defaultFrameGains->posKp().head(  constraint->dimensions() );
            cartesianConstraint->posKd()  = m_defaultFrameGains->posKd().head(  constraint->dimensions() );
            cartesianConstraint->velKp()  = m_defaultFrameGains->velKp().head(  constraint->dimensions() );
            cartesianConstraint->velMax() = m_defaultFrameGains->velMax().head( constraint->dimensions() );
        }
    }

    m_poseSolver->optimalAngleConstraint()->goalWeightVector()  = Eigen::VectorXd::Ones(m_poseSolver->optimalAngleConstraint()->dimensions()) * 0.01;

    m_vizDebugPublisher.reset( new re2::VisualDebugPublisher( "full_body_controler_viz_debug" ) );

    m_controlLoopThread = boost::thread( &CompliantBodyController::controlLoop, this );
}


CompliantBodyController::
~CompliantBodyController()
{
    m_shutdown = true;
//    m_controlLoopThread.join();
}


std::string
CompliantBodyController::
oppositeFootFrameId()
{
    if( m_baseFootFrameId == "r_foot" )
        return "l_foot";
    else
    if( m_baseFootFrameId == "l_foot" )
        return "r_foot";
    else
        throw std::runtime_error( "Unknown foot" );
}


void
CompliantBodyController::
setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg )
{
    boost::atomic_store( &m_lastStateMsg, boost::atomic_load( &latestStateMsg ) );
//    ROS_INFO_THROTTLE( 5, "Still receiving state messages" );
}



void
CompliantBodyController::
switchBaseFoot( const std::string & newBaseFootFrameId )
{
    boost::shared_ptr<std::string> newBaseFoot( new std::string );

    *newBaseFoot = newBaseFootFrameId;

    boost::atomic_store( &m_newBaseFoot, newBaseFoot );
    m_constraintsDirty = true;
}


void
CompliantBodyController::
setCenterOfMassGoal( const Eigen::Vector3d & comLocation, const Eigen::Vector3d & comWeight )
{
    Eigen::Vector6dPtr comGoal( new Eigen::Vector6d );

    comGoal->head(3) = comLocation;
    comGoal->tail(3) = comWeight;

    boost::atomic_store( &m_comGoal, comGoal );
    m_constraintsDirty = true;
}

//void
//CompliantBodyController::
//setOppositeFootGoal( const Eigen::Vector6d & frame, const Eigen::Vector6d & weights )
//{
//    assert( false );
//}

void
CompliantBodyController::
setOppositeFootGoal( const Eigen::Affine3d & frame, const Eigen::Vector6d & weights )
{
    FrameGoal::Ptr frameGoal( new FrameGoal );

    frameGoal->goalPose                = frame;
    frameGoal->weightMatrix.diagonal() = weights;

    boost::atomic_store( &m_oppFootGoal, frameGoal );
}

void
CompliantBodyController::
setLeftHandGoal(     const Eigen::Vector6d & frame, const Eigen::Vector6d & weights )
{
    assert( false );
}

void
CompliantBodyController::
setRightHandGoal(    const Eigen::Vector6d & frame, const Eigen::Vector6d & weights )
{
    assert( false );
}


void
CompliantBodyController::
setFrameGoal(  const FrameGoal::Ptr & frameGoal )
{
//    boost::lock_guard<boost::mutex> goalParamLock( m_goalParamMutex );
    ROS_INFO_STREAM_THROTTLE( 5, "Still receiving frame commands (Throttled)" );

    FrameGoalListPtr updateList;
    updateList = boost::atomic_exchange( &m_frameGoalUpdateList, updateList );


    if( updateList == NULL )
        updateList.reset( new FrameGoalList );

    updateList->push_back( frameGoal );

    boost::atomic_store( &m_frameGoalUpdateList, updateList );

    m_constraintsDirty = true;
}

void
CompliantBodyController::
setFrameGoalGains(  const FrameGoalGains::Ptr & frameGoalGains )
{
//    boost::lock_guard<boost::mutex> goalParamLock( m_goalParamMutex );
    FrameGoalGainsListPtr updateList;
    updateList = boost::atomic_exchange( &m_frameGoalGainsUpdateList, updateList );

    if( updateList == NULL )
        updateList.reset( new FrameGoalGainsList );

    updateList->push_back( frameGoalGains );

    boost::atomic_store( &m_frameGoalGainsUpdateList, updateList );

    m_constraintsDirty = true;
}


Eigen::VectorXd
projectJointPositions( const Eigen::VectorXd & jointPositions, const Eigen::VectorXd & jointVelocities, const ros::Duration & dt )
{
    Eigen::VectorXd projectedJointPositions;

    //  r  = r₀ + v₀t + a₀t²/2
    projectedJointPositions =   jointPositions
                              + jointVelocities * dt.toSec();
//                            + jointAccelerations * std::pow( dtSecs, 2 )/2;

    return projectedJointPositions;
}

//Eigen::VectorXd
//CompliantBodyController::
//projectJointVelocities( const ros::Time & time )
//{
////    double          dtSecs                 = (time - m_sampleTime).toSec();
//    Eigen::VectorXd projectedJointVelocities =   jointVelocities;
////                                             + jointAccelerations * dtSecs;
//    return projectedJointVelocities;
//}


ros::Time
CompliantBodyController::
getNextCommandTime( const ros::Time sampleTime )
{
    ROS_ERROR_THROTTLE( 4, "getNextCommandTime, not finished" );
    return ros::Time::now();
}


FrameConstraint::Ptr
CompliantBodyController::
findFrameConstraint( const std::string & name )
{
    BOOST_FOREACH( const SolverConstraint::Ptr & constraint,  m_poseSolver->constraints() )
    {
        FrameConstraint::Ptr frameConstraint;
        frameConstraint = boost::dynamic_pointer_cast<FrameConstraint>( constraint );

        if( frameConstraint
         && frameConstraint->tipElement()->segment.getName() == name )
        {
            return frameConstraint;
        }
    }

    return FrameConstraint::Ptr();
}



void
CompliantBodyController::
controlLoop()
{
    ros::NodeHandle                  node;
    boost::mutex                     mutex;
    boost::unique_lock<boost::mutex> lock( mutex );
//    Eigen::Affine3d                  defaultTransform = Eigen::Affine3d::Identity();
    Eigen::MatrixXd                  internalInputWeightMatrix;
    Eigen::VectorXd                  minInputValues     = m_atlasLookup->getTreeJointMinVector();
    Eigen::VectorXd                  maxInputValues     = m_atlasLookup->getTreeJointMaxVector();
    Eigen::VectorXd                  defaultJointAngles = m_atlasLookup->calcDefaultTreeJointAngles( 0.4 );
    atlas_msgs::AtlasCommand::Ptr    defaultAtlasCommand = m_atlasLookup->createDefaultJointCmdMsg( node );
    defaultAtlasCommand->k_effort = std::vector<uint8_t>( defaultAtlasCommand->k_effort.size(), 255 );

    ros::Time lastCmdTime = ros::Time::now(); // for profiling
    ros::Time unlockTime  = ros::Time::now(); // for profiling
    ros::Time waitTime    = ros::Time::now(); // for profiling

    while( ros::ok() && !m_shutdown )
    {
        waitTime = ros::Time::now();
        ros::Duration( std::fmod( ros::Time::now().toSec(), m_dt.toSec() ) ).sleep(); // sleep until next multiple of our period locked with time
        m_syncVar.wait( lock );
        unlockTime = ros::Time::now();

        if( ! updateParameters(ros::Time::now()) )
        {
            ROS_WARN( "CompliantBodyController update parameters failed" );
            continue;
        }

        publishDebug();

        // COM in base foot
        Eigen::Affine3d comInBaseFoot = Eigen::Affine3d::Identity();
        comInBaseFoot.translation() = m_poseSolver->comConstraint()->getCurrentOutput( m_treeJointPositions );


        atlas_msgs::AtlasCommand msg;


        m_jointController->setJointCommand( msg, m_nextCommandTime );
        lastCmdTime = ros::Time::now();

        ++m_iteration;
    }
}





void
CompliantBodyController::
updateConstraints()
{
    Eigen::Vector6dPtr comGoal;
    comGoal = boost::atomic_exchange( &m_comGoal, comGoal );
    if( comGoal )
    {
        m_poseSolver->comConstraint()->setGoal(             comGoal->head(3) );
        m_poseSolver->comConstraint()->setGoalWeightVector( comGoal->tail(3) );
    }


    FrameGoal::Ptr oppFootGoal;
    oppFootGoal = boost::atomic_exchange( &m_oppFootGoal, oppFootGoal );
    if( oppFootGoal )
    {
        m_poseSolver->oppFootConstraint()->setGoal( affineToTwist( oppFootGoal->goalPose ) );
        m_poseSolver->oppFootConstraint()->setGoalWeightMatrix( oppFootGoal->weightMatrix );
    }


    FrameGoalListPtr frameGoalUpdateList;
    frameGoalUpdateList = boost::atomic_exchange( &m_frameGoalUpdateList, frameGoalUpdateList );

    if( frameGoalUpdateList )
    {
        BOOST_FOREACH( const FrameGoal::Ptr & frameGoal, *frameGoalUpdateList )
        {
            FrameConstraint::Ptr frameConstraint;
            frameConstraint = findFrameConstraint( frameGoal->tipFrameId );

            if( ! frameConstraint )
            {
                if( frameGoal->baseFrameId.empty() )
                    frameGoal->baseFrameId = m_baseFootFrameId;

                frameConstraint = m_poseSolver->addFrameConstraint( frameGoal->baseFrameId, frameGoal->tipFrameId );
                ROS_INFO_STREAM( "Added constraint : " << frameGoal->baseFrameId << " to " << frameGoal->tipFrameId );

                if( frameConstraint )
                {
                    frameConstraint->posKp()  = m_defaultFrameGains->posKp().head(  frameConstraint->dimensions() );
                    frameConstraint->posKd()  = m_defaultFrameGains->posKd().head(  frameConstraint->dimensions() );
                    frameConstraint->velKp()  = m_defaultFrameGains->velKp().head(  frameConstraint->dimensions() );
                    frameConstraint->velMax() = m_defaultFrameGains->velMax().head( frameConstraint->dimensions() );
                }
            }

            if( frameConstraint )
            {
                if( frameGoal->baseFrameId == "" )
                    frameGoal->baseFrameId = m_baseFootFrameId;

//                frameConstraint->convertToNewBase( frameGoal->baseFrameId, m_treeJointPositions );

                // FIXME, need to set base frame in case it changed
                Eigen::Affine3d goalToConstraintBase = m_poseSolver->getTransformFromTo( frameGoal->goalFrameId, frameGoal->baseFrameId, m_treeJointPositions ); //FIXME

                Eigen::Affine3d goalInConstraintBase;
                goalInConstraintBase = goalToConstraintBase * frameGoal->goalPose;

                Eigen::MatrixXd weightMatrix = frameGoal->weightMatrix;
//                weightMatrix.block(0,0,3,3) =  weightMatrix.block(0,0,3,3) * goalToConstraintBase.rotation(); // FIXME Unsure if this will actually work
//                weightMatrix.block(3,3,3,3) = weightMatrix.block(3,3,3,3) * goalToConstraintBase.rotation() ; // FIXME Unsure if this will actually work

                frameConstraint->goalWeightMatrix() = weightMatrix;
                frameConstraint->setGoal( affineToTwist( goalInConstraintBase ) );

                bool   debug       = true;
                double debugPeriod = 0.1;
//                if( debug && (m_iteration % (int)(debugPeriod/m_dt.toSec())) == 0 )
                {
                    std::stringstream stringstream;
                    stringstream << goalToConstraintBase.translation();

//                    int hash = boost::hash<std::string>()(frameGoal->goalFrameId);
//                    Eigen::Vector4d color( 0,1,0,0.5 );
//                    m_vizDebugPublisher->publishPoint3( goalInConstraintBase.translation(), color, hash - 1 , frameGoal->baseFrameId );
//                    m_vizDebugPublisher->publishTextAt( stringstream.str(), 0.05, goalInConstraintBase.translation(), color, hash, frameGoal->baseFrameId );
//                    Eigen::Vector4d color1( 1,0,1,0.5 );
//                    m_vizDebugPublisher->publishPoint3( frameGoal->goalPose.translation(), color1, hash + 1, frameGoal->goalFrameId );
                }
            }
        }
    }

    FrameGoalGainsListPtr frameGoalGainsUpdateList;
    frameGoalGainsUpdateList = boost::atomic_exchange( &m_frameGoalGainsUpdateList, frameGoalGainsUpdateList );

    if( frameGoalGainsUpdateList )
    {
        BOOST_FOREACH( const FrameGoalGains::Ptr & frameGoalGains, *frameGoalGainsUpdateList )
        {
            FrameConstraint::Ptr frameConstraint;
            frameConstraint = findFrameConstraint( frameGoalGains->tipFrameId() );

            if( frameConstraint )
            {
                frameConstraint->posKp()  = frameGoalGains->posKp() ;
                frameConstraint->posKd()  = frameGoalGains->posKd() ;
                frameConstraint->velKp()  = frameGoalGains->velKp() ;
                frameConstraint->velMax() = frameGoalGains->velMax();
            }
        }
    }


    boost::shared_ptr<std::string> newBaseFoot;
    newBaseFoot = boost::atomic_exchange( &m_newBaseFoot, newBaseFoot );
    if( newBaseFoot )
    {
        BOOST_FOREACH( SolverConstraint::Ptr genericConstraint, m_poseSolver->constraints() )
        {
            CartesianConstraint::Ptr constraint;
            constraint = boost::dynamic_pointer_cast<CartesianConstraint>( genericConstraint );

            if( ! constraint )
                continue;

            constraint->convertToNewBase( *newBaseFoot, m_treeJointPositions );
        }

        m_baseFootFrameId = *newBaseFoot;
        m_poseSolver->oppFootConstraint()->setTipElement( oppositeFootFrameId() );
    }
}


bool
CompliantBodyController::
updateParameters( const ros::Time & nextCommandTime )
{
    atlas_msgs::AtlasState::ConstPtr lastStateMsg;
    lastStateMsg = boost::atomic_load( &m_lastStateMsg );


    if( !lastStateMsg )
    {
        ROS_WARN_STREAM_THROTTLE( 5, "update call though no lastStateMsg" );
        return false;
    }


    Eigen::VectorXd jointPositions  = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );
    Eigen::VectorXd jointVelocities = m_atlasLookup->cmdToTree( parseJointVelocities( lastStateMsg ) );
    ros::Time       sampleTime      = lastStateMsg->header.stamp;

//    m_nextCommandTime = getNextCommandTime( sampleTime );

    if( (nextCommandTime - sampleTime).toSec() > 5 )
    {
        ROS_WARN_STREAM( "sample to next command difference too large: " << (nextCommandTime - sampleTime)  );
        ROS_WARN_STREAM( "sampleTime: "      << sampleTime );
        ROS_WARN_STREAM( "nextCommandTime: " << nextCommandTime );
        return false;
    }

    m_treeJointPositions   = jointPositions; //projectJointPositions( jointPositions, jointVelocities, nextCommandTime - sampleTime );
    m_treeJointVelocities  = jointVelocities;


//    boost::unique_lock<boost::mutex> goalParamLock( m_goalParamMutex, boost::defer_lock );
    if( m_constraintsDirty ) //FIXME, this will need to be changed if goal changes when not dirty // && goalParamLock.try_lock() )
    {
        updateConstraints();

        m_constraintsDirty = false;
//        goalParamLock.unlock(); //Manually unlock to reduce the mutex lock time

        m_desiredPosition = Eigen::VectorXd(       m_poseSolver->goalDims() );
        m_outputWeights   = Eigen::VectorXd::Ones( m_poseSolver->goalDims() ).asDiagonal();
        m_posKp           = Eigen::VectorXd::Ones( m_poseSolver->goalDims() );
        m_velKp           = Eigen::VectorXd::Ones( m_poseSolver->goalDims() );
        m_maxVelocity     = Eigen::VectorXd::Ones( m_poseSolver->goalDims() );

        int atRow  = 0;
        int atDiag = 0;
        BOOST_FOREACH( SolverConstraint::Ptr constraint, m_poseSolver->constraints() )
        {
            int nextRow = atRow;
            nextRow = re2::insertRowsAt(  m_desiredPosition, constraint->goal(),             atRow  );
            nextRow = re2::insertRowsAt(  m_posKp,           constraint->posKp(),            atRow  );
            nextRow = re2::insertRowsAt(  m_velKp,           constraint->velKp(),            atRow  );
            nextRow = re2::insertRowsAt(  m_maxVelocity,     constraint->velMax(),           atRow  );
            atDiag  = re2::insertBlockAt( m_outputWeights,   constraint->goalWeightMatrix(), atDiag );

            atRow = nextRow;
        }

    }
//    else
//    if( m_constraintsDirty )
//    {
//        ROS_WARN( "Try lock failed in updateParameters, will try again next cycle. (Shouldn't be a problem unless it's happening all the time)" );
//    }

    return true;
}




void
CompliantBodyController::
publishDebug()
{
    bool debug = true;
    double debugPeriod = 0.1;
    if( debug && (m_iteration % (int)(debugPeriod/m_dt.toSec())) == 0 )
    {

        BOOST_FOREACH( SolverConstraint::Ptr genericConstraint, m_poseSolver->constraints() )
        {
            CartesianConstraint::Ptr constraint;
            constraint = boost::dynamic_pointer_cast<CartesianConstraint>( genericConstraint );

            if( ! constraint )
                continue;

            int             hash;
            Eigen::Vector4d color;
            std::string     frameName = constraint->baseFrameId() ; // m_baseFoot;

            hash  = boost::hash<std::string>()( constraint->name() + "goal" ) + constraint->startRow();
            color = colorFromHash( hash );
            color(3) = constraint->goalWeightMatrix().norm() / Eigen::Vector3d(1,1,1).norm(); // set alpha to normalized norm of the weight matrix
            m_vizDebugPublisher->publishPoint3( constraint->goal().head<3>(), color, hash, frameName );

            hash  = boost::hash<std::string>()( constraint->name() + "current" ) + constraint->startRow();
            color = colorFromHash( hash );
            color(3) = constraint->goalWeightMatrix().norm() / Eigen::Vector3d(1,1,1).norm(); // set alpha to normalized norm of the weight matrix
            m_vizDebugPublisher->publishPoint3( constraint->getCurrentOutput(m_treeJointPositions).head<3>(), color, hash, frameName );

            hash  = boost::hash<std::string>()( constraint->name() + "segment" ) + constraint->startRow();
            color = colorFromHash( hash );
            color(3) = constraint->goalWeightMatrix().norm() / Eigen::Vector3d(1,1,1).norm(); // set alpha to normalized norm of the weight matrix
            m_vizDebugPublisher->publishSegment( constraint->goal().head<3>(),
                                                 constraint->getCurrentOutput(m_treeJointPositions).head<3>(),
                                                 color, hash, frameName );
            m_vizDebugPublisher->publishTextAt( constraint->name(), 0.03, constraint->goal().head<3>(), color, hash + 1, frameName );


            std::ostringstream ss;
            ss << std::fixed   << std::setprecision(4);

            Eigen::Vector3d xyzDiff = constraint->getCurrentOutput(m_treeJointPositions).head(3) - constraint->goal().head<3>();
            ss << "xyz norm: " << xyzDiff.norm();
            m_vizDebugPublisher->publishTextAt( ss.str(),
                                                0.03,
                                                constraint->goal().head<3>() - Eigen::Vector3d(0,0,0.021),
                                                color, hash + 2, frameName );
            ss.str("");
            ss << "xyz weights: " << constraint->goalWeightVector().head(3).transpose();
            m_vizDebugPublisher->publishTextAt( ss.str(),
                                                0.03,
                                                constraint->goal().head<3>() - Eigen::Vector3d(0,0,0.042),
                                                color, hash + 3, frameName );

            if( constraint->goal().size() > 3 )
            {
                ss.str("");
                Eigen::Vector3d rpyDiff = constraint->getCurrentOutput(m_treeJointPositions).tail(3) - constraint->goal().tail<3>();
                ss << "rpy norm: " << rpyDiff.norm();
                m_vizDebugPublisher->publishTextAt( ss.str(),
                                                    0.03,
                                                    constraint->goal().head<3>() - Eigen::Vector3d(0,0,0.063),
                                                    color, hash + 4, frameName );

                ss.str("");
                ss << "rpy weights: " <<  constraint->goalWeightVector().tail(3).transpose();
                m_vizDebugPublisher->publishTextAt( ss.str(),
                                                    0.03,
                                                    constraint->goal().head<3>() - Eigen::Vector3d(0,0,0.084),
                                                    color, hash + 6, frameName );
            }
        }
    }
}




//        ROS_INFO_STREAM( "wait delay: "             << unlockTime - waitTime );
//        ROS_INFO_STREAM( "Process delay: "          << ros::Time::now() - unlockTime );
//        ROS_INFO_STREAM( "Delay between commands: " << ros::Time::now() - lastCmdTime );

//        if( !re2::is_finite( outputJointVelocities )
//            || (outputJointVelocities.array().abs() > 10).any())
//        {
//            ROS_ERROR_STREAM( "output joint velocities blew up!" << outputJointVelocities );
//            ROS_ERROR_STREAM( "output goalspace velocities "     << desiredOutputVelocity );
//            ROS_ERROR_STREAM( "positionError "                   << positionError );
//            ROS_ERROR_STREAM( "m_desiredPosition "               << m_desiredPosition );
//            ROS_ERROR_STREAM( "m_treeJointPositions "            << m_treeJointPositions );
//            continue;
////            ROS_ASSERT_MSG( false, "exiting" );
//        }

//        ROS_WARN_STREAM( "desiredPosition: \n"             << m_desiredPosition );
//        ROS_WARN_STREAM( "Position error: \n"              << positionError );
//        ROS_WARN_STREAM( "desiredOutputVelocity error: \n" << desiredOutputVelocity );
//        ROS_WARN_STREAM( "outputJointVelocities \n: "      << outputJointVelocities  );
//        ROS_WARN_STREAM( "m_outputWeights: \n"             << m_outputWeights  );
//        ROS_WARN_STREAM( "m_inputWeights: \n"              << m_inputWeights  );
//        ROS_WARN_STREAM( "masterJacobian: \n"              << masterJacobian );
//        ROS_WARN_STREAM( "pinvMasterJacobian: \n"          << pinvMasterJacobian  );
//        ROS_WARN_STREAM( "pinvMasterJacobian: \n"          << pinvMasterJacobian  );
//        ROS_WARN_STREAM( "output: \n"                      << output       );
//        ROS_WARN_STREAM( "outputError: \n"                 << outputError  );
//        ROS_WARN_STREAM( "m_treeJointPositions: \n"        << m_treeJointPositions  );

//        void createVelocityLimits()
//        {
//            Eigen::VectorXd distToLimit             =   (posDirJoints * (jointMaxs - jointPositions))
//                                                      + (negDirJoints * (jointMins - jointPositions));
//            Eigen::VectorXd maxJointAcceleration    =   (posDirJoints * getMaxJointPosAccel())
//                                                      + (negDirJoints * getMaxJointNegAccel());
//            // utf-8 characters follow
//            //  θ  = θ₀ + ω₀t + 1/2α₀t²
//            //  Δθ = ω₀t + 1/2α₀t²
//            //  ω  = ω₀ + α₀t
//            //  0  = ω₀ + α₀t
//            //  -ω₀ = α₀t
//            //  -ω₀/α₀ = t
//            //  Δθ =  ω₀(-ω₀/α₀) + 1/2α₀(-ω₀/α₀)²
//            //  Δθ = -ω₀²/α₀     + ω₀²/2α₀
//            //  Δθ = -ω₀²/2α₀
//            //  2α₀Δθ = -ω₀²
//            //  √(-Δθ2α₀) = ω₀
//            Eigen::VectorXd minDesiredJointVelocity = (-2 * maxJointAcceleration * distToLimit).array().sqrt();
//        }
//
//ROS_WARN_STREAM( "goalFrame id:\n"      << frameGoal->goalFrameId );
//ROS_WARN_STREAM( "baseFrame id:\n"      << frameGoal->baseFrameId );
//ROS_WARN_STREAM( "goalInGoalFrame:\n"      << frameGoal->goalPose.matrix() );
//ROS_WARN_STREAM( "goalInConstraintBase (but incorrectly calculated):\n" << goalInConstraintBase.matrix() );
//ROS_WARN_STREAM( "goalInConstraintBase (how I think it should be don):\n" << goalInConstraintBaseAlt.matrix() );
//ROS_WARN_STREAM( "goalToConstraintBase:\n" << goalToConstraintBase.matrix() );
//
//Eigen::MatrixXd weightMatrix = frameGoal->weightMatrix;
////                weightMatrix.block(0,0,3,3) = weightMatrix.block(0,0,3,3) * goalToConstraintBase.rotation(); // FIXME Unsure if this will actually work
////                weightMatrix.block(3,3,3,3) = weightMatrix.block(3,3,3,3) * goalToConstraintBase.rotation() ; // FIXME Unsure if this will actually work
//
//frameConstraint->goalWeightMatrix() = weightMatrix;
//frameConstraint->setGoal( affineToTwist( goalInConstraintBase ) );
//ROS_WARN_STREAM( "Goal:\n" << affineToTwist( goalInConstraintBase ).transpose() << "n" );
//ROS_WARN_STREAM( "Current output:\n" << frameConstraint->getCurrentOutput( m_treeJointPositions ) << "n" );
//ROS_WARN_STREAM( "Current output diff:\n" <<  affineToTwist( goalInConstraintBase ) - frameConstraint->getCurrentOutput( m_treeJointPositions ) << "n" );
//


}
}



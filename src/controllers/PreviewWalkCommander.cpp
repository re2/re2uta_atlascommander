/*
 * PreviewWalkCommander.cpp
 *
 *  Created on: Jun 4, 2013
 *      Author: andrew.somerville
 */

#include <walk_msgs/GetPath.h>
#include <re2uta/AtlasCommander/PreviewWalkCommander.hpp>
#include <re2uta/AtlasCommander/FullBodyControllerInterface.hpp>
#include <re2uta/walking/WalkPatternGenerator.hpp>
#include <re2uta/walking/StepTrajectoryGenerator.hpp>
#include <re2uta/walking/RosHalfstepTrajectoryGenerator.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <boost/shared_ptr.hpp>
#include <string>


namespace
{
    template <typename Type>
    Type exponentialWeightedAverage( Type oldVal, Type newVal, double newWeight )
    {
        return oldVal*(1-newWeight) + newWeight*(newVal);
    }

    template <typename Type>
    Type lowPassFilter( Type oldVal, Type newVal, double newWeight )
    {
        return exponentialWeightedAverage( oldVal, newVal, newWeight );
    }
}


namespace re2uta
{
namespace atlascommander
{



StepPose::Ptr generateContinuation( const StepPose::Ptr & stepPose, ros::Duration dt )
{
    StepPose::Ptr newStepPose( new StepPose(*stepPose) );

    double velocity = 0.3;
    newStepPose->swingFootPose() = Eigen::Translation3d( 0, 0, -velocity * dt.toSec() ) * stepPose->swingFootPose();
    newStepPose->comPose         = Eigen::Translation3d( 0, 0, -velocity * dt.toSec() ) * stepPose->comPose;

    return newStepPose;
}


PreviewWalkCommander::
PreviewWalkCommander( const FullBodyControllerInterface::Ptr & fullBodyController,
                      const urdf::Model & model,
                      const AtlasLookup::Ptr & atlasLookup,
                      const ros::Duration & dt,
                      StepPlannerType       stepPlannerType )
    : m_atlasLookup( atlasLookup )
{
    m_fullBodyController = fullBodyController;
    m_solver.reset( new FullBodyPoseSolver( model, m_atlasLookup->lookupString( "l_foot" ),
                                                   m_atlasLookup->lookupString( "r_foot" ),
                                                   m_atlasLookup->lookupString( "utorso" ) ) );
    m_halfStepSize    = 0.2;
    m_dt              = dt;
    m_shutdown        = false;
    m_stepPlannerType = stepPlannerType;


    double halfStepGeneratorDuration = m_halfStepGeneratorDuration.toSec();
    if( ! m_node.getParam( "/halfsteps_pattern_generator/step", halfStepGeneratorDuration ) )
        ROS_WARN_STREAM( "didn't find /halfsteps_pattern_generator/step, using default " << halfStepGeneratorDuration );
    else
        ROS_WARN_STREAM( "/halfsteps_pattern_generator/step is " << halfStepGeneratorDuration );
    m_halfStepGeneratorDuration.fromSec( halfStepGeneratorDuration );

    // Fg = 9.81 * 90 = 883N
    // Fg*distToEdge = Fd*stepLength
    // 883N * 0.13 = Fd*0.5
    // Fd = 182N
    m_forceThresh  = 50; // to be conservative // FIXME we should reduce this even more if possible

    m_kEfforts = std::vector<uint8_t>( m_atlasLookup->getNumCmdJoints(), 255 );
    m_fullBodyController->setKEfforts( m_kEfforts );

    m_controlLoopThread = boost::thread( &PreviewWalkCommander::commandLoop, this );
}


PreviewWalkCommander::
~PreviewWalkCommander()
{
    m_shutdown = true;
//    m_controlLoopThread.join();
}



void
PreviewWalkCommander::
setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg )
{
//    ROS_INFO_STREAM_THROTTLE( 5, "Setting local state" );
    boost::atomic_store( &m_lastStateMsg, latestStateMsg );
}



void
PreviewWalkCommander::
walkTo( const WalkGoal::Ptr & walkGoal )
{
    //FIXME we're not handling the frame id!
    setDest( walkGoal->goalPose );
}

void
PreviewWalkCommander::
setStepPlannerType( StepPlannerType stepPlannerType )
{
    ROS_ERROR( "PreviewWalkCommander Currently ignoring setStepPlannerType" );
    m_stepPlannerType = stepPlannerType;
}


void
PreviewWalkCommander::
shutdown()
{
    m_shutdown = true;
}





void
PreviewWalkCommander::
setDest( const Eigen::Affine3d & dest )
{
    Eigen::Affine3dPtr destPtr( new Eigen::Affine3d );
    *destPtr = dest;

    boost::atomic_store( &m_walkDestCmd, destPtr );
}



//Eigen::Affine3d
//PreviewWalkCommander::
//getImuToInertial()
//{
//    atlas_msgs::AtlasState::ConstPtr lastStateMsg;
//    lastStateMsg  = boost::atomic_load( &m_lastStateMsg );
//
//    Eigen::Affine3d    imuToInertial;
//    Eigen::Quaterniond imuToInertialRot;
//
//    imuToInertial.w() = lastStateMsg->orientation.w; //FIXME need to check if this needs to be inverted
//    imuToInertial.x() = lastStateMsg->orientation.x;
//    imuToInertial.y() = lastStateMsg->orientation.y;
//    imuToInertial.z() = lastStateMsg->orientation.z;
//
//    imuToInertial = imuToInertialRot.toRotationMatrix();
//
//    return imuToInertial;
//}
//
//
//Eigen::Affine3d
//PreviewWalkCommander::
//getFrameToInertial( const std::string & frameId )
//{
//    getImuToInertial()
//
//    m_solver->getPose( frameId, "imu_link", jointPositions );
//}



void
PreviewWalkCommander::
publishBaseFootToOdom( Foot baseFoot,
                       const atlas_msgs::AtlasState::ConstPtr & lastStateMsg,
                       const Eigen::Affine3d & baseFootInOdom )
{
//    Eigen::Affine3d baseFootToTreeRoot;
//    baseFootToTreeRoot = m_solver->getTransformFromTo( baseFootFrameId, "root", jointPositions );
//
//    Eigen::Affine3d odomInBaseFoot;
//    odomInBaseFoot = baseFootInOdom.inverse();
//
//    Eigen::Affine3d odomInTreeRootXform;
//    odomInTreeRootXform = baseFootToTreeRoot * odomInBaseFoot;

    std::string     swingFootFrameId = footToFrameId( otherFoot(baseFoot), m_atlasLookup );
    std::string     baseFootFrameId  = footToFrameId( baseFoot, m_atlasLookup );
    Eigen::VectorXd jointPositions   = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );

    tf::Transform odomToTreeRoot;
    tf::transformEigenToTF( baseFootInOdom.inverse(), odomToTreeRoot );
    m_tfBroadcaster.sendTransform( tf::StampedTransform( odomToTreeRoot,
                                                         ros::Time::now(),
                                                         baseFootFrameId,
                                                         "odom"  ));
}


StepPose::Ptr
PreviewWalkCommander::
moveStepPoseToNewOdom( const StepPose::Ptr & currentStepPose )
{
    StepPose::Ptr newStepPose( new StepPose( *currentStepPose ) );

    Eigen::Affine3d odomInBaseFoot;
    odomInBaseFoot.translation() = (currentStepPose->baseFootPose().translation() + currentStepPose->swingFootPose().translation())/2;

    Eigen::Quaterniond baseQuat(  currentStepPose->baseFootPose().rotation()  );
    Eigen::Quaterniond swingQuat( currentStepPose->swingFootPose().rotation() );

    odomInBaseFoot.linear() = baseQuat.slerp( 0.5, swingQuat ).toRotationMatrix();

    newStepPose->baseFootPose()  = odomInBaseFoot.inverse() * currentStepPose->baseFootPose() ;
    newStepPose->swingFootPose() = odomInBaseFoot.inverse() * currentStepPose->swingFootPose();
    newStepPose->comPose         = odomInBaseFoot.inverse() * currentStepPose->comPose        ;

    return newStepPose;
}



StepPose::Ptr
PreviewWalkCommander::
getCurrentStepPoseInBase( Foot baseFoot, const atlas_msgs::AtlasStateConstPtr & stateMsg )
{
    Eigen::VectorXd jointPositions   = m_atlasLookup->cmdToTree( parseJointPositions(  stateMsg ) );
    std::string     baseFootFrameId  = footToFrameId( baseFoot, m_atlasLookup );
    std::string     swingFootFrameId = footToFrameId( otherFoot(baseFoot), m_atlasLookup );

    StepPose::Ptr currentStepPose( new StepPose( baseFoot ) );

    currentStepPose->baseFootPose()  = Eigen::Affine3d::Identity();
    currentStepPose->swingFootPose() = m_solver->getPose(         baseFootFrameId, swingFootFrameId, jointPositions );
    currentStepPose->comPose         = m_solver->getCenterOfMass( baseFootFrameId,                   jointPositions );

    return currentStepPose;
}



StepPose::Ptr
PreviewWalkCommander::
odomToBase( StepPose::Ptr poseInOdom )
{
    StepPose::Ptr poseInBase( new StepPose( poseInOdom->baseFoot() ) );
    poseInBase->baseFootPose()  = Eigen::Affine3d::Identity();
    poseInBase->swingFootPose() = poseInOdom->baseFootPose().inverse() * poseInOdom->swingFootPose();
    poseInBase->comPose         = poseInOdom->baseFootPose().inverse() * poseInOdom->comPose;

//    ROS_INFO_STREAM( "FootGoal : " << poseInBase->swingFootPose().translation().transpose()    );
//    ROS_INFO_STREAM( "Twist    : " << affineToTwist( poseInBase->swingFootPose() ).transpose() );
//    ROS_INFO_STREAM( "weights  : " << footWeights.transpose() );
    return poseInBase;
}




void
PreviewWalkCommander::
commandLoop()
{
    std::string swingFootFrameId;
    std::string baseFootFrameId ;
    atlas_msgs::AtlasState::ConstPtr lastStateMsg;
    Eigen::VectorXd jointPositions;
    Foot            swingFoot = RIGHT_FOOT; //LEFT_FOOT;

    std::string lFootStr = m_atlasLookup->lookupString( "l_foot" );
    std::string rFootStr = m_atlasLookup->lookupString( "r_foot" );

    ros::NodeHandle node;
    ros::Publisher lFootTrajPub = node.advertise<nav_msgs::Path>( std::string( "/walkTrajectory/" ) + lFootStr, 5, true );
    ros::Publisher rFootTrajPub = node.advertise<nav_msgs::Path>( std::string( "/walkTrajectory/" ) + rFootStr, 5, true );
    ros::Publisher comTrajPub   = node.advertise<nav_msgs::Path>( "/walkTrajectory/com",    5, true );


    nav_msgs::Path lFootTraj;
    nav_msgs::Path rFootTraj;
    nav_msgs::Path comTraj;


    lFootTraj.header.frame_id = "/odom";
    rFootTraj.header.frame_id = "/odom";
    comTraj.header.frame_id   = "/odom";

    re2::VisualDebugPublisher vizDebug( "full_body_controler_viz_debug", "odom" );


    while( !(lastStateMsg = boost::atomic_load( &m_lastStateMsg ))  && ros::ok() && !m_shutdown )
    {
        ROS_WARN_STREAM_THROTTLE( 2, "No atlas state available, waiting to start PreviewWalkCommander command loop until seen" );
        m_dt.sleep();
    }

    //FIXME need IMU rotation correction
    StepPose::Ptr previousStepPoseInOdom; // use swing foot because it will be inverted later
    previousStepPoseInOdom = moveStepPoseToNewOdom( getCurrentStepPoseInBase( swingFoot, lastStateMsg ) );

    double stepWidth = 0.14;
    stepWidth = previousStepPoseInOdom->baseFootPose().translation().norm();
    ROS_INFO( "Got step width as: %f", stepWidth );
    //m_node.getParam( "step_width", stepWidth );

//
//    //FIXME need IMU rotation correction
//    StepPose::Ptr originalStepPose = getCurrentStepPoseInBase( otherFoot(swingFoot), lastStateMsg );
//
//    StepPose::Ptr originalStepPoseInOdom;
//    originalStepPoseInOdom = moveStepPoseToNewOdom( originalStepPose );
//    publishBaseFootToOdom( otherFoot(swingFoot), lastStateMsg, originalStepPoseInOdom->baseFootPose() );

    while( ros::ok() && !m_shutdown )
    {
/*        //FIXME need IMU rotation correction
        StepPose::Ptr previousStepPoseInOdom; // use swing foot because it will be inverted later
        previousStepPoseInOdom = moveStepPoseToNewOdom( getCurrentStepPoseInBase( swingFoot, lastStateMsg ) );*/

        //FIXME verify
        Eigen::Affine3d walkSource;
        walkSource = Eigen::Affine3d::Identity(); // start at 0,0,0 for now

        Eigen::Affine3dPtr walkDest;
        walkDest      = boost::atomic_load( &m_walkDestCmd ); //, walkDest ); // represents transform between start and dest between feet
        lastStateMsg  = boost::atomic_load( &m_lastStateMsg );


        if( !lastStateMsg )
        {
            m_dt.sleep();
            ROS_WARN_THROTTLE( 2, "NO STATE MESSAGE!" );
            continue;
        }

        // publish to swing foot because we are using previous poses feet
        publishBaseFootToOdom( otherFoot(swingFoot), lastStateMsg, previousStepPoseInOdom->swingFootPose() );

        if( !walkDest )
        {
            m_dt.sleep();
            continue;
        }
        ROS_INFO( "PreviewWalkCommander got new destination" );

        lFootTraj.poses.clear();
        rFootTraj.poses.clear();
        comTraj.  poses.clear();

        lFootTrajPub.publish( lFootTraj );
        rFootTrajPub.publish( rFootTraj );
        comTrajPub.  publish(   comTraj );


        //FIXME currently assuming that walkDest is in the frame of the base foot???
//        StepTrajectoryGenerator generator( originalStepPose, *walkDest, m_halfStepSize, currentStepPoseInOdom->baseFootPose().translation().norm() );
//        ros::Duration halfStepDt( 0.005 );
        RosHalfstepTrajectoryGenerator generator( previousStepPoseInOdom, // FIXME double check that this doesn't cause a problem
                                                  *walkDest,
                                                  m_halfStepSize,
                                                  stepWidth, // set our step width manually
//                                                  originalStepPoseInOdom->baseFootPose().translation().norm(),
                                                  m_halfStepGeneratorDuration,
                                                  m_stepPlannerType,
                                                  m_atlasLookup );
        ROS_INFO_STREAM( "Telling generator that our swing foot: " << footToFrameId( previousStepPoseInOdom->swingFoot, m_atlasLookup ) );
        StepPlan::Ptr           stepPlan;

        Eigen::Affine3dPtr latestDest = walkDest;
        while( latestDest == walkDest && !m_shutdown && ros::ok() ) // loop for full plan
        {
            ROS_INFO_STREAM( "Starting step..." );
            //fixme this function should take the current base foot as an argument
            stepPlan = generator.generateNextStep( previousStepPoseInOdom );// generator.generateNextStep( currentState )
            ros::Time     startTime = ros::Time::now();
            ros::Duration relativeTime( 0 );

            if( !stepPlan )
            {
                // we're done
                // if the desired walk dest hasn't changed then wipe it out so it doesn't run again when we break
                boost::atomic_compare_exchange( &m_walkDestCmd, &walkDest, Eigen::Affine3dPtr() );
                break;
            }


            std::string baseFootFrameId = footToFrameId( stepPlan->baseFoot(), m_atlasLookup );
            m_fullBodyController->switchBaseFoot( baseFootFrameId );
            ROS_INFO_STREAM( "Switching base to " << baseFootFrameId );

            swingFoot = stepPlan->swingFoot();
            ROS_INFO_STREAM( "relativeTime: " << relativeTime );


            int    timeStep        = 0;
            while(  stepPlan->completion( relativeTime ) <= 1 )
            {
                relativeTime = ros::Time::now() - startTime;
                lastStateMsg = boost::atomic_load( &m_lastStateMsg );
                ROS_INFO_STREAM_THROTTLE( 0.5, "completion:   " << stepPlan->completion( relativeTime ) );

                StepPose::Ptr nextStepPoseInOdom;
                nextStepPoseInOdom  = stepPlan->getAtTime( relativeTime );

                if( swingFoot != nextStepPoseInOdom->swingFoot )
                    ROS_ERROR( "Individual pose doesn't match plan" );

                publishBaseFootToOdom( otherFoot(swingFoot), lastStateMsg, nextStepPoseInOdom->baseFootPose() );

                StepPose::Ptr nextStepPoseInBase = odomToBase( nextStepPoseInOdom );

                Eigen::Vector6d footWeights = Eigen::Vector6d::Ones();

                Eigen::Vector3d comWeights( 1, 1, 0 );//FIXME pick better comz weight

                m_fullBodyController->setOppositeFootGoal( nextStepPoseInBase->swingFootPose(), footWeights );
                m_fullBodyController->setCenterOfMassGoal( nextStepPoseInBase->comPose,          comWeights );

                {
                    geometry_msgs::PoseStamped poseStamped;
//                    poseStamped.header.frame_id = "/odom";

                    tf::poseEigenToMsg( nextStepPoseInOdom->lFootPose, poseStamped.pose );
                    lFootTraj.poses.push_back( poseStamped );

                    tf::poseEigenToMsg( nextStepPoseInOdom->rFootPose, poseStamped.pose );
                    rFootTraj.poses.push_back( poseStamped );

                    poseStamped.pose.position.x = nextStepPoseInOdom->comPose.x();
                    poseStamped.pose.position.y = nextStepPoseInOdom->comPose.y();
                    poseStamped.pose.position.z = nextStepPoseInOdom->comPose.z();
                    comTraj.poses.push_back( poseStamped );
                }

                ++timeStep;
                m_dt.sleep();
                previousStepPoseInOdom = nextStepPoseInOdom;
            }



//            publishBaseFootToOdom( otherFoot(swingFoot), lastStateMsg, nextStepPoseInOdom->baseFootPose() );
            // Fully actuate foot goal in roll and pitch
            m_fullBodyController->setFrameGoalGains( buildActuationGoal( stepPlan->swingFoot(), 1 ) );

            swingFoot = otherFoot( swingFoot );

            latestDest = boost::atomic_load( &m_walkDestCmd );

            lFootTrajPub.publish( lFootTraj );
            rFootTrajPub.publish( rFootTraj );
            comTrajPub.publish(   comTraj );

            ROS_INFO_STREAM( "Step done!" );
        }

//        StepPose::Ptr previousStepPoseInOdom; // use swing foot because it will be inverted later
        previousStepPoseInOdom = moveStepPoseToNewOdom( getCurrentStepPoseInBase( swingFoot, lastStateMsg ) );

    }

    ROS_WARN( "Shutting down PreviewWalkCommander" );
}


FrameGoalGains::Ptr
PreviewWalkCommander::
buildActuationGoal( Foot foot, double factor )
{
    const int rollIndex = 3;

    FrameGoalGains::Ptr footGoalGains( new FrameGoalGains );
    footGoalGains->tipFrameId()                    = footToFrameId( foot, m_atlasLookup );
    *footGoalGains = *m_fullBodyController->defaultFrameGains();
    footGoalGains->posKp().segment( rollIndex, 2 ) *= factor;
    return footGoalGains;
}

bool
PreviewWalkCommander::
highFootForce( const atlas_msgs::AtlasState::ConstPtr & atlasStateMsg, Foot foot, ros::Duration dt )
{
    if( !atlasStateMsg )
        return false;

    Eigen::Vector3d force;
    if( foot == LEFT_FOOT )
        force =  Eigen::Vector3dConstMap( &atlasStateMsg->l_foot.force.x );
    else
        force =  Eigen::Vector3dConstMap( &atlasStateMsg->r_foot.force.x );

    //FIXME Filter with more samples
    m_filteredForce = lowPassFilter( m_filteredForce, force, dt.toSec()/m_dt.toSec() );

    if( m_filteredForce.norm() > m_forceThresh )
        return true;

    return false;
}



}
}

/*
 * CapturePointCommander.cpp
 *
 *  Created on: Jun 17, 2013
 *      Author: andrew.somerville
 *              Isura Ranatunga
 */

#include <re2uta/AtlasCommander/CapturePointCommander.hpp>
#include <re2uta/AtlasCommander/FullBodyControllerInterface.hpp>
#include <re2uta/walking/WalkPatternGenerator.hpp>
#include <re2uta/walking/StepTrajectoryGenerator.hpp>
#include <re2uta/walking/CirclePathStepGenerator.hpp>
#include <re2uta/walking/NaoPathStepGenerator.hpp>
#include <re2uta/walking/ManualStepGenerator.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <re2uta/CalculateCapturePoint.hpp>
#include <re2/eigen/eigen_util.h>
//#include <re2uta/CalculateSwingTraj.hpp>
#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <boost/shared_ptr.hpp>
#include <string>


namespace
{
    template <typename Type>
    Type exponentialWeightedAverage( Type oldVal, Type newVal, double newWeight )
    {
        return oldVal*(1-newWeight) + newWeight*(newVal);
    }

    template <typename Type>
    Type lowPassFilter( Type oldVal, Type newVal, double newWeight )
    {
        return exponentialWeightedAverage( oldVal, newVal, newWeight );
    }
}


namespace re2uta
{
namespace atlascommander
{

enum ControllerState
{
    STEP_READY = 0,
    SWITCH_BASE,
    DOUBLE_SUPPORT
};


CapturePointCommander::
CapturePointCommander( const FullBodyControllerInterface::Ptr & fullBodyController,
                       const urdf::Model & model,
                       const AtlasLookup::Ptr & atlasLookup,
                       StepPlannerType stepPlannerType,
                       const ros::Duration & dt )
    : m_atlasLookup( atlasLookup ) // this needs to be reverted to separate construction since this uses a separate thread.
{
    m_fullBodyController = fullBodyController;
    m_solver.reset( new FullBodyPoseSolver( model, m_atlasLookup->lookupString( "l_foot" ),
                                                   m_atlasLookup->lookupString( "r_foot" ),
                                                   m_atlasLookup->lookupString( "utorso" ) ) );
    m_atlasCaptureNode.reset( new AtlasCaptureNode(   model ) );

    m_instantaneousCapturePointPublisher  = m_node.advertise<visualization_msgs::Marker>   ( "/atlas/instantaneousCapturePoint", 1 );
    m_propagatedCapturePointPublisher     = m_node.advertise<visualization_msgs::Marker>   ( "/atlas/propagatedCapturePoint",    1 );
    m_supportPolygonPublisher             = m_node.advertise<geometry_msgs::PolygonStamped>( "/atlas/support_polygon",           1 );
    m_supportPolygonCenterPublisher       = m_node.advertise<visualization_msgs::Marker>(    "/atlas/support_polygon_center",    1 );
    m_CoPPublisher                        = m_node.advertise<visualization_msgs::Marker>(    "/atlas/CoP",                       1 );
    m_CoMPublisher                        = m_node.advertise<visualization_msgs::Marker>(    "/atlas/CoM",                       1 );

    m_halfStepSize = 0.2;
    m_dt = dt;

    // Fg = 9.81 * 90 = 883N
    // Fg*distToEdge = Fd*stepLength
    // 883N * 0.13 = Fd*0.5
    // Fd = 182N
    m_forceThresh  = 50; // to be conservative // FIXME we should reduce this even more if possible

    ControllerState m_controllerState;

    m_stepPlannerType = stepPlannerType;

    m_controlLoopThread = boost::thread( &CapturePointCommander::commandLoop, this );

    m_shutdown    = false;

    m_ankleZOffset  = -0.079342f;

}


CapturePointCommander::
~CapturePointCommander()
{
    m_shutdown    = true;
//    m_controlLoopThread.join();
}

void
CapturePointCommander::
setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg )
{
//    ROS_INFO_STREAM_THROTTLE( 5, "Setting local state" );
    boost::atomic_store( &m_lastStateMsg, latestStateMsg );
}



void
CapturePointCommander::
walkTo( const WalkGoal::Ptr & walkGoal )
{
    //FIXME we're not handling the frame id!
    setDest( walkGoal->goalPose );

    boost::atomic_store( &m_walkGoalCmd, walkGoal );
}

void
CapturePointCommander::
setStepPlannerType( StepPlannerType stepPlannerType )
{
    ROS_ERROR( "CapturePointCommander Currently not ignoring setStepPlannerType" );
    m_stepPlannerType = stepPlannerType;
}


void
CapturePointCommander::
shutdown()
{
    m_shutdown = true;
}





void
CapturePointCommander::
setDest( const Eigen::Affine3d & dest )
{
    Eigen::Affine3dPtr destPtr( new Eigen::Affine3d );
    *destPtr = dest;

    boost::atomic_store( &m_walkDestCmd, destPtr );
}




void
CapturePointCommander::
publishBaseFootToOdom( Foot baseFoot,
                       const atlas_msgs::AtlasState::ConstPtr & lastStateMsg,
                       const Eigen::Affine3d & baseFootInOdom )
{
//    Eigen::Affine3d baseFootToTreeRoot;
//    baseFootToTreeRoot = m_solver->getTransformFromTo( baseFootFrameId, "root", jointPositions );
//
//    Eigen::Affine3d odomInBaseFoot;
//    odomInBaseFoot = baseFootInOdom.inverse();
//
//    Eigen::Affine3d odomInTreeRootXform;
//    odomInTreeRootXform = baseFootToTreeRoot * odomInBaseFoot;

    std::string     swingFootFrameId = footToFrameId( otherFoot(baseFoot), m_atlasLookup );
    std::string     baseFootFrameId  = footToFrameId( baseFoot,            m_atlasLookup );
    Eigen::VectorXd jointPositions   = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );

    tf::Transform odomToTreeRoot;
    tf::transformEigenToTF( baseFootInOdom.inverse(), odomToTreeRoot );
    m_tfBroadcaster.sendTransform( tf::StampedTransform( odomToTreeRoot,
                                                         ros::Time::now(),
                                                         baseFootFrameId,
                                                         "odom"  ));
}


StepPose::Ptr
CapturePointCommander::
moveStepPoseToNewOdom( const StepPose::Ptr & currentStepPose )
{
    StepPose::Ptr newStepPose( new StepPose( *currentStepPose ) );

    Eigen::Affine3d odomInBaseFoot;
    odomInBaseFoot.translation() = (currentStepPose->baseFootPose().translation() + currentStepPose->swingFootPose().translation())/2;

    Eigen::Quaterniond baseQuat(  currentStepPose->baseFootPose().rotation()  );
    Eigen::Quaterniond swingQuat( currentStepPose->swingFootPose().rotation() );

    odomInBaseFoot.linear() = baseQuat.slerp( 0.5, swingQuat ).toRotationMatrix();

    newStepPose->baseFootPose()  = odomInBaseFoot.inverse() * currentStepPose->baseFootPose() ;
    newStepPose->swingFootPose() = odomInBaseFoot.inverse() * currentStepPose->swingFootPose();
    newStepPose->comPose         = odomInBaseFoot.inverse() * currentStepPose->comPose        ;

    return newStepPose;
}



StepPose::Ptr
CapturePointCommander::
getCurrentStepPoseInBase( Foot baseFoot, const atlas_msgs::AtlasStateConstPtr & stateMsg )
{
    Eigen::VectorXd jointPositions   = m_atlasLookup->cmdToTree( parseJointPositions(  stateMsg ) );
    std::string     baseFootFrameId  = footToFrameId( baseFoot,            m_atlasLookup );
    std::string     swingFootFrameId = footToFrameId( otherFoot(baseFoot), m_atlasLookup );

    StepPose::Ptr currentStepPose( new StepPose( baseFoot ) );

    currentStepPose->baseFootPose()  = Eigen::Affine3d::Identity();
    currentStepPose->swingFootPose() = m_solver->getPose(         baseFootFrameId, swingFootFrameId, jointPositions );
    currentStepPose->comPose         = m_solver->getCenterOfMass( baseFootFrameId,                   jointPositions );

    return currentStepPose;
}



StepPose::Ptr
CapturePointCommander::
odomToBase( StepPose::Ptr poseInOdom )
{
    StepPose::Ptr poseInBase( new StepPose( poseInOdom->baseFoot() ) );
    poseInBase->baseFootPose()  = Eigen::Affine3d::Identity();
    poseInBase->swingFootPose() = poseInOdom->baseFootPose().inverse() * poseInOdom->swingFootPose();
    poseInBase->comPose         = poseInOdom->baseFootPose().inverse() * poseInOdom->comPose;

//    ROS_INFO_STREAM( "FootGoal : " << poseInBase->swingFootPose().translation().transpose()    );
//    ROS_INFO_STREAM( "Twist    : " << affineToTwist( poseInBase->swingFootPose() ).transpose() );
//    ROS_INFO_STREAM( "weights  : " << footWeights.transpose() );
    return poseInBase;
}

void
CapturePointCommander::
commandLoop()
{
    atlas_msgs::AtlasState::ConstPtr lastStateMsg;
    Eigen::VectorXd jointPositions;
//    Foot            swingFoot = LEFT_FOOT;

    std::string lFootStr = m_atlasLookup->lookupString( "l_foot" );
    std::string rFootStr = m_atlasLookup->lookupString( "r_foot" );

    ros::NodeHandle node;
    ros::Publisher lFootTrajPub = node.advertise<nav_msgs::Path>( std::string( "/walkTrajectory/l_foot" ) + lFootStr, 5, true );
    ros::Publisher rFootTrajPub = node.advertise<nav_msgs::Path>( std::string( "/walkTrajectory/r_foot" ) + rFootStr, 5, true );
    ros::Publisher comTrajPub   = node.advertise<nav_msgs::Path>( "/walkTrajectory/com",    5, true );


    nav_msgs::Path lFootTraj;
    nav_msgs::Path rFootTraj;
    nav_msgs::Path comTraj;


    lFootTraj.header.frame_id = "/odom";
    rFootTraj.header.frame_id = "/odom";
    comTraj.header.frame_id   = "/odom";

    re2::VisualDebugPublisher vizDebug( "full_body_controler_viz_debug", "odom" );


    while( !(lastStateMsg = boost::atomic_load( &m_lastStateMsg ))  && ros::ok() && !m_shutdown )
    {
        ROS_WARN_STREAM_THROTTLE( 2, "No atlas state available, waiting to start PreviewWalkCommander command loop until seen" );
        m_dt.sleep();
    }

    //FIXME need IMU rotation correction
    StepPose::Ptr previousStepPoseInOdom; // use swing foot because it will be inverted later
    previousStepPoseInOdom = moveStepPoseToNewOdom( getCurrentStepPoseInBase( LEFT_FOOT, lastStateMsg ) );


//    StepPose::Ptr previousStepPoseInBase = odomToBase( previousStepPoseInOdom );

//
//    //FIXME need IMU rotation correction
//    StepPose::Ptr originalStepPose = getCurrentStepPoseInBase( otherFoot(swingFoot), lastStateMsg );
//
//    StepPose::Ptr originalStepPoseInOdom;
//    originalStepPoseInOdom = moveStepPoseToNewOdom( originalStepPose );
//    publishBaseFootToOdom( otherFoot(swingFoot), lastStateMsg, originalStepPoseInOdom->baseFootPose() );




    // Iterations to be used to throttle publishing
    int pubIterNo = 0;

    bool doneMovingComToSupportFoot = false;

    while( ros::ok() && !m_shutdown )
    {
        //FIXME verify
        Eigen::Affine3d walkSource;
        walkSource = Eigen::Affine3d::Identity(); // start at 0,0,0 for now

        Eigen::Affine3dPtr walkDest;
        walkDest      = boost::atomic_exchange( &m_walkDestCmd, walkDest ); // represents transform between start and dest between feet
        lastStateMsg  = boost::atomic_load( &m_lastStateMsg );



        if( !lastStateMsg )
        {
            m_dt.sleep();
            ROS_WARN_THROTTLE( 2, "NO STATE MESSAGE!" );
            continue;
        }

        // publish to swing foot because we are using previous poses feet
        publishBaseFootToOdom( otherFoot(previousStepPoseInOdom->swingFoot), lastStateMsg, previousStepPoseInOdom->baseFootPose() );

        m_atlasCaptureNode->updateMemberData( lastStateMsg );
        m_CoPPublisher.publish(            m_atlasCaptureNode->getCoPMarker( ) );
        m_CoMPublisher.publish(            m_atlasCaptureNode->getComProjectedMarker( lastStateMsg->orientation ) );
        m_supportPolygonPublisher.publish( m_atlasCaptureNode->getSupportPolygonMarker() );


        if( !walkDest )
        {
            m_dt.sleep();
            continue;
        }

        ROS_INFO( "CapturePointCommander got new destination" );
        ROS_INFO( "dt %f", m_dt.toSec() );

        lFootTraj.poses.clear();
        rFootTraj.poses.clear();
        comTraj.  poses.clear();

        lFootTrajPub.publish( lFootTraj );
        rFootTrajPub.publish( rFootTraj );
        comTrajPub.  publish(   comTraj );


        StepPose::ConstPtr beginStepPose           ;
        Eigen::Affine3d    dest                    ;
        double             halfStepSize            ;
        double             footSpreadOffset = 0.14 ;
        ros::Duration      halfstepDt              ;


        StepPose::Ptr currentStepInOdom( new StepPose( *previousStepPoseInOdom ) );
        currentStepInOdom->swingFoot = otherFoot(previousStepPoseInOdom->swingFoot);

        WalkGoal::Ptr walkGoal = boost::atomic_load( &m_walkGoalCmd );
        if( walkGoal )
            currentStepInOdom->swingFoot = walkGoal->swingLegSuggestion;

        if( m_stepPlannerType == atlascommander::WalkCommanderInterface::CIRCLE_PLANNER )
            m_stepGenerator.reset( new CirclePathStepGenerator( currentStepInOdom,
                                                                *walkDest,
                                                                halfStepSize,
                                                                footSpreadOffset,
                                                                currentStepInOdom->swingFoot ) );
        else
        if( m_stepPlannerType == atlascommander::WalkCommanderInterface::NAO_PLANNER )
        {
            Eigen::Affine3d start = interpolate( currentStepInOdom->rFootPose, currentStepInOdom->lFootPose, 0.5 );
            m_stepGenerator.reset( new NaoPathStepGenerator( start, *walkDest, footSpreadOffset ) );
        }
        else
        if( m_stepPlannerType == atlascommander::WalkCommanderInterface::MANUAL_PLANNER )
        {
            Eigen::Affine3d start = interpolate( currentStepInOdom->rFootPose, currentStepInOdom->lFootPose, 0.5 );
            m_stepGenerator.reset( new ManualStepGenerator( currentStepInOdom, *walkDest ) );
        }


        ROS_INFO_STREAM( "Telling generator that our swing foot: " << footToFrameId( currentStepInOdom->swingFoot, m_atlasLookup ) );

        bool startPose = true;
        int stepIndex = 0;
        Eigen::Affine3dPtr latestDest = walkDest;
        while(    latestDest == walkDest
               && !m_shutdown
               && ros::ok()
               && !m_stepGenerator->hasArrived( previousStepPoseInOdom->swingFootPose() )
               && !m_stepGenerator->hasArrived( previousStepPoseInOdom->baseFootPose()  )
              )
        {
            ROS_INFO_STREAM( "Starting step..." );
            //FIXME this function should take the current base foot as an argument
            StepPose::Ptr nextStepPoseInOdom( new StepPose( previousStepPoseInOdom->swingFoot ) );

            nextStepPoseInOdom->baseFootPose()  = previousStepPoseInOdom->swingFootPose();
            nextStepPoseInOdom->swingFootPose() = m_stepGenerator->generateNextStep( previousStepPoseInOdom->swingFootPose(),
                                                                                     previousStepPoseInOdom->baseFootPose(), stepIndex );
//            ROS_WARN_STREAM( "nextStepPoseInOdom->swingFootPose() : "    << nextStepPoseInOdom->swingFootPose().translation().transpose() );
//            ROS_WARN_STREAM( "previousStepPoseInOdom->baseFootPose() : " << previousStepPoseInOdom->baseFootPose().translation().transpose() );


            StepPose::Ptr nextStepPoseInBase;
            nextStepPoseInBase = odomToBase( nextStepPoseInOdom );

            std::string baseFootFrameId = footToFrameId(nextStepPoseInOdom->baseFoot(), m_atlasLookup );//"l_foot";
            m_fullBodyController->switchBaseFoot( baseFootFrameId );

            lastStateMsg   = boost::atomic_load( &m_lastStateMsg );
            jointPositions = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );

            //////////
            // UPDATES m_atlasCaptureNode member data IMPORTANT!!
            m_atlasCaptureNode->updateMemberDataNoDouble( lastStateMsg );
            //////////
            Eigen::Vector3d centerOfMass     = m_solver->getCenterOfMass( baseFootFrameId, jointPositions );
            Eigen::Vector3d centerOfSupport  = m_atlasCaptureNode->getSupportPolygonCenter();
            m_atlasCaptureNode->updateCOM( baseFootFrameId,  jointPositions );
            m_atlasCaptureNode->updateInstantaneousCapturePoint( lastStateMsg    );
            m_atlasCaptureNode->updatePropagatedCapturePoint(    lastStateMsg, 1 );
            m_atlasCaptureNode->updateCoP(lastStateMsg->l_foot, lastStateMsg->r_foot, m_l_footPos.translation(), m_r_footPos.translation() );


            publishBaseFootToOdom( nextStepPoseInOdom->baseFoot(), lastStateMsg, nextStepPoseInOdom->baseFootPose() );

            ros::Time     startTime = ros::Time::now();
            ros::Duration relativeTime( 0 );

            centerOfSupport = m_atlasCaptureNode->getSupportPolygonCenter();
            centerOfMass    = m_solver->getCenterOfMass( baseFootFrameId, jointPositions );

            m_CoPPublisher.publish( m_atlasCaptureNode->getCoPMarker( ) );
            m_CoMPublisher.publish( m_atlasCaptureNode->getComProjectedMarker( lastStateMsg->orientation ) );
            m_supportPolygonPublisher.publish( m_atlasCaptureNode->getSupportPolygonMarker() );


            // Setting pelvis orientation constraint
//            FrameGoal::Ptr frameGoal( new FrameGoal );
//
//
//            frameGoal->baseFrameId   = baseFootFrameId;
//            frameGoal->goalFrameId   = baseFootFrameId;
//            frameGoal->tipFrameId    = m_atlasLookup->lookupString( "utorso" );
//            frameGoal->goalPose      = Eigen::Affine3d::Identity();
//            frameGoal->weightMatrix  = Eigen::MatrixXd::Zero( 6, 6 );
//            frameGoal->weightMatrix.diagonal().head(3) = Eigen::Vector3d::Zero();
//            frameGoal->weightMatrix.diagonal().tail(3) = Eigen::Vector3d( 0.001, 0.001, 0.001 );
//            m_fullBodyController->setFrameGoal( frameGoal );


//            FrameGoal::Ptr frameGoal( new FrameGoal );
//            frameGoal->baseFrameId   = baseFootFrameId;
//            frameGoal->goalFrameId   = baseFootFrameId;
//            frameGoal->tipFrameId    = m_atlasLookup->lookupString( "pelvis" );
//            frameGoal->goalPose      = Eigen::Affine3d::Identity();
//            frameGoal->weightMatrix  = Eigen::MatrixXd::Zero( 6, 6 );
//            frameGoal->weightMatrix.diagonal().head(3) = Eigen::Vector3d::Zero();
//            frameGoal->weightMatrix.diagonal().tail(3) = Eigen::Vector3d( 0.01, 0.05, 0.01 );
//            m_fullBodyController->setFrameGoal( frameGoal );


            Eigen::Vector3d comStart     = centerOfMass;
            Eigen::Vector3d comEnd       = centerOfSupport;
            comEnd.z()                   = comStart.z();
            Eigen::Vector3d comDiff      = comEnd - comStart;

            Eigen::Vector3d comWeights( 1, 1, 0.00 );
            ros::Duration   timeToStep     = ros::Duration( 4 );
            ros::Time       timeStart      = ros::Time::now();
            ROS_INFO_STREAM( "ComEnd: " << comEnd.transpose() );

            relativeTime   = ros::Duration( 0 );
            while( relativeTime < timeToStep && ros::ok() && !m_shutdown ) // && !doneMovingComToSupportFoot )
            {
                relativeTime = ros::Time::now() - timeStart;
                double           completionFraction = relativeTime.toSec() / timeToStep.toSec();
                Eigen::Vector3d desiredComPose      = comStart + comDiff*completionFraction ;

                m_fullBodyController->setCenterOfMassGoal( desiredComPose, comWeights );
//                ROS_INFO_STREAM_THROTTLE( 0.2, "currentCom:         " << m_fullBodyController-> );
                ROS_INFO_STREAM( "comTarget:          " << desiredComPose.transpose()   );
                ROS_INFO_STREAM( "completionFraction: " << completionFraction  );
                m_dt.sleep();
            }

            m_fullBodyController->setCenterOfMassGoal( comEnd, comWeights );

        }

//            Eigen::Affine3d odomToBase = nextStepPoseInOdom->baseFootPose().inverse();
//
////            Eigen::Affine3d swingStartInBase  = odomToBase * previousStepPoseInOdom->baseFootPose();
//            Eigen::Affine3d swingStartInBase  = m_solver->getPose( baseFootFrameId, footToFrameId(nextStepPoseInOdom->swingFoot), jointPositions );
//            Eigen::Affine3d swingMiddleInBase = Eigen::Translation3d( 0, 0, 0.07 ) * swingStartInBase;
//            Eigen::Affine3d swingEndInBase    = odomToBase * nextStepPoseInOdom->swingFootPose();
//            ROS_WARN_STREAM( "nextStepPoseInOdom->swingFootPose() : " << nextStepPoseInOdom->swingFootPose().translation().transpose() );
//
//
//            Eigen::Affine3dVector poseInBaseVector;
//            poseInBaseVector.push_back( swingStartInBase  );
//            poseInBaseVector.push_back( swingMiddleInBase );
//            poseInBaseVector.push_back( swingEndInBase    );
//
////            ROS_WARN_STREAM( "swingStartInBase : " << swingStartInBase.translation().transpose() );
////            ROS_WARN_STREAM( "swingMiddleInBase: " << swingMiddleInBase.translation().transpose());
////            ROS_WARN_STREAM( "swingEndInBase   : " << swingEndInBase.translation().transpose()   );
//
//            std::vector<double> trajPointTimes;
//            trajPointTimes.push_back( 2 );
//            trajPointTimes.push_back( 4 );
//            std::vector<double>::iterator trajPointTimeItor = trajPointTimes.begin();
//
//
//            typedef Eigen::aligned_allocator<Eigen::Vector6d>               Vector6dAllignedAllocator;
//            typedef std::vector<Eigen::Vector6d,Vector6dAllignedAllocator>  Vector6dVector;
//
//            Eigen::Vector6d footWeights;
//            Vector6dVector  footWeightList;
//            footWeights << 1,1,1,0.5,0.5,0.5;
//            footWeightList.push_back( footWeights );
//            footWeights << 1,1,1,0.5,0.5,0.5;
//            footWeightList.push_back( footWeights );
//            footWeights << 1,1,1,0.5,0.5,0.5;
//            footWeightList.push_back( footWeights );
//            Vector6dVector::iterator footWeightItor = footWeightList.begin();
//
//            int poseIndex = 0;
//            BOOST_FOREACH( const Eigen::Affine3d & poseInBase, poseInBaseVector )
//            {
//                lastStateMsg   = boost::atomic_load( &m_lastStateMsg );
//                jointPositions = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );
//
//                startTime    = ros::Time::now();
//                relativeTime = ros::Duration(0);
//                Eigen::Affine3d nextPoseInBase = poseInBaseVector[ poseIndex + 1 ]; // Ok because of break at bottom
//
//                ROS_WARN( "Pose : %i", poseIndex );
//                ROS_WARN_STREAM( "first  pose \n" << poseInBase.matrix()     );
//                ROS_WARN_STREAM( "second pose \n" << nextPoseInBase.matrix() );
//
//
//                double completionFactor = relativeTime.toSec() / *trajPointTimeItor;
//                while( completionFactor <= 1.0 )
//                {
//                    relativeTime = ros::Time::now() - startTime;
//                    completionFactor = relativeTime.toSec() / *trajPointTimeItor;
//                    Eigen::Affine3d oppFootGoalInBase = interpolate( poseInBase, nextPoseInBase, completionFactor );
//
//                    lastStateMsg   = boost::atomic_load( &m_lastStateMsg );
//                    jointPositions = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );
//
////                    ROS_INFO_STREAM_THROTTLE( 0.2, "InterpolatedPose " << oppFootGoalInBase.translation().transpose() );
////                    ROS_INFO_STREAM_THROTTLE( 0.2, "Foot weights     " << footWeightItor->transpose() );
//
//
////                    //////////
////                    // UPDATES m_atlasCaptureNode member data IMPORTANT!!
////                    m_atlasCaptureNode->updateMemberDataNoDouble( lastStateMsg );
////                    //////////
////
////                    centerOfSupport = m_atlasCaptureNode->getSupportPolygonCenter();
////                    centerOfMass    = m_solver->getCenterOfMass( baseFootFrameId, jointPositions );
////
//                    m_fullBodyController->setOppositeFootGoal( oppFootGoalInBase, *footWeightItor );
////                    m_fullBodyController->setCenterOfMassGoal( centerOfSupport,    comWeights    );
////
////                    ROS_ERROR_STREAM( "oppFootGoalInBase : " << oppFootGoalInBase.matrix() );
////                    ROS_ERROR_STREAM( "centerOfSupport :   " << centerOfSupport            );
//
////                    //////////////////////////////
////                    // Publish markers
////                    if( pubIterNo%10 == 0 )
////                    {
////                        // These will publish the capture points and support polygon
////                        m_instantaneousCapturePointPublisher.publish( m_atlasCaptureNode->getInstantaneousCapturePointMarker() );
////                        m_propagatedCapturePointPublisher.publish(    m_atlasCaptureNode->getPropagatedCapturePointMarker() );
////                        m_supportPolygonPublisher.publish(            m_atlasCaptureNode->getSupportPolygonMarker() );
////                        m_supportPolygonCenterPublisher.publish(      m_atlasCaptureNode->getSupportPolygonCenterMarker() );
////                        m_CoPPublisher.publish(                       m_atlasCaptureNode->getCoPMarker( ) );
////                        m_CoMPublisher.publish(                       m_atlasCaptureNode->getComProjectedMarker( lastStateMsg->orientation ) );
////                    }
////                    ++pubIterNo;
////                    //////////////////////////////
//
//                    if( highFootForce( lastStateMsg, nextStepPoseInBase->swingFoot, m_dt ) && completionFactor > 0.5 && poseIndex > 0 )
//                        break;
//
//                    m_dt.sleep();
//                }
//
//                if( highFootForce( lastStateMsg, nextStepPoseInBase->swingFoot, m_dt ) && poseIndex > 0 )
//                {
//                    lastStateMsg   = boost::atomic_load( &m_lastStateMsg );
//                    jointPositions = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );
//                    nextStepPoseInBase->swingFootPose() = m_solver->getPose( baseFootFrameId, footToFrameId( nextStepPoseInBase->swingFoot ), jointPositions );
//                    nextStepPoseInOdom->swingFootPose() = nextStepPoseInOdom->baseFootPose().inverse() * nextStepPoseInBase->swingFootPose();
//                    break;
//                }
//                else
//                {
////                    ROS_FATAL_STREAM( "Else: " );
//
//                    m_fullBodyController->setOppositeFootGoal( nextPoseInBase, *footWeightItor ); // In case we didnt finish in the loop
////                    ROS_ERROR_STREAM( "oppFootGoalInBase : " << nextPoseInBase.matrix() );
//                }
//
//                ++poseIndex;
//                ++trajPointTimeItor;
//                ++footWeightItor;
//
//                if( poseIndex >= poseInBaseVector.size() - 1 )
//                    break;
//            }
//
//            while( /*!done*/ false )
//            {
//                if( /*forceHigh*/ false )
//                {
//                    // do stuff that you should do when force is high
//                }
//                else
//                {
//                    //stepdown
//                }
//            }
//
//            m_dt.sleep();
//
//
//            previousStepPoseInOdom = nextStepPoseInOdom;
//            ++stepIndex;
//
////        }
////    }
////}
//
//
//
//
//
//
//
//
//
//
////            //////////
////            // UPDATES m_atlasCaptureNode member data IMPORTANT!!
////            m_atlasCaptureNode->updateMemberDataNoDouble( lastStateMsg );
////            //////////
////            //////////
////
////
////
////
////            std::string     baseName;
////
////            if( m_atlasCaptureNode->getBaseName() == "l_foot" )
////            {
////                  baseName       = "l_foot";
////                  m_l_footPos    = Eigen::Affine3d::Identity();
////                  m_r_footPos    = m_solver->getTransformFromTo( "r_foot", "l_foot", jointPositions );
////                  m_swingFootPos = m_r_footPos.translation();
////            }
////            else
////            {
////                  baseName       = "r_foot";
////                  m_r_footPos    = Eigen::Affine3d::Identity();
////                  m_l_footPos    = m_solver->getTransformFromTo( "l_foot", "r_foot", jointPositions );
////                  m_swingFootPos = m_l_footPos.translation();
////            }
////
////            m_fullBodyController->switchBaseFoot( baseName );
////
////
////
////
////
////
////            // Get COM location
////            m_COM = m_solver->getCenterOfMass( baseFootFrameId,                   jointPositions );
////
////            m_atlasCaptureNode->updateCOM( baseName,  jointPositions );
////
////            m_atlasCaptureNode->updateInstantaneousCapturePoint( lastStateMsg    );
////            m_atlasCaptureNode->updatePropagatedCapturePoint(    lastStateMsg, 1 );
////            m_atlasCaptureNode->updateCoP(lastStateMsg->l_foot, lastStateMsg->r_foot, m_l_footPos.translation(), m_r_footPos.translation() );
////            m_centerOfSupport = m_atlasCaptureNode->getSupportPolygonCenter();
////
////
////            // Setting pelvis orientation constraint
////            FrameGoal::Ptr frameGoal( new FrameGoal );
////
////            frameGoal->baseFrameId   = baseName;
////            frameGoal->goalFrameId   = baseName; // TODO should this be in base or swing
////            frameGoal->tipFrameId    = "pelvis";
////
////            frameGoal->goalPose      = Eigen::Affine3d::Identity();
////
////            Eigen::VectorXd pelvisConstraint = Eigen::VectorXd::Zero( 6 );
////            pelvisConstraint.head(3) = Eigen::Vector3d::Zero();
////            pelvisConstraint.tail(3) = Eigen::Vector3d( 0.01, 0.05, 0.01 );
////
////            frameGoal->weightMatrix = pelvisConstraint.asDiagonal();
////
////            m_fullBodyController->setFrameGoal( frameGoal );
////
////
////
////
////            Eigen::Vector3d desiredComPose = Eigen::Vector3d::Zero();
////
////
////            ros::Duration timeToStep     = ros::Duration( 3 );
////            relativeTime   = ros::Duration( 0 );
////            ros::Time     timeStart      = ros::Time::now();
////
////            Eigen::Vector3d comStart     = m_COM;
////            Eigen::Vector3d comEnd       = m_centerOfSupport;
////            Eigen::Vector3d comDiff      = comEnd - comStart;
////
////
////            double completionFraction = 0;
////            desiredComPose = comStart;
////
////            Eigen::Vector3d comWeights( 1, 1, 0 );
////
////            while( relativeTime < timeToStep && !doneMovingComToSupportFoot )
////            {
////                ROS_INFO_STREAM( "completionFraction: " << completionFraction );
////                ROS_INFO_STREAM( "comDiff: " << comDiff.transpose() );
////                ROS_INFO_STREAM( "comStart: " << m_COM.transpose() );
////                ROS_INFO_STREAM( "Moving COM to support base!" << desiredComPose.transpose() );
////
////                relativeTime = ros::Time::now() - timeStart;
////
////                completionFraction = relativeTime.toSec() / timeToStep.toSec();
////                desiredComPose       = comDiff*completionFraction + comStart;
////
////                m_fullBodyController->setCenterOfMassGoal( desiredComPose, comWeights );
////
////                m_dt.sleep();
////
////            }
////
////            doneMovingComToSupportFoot = true;
////
////            m_fullBodyController->setCenterOfMassGoal( m_centerOfSupport, comWeights );
////
////
////
////
////
////
////
////            //////////////////////////////
////            // Take single step
////
////            if( true /*takeStep*/ )
////            {
////                double swingDownTime             = 1;
////                double swingUpTime               = 0.5;
////                double totalSwingTime            = swingUpTime + swingDownTime;
////
////
////                ROS_INFO_STREAM( "Taking step 1!" );
////
////                ros::Duration   timeToStep       = ros::Duration( swingUpTime );
////                ros::Duration   relativeTime     = ros::Duration( 0 );
////                ros::Time       timeStart        = ros::Time::now();
////
////                Eigen::Vector3d swingStart       = m_swingFootPos;
////                Eigen::Vector3d swingEnd         = swingStart;
////                                swingEnd( 2 )    = 0.1;
////                Eigen::Vector3d swingDiff        = swingEnd - swingStart;
////
////                double completionFraction = 0;
////
////                //
////                // Swing up phase
////                //
////                m_fullBodyController->switchBaseFoot( baseName );
////                timeStart        = ros::Time::now();
////                while( relativeTime < timeToStep )
////                {
////                    ROS_INFO_STREAM( "Swing up phase!" );
////                    relativeTime       = ros::Time::now() - timeStart;
////                    completionFraction = relativeTime.toSec() / timeToStep.toSec();
////                    m_swingPosition    = swingDiff*completionFraction + swingStart;
////
////                    //////////////////////////////
////                    // Swing Controller
////
////                    Eigen::Vector6d swingLegWeights;
////                    swingLegWeights << 1, 1, 1, 1, 1, 1;
////
////                    Eigen::Affine3d swingLegPose = Eigen::Affine3d::Identity();
////                    swingLegPose.translation() = m_swingPosition;
////
////    //                std::cout << m_swingPosition.transpose();
////
////                    FrameGoal::Ptr frameGoal( new FrameGoal );
////
////                    frameGoal->baseFrameId  = baseName;
////                    frameGoal->goalFrameId  = baseName;
////                    frameGoal->tipFrameId   = "l_foot";
////
////                    frameGoal->goalPose     = swingLegPose;
////                    frameGoal->weightMatrix = swingLegWeights.asDiagonal();
////
////                    m_fullBodyController->setFrameGoal( frameGoal );
////
////                    m_dt.sleep();
////
////                }
////
////
////                // Get new state message
////                lastStateMsg  = boost::atomic_load( &m_lastStateMsg );
////
////                /*
////                 * This function will calculate the propagated Capture Point (CP)
////                 */
////                m_atlasCaptureNode->updateMemberData(                lastStateMsg                 );
////                m_atlasCaptureNode->updateInstantaneousCapturePoint( lastStateMsg                 );
////                m_atlasCaptureNode->updatePropagatedCapturePoint(    lastStateMsg, totalSwingTime );
////
////                Eigen::Vector3d propagatedCapturePoint = m_atlasCaptureNode->getPropagatedCapturePoint();
////                Eigen::Vector3d swingFootStart         = m_atlasCaptureNode->getSwingFootPose();
////
////
////                ROS_INFO_STREAM( "Taking step 2!" );
////
////                timeToStep       = ros::Duration( swingDownTime );
////                relativeTime     = ros::Duration( 0 );
////                timeStart        = ros::Time::now();
////
////                swingStart       = swingFootStart;
////                swingEnd         = swingFootStart;
////                swingEnd.z()     = 0;
////                swingEnd.x()     = swingEnd.x() + 0.1;
////
////                swingDiff        = swingEnd - swingStart;
////
//////        }
//            //    }
//            //}
////                ROS_WARN_STREAM( "Swing start: " << swingStart.transpose() );
////                ROS_WARN_STREAM( "Swing end: "   << swingEnd.transpose()   );
////                ROS_WARN_STREAM( "Swing swingDiff: "   << swingDiff.transpose() );
////                getchar();
////
////                completionFraction = 0;
////
////                timeStart        = ros::Time::now();
////                while( relativeTime < timeToStep )
////                {
////                    relativeTime = ros::Time::now() - timeStart;
////
////                    //
////                    // Stepping phase
////                    //
////                    relativeTime       = ros::Time::now() - timeStart;
////                    completionFraction = relativeTime.toSec() / timeToStep.toSec();
////                    m_swingPosition    = swingDiff*completionFraction + swingStart;
////
////                    //////////////////////////////
////                    // Swing Controller
////
////                    m_fullBodyController->switchBaseFoot( baseName );
////
////                    Eigen::Vector6d swingLegWeights;
////                    swingLegWeights << 1, 1, 1, 1, 1, 1;
////
////                    Eigen::Affine3d swingLegPose = Eigen::Affine3d::Identity();
////                    swingLegPose.translation() = m_swingPosition;
////
////                    ROS_WARN_STREAM( "Swing completionFraction: "   << completionFraction );
////                    ROS_WARN_STREAM( "Swing command: " << m_swingPosition.transpose() );
////    //                getchar();
////
////    //                std::cout << m_swingPosition.transpose();
////
////                    FrameGoal::Ptr frameGoal( new FrameGoal );
////
////                    frameGoal->baseFrameId  = baseName;
////                    frameGoal->goalFrameId  = baseName;
////                    frameGoal->tipFrameId   = "l_foot";
////
////                    frameGoal->goalPose     = swingLegPose;
////                    frameGoal->weightMatrix = swingLegWeights.asDiagonal();
////
////                    m_fullBodyController->setFrameGoal( frameGoal );
////
////                    m_dt.sleep();
////                }
////
////
////            }
////
////
////
////
////
////
////
////
//            //////////////////////////////
//            // Publish markers
//            if( pubIterNo%10 == 0 )
//            {
//                // These will publish the capture points and support polygon
//                m_instantaneousCapturePointPublisher.publish( m_atlasCaptureNode->getInstantaneousCapturePointMarker() );
//                m_propagatedCapturePointPublisher.publish(    m_atlasCaptureNode->getPropagatedCapturePointMarker() );
//                m_supportPolygonPublisher.publish(            m_atlasCaptureNode->getSupportPolygonMarker() );
//                m_supportPolygonCenterPublisher.publish(      m_atlasCaptureNode->getSupportPolygonCenterMarker() );
//                m_CoPPublisher.publish(                       m_atlasCaptureNode->getCoPMarker( ) );
//                m_CoMPublisher.publish(                       m_atlasCaptureNode->getComProjectedMarker( lastStateMsg->orientation ) );
//            }
//            ++pubIterNo;
//            //////////////////////////////
////
////
////
////
////
////
////
////            //////////////////////////////
////            // Capture Point Safety Trigger
////
////            // Preempt if capture point outside support polygon
////            if( false && !m_atlasCaptureNode->isStateCaptured(  lastStateMsg, m_instantaneousCapturePoint ) )
////            {
////
////                ROS_INFO_STREAM( "Executing capture point trajectory!" );
////
////                double stepTime                  = 2;
////                double swingUpTime               = 1;
////
////                ros::Duration timeToStep         = ros::Duration( 2 );
////                ros::Duration relativeTime       = ros::Duration( 0 );
////                ros::Time     timeStart          = ros::Time::now();
////
////                //////////
////                // UPDATES m_atlasCaptureNode member data IMPORTANT!!
////                m_atlasCaptureNode->updateMemberDataNoDouble( lastStateMsg );
////                //////////
////                //////////
////
////                Eigen::Vector3d swingUpStart     = m_atlasCaptureNode->getSwingFootPose();
////                Eigen::Vector3d swingDownStart   = swingUpStart;
////                Eigen::Vector3d swingEnd         = m_atlasCaptureNode->getPropagatedCapturePoint();
////                Eigen::Vector3d swingDownDiff    = swingEnd - swingDownStart;
////                Eigen::Vector3d swingUpDiff      = swingDownStart - swingUpStart;
////
////                swingDownStart( 2 )              = 0.01;
////
////                double completionFraction = 0;
////
////                while( relativeTime < timeToStep  )
////                {
////
////                    relativeTime = ros::Time::now() - timeStart;
////
////                    // Get new state message
////                    lastStateMsg  = boost::atomic_load( &m_lastStateMsg );
////
////                    //
////                    // Swing up phase
////                    //
////                    while( relativeTime.toSec() < swingUpTime )
////                    {
////                        relativeTime       = ros::Time::now() - timeStart;
////                        completionFraction = relativeTime.toSec() / timeToStep.toSec();
////                        m_swingPosition    = swingUpDiff*completionFraction + swingUpStart;
////
////                        // Save the last swing position so that swing down can use after exit
////                        swingDownStart       = m_swingPosition;
////
////                        m_dt.sleep();
////                    }
////
////                    //
////                    // Stepping phase
////                    //
////                    relativeTime       = ros::Time::now() - timeStart;
////                    completionFraction = relativeTime.toSec() / timeToStep.toSec();
////                    m_swingPosition    = swingDownDiff*completionFraction + swingDownStart;
////
////
////
////    //                /*
////    //                 * This function will calculate the propagated Capture Point (CP)
////    //                 */
////    //                m_atlasCaptureNode->updateInstantaneousCapturePoint( lastStateMsg );
////    //                m_atlasCaptureNode->updatePropagatedCapturePoint(    lastStateMsg, timeToStep );
////    //
////    //                Eigen::Vector3d propagatedCapturePoint = m_atlasCaptureNode->getPropagatedCapturePoint();
////    //                Eigen::Vector3d swingFootStart         = m_atlasCaptureNode->getSwingFootPose();
////
////
////
////
////                    //////////////////////////////
////                    // Swing Controller
////
////                    m_fullBodyController->switchBaseFoot( m_atlasCaptureNode->getBaseName() );
////
////                    Eigen::Vector6d swingLegWeights;
////                    swingLegWeights << 1, 1, 1, 0, 0, 0;
////
////                    Eigen::Affine3d swingLegPose = Eigen::Affine3d::Identity();
////                    swingLegPose.translation() = m_swingPosition;
////
////    //                std::cout << m_swingPosition.transpose();
////
////                    FrameGoal::Ptr frameGoal( new FrameGoal );
////
////                    frameGoal->baseFrameId  = m_atlasCaptureNode->getBaseName();
////                    frameGoal->goalFrameId  = m_atlasCaptureNode->getBaseName();
////                    frameGoal->tipFrameId   = m_atlasCaptureNode->getSwingName();
////
////                    frameGoal->goalPose     = swingLegPose;
////                    frameGoal->weightMatrix = swingLegWeights.asDiagonal();
////
////                    m_fullBodyController->setFrameGoal( frameGoal );
////
////                    m_dt.sleep();
//
////                }
//
////            }
//
//
//        }
//
//
//    }

}
}


FrameGoalGains::Ptr
CapturePointCommander::
buildActuationGoal( Foot foot, double factor )
{
    const int rollIndex = 3;

    FrameGoalGains::Ptr footGoalGains( new FrameGoalGains );
    footGoalGains->tipFrameId()                    = footToFrameId( foot, m_atlasLookup );
    *footGoalGains = *m_fullBodyController->defaultFrameGains();
    footGoalGains->posKp().segment( rollIndex, 2 ) *= factor;
    return footGoalGains;
}

bool
CapturePointCommander::
highFootForce( const atlas_msgs::AtlasState::ConstPtr & atlasStateMsg, Foot foot, ros::Duration dt )
{
    if( !atlasStateMsg )
        return false;

    Eigen::Vector3d force;
    if( foot == LEFT_FOOT )
        force =  Eigen::Vector3dConstMap( &atlasStateMsg->l_foot.force.x );
    else
        force =  Eigen::Vector3dConstMap( &atlasStateMsg->r_foot.force.x );

    //FIXME Filter with more samples
    m_filteredForce = lowPassFilter( m_filteredForce, force, dt.toSec()/m_dt.toSec() );

    if( m_filteredForce.norm() > m_forceThresh )
        return true;

    return false;
}



}
}

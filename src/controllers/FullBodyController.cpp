/*
 * PositionController.12cpp
 *
 *  Created on: May 8, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/AtlasCommander/FullBodyController.hpp>
#include <re2uta/AtlasCommander/JointController.hpp>
#include <re2uta/AtlasCommander/JointGoal.hpp>
#include <re2uta/constraints/InputConstraint.hpp>
#include <re2uta/constraints/CartesianConstraint.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <re2uta/gradient_descent.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <re2/eigen/eigen_util.h>
#include <re2/matrix_conversions.h>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <atlas_msgs/AtlasState.h>
#include <tf_conversions/tf_eigen.h>
#include <ros/ros.h>
#include <boost/thread/condition_variable.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

namespace
{
    Eigen::Vector4d colorFromHash( int hash )
    {
        double red   = ((hash >> 16) & 0x0000FF) /255.0;
        double green = ((hash >>  8) & 0x0000FF) /255.0;
        double blue  = ((hash >>  0) & 0x0000FF) /255.0;

        return Eigen::Vector4d( red, green, blue, 1 );
    }
}



namespace re2uta
{
namespace atlascommander
{



atlas_msgs::AtlasCommand::Ptr
buildAtlasVelocityCommand( const Eigen::VectorXd & velocities,
                           const atlas_msgs::AtlasCommand::ConstPtr & defaultMsg = atlas_msgs::AtlasCommand::ConstPtr() )
{
    atlas_msgs::AtlasCommand::Ptr atlasCommand( new atlas_msgs::AtlasCommand );
    if( defaultMsg )
        *atlasCommand = *defaultMsg;

    atlasCommand->position    = std::vector<double>( velocities.size(), 0 );
    atlasCommand->velocity    = std::vector<double>( velocities.size(), 0 );
    atlasCommand->kp_velocity = re2::toStd( Eigen::VectorXf(re2::toEigen( atlasCommand->kp_position ) ) );
    atlasCommand->kp_position = std::vector<float>( velocities.size(), 0 );
    atlasCommand->kd_position = std::vector<float>( velocities.size(), 0 );

    std::copy( velocities.data(), velocities.data() + velocities.size(), atlasCommand->velocity.begin() );

    return atlasCommand;
}

atlas_msgs::AtlasCommand::Ptr
buildAtlasPositionCommand( const Eigen::VectorXd & positions,
                           const atlas_msgs::AtlasCommand::ConstPtr & defaultMsg = atlas_msgs::AtlasCommand::ConstPtr() )
{
    atlas_msgs::AtlasCommand::Ptr atlasCommand( new atlas_msgs::AtlasCommand );
    if( defaultMsg )
        *atlasCommand = *defaultMsg;

    atlasCommand->position.resize( positions.size(), 0 );
    atlasCommand->velocity.resize( positions.size(), 0 );

    std::copy( positions.data(), positions.data() + positions.size(), atlasCommand->position.begin() );

    return atlasCommand;
}




FullBodyController::
FullBodyController( const urdf::Model & model,
                    AtlasLookup::Ptr const & atlasLookup,
                    boost::condition_variable & syncVar,
                    const JointController::Ptr & jointController,
                    ros::Duration dt)
    : m_syncVar( syncVar )
    , m_atlasLookup( atlasLookup )

{
    m_iteration        = 0;
    m_dt               = dt;
    m_jointController  = jointController;
    m_shutdown         = false;
    m_constraintsDirty = true;
//    m_commandType      = POSITION_CMD;
    m_commandType      = VELOCITY_CMD;
//    m_jointControlType = POSITION_CONTROL;
    m_jointControlType = VELOCITY_CONTROL;

    std::string controlTypeString = "VELOCITY_CONTROL";
    ros::NodeHandle("~").getParam( "control_type", controlTypeString );

    if( boost::to_upper_copy( controlTypeString ) == "POSITION_CONTROL" )
        m_jointControlType = POSITION_CONTROL;
    else
    if( boost::to_upper_copy( controlTypeString ) == "VELOCITY_CONTROL" )
        m_jointControlType = VELOCITY_CONTROL;


    m_defaultTransform = Eigen::Affine3d::Identity();
    m_poseSolver.reset( new  FullBodyPoseSolver( model, m_atlasLookup->lookupString( "l_foot" ),
                                                        m_atlasLookup->lookupString( "r_foot" ),
                                                        m_atlasLookup->lookupString( "utorso" ) ) );
//    m_baseFootFrameId = "r_foot";
    m_baseFootFrameId.reset( new std::string( m_atlasLookup->lookupString("l_foot") ) );

    m_inputWeights              = Eigen::MatrixXd::Identity( m_atlasLookup->getNumTreeJoints(), m_atlasLookup->getNumTreeJoints() );
    m_defaultTreeJointPositions = m_atlasLookup->calcDefaultTreeJointAngles( 0.4 );
    m_treeJointPositions        = m_defaultTreeJointPositions;

    for( int i = 0; i < m_defaultTreeJointPositions.size(); ++i )
    {
        ROS_WARN( "Default angle for: %i, %s, is: %f", i, m_atlasLookup->treeQnrToJointName(i).c_str(), m_defaultTreeJointPositions(i) );
    }

    const KDL::TreeElement * baseElement;
    const KDL::TreeElement * oppositeFootElement;

    baseElement         = &getTreeElement( *m_poseSolver->tree(), *m_baseFootFrameId     );
    oppositeFootElement = &getTreeElement( *m_poseSolver->tree(), oppositeFootFrameId() );

    m_poseSolver->comConstraint()->    setBaseElement( baseElement );
    m_poseSolver->postureConstraint()->setBaseElement( baseElement );
    m_poseSolver->postureConstraint()->setTipElement( m_atlasLookup->lookupString( "utorso" ) );
    m_poseSolver->oppFootConstraint()->setBaseElement( baseElement );
    m_poseSolver->oppFootConstraint()->setTipElement( oppositeFootElement );


    ros::NodeHandle node;

    double defaultKp     = 10;
    node.getParam( "/drc/constraints/default/kp", defaultKp );

    double defaultKd     = defaultKp * 0.5;
    node.getParam( "/drc/constraints/default/kd", defaultKd );

    double defaultVelKp  = 0;
    node.getParam( "/drc/constraints/default/vel_kd", defaultVelKp );

    double defaultVelMax = 10;
    node.getParam( "/drc/constraints/default/vel_max", defaultVelMax );

    BOOST_FOREACH( const SolverConstraint::Ptr & constraint,  m_poseSolver->constraints() )
    {
        constraint->setGoal( constraint->getCurrentOutput( m_defaultTreeJointPositions ) );

        CartesianConstraint::Ptr cartesianConstraint;
        cartesianConstraint = boost::dynamic_pointer_cast<CartesianConstraint>( constraint );
        if( cartesianConstraint )
        {
            cartesianConstraint->posKp()  =  (Eigen::VectorXd::Ones(6) * defaultKp    ).head( constraint->dimensions() );
            cartesianConstraint->posKd()  =  (Eigen::VectorXd::Ones(6) * defaultKd    ).head( constraint->dimensions() );
            cartesianConstraint->velKp()  =  (Eigen::VectorXd::Ones(6) * defaultVelKp ).head( constraint->dimensions() );
            cartesianConstraint->velMax() =  (Eigen::VectorXd::Ones(6) * defaultVelMax).head( constraint->dimensions() );
        }
    }

    m_poseSolver->postureConstraint()->goalWeightVector()       = Eigen::VectorXd::Zero(6);
    m_poseSolver->postureConstraint()->goalWeightVector()(3)    = 0.01; // rot x
    node.getParam( "/drc/constraints/posture/goal_weight/rotx", m_poseSolver->postureConstraint()->goalWeightVector()(3) );

    m_poseSolver->postureConstraint()->goalWeightVector()(4)    = 0.01;  // rot y
    node.getParam( "/drc/constraints/posture/goal_weight/roty", m_poseSolver->postureConstraint()->goalWeightVector()(4) );

    m_poseSolver->postureConstraint()->goalWeightVector()(5)    = 0.0001; // rot z
    node.getParam( "/drc/constraints/posture/goal_weight/rotz", m_poseSolver->postureConstraint()->goalWeightVector()(5) );


    m_poseSolver->comConstraint()->    goalWeightVector()(0)    = 1;
    node.getParam( "/drc/constraints/com/goal_weight",   m_poseSolver->comConstraint()->goalWeightVector()(0) );
    node.getParam( "/drc/constraints/com/goal_weight/x", m_poseSolver->comConstraint()->goalWeightVector()(0) );

    m_poseSolver->comConstraint()->    goalWeightVector()(1)    = 1;
    node.getParam( "/drc/constraints/com/goal_weight",   m_poseSolver->comConstraint()->goalWeightVector()(1) );
    node.getParam( "/drc/constraints/com/goal_weight/y", m_poseSolver->comConstraint()->goalWeightVector()(1) );

    m_poseSolver->comConstraint()->    goalWeightVector()(2)    = 0.0001;
    node.getParam( "/drc/constraints/com/goal_weight",   m_poseSolver->comConstraint()->goalWeightVector()(2) );
    node.getParam( "/drc/constraints/com/goal_weight/z", m_poseSolver->comConstraint()->goalWeightVector()(2) );

    double comKp = 5;
    node.getParam( "/drc/constraints/com/kp", comKp );
    m_poseSolver->comConstraint()->posKp()    = Eigen::Vector3d::Ones() * comKp;

    double comKd = 1;
    node.getParam( "/drc/constraints/com/kd", comKd );
    m_poseSolver->comConstraint()->posKd()    = Eigen::Vector3d::Ones() * comKd;

    m_poseSolver->comConstraint()->goal().x() = +0.04;
    node.getParam( "/drc/constraints/com/goal/x", m_poseSolver->comConstraint()->goal().x() );


    m_defaultFrameGains.reset( new FrameGoalGains ); //FIXME:

    double defaultFrameKp     = 1;
    node.getParam( "/drc/constraints/default_frame/kp", defaultFrameKp );
    m_defaultFrameGains->posKp()  = Eigen::VectorXd::Ones(6) * defaultFrameKp;

    double defaultFrameKd     = defaultKp * 0.5;
    node.getParam( "/drc/constraints/default_frame/kd", defaultFrameKd );
    m_defaultFrameGains->posKd()  = Eigen::VectorXd::Ones(6) * defaultFrameKd;

    double defaultFrameVelKp  = 0;
    node.getParam( "/drc/constraints/default_frame/vel_kd", defaultFrameVelKp );
    m_defaultFrameGains->velKp()  = Eigen::VectorXd::Ones(6) * defaultFrameVelKp;

    double defaultFrameVelMax = 10;
    node.getParam( "d/rc/constraints/default_frame/vel_max", defaultFrameVelMax );
    m_defaultFrameGains->velMax() = Eigen::VectorXd::Ones(6) * defaultFrameVelMax;


    Eigen::VectorXd optimalAngles = m_atlasLookup->calcDefaultTreeJointAngles( 0.4 );
    //optimalAngles( m_atlasLookup->segmentNameToQnr("head") ) = 0.6; // commented out because person_urdf's head is locked at the moment


    m_defaultOptimalAngles = optimalAngles;
    m_poseSolver->optimalAngleConstraint()->setGoal( m_defaultOptimalAngles );
    int numAngles = m_poseSolver->optimalAngleConstraint()->dimensions();

    double defaultOptAngleKp = 0.5;
    node.getParam( "/drc/constraints/optimal_angle/kp", defaultOptAngleKp );
    m_poseSolver->optimalAngleConstraint()->posKp()             = Eigen::VectorXd::Ones( numAngles ) * defaultOptAngleKp;

    double defaultOptAngleWeight = 0.01;
    node.getParam( "/drc/constraints/optimal_angle/goal_weight",         defaultOptAngleWeight );
    node.getParam( "/drc/constraints/optimal_angle/goal_weight/default", defaultOptAngleWeight );
    m_poseSolver->optimalAngleConstraint()->goalWeightVector()  = Eigen::VectorXd::Ones( numAngles ) * defaultOptAngleWeight;

    typedef std::map<std::string, double> StringDoubleMap;
    StringDoubleMap jointWeightMap;

    node.getParam("/drc/constraints/optimal_angle/goal_weight", jointWeightMap );

    BOOST_FOREACH( StringDoubleMap::value_type & entry, jointWeightMap )
    {
        std::string const & jointName = entry.first;
        double const &      value     = entry.second;

        if( jointName == "default" )
            continue;

        int qNum = m_atlasLookup->jointNameToQnr( jointName );

        if( qNum > -1 && qNum < m_poseSolver->optimalAngleConstraint()->goalWeightVector().size() ) // ignore/reject bad lookup
        {
            m_poseSolver->optimalAngleConstraint()->goalWeightVector()( qNum ) = value;
            ROS_WARN( "setting goal_weight %s to %f", jointName.c_str(), value );
        }
        else
            ROS_WARN( "ignoring bad value: %s", jointName.c_str() );
    }

    // set head weight higher
//    m_poseSolver->optimalAngleConstraint()->goalWeightVector()( m_atlasLookup->segmentNameToQnr( m_atlasLookup->lookupString( "head" ) ) ) = 0.5;

//    m_poseSolver->optimalAngleConstraint()->goalWeightVector()( m_atlasLookup->jointNameToQnr( "back_bky" ) ) = 0.1;
//    m_poseSolver->optimalAngleConstraint()->goalWeightVector()( m_atlasLookup->jointNameToQnr( "back_bkx" ) ) = 0.1;

    m_vizDebugPublisher.reset( new re2::VisualDebugPublisher( "full_body_controler_viz_debug" ) );

    m_controlLoopThread = boost::thread( &FullBodyController::controlLoop, this );


    Eigen::VectorXd bdiCmdDefaults(m_atlasLookup->getNumCmdJoints());

    bdiCmdDefaults = m_atlasLookup->calcDefaultCmdJointAngles( 0.4 );

    m_bdiTreeDefaults = m_atlasLookup->cmdToTree( bdiCmdDefaults );
}


FullBodyController::
~FullBodyController()
{
    m_shutdown = true;
//    m_controlLoopThread.join();
}


void
FullBodyController::
setKEfforts( const std::vector<uint8_t> & kefforts )
{
    atlas_msgs::AtlasState::ConstPtr latestStateMsg;
    latestStateMsg = boost::atomic_load( &m_lastStateMsg );

    Eigen::VectorXd optimalAngles = m_defaultOptimalAngles;

    if( latestStateMsg )
        optimalAngles = m_atlasLookup->cmdToTree( parseJointPositions( latestStateMsg ) );

    int cmdJointIndex = 0;
    double portionSum = 0;
    BOOST_FOREACH( const uint8_t & intScale, kefforts )
    {
        int qnr = m_atlasLookup->jointIndexToQnr(cmdJointIndex);

        if( qnr >= 0 )
        {
            double portion = intScale/255.0;
            optimalAngles(qnr) =    (portion)   * optimalAngles(qnr)
                                 +  (1-portion) * m_bdiTreeDefaults(qnr);
            portionSum += portion;
        }
        ++cmdJointIndex;
    }
    ROS_WARN( "Adjusting Optimal angles in case of hand off to bdi" );
    setJointGoal( optimalAngles );

    double avgPortion     = portionSum/kefforts.size();
    double inversePortion = 1 - avgPortion;

    ros::TimerOptions jointConstraintTimerOptions;
    jointConstraintTimerOptions = ros::TimerOptions( ros::Duration(3 * avgPortion),
                                             boost::bind( &FullBodyController::handleJointConstraintTimerEvent, this, _1 ),
                                             &m_privateCallbackQueue,
                                             true, true  );
//    m_jointConstraintTimer = m_node.createTimer( jointConstraintTimerOptions );

    ros::TimerOptions kEffortTimerOptions;
    kEffortTimerOptions = ros::TimerOptions( ros::Duration(3 * inversePortion),
                                             boost::bind( &FullBodyController::handleKEffortsTimerEvent, this, _1, kefforts ),
                                             &m_privateCallbackQueue,
                                             true, true  );

    ROS_WARN( "Starting timer for K Effort set" );
    m_kEffortsTimer = m_node.createTimer( kEffortTimerOptions );
}




void
FullBodyController::
handleJointConstraintTimerEvent( const ros::TimerEvent & timerEvent )
{
    ROS_WARN( "Sending K Efforts to JointController" );
    Eigen::VectorXd optimalAngles = m_defaultOptimalAngles;
    setJointGoal( optimalAngles );
}

void
FullBodyController::
handleKEffortsTimerEvent( const ros::TimerEvent & timerEvent, std::vector<uint8_t> kefforts )
{
    ROS_WARN( "Sending K Efforts to JointController" );
    m_jointController->setKEfforts( kefforts );
}



std::string
FullBodyController::
getBaseFrameId()
{
    boost::shared_ptr<std::string> baseFrameId( new std::string );
    baseFrameId = boost::atomic_load( &m_baseFootFrameId );
    return *baseFrameId;
}


std::string
FullBodyController::
oppositeFootFrameId()
{
    if( *m_baseFootFrameId == m_atlasLookup->lookupString("r_foot") )
        return m_atlasLookup->lookupString("l_foot");
    else
    if( *m_baseFootFrameId == m_atlasLookup->lookupString("l_foot") )
        return m_atlasLookup->lookupString("r_foot");
    else
        throw std::runtime_error( "Unknown foot" );
}


void
FullBodyController::
setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg )
{
    boost::atomic_store( &m_lastStateMsg, boost::atomic_load( &latestStateMsg ) );
//    ROS_INFO_THROTTLE( 5, "Still receiving state messages" );
}



void
FullBodyController::
switchBaseFoot( const std::string & newBaseFootFrameId )
{
    boost::shared_ptr<std::string> newBaseFoot( new std::string );

    *newBaseFoot = newBaseFootFrameId;

    boost::atomic_store( &m_newBaseFoot, newBaseFoot );
    m_constraintsDirty = true;
}


void
FullBodyController::
setCenterOfMassGoal( const Eigen::Vector3d & comLocation, const Eigen::Vector3d & comWeight )
{
    Eigen::Vector6dPtr comGoal( new Eigen::Vector6d );

    comGoal->head(3) = comLocation;
    comGoal->tail(3) = comWeight;

    boost::atomic_store( &m_comGoal, comGoal );
    m_constraintsDirty = true;
}

void
FullBodyController::
setCenterOfMassGoalGains( const Eigen::Vector3d & posKp, const Eigen::Vector3d & posKd )
{
    ComGains::Ptr comGains( new ComGains );

//    *comGains = *defaultFrameGains();
    comGains->posKp() = posKp;
    comGains->posKd() = posKd;

    boost::atomic_store( &m_comGains, comGains );

    m_constraintsDirty = true;
}

//void
//FullBodyController::
//setOppositeFootGoal( const Eigen::Vector6d & frame, const Eigen::Vector6d & weights )
//{
//    assert( false );
//}

void
FullBodyController::
setOppositeFootGoal( const Eigen::Affine3d & frame, const Eigen::Vector6d & weights )
{
    FrameGoal::Ptr frameGoal( new FrameGoal );

    frameGoal->goalPose                = frame;
    frameGoal->weightMatrix.diagonal() = weights;

    boost::atomic_store( &m_oppFootGoal, frameGoal );
    m_constraintsDirty = true;
}

void
FullBodyController::
setLeftHandGoal(     const Eigen::Vector6d & frame, const Eigen::Vector6d & weights )
{
    assert( false );
}

void
FullBodyController::
setRightHandGoal(    const Eigen::Vector6d & frame, const Eigen::Vector6d & weights )
{
    assert( false );
}


void
FullBodyController::
setJointGoal(  const Eigen::VectorXd & jointAngles )
{
    JointGoal::Ptr jointGoal( new JointGoal );

    jointGoal->jointGoal = jointAngles;

    boost::atomic_store( &m_jointGoal, jointGoal );

    m_constraintsDirty = true;
}


void
FullBodyController::
setFrameGoal(  const FrameGoal::Ptr & frameGoal )
{
//    boost::lock_guard<boost::mutex> goalParamLock( m_goalParamMutex );
    ROS_INFO_STREAM_THROTTLE( 5, "Still receiving frame commands (Throttled)" );

    FrameGoalListPtr updateList;
    updateList = boost::atomic_exchange( &m_frameGoalUpdateList, updateList );


    if( updateList == NULL )
        updateList.reset( new FrameGoalList );

    updateList->push_back( frameGoal );

    boost::atomic_store( &m_frameGoalUpdateList, updateList );

    m_constraintsDirty = true;
}

void
FullBodyController::
setFrameGoalGains(  const FrameGoalGains::Ptr & frameGoalGains )
{
//    boost::lock_guard<boost::mutex> goalParamLock( m_goalParamMutex );
    FrameGoalGainsListPtr updateList;
    updateList = boost::atomic_exchange( &m_frameGoalGainsUpdateList, updateList );

    if( updateList == NULL )
        updateList.reset( new FrameGoalGainsList );

    updateList->push_back( frameGoalGains );

    boost::atomic_store( &m_frameGoalGainsUpdateList, updateList );

    m_constraintsDirty = true;
}


Eigen::VectorXd
projectJointPositions( const Eigen::VectorXd & jointPositions, const Eigen::VectorXd & jointVelocities, const ros::Duration & dt )
{
    Eigen::VectorXd projectedJointPositions;

    //  r  = r₀ + v₀t + a₀t²/2
    projectedJointPositions =   jointPositions
                              + jointVelocities * dt.toSec();
//                            + jointAccelerations * std::pow( dtSecs, 2 )/2;

    return projectedJointPositions;
}

//Eigen::VectorXd
//FullBodyController::
//projectJointVelocities( const ros::Time & time )
//{
////    double          dtSecs                 = (time - m_sampleTime).toSec();
//    Eigen::VectorXd projectedJointVelocities =   jointVelocities;
////                                             + jointAccelerations * dtSecs;
//    return projectedJointVelocities;
//}


ros::Time
FullBodyController::
getNextCommandTime( const ros::Time sampleTime )
{
    ROS_ERROR_THROTTLE( 4, "getNextCommandTime, not finished" );
    return ros::Time::now();
}


FrameConstraint::Ptr
FullBodyController::
findFrameConstraint( const std::string & name )
{
    BOOST_FOREACH( const SolverConstraint::Ptr & constraint,  m_poseSolver->constraints() )
    {
        FrameConstraint::Ptr frameConstraint;
        frameConstraint = boost::dynamic_pointer_cast<FrameConstraint>( constraint );

        if( frameConstraint
         && frameConstraint->tipElement()->segment.getName() == name )
        {
            return frameConstraint;
        }
    }

    return FrameConstraint::Ptr();
}



void
FullBodyController::
controlLoop()
{
    ros::NodeHandle                  node;
    boost::mutex                     mutex;
    boost::unique_lock<boost::mutex> lock( mutex );
//    Eigen::Affine3d                  defaultTransform = Eigen::Affine3d::Identity();
    Eigen::MatrixXd                  internalInputWeightMatrix;
    Eigen::VectorXd                  minInputValues     = m_atlasLookup->getTreeJointMinVector();
    Eigen::VectorXd                  maxInputValues     = m_atlasLookup->getTreeJointMaxVector();
    Eigen::VectorXd                  defaultJointAngles = m_atlasLookup->calcDefaultTreeJointAngles( 0.4 );
    atlas_msgs::AtlasCommand::Ptr    defaultAtlasCommand = m_atlasLookup->createDefaultJointCmdMsg( node );
    defaultAtlasCommand->k_effort = std::vector<uint8_t>( defaultAtlasCommand->k_effort.size(), 255 );
    Eigen::VectorXd                  prevPositionError;
    Eigen::VectorXd                  prevVelocityError;


    m_nextCommandTime = ros::Time::now();

    ros::Time lastCmdTime = ros::Time::now(); // for profiling
    ros::Time unlockTime  = ros::Time::now(); // for profiling
    ros::Time waitTime    = ros::Time::now(); // for profiling

    while( ros::ok() && !m_shutdown )
    {
        m_privateCallbackQueue.callAvailable();
        ros::Duration( std::fmod( ros::Time::now().toSec(), m_dt.toSec() ) ).sleep(); // sleep until next multiple of our period locked with time
        m_syncVar.wait( lock );

        if( ! updateParameters(ros::Time::now()) )
        {
            ROS_WARN( "FullBodyController update parameters failed" );
            continue;
        }

        publishDebug();

        Eigen::Affine3d comInBaseFoot = Eigen::Affine3d::Identity();
        comInBaseFoot.translation() = m_poseSolver->comConstraint()->getCurrentOutput( m_treeJointPositions );


        tf::Transform baseFootToCom;
        tf::transformEigenToTF( comInBaseFoot, baseFootToCom );
        m_tfBroadcaster.sendTransform( tf::StampedTransform( baseFootToCom,
                                                             ros::Time::now(),
                                                             m_poseSolver->comConstraint()->baseFrameId(),
                                                             std::string( "/com" ) ));

        atlas_msgs::AtlasCommand::Ptr msg;

        ROS_INFO_STREAM_THROTTLE( 10, "FullBodyController control loop still running (Throttled) iterations: " << m_iteration );

        if( m_jointControlType == POSITION_CONTROL )
        {
            ROS_INFO_STREAM_THROTTLE( 10, "Position Control Mode (throttled)" );
            Eigen::VectorXd positions = m_poseSolver->solvePose( m_treeJointPositions );

            if( positions.size() != 0 ) //m_commandType == POSITION_CMD )
            {
                msg = buildAtlasPositionCommand( m_atlasLookup->treeToCmd( positions ), defaultAtlasCommand );
                msg->kp_position = std::vector<float>(msg->kp_position.size(),  10*m_dt.toSec() );
            }
        }
        else
        if( m_jointControlType == VELOCITY_CONTROL )
        {
            ROS_INFO_STREAM_THROTTLE( 10, "Velocity Control Mode (throttled)" );

            Eigen::MatrixXd masterJacobian   = m_poseSolver->calcMasterJacobian( m_treeJointPositions, m_defaultTransform );
//            Eigen::VectorXd positionError    = m_desiredPosition - m_poseSolver->calcCurrentPose(     m_treeJointPositions, defaultTransform  );

            Eigen::VectorXd positionError   = Eigen::VectorXd(       m_poseSolver->goalDims() );
            Eigen::VectorXd velocityError   = Eigen::VectorXd::Zero( m_poseSolver->goalDims() );

            int atRow  = 0;
            int atDiag = 0;
            BOOST_FOREACH( SolverConstraint::Ptr constraint, m_poseSolver->constraints() )
            {
                int nextRow = atRow;
                nextRow = re2::insertRowsAt( positionError, constraint->getPositionError(m_treeJointPositions),                       atRow  );
                nextRow = re2::insertRowsAt( velocityError, constraint->getVelocityError(m_treeJointPositions,m_treeJointVelocities), atRow  );
                atRow   = nextRow;
            }

            if( prevPositionError.size() != positionError.size() ) // if the dimensions have changed, or first run
                prevPositionError = positionError;

            if( prevVelocityError.size() != velocityError.size() ) // if the dimensions have changed, or first run
                prevVelocityError = velocityError;

            Eigen::VectorXd positionErrorDeriv    =  (positionError - prevPositionError); // / m_dt.toSec();
            Eigen::VectorXd desiredOutputVelocity =  (positionError.array() * m_posKp.array()           )
                                                     -  (m_posKd.array()    * positionErrorDeriv.array());
//                                                     +  (m_velKp.array()       * velocityError.array()     );
            prevPositionError = positionError;

            double lambda = 0.1;
            Eigen::MatrixXd pinvMasterJacobian    = weightedPseudoInverse( masterJacobian, internalInputWeightMatrix, m_outputWeights, lambda );
            Eigen::VectorXd outputJointVelocities = pinvMasterJacobian * desiredOutputVelocity;
//            Eigen::VectorXd output                = masterJacobian * outputJointVelocities;
//           Eigen::VectorXd outputError           = output - desiredOutputVelocity;



            if( m_commandType == VELOCITY_CMD )
            {
                msg = buildAtlasVelocityCommand( m_atlasLookup->treeToCmd( outputJointVelocities ), defaultAtlasCommand );
//                if( outputJointVelocities.norm() < 0.3 )
                {
//                    std::stringstream debugOut;
//                    BOOST_FOREACH( SolverConstraint::Ptr constraint, m_poseSolver->constraints() )
//                    {
//                        if( constraint->dimensions() > 6 )
//                            continue;

//                        Eigen::VectorXd output = constraint->getCurrentOutput( m_treeJointPositions );
//                        Eigen::VectorXd goal   = constraint->goal();
//                        Eigen::VectorXd positionError = constraint->getPositionError(m_treeJointPositions);
//                        debugOut << std::fixed << std::setprecision(4)
//                                 << "\nConstraint:      " << constraint->name()
//                                 << "\nrow:   "           << constraint->startRow()
//                                 << "\nposKp: "           << constraint->posKp().transpose()
//                                 << "\nposKd: "           << constraint->posKd().transpose()
//                                 << "\nGoal weights:    " << constraint->goalWeightVector().transpose()
//                                 << "\nposition error:  " << positionError.transpose()
//                                 << "\n    error norm:  " << positionError.transpose().norm()
//                                 << "\n  weighed norm:  " << (constraint->goalWeightMatrix() * positionError).norm()
//                                 << "\n    output:      " << output.transpose()
//                                 << "\n    goal:        " << constraint->goal().transpose();
//
//                        CartesianConstraint::Ptr cartConstraint = boost::dynamic_pointer_cast<CartesianConstraint>( constraint );
//                        if( cartConstraint )
//                        {
//                            debugOut << "\n   base : " << cartConstraint->baseFrameId() ;
//                        }
//                        debugOut << "\n\n";
//                    }
//                    ROS_INFO_STREAM_THROTTLE( 2, "CONSTAINT INFO:" );
//                    ROS_INFO_STREAM_THROTTLE( 2, debugOut.str() );
//                    ROS_INFO_STREAM_THROTTLE( 2, "FREEZE INFO: " );
//                    debugOut.clear();
//
//                    Eigen::VectorXd internalInputWeightVector = internalInputWeightMatrix.diagonal();
//
//                    for( int i = 0; i < internalInputWeightVector.size(); ++i )
//                    {
//                        debugOut << m_atlasLookup->treeQnrToJointName(i) << " " << internalInputWeightVector(i) << "\n";
//                    }
//                    debugOut << "\n\n";
//                    ROS_INFO_STREAM_THROTTLE( 2, debugOut.str() );

                }
            }
            else
            if( m_commandType == POSITION_CMD )
            {
                Eigen::VectorXd positions = m_treeJointPositions + outputJointVelocities;
                msg = buildAtlasPositionCommand( m_atlasLookup->treeToCmd( positions ), defaultAtlasCommand );
            }

            Eigen::VectorXd predictedAngles = m_treeJointPositions + outputJointVelocities * m_dt.toSec();
            Eigen::VectorXd preIntrusions   = calcNormalizedIntrusion( minInputValues, maxInputValues, defaultJointAngles, m_treeJointPositions,  1 );
            Eigen::VectorXd intrusions      = calcNormalizedIntrusion( minInputValues, maxInputValues, defaultJointAngles, predictedAngles,       1 );
            Eigen::VectorXd intrusionDiff   = intrusions - preIntrusions;

            Eigen::VectorXd wrongDirectionIntrusions =  (intrusionDiff.array() >= 0).cast<double>()
                                                      * intrusions.array().min( Eigen::ArrayXd::Ones(intrusions.rows()) );

            internalInputWeightMatrix = (Eigen::VectorXd::Ones(outputJointVelocities.rows()) - wrongDirectionIntrusions).asDiagonal();

        }

        m_jointController->setJointCommand( msg, m_nextCommandTime );
        lastCmdTime = ros::Time::now();

        ++m_iteration;
    }
}




//FIXME this is in two places which it shouldn't be;
Eigen::Affine3d
FullBodyController::
lookupTransformFromTo( const std::string & fromFrame, const std::string & toFrame )
{
    tf::StampedTransform tfTransform;
    m_tfListener.waitForTransform( toFrame, fromFrame, ros::Time(0), ros::Duration(0) );
    m_tfListener.lookupTransform(  toFrame, fromFrame, ros::Time(0), tfTransform );

    Eigen::Matrix4d homoTransform = re2::convertTfToEigen4x4( tfTransform );

    return Eigen::Affine3d( homoTransform );
}




void
FullBodyController::
updateConstraints()
{
    JointGoal::Ptr jointGoal;
    jointGoal = boost::atomic_exchange( &m_jointGoal, jointGoal );
    if( jointGoal )
    {
        ROS_WARN( "Got joint goal" );
        m_poseSolver->optimalAngleConstraint()->setGoal( jointGoal->jointGoal );
    }

    Eigen::Vector6dPtr comGoal;
    comGoal = boost::atomic_exchange( &m_comGoal, comGoal );
    if( comGoal )
    {
        m_poseSolver->comConstraint()->setGoal(             comGoal->head(3) );
        m_poseSolver->comConstraint()->setGoalWeightVector( comGoal->tail(3) );
    }

    ComGains::Ptr comGains;
    comGains = boost::atomic_exchange( &m_comGains, comGains );
    if( comGains )
    {
        m_poseSolver->comConstraint()->posKp() = comGains->posKp();
        m_poseSolver->comConstraint()->posKd() = comGains->posKd();
    }


    FrameGoal::Ptr oppFootGoal;
    oppFootGoal = boost::atomic_exchange( &m_oppFootGoal, oppFootGoal );
    if( oppFootGoal )
    {
        m_poseSolver->oppFootConstraint()->setGoal( affineToTwist( oppFootGoal->goalPose ) );
        m_poseSolver->oppFootConstraint()->setGoalWeightMatrix( oppFootGoal->weightMatrix );
    }


    FrameGoalListPtr frameGoalUpdateList;
    frameGoalUpdateList = boost::atomic_exchange( &m_frameGoalUpdateList, frameGoalUpdateList );

    if( frameGoalUpdateList )
    {
        BOOST_FOREACH( const FrameGoal::Ptr & frameGoal, *frameGoalUpdateList )
        {
            FrameConstraint::Ptr frameConstraint;
            frameConstraint = findFrameConstraint( frameGoal->tipFrameId );

            if( ! frameConstraint )
            {
                if( frameGoal->baseFrameId.empty() )
                    frameGoal->baseFrameId = *m_baseFootFrameId;

                frameConstraint = m_poseSolver->addFrameConstraint( frameGoal->baseFrameId, frameGoal->tipFrameId );
                ROS_INFO_STREAM( "Added constraint : " << frameGoal->baseFrameId << " to " << frameGoal->tipFrameId );

                if( frameConstraint )
                {
                    frameConstraint->posKp()  = m_defaultFrameGains->posKp().head(  frameConstraint->dimensions() );
                    frameConstraint->posKd()  = m_defaultFrameGains->posKd().head(  frameConstraint->dimensions() );
                    frameConstraint->velKp()  = m_defaultFrameGains->velKp().head(  frameConstraint->dimensions() );
                    frameConstraint->velMax() = m_defaultFrameGains->velMax().head( frameConstraint->dimensions() );
                }
            }

            if( frameConstraint )
            {
                if( frameGoal->baseFrameId == "" )
                    frameGoal->baseFrameId = *m_baseFootFrameId;

//                frameConstraint->convertToNewBase( frameGoal->baseFrameId, m_treeJointPositions );

                // FIXME, need to set base frame in case it changed
                Eigen::Affine3d goalToConstraintBase;
                try
                {
                    goalToConstraintBase = m_poseSolver->getTransformFromTo( frameGoal->goalFrameId, frameGoal->baseFrameId, m_treeJointPositions ); //FIXME
                }
                catch( ... ) // FIXME this should be specific to the exception type we're throwing, but we're just using runtime error at the moment
                {
                    try
                    {
                        goalToConstraintBase = lookupTransformFromTo( frameGoal->goalFrameId, frameGoal->baseFrameId ) ;
//                        ROS_INFO( "Successfull lookup in tf tree" );
                    }
                    catch( ... )
                    {
                        ROS_ERROR_STREAM( "Couldn't update transfrom because we got an error trying to lookup transfrom from "
                                           << frameGoal->goalFrameId << " to " << frameGoal->baseFrameId );
                        continue;
                    }
                }

                Eigen::Affine3d goalInConstraintBase;
                goalInConstraintBase = goalToConstraintBase * frameGoal->goalPose;

                Eigen::Affine3d weightToBase = Eigen::Affine3d::Identity();
//                Eigen::Affine3d weightToBase( goalToConstraintBase.rotation() );
//                Eigen::Vector3d testVector(1,1,1);
//                if( (testVector.transpose() * weightToBase.linear() * testVector) <= 0 )
//                {
//                    ROS_ERROR_STREAM( "Flipping because non-positive definited detected" );
//                    weightToBase.linear() = weightToBase.linear() * -1;
//                }

                Eigen::MatrixXd weightMatrix = frameGoal->weightMatrix;
                weightMatrix.block(0,0,3,3) =  weightToBase.rotation() * weightMatrix.block(0,0,3,3); // FIXME Unsure if this will actually work
//                weightMatrix.block(0,0,3,3) =  weightMatrix.block(0,0,3,3) * goalToConstraintBase.rotation(); // FIXME Unsure if this will actually work
//                weightMatrix.block(3,3,3,3) = weightMatrix.block(3,3,3,3) * goalToConstraintBase.rotation() ; // FIXME Unsure if this will actually work

//                for( int i = 0; i < weightMatrix.size(); ++i )
//                {
//                    weightMatrix.array()(i) = std::abs( weightMatrix.array()(i) );
//                }

                frameConstraint->goalWeightMatrix() = weightMatrix;
                frameConstraint->setGoal( affineToTwist( goalInConstraintBase ) );

                bool   debug       = true;
                double debugPeriod = 0.1;
//                if( debug && (m_iteration % (int)(debugPeriod/m_dt.toSec())) == 0 )
                {
                    std::stringstream stringstream;
                    stringstream << goalToConstraintBase.translation();

//                    int hash = boost::hash<std::string>()(frameGoal->goalFrameId);
//                    Eigen::Vector4d color( 0,1,0,0.5 );
//                    m_vizDebugPublisher->publishPoint3( goalInConstraintBase.translation(), color, hash - 1 , frameGoal->baseFrameId );
//                    m_vizDebugPublisher->publishTextAt( stringstream.str(), 0.05, goalInConstraintBase.translation(), color, hash, frameGoal->baseFrameId );
//                    Eigen::Vector4d color1( 1,0,1,0.5 );
//                    m_vizDebugPublisher->publishPoint3( frameGoal->goalPose.translation(), color1, hash + 1, frameGoal->goalFrameId );
                }
            }
        }
    }

    FrameGoalGainsListPtr frameGoalGainsUpdateList;
    frameGoalGainsUpdateList = boost::atomic_exchange( &m_frameGoalGainsUpdateList, frameGoalGainsUpdateList );

    if( frameGoalGainsUpdateList )
    {
        BOOST_FOREACH( const FrameGoalGains::Ptr & frameGoalGains, *frameGoalGainsUpdateList )
        {
            FrameConstraint::Ptr frameConstraint;
            frameConstraint = findFrameConstraint( frameGoalGains->tipFrameId() );

            if( frameConstraint )
            {
                frameConstraint->posKp()  = frameGoalGains->posKp() ;
                frameConstraint->posKd()  = frameGoalGains->posKd() ;
                frameConstraint->velKp()  = frameGoalGains->velKp() ;
                frameConstraint->velMax() = frameGoalGains->velMax();
            }
        }
    }


    boost::shared_ptr<std::string> newBaseFoot;
    newBaseFoot = boost::atomic_exchange( &m_newBaseFoot, newBaseFoot );
    if( newBaseFoot && ( *newBaseFoot != *m_baseFootFrameId ) )
    {
        boost::atomic_store( &m_baseFootFrameId, newBaseFoot );

        if( m_atlasLookup->lookupString( *newBaseFoot ).find( "foot" ) != std::string::npos )
        {
            // set the new tip and allow the constraint to be updated automatically below
            m_poseSolver->oppFootConstraint()->setTipElement( oppositeFootFrameId() );
            m_poseSolver->oppFootConstraint()->goal() = affineToTwist( Eigen::Affine3d::Identity() );
        }

        BOOST_FOREACH( SolverConstraint::Ptr genericConstraint, m_poseSolver->constraints() )
        {
            CartesianConstraint::Ptr constraint;
            constraint = boost::dynamic_pointer_cast<CartesianConstraint>( genericConstraint );

            if( ! constraint )
                continue;

            constraint->convertToNewBase( *newBaseFoot, m_treeJointPositions );
        }
    }
}


bool
FullBodyController::
updateParameters( const ros::Time & nextCommandTime )
{
    atlas_msgs::AtlasState::ConstPtr lastStateMsg;
    lastStateMsg = boost::atomic_load( &m_lastStateMsg );


    if( !lastStateMsg )
    {
        ROS_WARN_STREAM_THROTTLE( 5, "update call though no lastStateMsg" );
        return false;
    }


    Eigen::VectorXd jointPositions  = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );
    Eigen::VectorXd jointVelocities = m_atlasLookup->cmdToTree( parseJointVelocities( lastStateMsg ) );
    ros::Time       sampleTime      = lastStateMsg->header.stamp;

//    m_nextCommandTime = getNextCommandTime( sampleTime );

    if( (nextCommandTime - sampleTime).toSec() > 5 )
    {
        ROS_WARN_STREAM( "sample to next command difference too large: " << (nextCommandTime - sampleTime)  );
        ROS_WARN_STREAM( "last state message time: " << sampleTime );
        ROS_WARN_STREAM( "nextCommandTime: "         << nextCommandTime );
        return false;
    }

    m_treeJointPositions   = jointPositions; //projectJointPositions( jointPositions, jointVelocities, nextCommandTime - sampleTime );
    m_treeJointVelocities  = jointVelocities;


//    boost::unique_lock<boost::mutex> goalParamLock( m_goalParamMutex, boost::defer_lock );
    if( m_constraintsDirty ) //FIXME, this will need to be changed if goal changes when not dirty // && goalParamLock.try_lock() )
    {
        updateConstraints();

        m_constraintsDirty = false;
//        goalParamLock.unlock(); //Manually unlock to reduce the mutex lock time

        m_outputWeights   = Eigen::VectorXd::Ones( m_poseSolver->goalDims() ).asDiagonal();
        m_posKp           = Eigen::VectorXd::Ones( m_poseSolver->goalDims() );
        m_posKd           = Eigen::VectorXd::Ones( m_poseSolver->goalDims() );
        m_velKp           = Eigen::VectorXd::Ones( m_poseSolver->goalDims() );
        m_maxVelocity     = Eigen::VectorXd::Ones( m_poseSolver->goalDims() );


        int atRow  = 0;
        int atDiag = 0;
        BOOST_FOREACH( SolverConstraint::Ptr constraint, m_poseSolver->constraints() )
        {
            int nextRow = atRow;
            nextRow = re2::insertRowsAt(  m_posKp,           constraint->posKp(),            atRow  );
            nextRow = re2::insertRowsAt(  m_posKd,           constraint->posKd(),            atRow  );
            nextRow = re2::insertRowsAt(  m_velKp,           constraint->velKp(),            atRow  );
            nextRow = re2::insertRowsAt(  m_maxVelocity,     constraint->velMax(),           atRow  );
            atDiag  = re2::insertBlockAt( m_outputWeights,   constraint->goalWeightMatrix(), atDiag );

            atRow = nextRow;
        }
    }
//    else
//    if( m_constraintsDirty )
//    {
//        ROS_WARN( "Try lock failed in updateParameters, will try again next cycle. (Shouldn't be a problem unless it's happening all the time)" );
//    }

    return true;
}

visualization_msgs::Marker
FullBodyController::
publishOppFootInBase()
{

    Eigen::Vector3d oppFootPosition = Eigen::Vector3d::Zero();

    visualization_msgs::Marker oppFootInBase;

    oppFootInBase.header.stamp = ros::Time::now();
    // FIXME change this
    oppFootInBase.header.frame_id = oppositeFootFrameId();
    oppFootInBase.type = visualization_msgs::Marker::SPHERE;
    oppFootInBase.pose.position.x = oppFootPosition(0);
    oppFootInBase.pose.position.y = oppFootPosition(1);
    oppFootInBase.pose.position.z = oppFootPosition(2);
    //oppFootInBase.pose.orientation.
        //tf::quaternionTFToMsg(tf_to_support_.getRotation(), com_marker.pose.orientation);
    oppFootInBase.scale.x = 0.03;
    oppFootInBase.scale.y = 0.03;
    oppFootInBase.scale.z = 0.005;
    oppFootInBase.color.a = 1.0;
    oppFootInBase.color.g = 1.0;
    oppFootInBase.color.r = 0.0;
    oppFootInBase.color.b = 0.0;

    return oppFootInBase;
}


void
FullBodyController::
publishDebug()
{
    bool debug = true;
    double debugPeriod = 0.1;
    if( debug && (m_iteration % (int)(debugPeriod/m_dt.toSec())) == 0 )
    {

        BOOST_FOREACH( SolverConstraint::Ptr genericConstraint, m_poseSolver->constraints() )
        {
            CartesianConstraint::Ptr constraint;
            constraint = boost::dynamic_pointer_cast<CartesianConstraint>( genericConstraint );

            if( ! constraint )
                continue;

            int             hash;
            Eigen::Vector4d color;
            std::string     frameName = constraint->baseFrameId() ; // m_baseFoot;

            hash  = boost::hash<std::string>()( constraint->name() + "goal" ) + constraint->startRow();
            color = colorFromHash( hash );
            color(3) = constraint->goalWeightMatrix().norm() / Eigen::Vector3d(1,1,1).norm(); // set alpha to normalized norm of the weight matrix
            m_vizDebugPublisher->publishPoint3( constraint->goal().head<3>(), color, hash, frameName );

            hash  = boost::hash<std::string>()( constraint->name() + "current" ) + constraint->startRow();
            color = colorFromHash( hash );
            color(3) = constraint->goalWeightMatrix().norm() / Eigen::Vector3d(1,1,1).norm(); // set alpha to normalized norm of the weight matrix
            m_vizDebugPublisher->publishPoint3( constraint->getCurrentOutput(m_treeJointPositions).head<3>(), color, hash, frameName );

            hash  = boost::hash<std::string>()( constraint->name() + "segment" ) + constraint->startRow();
            color = colorFromHash( hash );
            color(3) = constraint->goalWeightMatrix().norm() / Eigen::Vector3d(1,1,1).norm(); // set alpha to normalized norm of the weight matrix
            m_vizDebugPublisher->publishSegment( constraint->goal().head<3>(),
                                                 constraint->getCurrentOutput(m_treeJointPositions).head<3>(),
                                                 color, hash, frameName );
            m_vizDebugPublisher->publishTextAt( constraint->name(), 0.03, constraint->goal().head<3>(), color, hash + 1, frameName );


            std::ostringstream ss;
            ss << std::fixed   << std::setprecision(4);

            Eigen::Vector3d xyzDiff = constraint->getCurrentOutput(m_treeJointPositions).head(3) - constraint->goal().head<3>();
            ss << "xyz norm: " << xyzDiff.norm();
            m_vizDebugPublisher->publishTextAt( ss.str(),
                                                0.03,
                                                constraint->goal().head<3>() - Eigen::Vector3d(0,0,0.021),
                                                color, hash + 2, frameName );
            ss.str("");
            ss << "xyz weights: " << constraint->goalWeightVector().head(3).transpose();
            m_vizDebugPublisher->publishTextAt( ss.str(),
                                                0.03,
                                                constraint->goal().head<3>() - Eigen::Vector3d(0,0,0.042),
                                                color, hash + 3, frameName );

            if( constraint->goal().size() > 3 )
            {
                ss.str("");
                Eigen::Vector3d rpyDiff = constraint->getCurrentOutput(m_treeJointPositions).tail(3) - constraint->goal().tail<3>();
                ss << "rpy norm: " << rpyDiff.norm();
                m_vizDebugPublisher->publishTextAt( ss.str(),
                                                    0.03,
                                                    constraint->goal().head<3>() - Eigen::Vector3d(0,0,0.063),
                                                    color, hash + 4, frameName );

                ss.str("");
                ss << "rpy weights: " <<  constraint->goalWeightVector().tail(3).transpose();
                m_vizDebugPublisher->publishTextAt( ss.str(),
                                                    0.03,
                                                    constraint->goal().head<3>() - Eigen::Vector3d(0,0,0.084),
                                                    color, hash + 6, frameName );
            }
        }
    }
}




//        ROS_INFO_STREAM( "wait delay: "             << unlockTime - waitTime );
//        ROS_INFO_STREAM( "Process delay: "          << ros::Time::now() - unlockTime );
//        ROS_INFO_STREAM( "Delay between commands: " << ros::Time::now() - lastCmdTime );

//        if( !re2::is_finite( outputJointVelocities )
//            || (outputJointVelocities.array().abs() > 10).any())
//        {
//            ROS_ERROR_STREAM( "output joint velocities blew up!" << outputJointVelocities );
//            ROS_ERROR_STREAM( "output goalspace velocities "     << desiredOutputVelocity );
//            ROS_ERROR_STREAM( "positionError "                   << positionError );
//            ROS_ERROR_STREAM( "m_desiredPosition "               << m_desiredPosition );
//            ROS_ERROR_STREAM( "m_treeJointPositions "            << m_treeJointPositions );
//            continue;
////            ROS_ASSERT_MSG( false, "exiting" );
//        }

//        ROS_WARN_STREAM( "desiredPosition: \n"             << m_desiredPosition );
//        ROS_WARN_STREAM( "Position error: \n"              << positionError );
//        ROS_WARN_STREAM( "desiredOutputVelocity error: \n" << desiredOutputVelocity );
//        ROS_WARN_STREAM( "outputJointVelocities \n: "      << outputJointVelocities  );
//        ROS_WARN_STREAM( "m_outputWeights: \n"             << m_outputWeights  );
//        ROS_WARN_STREAM( "m_inputWeights: \n"              << m_inputWeights  );
//        ROS_WARN_STREAM( "masterJacobian: \n"              << masterJacobian );
//        ROS_WARN_STREAM( "pinvMasterJacobian: \n"          << pinvMasterJacobian  );
//        ROS_WARN_STREAM( "pinvMasterJacobian: \n"          << pinvMasterJacobian  );
//        ROS_WARN_STREAM( "output: \n"                      << output       );
//        ROS_WARN_STREAM( "outputError: \n"                 << outputError  );
//        ROS_WARN_STREAM( "m_treeJointPositions: \n"        << m_treeJointPositions  );

//        void createVelocityLimits()
//        {
//            Eigen::VectorXd distToLimit             =   (posDirJoints * (jointMaxs - jointPositions))
//                                                      + (negDirJoints * (jointMins - jointPositions));
//            Eigen::VectorXd maxJointAcceleration    =   (posDirJoints * getMaxJointPosAccel())
//                                                      + (negDirJoints * getMaxJointNegAccel());
//            // utf-8 characters follow
//            //  θ  = θ₀ + ω₀t + 1/2α₀t²
//            //  Δθ = ω₀t + 1/2α₀t²
//            //  ω  = ω₀ + α₀t
//            //  0  = ω₀ + α₀t
//            //  -ω₀ = α₀t
//            //  -ω₀/α₀ = t
//            //  Δθ =  ω₀(-ω₀/α₀) + 1/2α₀(-ω₀/α₀)²
//            //  Δθ = -ω₀²/α₀     + ω₀²/2α₀
//            //  Δθ = -ω₀²/2α₀
//            //  2α₀Δθ = -ω₀²
//            //  √(-Δθ2α₀) = ω₀
//            Eigen::VectorXd minDesiredJointVelocity = (-2 * maxJointAcceleration * distToLimit).array().sqrt();
//        }
//
//ROS_WARN_STREAM( "goalFrame id:\n"      << frameGoal->goalFrameId );
//ROS_WARN_STREAM( "baseFrame id:\n"      << frameGoal->baseFrameId );
//ROS_WARN_STREAM( "goalInGoalFrame:\n"      << frameGoal->goalPose.matrix() );
//ROS_WARN_STREAM( "goalInConstraintBase (but incorrectly calculated):\n" << goalInConstraintBase.matrix() );
//ROS_WARN_STREAM( "goalInConstraintBase (how I think it should be don):\n" << goalInConstraintBaseAlt.matrix() );
//ROS_WARN_STREAM( "goalToConstraintBase:\n" << goalToConstraintBase.matrix() );
//
//Eigen::MatrixXd weightMatrix = frameGoal->weightMatrix;
////                weightMatrix.block(0,0,3,3) = weightMatrix.block(0,0,3,3) * goalToConstraintBase.rotation(); // FIXME Unsure if this will actually work
////                weightMatrix.block(3,3,3,3) = weightMatrix.block(3,3,3,3) * goalToConstraintBase.rotation() ; // FIXME Unsure if this will actually work
//
//frameConstraint->goalWeightMatrix() = weightMatrix;
//frameConstraint->setGoal( affineToTwist( goalInConstraintBase ) );
//ROS_WARN_STREAM( "Goal:\n" << affineToTwist( goalInConstraintBase ).transpose() << "n" );
//ROS_WARN_STREAM( "Current output:\n" << frameConstraint->getCurrentOutput( m_treeJointPositions ) << "n" );
//ROS_WARN_STREAM( "Current output diff:\n" <<  affineToTwist( goalInConstraintBase ) - frameConstraint->getCurrentOutput( m_treeJointPositions ) << "n" );
//


}
}



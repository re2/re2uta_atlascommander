/*
 * CrawlCommander.cpp
 *
 *  Created on: Jun 17, 2013
 *      Author: Ghassan Atmeh & Adrian Rodriguez
 */


#include <walk_msgs/GetPath.h>
#include <re2uta/AtlasCommander/CrawlCommander.hpp>
#include <re2uta/AtlasCommander/FullBodyControllerInterface.hpp>
#include <re2uta/walking/WalkPatternGenerator.hpp>
#include <re2uta/walking/StepTrajectoryGenerator.hpp>
#include <re2uta/AtlasCommander/JointController.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <boost/shared_ptr.hpp>
#include <string>
#include <sandia_hand_msgs/SimpleGraspSrv.h>


namespace
{
    template <typename Type>
    Type exponentialWeightedAverage( Type oldVal, Type newVal, double newWeight )
    {
        return oldVal*(1-newWeight) + newWeight*(newVal);
    }

    template <typename Type>
    Type lowPassFilter( Type oldVal, Type newVal, double newWeight )
    {
        return exponentialWeightedAverage( oldVal, newVal, newWeight );
    }
}


namespace re2uta
{
namespace atlascommander
{

CrawlCommander::
CrawlCommander( const FullBodyControllerInterface::Ptr & fullBodyController,
                const urdf::Model & model,
                const AtlasLookup::Ptr & atlasLookup,
                const ros::Duration & dt,
                StepPlannerType       stepPlannerType )
    : m_atlasLookup( atlasLookup )
{
    m_fullBodyController = fullBodyController;
    m_solver.reset( new FullBodyPoseSolver( model, m_atlasLookup->lookupString( "l_foot" ),
                                                   m_atlasLookup->lookupString( "r_foot" ),
                                                   m_atlasLookup->lookupString( "utorso" ) ) );

    m_halfStepSize    = 0.2;
    m_dt              = dt;
    m_shutdown        = false;
    m_stepPlannerType = stepPlannerType;

    m_forceThresh = 50;

    m_controlLoopThread = boost::thread( &CrawlCommander::commandLoop, this );

    m_leftGripperSrv    = m_node.serviceClient<sandia_hand_msgs::SimpleGraspSrv>("/sandia_hands/l_hand/simple_grasp", true);
    m_rightGripperSrv   = m_node.serviceClient<sandia_hand_msgs::SimpleGraspSrv>("/sandia_hands/r_hand/simple_grasp", true);



}

CrawlCommander::
~CrawlCommander()
{
    m_shutdown = true;
//    m_controlLoopThread.join();
}



void
CrawlCommander::
setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg )
{
//    ROS_INFO_STREAM_THROTTLE( 5, "Setting local state" );
    boost::atomic_store( &m_lastStateMsg, latestStateMsg );
}



void
CrawlCommander::
walkTo( const WalkGoal::Ptr & walkGoal )
{
    //FIXME we're not handling the frame id!
    setDest( walkGoal->goalPose );
}

void
CrawlCommander::
setStepPlannerType( StepPlannerType stepPlannerType )
{
    ROS_ERROR( "CrawlCommander Currently ignoring setStepPlannerType" );
    m_stepPlannerType = stepPlannerType;
}


void
CrawlCommander::
shutdown()
{
    m_shutdown = true;
}





void
CrawlCommander::
setDest( const Eigen::Affine3d & dest )
{
    Eigen::Affine3dPtr destPtr( new Eigen::Affine3d );
    *destPtr = dest;

    boost::atomic_store( &m_walkDestCmd, destPtr );
}



CrawlPose::Ptr
CrawlCommander::
moveCrawlPoseToNewOdom( const CrawlPose::Ptr & currentCrawlPose )
{
    CrawlPose::Ptr newCrawlPose( new CrawlPose( *currentCrawlPose ) );

/*
    Eigen::Affine3d odomInBaseLimb;
    odomInBaseLimb.translation() = (currentCrawlPose->baseLimbPose().translation() + currentCrawlPose->swingLimbPose().translation())/2;

    Eigen::Quaterniond baseQuat(  currentCrawlPose->baseLimbPose().rotation()  );
    Eigen::Quaterniond swingQuat( currentCrawlPose->swingLimbPose().rotation() );

    odomInBaseLimb.linear() = baseQuat.slerp( 0.5, swingQuat ).toRotationMatrix();

    newCrawlPose->baseLimbPose()  = odomInBaseLimb.inverse() * currentCrawlPose->baseLimbPose() ;
    newCrawlPose->swingLimbPose() = odomInBaseLimb.inverse() * currentCrawlPose->swingLimbPose();
    newCrawlPose->comPose         = odomInBaseLimb.inverse() * currentCrawlPose->comPose        ;
*/

    return newCrawlPose;
}



CrawlPose::Ptr
CrawlCommander::
getCurrentPosesInBase( Limb baseLimb, const atlas_msgs::AtlasStateConstPtr & stateMsg )
{
    Eigen::VectorXd jointPositions   = m_atlasLookup->cmdToTree( parseJointPositions(  stateMsg ) );
    std::string     baseLimbFrameId  = limbToFrameId( baseLimb, m_atlasLookup );
    std::string     swingLimbFrameIdOne;
    std::string     swingLimbFrameIdTwo;
    std::string     swingLimbFrameIdThree;

    std::string lFootStr = m_atlasLookup->lookupString( "l_foot" );
    std::string rFootStr = m_atlasLookup->lookupString( "r_foot" );
    std::string lHandStr = m_atlasLookup->lookupString( "l_hand" );
    std::string rHandStr = m_atlasLookup->lookupString( "r_hand" );

    if (baseLimbFrameId == lFootStr)
    {
        // FIXME:: the second foot is actually not moving we may need to rename
        swingLimbFrameIdOne   = rFootStr;
        swingLimbFrameIdTwo   = lFootStr;
        swingLimbFrameIdThree = rHandStr;

    }
    else
    {
        // FIXME:: the second foot is actually not moving we may need to rename
        swingLimbFrameIdOne   = rHandStr;
        swingLimbFrameIdTwo   = lFootStr;
        swingLimbFrameIdThree = rFootStr;
    }



    CrawlPose::Ptr currentCrawlPose( new CrawlPose( baseLimb ) );

    currentCrawlPose->baseLimbPose()       = Eigen::Affine3d::Identity();
    currentCrawlPose->swingLimbPoseOne()   = m_solver->getPose(         baseLimbFrameId, swingLimbFrameIdOne,   jointPositions );
    currentCrawlPose->swingLimbPoseTwo()   = m_solver->getPose(         baseLimbFrameId, swingLimbFrameIdTwo,   jointPositions );
    currentCrawlPose->swingLimbPoseThree() = m_solver->getPose(         baseLimbFrameId, swingLimbFrameIdThree, jointPositions );
    currentCrawlPose->comPose              = m_solver->getCenterOfMass( baseLimbFrameId,                        jointPositions );

    return currentCrawlPose;
}



void
CrawlCommander::
sleepWithTfPublish( const std::string & baseFrameId, const Eigen::Affine3d & frameToOdom, double seconds )
{
    ros::Duration dt( 0.02 );
    ros::Time timeout = ros::Time::now() + ros::Duration( seconds );
    while( ros::Time::now() < timeout )
    {
        publishInertialFrameToOdom( baseFrameId, frameToOdom );
        dt.sleep();
    }
}


Eigen::Affine3d
CrawlCommander::
getImuToInertial()
{
    atlas_msgs::AtlasState::ConstPtr lastStateMsg;
    lastStateMsg  = boost::atomic_load( &m_lastStateMsg );

    Eigen::Affine3d    imuToInertial;
    Eigen::Quaterniond imuToInertialRot;

    imuToInertialRot.w() = lastStateMsg->orientation.w; //FIXME need to check if this needs to be inverted
    imuToInertialRot.x() = lastStateMsg->orientation.x;
    imuToInertialRot.y() = lastStateMsg->orientation.y;
    imuToInertialRot.z() = lastStateMsg->orientation.z;

    imuToInertial = imuToInertialRot.toRotationMatrix();

    return imuToInertial;
}


Eigen::Affine3d
CrawlCommander::
getFrameToInertial( const std::string & frameId )
{
    Eigen::Affine3d inertialToImu = getImuToInertial().inverse();

    atlas_msgs::AtlasState::ConstPtr lastStateMsg = boost::atomic_load( &m_lastStateMsg );
    Eigen::VectorXd treeJointPositions  = m_atlasLookup->cmdToTree( parseJointPositions( lastStateMsg ) );

    Eigen::Affine3d imuLinkToFrame   = m_solver->getPose( frameId, "imu_link", treeJointPositions ).inverse(); //NOTE: INVERSE!
    Eigen::Affine3d inertialToFrame( Eigen::Affine3d::Identity() * (inertialToImu * imuLinkToFrame).rotation() );

    return inertialToFrame.inverse(); // inertialInFrame is eq to frameToInertial
}


void
CrawlCommander::
publishInertialFrameToOdom( const std::string & frameId,  const Eigen::Affine3d & frameToOdom )
{
    atlas_msgs::AtlasState::ConstPtr lastStateMsg;
    lastStateMsg  = boost::atomic_load( &m_lastStateMsg );

    Eigen::VectorXd treeJointPositions;
    treeJointPositions = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );

    Eigen::Affine3d imuToFrame = m_solver->getPose( frameId, "imu_link", treeJointPositions );

    Eigen::Affine3d    imuToInertial;
    Eigen::Quaterniond imuToInertialQuat;

    imuToInertialQuat.w() = lastStateMsg->orientation.w;
    imuToInertialQuat.x() = lastStateMsg->orientation.x;
    imuToInertialQuat.y() = lastStateMsg->orientation.y;
    imuToInertialQuat.z() = lastStateMsg->orientation.z;

    Eigen::Affine3d inertialToImu( imuToInertialQuat.inverse() );
    Eigen::Affine3d inertialToFrame( (imuToFrame * inertialToImu).rotation() );

    Eigen::Affine3d inertialToOdom = frameToOdom  * inertialToFrame;

    tf::Transform inertialToFrameTf; // intertial to foot
    tf::transformEigenToTF( inertialToFrame, inertialToFrameTf );
    m_tfBroadcaster.sendTransform( tf::StampedTransform( inertialToFrameTf,
                                                         ros::Time::now(),
                                                         frameId,
                                                         "odom"  ));
}





void
CrawlCommander::
commandLoop()
{
    atlas_msgs::AtlasState::ConstPtr lastStateMsg;
    Eigen::VectorXd                  jointPositions;
    ros::NodeHandle                  node;

    while( !(lastStateMsg = boost::atomic_load( &m_lastStateMsg ))  && ros::ok() && !m_shutdown )
    {
        ROS_WARN_STREAM_THROTTLE( 2, "No atlas state available, waiting to start CrawlCommander command loop until seen" );
        m_dt.sleep();
    }

    int iteration = 0;
    while( ros::ok() && !m_shutdown )
    {
        Eigen::Affine3dPtr walkDest;
        walkDest      = boost::atomic_load( &m_walkDestCmd ); //, walkDest ); // represents transform between start and dest between feet
        lastStateMsg  = boost::atomic_load( &m_lastStateMsg );

        if( !lastStateMsg )
        {
            m_dt.sleep();
            ROS_WARN_THROTTLE( 2, "NO STATE MESSAGE!" );
            continue;
        }

        std::string     baseFrameId        = m_atlasLookup->lookupString( "r_foot" );
        Eigen::VectorXd treeJointPositions = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );
        m_fullBodyController->switchBaseFoot( baseFrameId );

        publishInertialFrameToOdom( baseFrameId, Eigen::Affine3d::Identity() );

        if( !walkDest )
        {
            m_dt.sleep();
            continue;
        }

        ROS_WARN( "MainLoop iteration : %i", iteration );

        if( iteration == 0 )  getToGround();
        if( iteration == 1 )  orientOnGround();
        if( iteration == 2 )  sitUp();
//        if( iteration == 3 )  sitUp();
//        if( iteration == 4 )  sitUp();
//        if( iteration == 5 )  sitUp();

        while( iteration > 2 )
        {
            if( isForwardBackward( walkDest ) )
            {
                scootForwardBackward( walkDest );
            }
            else
            if( isToSide( walkDest ) )
            {
                turn( walkDest );
            }
        }

        ++iteration;
        boost::atomic_store( &m_walkDestCmd, Eigen::Affine3dPtr() );
    }
}



/////////////////////////////////////////////////////////////////////////////////////////////////////



void
CrawlCommander::
getToGround()
{
    Eigen::Affine3dPtr               walkDest;
    atlas_msgs::AtlasState::ConstPtr lastStateMsg;

    std::string                      baseFrameId;
    Eigen::VectorXd                  treeJointPositions;

    int iteration = 0;
    while( iteration < 1 )
    {
        walkDest            = boost::atomic_load( &m_walkDestCmd );
        lastStateMsg        = boost::atomic_load( &m_lastStateMsg );

        baseFrameId         = m_atlasLookup->lookupString( "r_foot" );
        treeJointPositions  = m_atlasLookup->cmdToTree( parseJointPositions( lastStateMsg ) );

        publishInertialFrameToOdom( baseFrameId, Eigen::Affine3d::Identity() );

        if( !walkDest )
        {
            m_dt.sleep();
            continue;
        }

        ROS_WARN( "getToGround iteration : %i", iteration );
        if( iteration == 0 )
        {
            unconstrainPosture( baseFrameId,  treeJointPositions );

            sleepWithTfPublish( baseFrameId, Eigen::Affine3d::Identity(), 3 );

            handX( baseFrameId,  treeJointPositions );

            sleepWithTfPublish( baseFrameId, Eigen::Affine3d::Identity(), 3 );

            dropPelvis( baseFrameId,  treeJointPositions );

            sleepWithTfPublish( baseFrameId, Eigen::Affine3d::Identity(), 3 );

            changeComInSquat( baseFrameId );
        }


        ++iteration;
        boost::atomic_store( &m_walkDestCmd, Eigen::Affine3dPtr() );
    }
}




void
CrawlCommander::
unconstrainPosture( const std::string & baseFrameId, const Eigen::VectorXd & treeJointPositions   )
{

    Eigen::Affine3d pelvisPose;
    pelvisPose = m_solver->getPose( baseFrameId,  m_atlasLookup->lookupString( "utorso" ), treeJointPositions );

    FrameGoal::Ptr frameGoal;
    frameGoal.reset( new FrameGoal );

    Eigen::VectorXd pelvisWeights;
    pelvisWeights           = Eigen::VectorXd::Zero( 6 );
    pelvisWeights.tail(3)   = Eigen::Vector3d( 0.001, 0.001, 0.0001 );

    frameGoal->baseFrameId  = baseFrameId;
    frameGoal->goalFrameId  = baseFrameId;
    frameGoal->tipFrameId   = m_atlasLookup->lookupString( "utorso" );
    frameGoal->goalPose     = pelvisPose;
    frameGoal->weightMatrix = pelvisWeights.asDiagonal();

//    FrameGoalGains::Ptr frameGoalGains;
//    frameGoalGains.reset( new FrameGoalGains );
//    frameGoalGains->tipFrameId()  = "utorso";
//    *frameGoalGains = *m_fullBodyController->defaultFrameGains();
//    frameGoalGains->posKp().head(3) = Eigen::VectorXd::Ones(3) * 1;

    m_fullBodyController->setFrameGoal( frameGoal );

//    m_fullBodyController->set( baseFrameId );
}


void
CrawlCommander::
limbsDownFromPelvis( const std::string & baseFrameId, const Eigen::VectorXd & treeJointPositions  )
{
    std::vector<std::string> names;
    names.push_back( m_atlasLookup->lookupString( "l_hand" ) );
    names.push_back( m_atlasLookup->lookupString( "r_hand" ) );
    names.push_back( m_atlasLookup->lookupString( "l_foot" ) );
    names.push_back( m_atlasLookup->lookupString( "r_foot" ) );

    ROS_WARN( "Hand X iteration" );

    Eigen::Vector3dVector tipAbsMotion;

    tipAbsMotion.push_back( Eigen::Vector3d( -0.02, 0, 0 ) );  // l_hand
    tipAbsMotion.push_back( Eigen::Vector3d( -0.02, 0, 0 ) );  // r_hand
    tipAbsMotion.push_back( Eigen::Vector3d(  0.02, 0, 0 ) );  // l_foot
    tipAbsMotion.push_back( Eigen::Vector3d( -0.02, 0, 0 ) );  // r_foot

    int tipIndex = 0;

    BOOST_FOREACH( const std::string & tipName, names )
    {
        FrameGoalGains::Ptr frameGoalGains;
        Eigen::Affine3d limbPose;
        Eigen::VectorXd handWeights;

        limbPose = m_solver->getPose( baseFrameId,  tipName, treeJointPositions );
        limbPose.translation().x() = tipAbsMotion[ tipIndex ].x();

        FrameGoal::Ptr frameGoal;
        frameGoal.reset( new FrameGoal );

        handWeights = Eigen::VectorXd::Zero( 6 );
        handWeights.head(3) = Eigen::Vector3d( 0.1, 0.2, 0.0 );
        handWeights.tail(3) = Eigen::Vector3d( 0.0, 0.0, 0.0 );

        frameGoal->baseFrameId  = baseFrameId;
        frameGoal->goalFrameId  = baseFrameId;
        frameGoal->tipFrameId   = tipName;
        frameGoal->goalPose     = limbPose;
        frameGoal->weightMatrix = handWeights.asDiagonal();

        frameGoalGains.reset( new FrameGoalGains );
        frameGoalGains->tipFrameId()  = tipName;
        *frameGoalGains = *m_fullBodyController->defaultFrameGains();
        frameGoalGains->posKp().head(3) = Eigen::VectorXd::Ones(3) * 1;

        m_fullBodyController->setFrameGoal( frameGoal );
        m_fullBodyController->setFrameGoalGains( frameGoalGains );

        ++tipIndex;
    }

    // wipe out command so we don't continually send the same thing
}


void
CrawlCommander::
handX( const std::string & baseFrameId, const Eigen::VectorXd & treeJointPositions  )
{
    std::vector<std::string> names;
    names.push_back( m_atlasLookup->lookupString( "l_hand" )  );
    names.push_back( m_atlasLookup->lookupString( "r_hand" )  );

    ROS_WARN( "Hand X iteration" );

    BOOST_FOREACH( const std::string & tipName, names )
    {
        FrameGoalGains::Ptr frameGoalGains;
        Eigen::Affine3d handPose;
        Eigen::VectorXd handWeights;

        handPose = m_solver->getPose( baseFrameId,  tipName, treeJointPositions );
        handPose.translation().x() = -0.03;

        FrameGoal::Ptr frameGoal;
        frameGoal.reset( new FrameGoal );

        handWeights = Eigen::VectorXd::Zero( 6 );
        handWeights.head(3) = Eigen::Vector3d( 0.1, 0.2, 0.0 );
        handWeights.tail(3) = Eigen::Vector3d( 0.0, 0.0, 0.0 );

        frameGoal->baseFrameId  = baseFrameId;
        frameGoal->goalFrameId  = baseFrameId;
        frameGoal->tipFrameId   = tipName;
        frameGoal->goalPose     = handPose;
        frameGoal->weightMatrix = handWeights.asDiagonal();

        frameGoalGains.reset( new FrameGoalGains );
        frameGoalGains->tipFrameId()  = tipName;
        *frameGoalGains = *m_fullBodyController->defaultFrameGains();
        frameGoalGains->posKp().head(3) = Eigen::VectorXd::Ones(3) * 1;

        m_fullBodyController->setFrameGoal( frameGoal );
        m_fullBodyController->setFrameGoalGains( frameGoalGains );
    }

    // wipe out command so we don't continually send the same thing
}

void
CrawlCommander::
handDrop( const std::string & baseFrameId, const Eigen::VectorXd & treeJointPositions  )
{

    ROS_WARN( "Hand Drop iteration" );
    std::vector<std::string> names;
    names.push_back( m_atlasLookup->lookupString( "l_hand" )  );
    names.push_back( m_atlasLookup->lookupString( "r_hand" )  );


    BOOST_FOREACH( const std::string & tipName, names )
    {
        FrameGoalGains::Ptr frameGoalGains;
        Eigen::Affine3d handPose;
        Eigen::VectorXd handWeights;

        handPose = m_solver->getPose( baseFrameId,  tipName, treeJointPositions );
        handPose.translation().z() = 0.0;
        handPose.translation().x() = -0.1;

        FrameGoal::Ptr frameGoal;
        frameGoal.reset( new FrameGoal );

        handWeights = Eigen::VectorXd::Zero( 6 );
        handWeights.head(3) = Eigen::Vector3d( 0.3, 0.3, 0.5 );
        handWeights.tail(3) = Eigen::Vector3d( 0.0, 0.0, 0.0 );

        frameGoal->baseFrameId  = baseFrameId;
        frameGoal->goalFrameId  = baseFrameId;
        frameGoal->tipFrameId   = tipName;
        frameGoal->goalPose     = handPose;
        frameGoal->weightMatrix = handWeights.asDiagonal();

        frameGoalGains.reset( new FrameGoalGains );
        frameGoalGains->tipFrameId()  = tipName;
        *frameGoalGains = *m_fullBodyController->defaultFrameGains();
        frameGoalGains->posKp().head(3) = Eigen::VectorXd::Ones(3) * 1;

        m_fullBodyController->setFrameGoal( frameGoal );
        m_fullBodyController->setFrameGoalGains( frameGoalGains );
    }

    // wipe out command so we don't continually send the same thing
}


void
CrawlCommander::
dropPelvis( const std::string & baseFrameId, const Eigen::VectorXd & treeJointPositions  )
{
    ROS_WARN( "Drop pelvis iteration" );

    std::string tipName = m_atlasLookup->lookupString( "pelvis" );

    Eigen::Affine3d pelvisPose;
    pelvisPose = m_solver->getPose( baseFrameId,  tipName, treeJointPositions );

//            pelvisPose = Eigen::Translation3d( 0, 0, -0.4 ) * pelvisPose;
    pelvisPose.translation().z() -= 0.3;

    FrameGoal::Ptr frameGoal;
    frameGoal.reset( new FrameGoal );

    Eigen::VectorXd pelvisWeights = Eigen::VectorXd::Zero( 6 );
    pelvisWeights.head(3) = Eigen::Vector3d( 0.0, 0.0, 0.3 );
    pelvisWeights.tail(3) = Eigen::Vector3d( 0.0, 0.0, 0.0 );

    frameGoal->baseFrameId  = baseFrameId;
    frameGoal->goalFrameId  = baseFrameId; // TODO should this be in base or swing
    frameGoal->tipFrameId   = tipName;
    frameGoal->goalPose     = pelvisPose;
    frameGoal->weightMatrix = pelvisWeights.asDiagonal();

    FrameGoalGains::Ptr frameGoalGains;
    frameGoalGains.reset( new FrameGoalGains );
    frameGoalGains->tipFrameId()  = tipName;
    *frameGoalGains = *m_fullBodyController->defaultFrameGains();
    frameGoalGains->posKp().head(3) = Eigen::VectorXd::Ones(3) * 0.5;

    m_fullBodyController->setFrameGoal( frameGoal );
    m_fullBodyController->setFrameGoalGains( frameGoalGains );
}



void
CrawlCommander::
changeComInSquat( const std::string & baseFrameId )
{
    atlas_msgs::AtlasState::ConstPtr lastStateMsg;
    Eigen::VectorXd                  treeJointPositions;
    lastStateMsg        = boost::atomic_load( &m_lastStateMsg );
    treeJointPositions  = m_atlasLookup->cmdToTree( parseJointPositions( lastStateMsg ) );


    Eigen::Vector3d comLocation;
    comLocation = m_solver->getCenterOfMass( baseFrameId,  treeJointPositions );
    comLocation.x() = 0.2;
    comLocation.z() = 0.6;

    Eigen::Vector3d comWeights( 1, 1, 0.1 );
    m_fullBodyController->setCenterOfMassGoalGains( Eigen::Vector3d::Ones() * 1, Eigen::Vector3d::Ones() * 1  );
    m_fullBodyController->setCenterOfMassGoal( comLocation, comWeights );

    std::vector<std::string> names;
    names.push_back( m_atlasLookup->lookupString( "l_hand" ) );
    names.push_back( m_atlasLookup->lookupString( "r_hand" ) );

    BOOST_FOREACH( const std::string & tipName, names )
    {
        FrameGoalGains::Ptr frameGoalGains;
        Eigen::Affine3d handPose;
        Eigen::VectorXd handWeights;

        handPose = m_solver->getPose( baseFrameId,  tipName, treeJointPositions );
        handPose.translation().x() = -0.4;

        FrameGoal::Ptr frameGoal;
        frameGoal.reset( new FrameGoal );

        handWeights = Eigen::VectorXd::Zero( 6 );
        handWeights.head(3) = Eigen::Vector3d( 0.1, 0.2, 0.0 );
        handWeights.tail(3) = Eigen::Vector3d( 0.0, 0.0, 0.0 );

        frameGoal->baseFrameId  = baseFrameId;
        frameGoal->goalFrameId  = baseFrameId;
        frameGoal->tipFrameId   = tipName;
        frameGoal->goalPose     = handPose;
        frameGoal->weightMatrix = handWeights.asDiagonal();

        frameGoalGains.reset( new FrameGoalGains );
        frameGoalGains->tipFrameId()  = tipName;
        *frameGoalGains = *m_fullBodyController->defaultFrameGains();
        frameGoalGains->posKp().head(3) = Eigen::VectorXd::Ones(3) * 1;

        m_fullBodyController->setFrameGoal( frameGoal );
        m_fullBodyController->setFrameGoalGains( frameGoalGains );

        sleepWithTfPublish( baseFrameId, getFrameToInertial(baseFrameId), 0.5 ); //FIXME this is bad

        Eigen::Vector3d comWeights( 0, 0, 0 );
        m_fullBodyController->setCenterOfMassGoalGains( Eigen::Vector3d::Ones() * 1, Eigen::Vector3d::Ones() * 1  );
        m_fullBodyController->setCenterOfMassGoal( comLocation, comWeights );
    }
}



/////////////////////////////////////////////////////////////////////////////////////////////////////

void
CrawlCommander::
orientOnGround()
{
    Eigen::Affine3dPtr               walkDest;
    atlas_msgs::AtlasState::ConstPtr lastStateMsg;

    std::string                      baseFrameId;
    Eigen::VectorXd                  treeJointPositions;

    int iteration = 0;
    while( iteration < 2 )
    {
        walkDest            = boost::atomic_load( &m_walkDestCmd );
        lastStateMsg        = boost::atomic_load( &m_lastStateMsg );

        baseFrameId = m_atlasLookup->lookupString( "pelvis" );
        m_fullBodyController->switchBaseFoot( baseFrameId );
        publishInertialFrameToOdom( baseFrameId, Eigen::Affine3d::Identity() );


        if( !walkDest )
        {
            m_dt.sleep();
            continue;
        }

        lastStateMsg        = boost::atomic_load( &m_lastStateMsg );
        treeJointPositions  = m_atlasLookup->cmdToTree( parseJointPositions( lastStateMsg ) );

        ROS_WARN( "orientOnGround iteration : %i", iteration );

        if( iteration == 0 )
        {
            spreadArmsOnGround();
        }
        if( iteration == 1 )
        {
            extendLegs();
        }

        ++iteration;
        boost::atomic_store( &m_walkDestCmd, Eigen::Affine3dPtr() );
    }
}



void
CrawlCommander::
spreadArmsOnGround()
{
    std::vector<std::string> names;
    names.push_back( m_atlasLookup->lookupString( "l_hand" ) );
    names.push_back( m_atlasLookup->lookupString( "r_hand" ) );

    std::string baseName = m_atlasLookup->lookupString( "utorso" );

    BOOST_FOREACH( const std::string & tipName, names )
    {
        FrameGoalGains::Ptr frameGoalGains;
        Eigen::Affine3d pose;
        Eigen::VectorXd weights;

        atlas_msgs::AtlasState::ConstPtr lastStateMsg;
        Eigen::VectorXd                  treeJointPositions;

        lastStateMsg        = boost::atomic_load( &m_lastStateMsg );
        treeJointPositions  = m_atlasLookup->cmdToTree( parseJointPositions( lastStateMsg ) );

        pose = m_solver->getPose( baseName,  tipName, treeJointPositions );
        pose.translation().z() =  0.0;
        pose.translation().x() = -0.2;

        if( tipName == m_atlasLookup->lookupString( "l_foot" ) )
            pose.translation().y() =  0.4;
        if( tipName == m_atlasLookup->lookupString( "r_foot" ) )
            pose.translation().y() = -0.4;

        FrameGoal::Ptr frameGoal;
        frameGoal.reset( new FrameGoal );

        weights         = Eigen::VectorXd::Zero( 6 );
        weights.head(3) = Eigen::Vector3d( 0.3, 0.5, 0.05 );
        weights.tail(3) = Eigen::Vector3d( 0.0, 0.0, 0.0 );

        frameGoal->baseFrameId  = baseName;
        frameGoal->goalFrameId  = baseName;
        frameGoal->tipFrameId   = tipName;
        frameGoal->goalPose     = pose;
        frameGoal->weightMatrix = weights.asDiagonal();

        frameGoalGains.reset( new FrameGoalGains );
        frameGoalGains->tipFrameId()  = tipName;
        *frameGoalGains = *m_fullBodyController->defaultFrameGains();
        frameGoalGains->posKp().head(3) = Eigen::VectorXd::Ones(3) * 1;

        m_fullBodyController->setFrameGoal(      frameGoal      );
        m_fullBodyController->setFrameGoalGains( frameGoalGains );
    }

}

void
CrawlCommander::
extendLegs()
{
    std::vector<std::string> names;
    names.push_back( m_atlasLookup->lookupString( "l_foot" ) );
    names.push_back( m_atlasLookup->lookupString( "r_foot" ) );

    std::string baseName = m_atlasLookup->lookupString( "utorso" );

    BOOST_FOREACH( const std::string & tipName, names )
    {
        FrameGoalGains::Ptr frameGoalGains;
        Eigen::Affine3d pose;
        Eigen::VectorXd weights;

        atlas_msgs::AtlasState::ConstPtr lastStateMsg;
        Eigen::VectorXd                  treeJointPositions;

        lastStateMsg        = boost::atomic_load( &m_lastStateMsg );
        treeJointPositions  = m_atlasLookup->cmdToTree( parseJointPositions( lastStateMsg ) );

        pose = m_solver->getPose( baseName,  tipName, treeJointPositions );
        pose.translation().z() = -0.8;
        pose.translation().x() =  0.0;

        if( tipName == m_atlasLookup->lookupString( "l_foot" ) )
            pose.translation().y() =  0.08;
        if( tipName == m_atlasLookup->lookupString( "r_foot" ) )
            pose.translation().y() = -0.08;

        FrameGoal::Ptr frameGoal;
        frameGoal.reset( new FrameGoal );

        weights         = Eigen::VectorXd::Zero( 6 );
        weights.head(3) = Eigen::Vector3d( 0.3, 0.1, 0.5 );
        weights.tail(3) = Eigen::Vector3d( 0.0, 0.0, 0.0 );

        frameGoal->baseFrameId  = baseName;
        frameGoal->goalFrameId  = baseName;
        frameGoal->tipFrameId   = tipName;
        frameGoal->goalPose     = pose;
        frameGoal->weightMatrix = weights.asDiagonal();

        frameGoalGains.reset( new FrameGoalGains );
        frameGoalGains->tipFrameId()  = tipName;
        *frameGoalGains = *m_fullBodyController->defaultFrameGains();
        frameGoalGains->posKp().head(3) = Eigen::VectorXd::Ones(3) * 1;

        m_fullBodyController->setFrameGoal(      frameGoal      );
        m_fullBodyController->setFrameGoalGains( frameGoalGains );
    }


//    FrameGoalGains::Ptr frameGoalGains;
//    Eigen::Affine3d handPose;
//    Eigen::VectorXd handWeights;
//
//    handPose = m_solver->getPose( "pelvis",  "l_foot", treeJointPositions );
//
//    FrameGoal::Ptr frameGoal;
//    frameGoal.reset( new FrameGoal );
//
//    handWeights = Eigen::VectorXd::Zero( 6 );
//    handWeights.head(3) = Eigen::Vector3d( 0.1, 0.1, 0.1 );
//
//    frameGoal->baseFrameId  = "pelvis";
//    frameGoal->goalFrameId  = "pelvis";
//    frameGoal->tipFrameId   = "l_foot";
//    frameGoal->goalPose     = handPose;
//    frameGoal->weightMatrix = handWeights.asDiagonal();
//
//    frameGoalGains.reset( new FrameGoalGains );
//    frameGoalGains->tipFrameId()  = "l_foot";
//    *frameGoalGains = *m_fullBodyController->defaultFrameGains();
//    frameGoalGains->posKp().head(3) = Eigen::VectorXd::Ones(3) * 0.5;
//
//    m_fullBodyController->setFrameGoal( frameGoal );
//
//    frameGoal.reset( new FrameGoal(*frameGoal) );
//    handPose = m_solver->getPose( "pelvis",  "r_foot", treeJointPositions );
//
//    frameGoal->baseFrameId  = "pelvis";
//    frameGoal->goalFrameId  = "pelvis";
//    frameGoal->tipFrameId   = "r_foot";
//    frameGoal->goalPose     = handPose;
//    m_fullBodyController->setFrameGoal( frameGoal );
}


/////////////////////////////////////////////////////////////////////////////////////////////////////

void
CrawlCommander::
sitUp()
{
    Eigen::Affine3dPtr               walkDest;
    atlas_msgs::AtlasState::ConstPtr lastStateMsg;

    std::string                      baseFrameId;
    Eigen::VectorXd                  treeJointPositions;

    int iteration = 0;
    while( iteration < 2 )
    {
        walkDest            = boost::atomic_load( &m_walkDestCmd );
        lastStateMsg        = boost::atomic_load( &m_lastStateMsg );

        baseFrameId = m_atlasLookup->lookupString( "pelvis" );
        m_fullBodyController->switchBaseFoot( baseFrameId );
        publishInertialFrameToOdom( baseFrameId, Eigen::Affine3d::Identity() );


        if( !walkDest )
        {
            m_dt.sleep();
            continue;
        }

        lastStateMsg        = boost::atomic_load( &m_lastStateMsg );
        treeJointPositions  = m_atlasLookup->cmdToTree( parseJointPositions( lastStateMsg ) );

        ROS_WARN( "sitUp iteration : %i", iteration );


        if( iteration == 2 )
        {
            ROS_WARN( "This part is crap" );


//            FrameGoalGains::Ptr frameGoalGains;
//            Eigen::Affine3d utorsoPose;
//            Eigen::VectorXd utorsoWeights;
//
//            utorsoPose = m_solver->getPose( "l_uleg",  "utorso", treeJointPositions );
//
//            FrameGoal::Ptr frameGoal;
//            frameGoal.reset( new FrameGoal );
//
//            utorsoWeights = Eigen::VectorXd::Zero( 6 );
//            utorsoWeights.tail(3) = Eigen::Vector3d( 0.1, 0.1, 0.1 );
//
//            Eigen::Affine3d utorsoRotation( Eigen::AngleAxisd( M_PI/3, Eigen::YAxis3d ) );
//
//            frameGoal->baseFrameId  = "l_uleg";
//            frameGoal->goalFrameId  = "l_uleg";
//            frameGoal->tipFrameId   = "utorso";
//            frameGoal->goalPose     = utorsoRotation;
//            frameGoal->weightMatrix = utorsoWeights.asDiagonal();
//
//            frameGoalGains.reset( new FrameGoalGains );
//            frameGoalGains->tipFrameId()  = "utorso";
//            *frameGoalGains = *m_fullBodyController->defaultFrameGains();
//            frameGoalGains->posKp().head(3) = Eigen::VectorXd::Ones(3) * 0.5;
//            m_fullBodyController->setFrameGoal( frameGoal );
//        }
//        if( iteration == 2 )
//        {
//            FrameGoalGains::Ptr frameGoalGains;
//            Eigen::Affine3d handPose;
//            Eigen::VectorXd handWeights;
//
//            handPose = m_solver->getPose( "l_uleg",  "l_hand", treeJointPositions );
//            handPose.translation().x() += 0.5;
//
//            FrameGoal::Ptr frameGoal;
//            frameGoal.reset( new FrameGoal );
//
//            handWeights = Eigen::VectorXd::Zero( 6 );
//            handWeights.head(3) = Eigen::Vector3d( 0.1, 0.0, 0.0 );
//
//            frameGoal->baseFrameId  = "l_uleg";
//            frameGoal->goalFrameId  = "l_uleg";
//            frameGoal->tipFrameId   = "l_hand";
//            frameGoal->goalPose     = handPose;
//            frameGoal->weightMatrix = handWeights.asDiagonal();
//
//            frameGoalGains.reset( new FrameGoalGains );
//            frameGoalGains->tipFrameId()  = "l_hand";
//            *frameGoalGains = *m_fullBodyController->defaultFrameGains();
//            frameGoalGains->posKp().head(3) = Eigen::VectorXd::Ones(3) * 0.5;
//
//            m_fullBodyController->setFrameGoal( frameGoal );
//
//            handPose = m_solver->getPose( "l_uleg",  "r_hand", treeJointPositions );
//            handPose.translation().x() += 0.5;
//
//
//            frameGoal.reset( new FrameGoal(*frameGoal) );
//
//            handWeights = Eigen::VectorXd::Zero( 6 );
//            handWeights.head(3) = Eigen::Vector3d( 0.1, 0.1, 0.1 );
//
//
//            frameGoal->baseFrameId  = "l_uleg";
//            frameGoal->goalFrameId  = "l_uleg";
//            frameGoal->tipFrameId   = "r_hand";
//            frameGoal->goalPose     = handPose;
//            frameGoal->weightMatrix = handWeights.asDiagonal();
//
//            frameGoalGains.reset( new FrameGoalGains );
//            frameGoalGains->tipFrameId()  = "r_hand";
//            *frameGoalGains = *m_fullBodyController->defaultFrameGains();
//            frameGoalGains->posKp().head(3) = Eigen::VectorXd::Ones(3) * 0.5;
//
//            m_fullBodyController->setFrameGoal( frameGoal );
        }

        ++iteration;
        boost::atomic_store( &m_walkDestCmd, Eigen::Affine3dPtr() );
    }
}











/////////////////////////////////////////////////////

bool CrawlCommander::isForwardBackward(    const Eigen::Affine3dPtr & walkDest ) { return false; }
bool CrawlCommander::isToSide(             const Eigen::Affine3dPtr & walkDest ) { return false; }

void CrawlCommander::turn(                 const Eigen::Affine3dPtr & walkDest ) {}
void CrawlCommander::scootForwardBackward( const Eigen::Affine3dPtr & walkDest ) {}


////////////////////////////////////////////////////


FrameGoalGains::Ptr
CrawlCommander::
buildActuationGoal( Limb limb, double factor )
{
    const int rollIndex = 3;

    FrameGoalGains::Ptr limbGoalGains( new FrameGoalGains );
    limbGoalGains->tipFrameId()                    = limbToFrameId( limb, m_atlasLookup );
    *limbGoalGains = *m_fullBodyController->defaultFrameGains();
    limbGoalGains->posKp().segment( rollIndex, 2 ) *= factor;
    return limbGoalGains;
}

bool
CrawlCommander::
highLimbForce( const atlas_msgs::AtlasState::ConstPtr & atlasStateMsg, Limb limb, ros::Duration dt )
{
    if( !atlasStateMsg )
        return false;

    Eigen::Vector3d force;
    if( limb == LEFT_FOOT_LIMB )
        force =  Eigen::Vector3dConstMap( &atlasStateMsg->l_foot.force.x );
    else
        force =  Eigen::Vector3dConstMap( &atlasStateMsg->r_foot.force.x );

    //FIXME Filter with more samples
    m_filteredForce = lowPassFilter( m_filteredForce, force, dt.toSec()/m_dt.toSec() );

    if( m_filteredForce.norm() > m_forceThresh )
        return true;

    return false;
}



}
}

//            ROS_WARN( "iteration 2" );
//          handDrop( baseFrameId,  treeJointPositions );
//        }
//        if( iteration == 3 )
//        {
//            ROS_WARN( "iteration 2" );
//            ROS_WARN( "Com forward X iteration" );
//            treeJointPositions = m_atlasLookup->cmdToTree( parseJointPositions( lastStateMsg ) );
//            m_fullBodyController->switchBaseFoot( baseFrameId );
//
//            Eigen::Vector3d comLocation;
//            comLocation = m_solver->getCenterOfMass( baseFrameId,  treeJointPositions );
//            comLocation.x() = 0.05;
//            comLocation.z() = 0.5;
//
//            Eigen::Vector3d comWeights( 0,0,0 );
//            m_fullBodyController->setCenterOfMassGoalGains( Eigen::Vector3d::Ones() * 1, Eigen::Vector3d::Ones() * 1  );
//            m_fullBodyController->setCenterOfMassGoal( comLocation, comWeights );
//        }


//        if( iteration == 0 )
//      {
//
//          limbsDownFromPelvis( baseFrameId, treeJointPositions );
//
//      }

//
//        ++iteration;
//
//        boost::atomic_store( &m_walkDestCmd, Eigen::Affine3dPtr() );



//        m_swingLimbStartPos = m_solver->FullBodyPoseSolver::getPose( limbToFrameId( LEFT_FOOT_LIMB ),
//                                                                   limbToFrameId( otherLimb( LEFT_FOOT_LIMB ) ),
//                                                                   stepJointPositions ).translation();
//        m_swingPosition = m_swingLimbStartPos;
//
//      m_fullBodyController->switchBaseFoot( limbToFrameId( otherLimb( LEFT_FOOT_LIMB ) ) );
//
//      Eigen::Vector6d swingLimbWeights;
//      swingLimbWeights << 1, 1, 1, 1, 1, 1;
//
//      Eigen::Affine3d swingLimbPose = Eigen::Affine3d::Identity();
//      swingLimbPose.translation() = m_swingPosition;
//
//      FrameGoal::Ptr frameGoal( new FrameGoal );
//
//      frameGoal->baseFrameId  = limbToFrameId( LEFT_FOOT_LIMB );
//      frameGoal->goalFrameId  = limbToFrameId( otherLimb( LEFT_FOOT_LIMB ) ); // TODO should this be in base or swing
//      frameGoal->tipFrameId   = limbToFrameId( otherLimb( LEFT_FOOT_LIMB ) );
//
//      frameGoal->goalPose     = swingLimbPose;
//      frameGoal->weightMatrix = swingLimbWeights.asDiagonal();
//
//      m_fullBodyController->setFrameGoal( frameGoal );

//
//
//
//    }
//
//}


/*
 * BdiStepCommander.cpp
 *
 *  Created on: Apr 29, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/AtlasCommander/BdiStepCommander.hpp>
#include <re2uta/walking/CirclePathStepGenerator.hpp>
#include <re2uta/walking/ManualStepGenerator.hpp>
#include <re2uta/walking/StepGenerator.hpp>

#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>

#include <urdf/model.h>

#include <humanoid_nav_msgs/PlanFootsteps.h>
#include <atlas_msgs/AtlasSimInterfaceCommand.h>
#include <atlas_msgs/AtlasSimInterfaceState.h>
//#include <atlas_msgs/AtlasControlTypes.h>
#include <re2uta/AtlasCommander/types.hpp>
#include <re2/eigen/eigen_util.h>
#include <re2/matrix_conversions.h>

#include <eigen_conversions/eigen_msg.h>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_broadcaster.h>
#include <eigen_conversions/eigen_kdl.h>
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <boost/foreach.hpp>

namespace re2uta
{

namespace atlascommander
{

BdiStepCommander::
BdiStepCommander( const FullBodyControllerInterface::Ptr & fullBodyController,
//                  const JointController::Ptr & jointController,
                  const urdf::Model & model,
                  AtlasLookup::Ptr const & atlasLookup,
                  StepPlannerType stepPlannerType )
    : m_atlasLookup( atlasLookup )
{
    m_model                  = model;
    m_aggPose                = Eigen::Affine3d::Identity();
    m_aggPoseAtCommandTime   = Eigen::Affine3d::Identity();
    m_nextStepIndex          = 0;
    m_lastStepIndexNeeded    = 0;
    m_nextFootIndex          = 0;
    m_planStepIndex          = 0;
    m_bdiStateIndex          = 0;
    m_odomFrameId            = "/odom";
    m_isSwaying              = false;
    m_shutdown               = false;



    ros::SubscribeOptions bdiStateOptions;
    bdiStateOptions = ros::SubscribeOptions::create<atlas_msgs::AtlasSimInterfaceState>( "/atlas/atlas_sim_interface_state", 1,
                                                       boost::bind( &BdiStepCommander::handleBdiStateMessage, this, _1 ),
                                                       ros::VoidPtr(),
                                                       &m_stateUpdateThreadCallbackQueue );
    bdiStateOptions.transport_hints = ros::TransportHints().tcpNoDelay( true );

    m_bdiAtlasStateSub     = m_node.subscribe( bdiStateOptions );


    m_bdiAtlasCommandPub     = m_node.advertise<atlas_msgs::AtlasSimInterfaceCommand>( "/atlas/atlas_sim_interface_command", 1 );
    m_footStepPlannerClient  = m_node.serviceClient<humanoid_nav_msgs::PlanFootsteps>( "/plan_footsteps", true );

    m_footstepMarkerPub      = m_node.advertise<visualization_msgs::MarkerArray>( "/footstep_planner/footsteps_array", 1 );
    m_stepPlannerType        = stepPlannerType;

//    m_jointController        = jointController;
    m_fullBodyController     = fullBodyController;

    m_kEfforts = std::vector<uint8_t>( m_atlasLookup->getNumCmdJoints(), 0 );
    m_kEfforts[m_atlasLookup->segmentNameToJointIndex( "head" )] = 255;

    m_fullBodyController->setKEfforts( m_kEfforts );

    m_bdiUpdateThread = boost::thread( &BdiStepCommander::stateUpdateCallbackLoop, this );
    ROS_INFO( "BDI StepCommander should be started" );

}

BdiStepCommander::
~BdiStepCommander()
{
    m_fullBodyController->setKEfforts( std::vector<uint8_t>( m_atlasLookup->getNumCmdJoints(), 255 ) );

    m_shutdown = true;
    m_bdiAtlasStateSub.shutdown();

//    m_bdiUpdateThread.join();
}



void
BdiStepCommander::
stateUpdateCallbackLoop()
{
    ros::Duration(4).sleep(); //FIXME this is a horrible way to do this, but we're waiting for k-gains to be set in the joint controller

    ROS_INFO( "Starting BdiStepCommander stateUpdateLoop" );
    if( m_stepPlannerType == NAO_PLANNER )
    {
        ROS_INFO( "waiting for footstepplannerClient..." );
        m_footStepPlannerClient.waitForExistence( ros::Duration( 0.5 ) );
        ROS_INFO( "Found BdiStepCommander footstepplannerClient" );
    }

    while( ros::ok()  && ! m_shutdown )
    {
        // timeout set to 0.1 so that it will check ros::ok() often enough to exit on ctrl+c
        m_stateUpdateThreadCallbackQueue.callAvailable( ros::WallDuration(0.1) );
    }
}


void
BdiStepCommander::
shutdown()
{
    m_shutdown = true;
}



void
BdiStepCommander::
setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg )
{

}


void
BdiStepCommander::
handleBdiStateMessage( const atlas_msgs::AtlasSimInterfaceStateConstPtr & msg )
{
    m_lastBdiStateMsg = msg;
    enum { LEFT=0, RIGHT=1 };



    if( m_bdiStateIndex % 20 == 0 )
    {
        Eigen::Affine3d odomToFoot;
        tf::poseMsgToEigen( msg->foot_pos_est.elems[LEFT], odomToFoot );

        tf::Transform footToOdomTf;
        tf::transformEigenToTF( odomToFoot.inverse(), footToOdomTf );
        m_tfBroadcaster.sendTransform( tf::StampedTransform( footToOdomTf,
                                                             ros::Time::now(),
                                                             std::string( "/" ) + m_atlasLookup->lookupString( "l_foot" ),
                                                             std::string( "/odom" ) ));
        ROS_INFO_STREAM_THROTTLE( 5, "BDIcommander publishing odom" );
    }
    ++m_bdiStateIndex;


    tf::poseMsgToEigen( msg->foot_pos_est[LEFT],  m_leftFootPose  );
    tf::poseMsgToEigen( msg->foot_pos_est[RIGHT], m_rightFootPose );


    m_aggPose.translation() = (m_leftFootPose.translation() + m_rightFootPose.translation()) / 2;
    Eigen::Quaterniond aggRotation = Eigen::Quaterniond( m_leftFootPose.rotation() ).slerp( 0.5, Eigen::Quaterniond( m_rightFootPose.rotation() ) );
    m_aggPose.linear() = aggRotation.toRotationMatrix();


//    tf::Transform treeRootToBase;
//    tf::transformEigenToTF( rightFootPose, treeRootToBase );
//    m_tfBroadcaster.sendTransform( tf::StampedTransform( treeRootToBase,
//                                                         ros::Time::now(),
//                                                         std::string( "/odom" ),
//                                                         "/r_foot" ));
//
//

    if( msg->step_feedback.status_flags )
        ROS_INFO_STREAM( "status flags    : " << std::hex << msg->step_feedback.status_flags   );

    ROS_INFO_STREAM_THROTTLE( 5, "current behavior: " << msg->current_behavior );
    ROS_INFO_STREAM_THROTTLE( 5, "desired behavior: " << msg->desired_behavior );
    ROS_INFO_STREAM_THROTTLE( 5, "error code      : " << msg->error_code       );
    ROS_INFO_STREAM_THROTTLE( 5, "status flags    : " << std::hex << msg->step_feedback.status_flags   );
    ROS_INFO_STREAM_THROTTLE( 5, "trans_from_behavior_index : " << std::hex << msg->behavior_feedback.trans_from_behavior_index );
    ROS_INFO_STREAM_THROTTLE( 5, "trans_to_behavior_index   : " << std::hex << msg->behavior_feedback.trans_to_behavior_index   );
    ROS_INFO_STREAM_THROTTLE( 5, "step status flag          : " << std::hex << msg->step_feedback.status_flags   );
    ROS_INFO_STREAM_THROTTLE( 5, "next step needed flag     : " << std::hex << msg->walk_feedback.next_step_index_needed  );
    ROS_INFO_STREAM_THROTTLE( 5, "m_aggPose                 : " << m_aggPose.translation().transpose()               );
    ROS_INFO_STREAM_THROTTLE( 5, "m_aggPoseAtCommandTime    : " << m_aggPoseAtCommandTime.translation().transpose()  );
    ROS_INFO_STREAM_THROTTLE( 5, "k effort                  : " << (int)msg->k_effort[0] );


    WalkGoalVectorPtr walkGoalVector;
    walkGoalVector = boost::atomic_exchange( &m_latestWalkGoalVector, walkGoalVector );
    if( walkGoalVector )
    {
        ROS_ERROR( "Not handling multiple points yet, passing last of %lu to single goal handler", walkGoalVector->size() );
        boost::atomic_store( &m_latestWalkGoal, walkGoalVector->back() );
    }

    WalkGoal::Ptr walkGoal;
    walkGoal = boost::atomic_exchange( &m_latestWalkGoal, walkGoal );
    if( walkGoal )
    {
        ROS_INFO( "Got walk goal, planning" );
        try
        {
            Eigen::Affine3d toOdom = Eigen::Affine3d::Identity();
            if( m_odomFrameId != walkGoal->goalFrameId )
            {
                Eigen::Affine3d toOdom = lookupTransform( walkGoal->goalFrameId, m_odomFrameId );
            }
            WalkGoal::Ptr walkGoalInOdom( new WalkGoal( *walkGoal ) );
            walkGoalInOdom->goalPose = toOdom * walkGoalInOdom->goalPose;


            m_plannedSteps         = planStepsTo( walkGoalInOdom );
            m_planStepIndex        = 0;
            m_aggPoseAtCommandTime = m_aggPose;

            if( m_lastBdiStateMsg )
            {
                m_lastStepIndexNeeded = m_lastBdiStateMsg->walk_feedback.next_step_index_needed - 1;
            }
        }
        catch( ... )
        {
            ROS_ERROR_STREAM( "LOOKUP FAILURE from: " <<  walkGoal->goalFrameId << " to " << m_odomFrameId );
            ROS_ERROR_STREAM( "caught exception: " );
        }
    }

    publishStepCluster( msg );
}

//
//        void
//        publishStep( const atlas_msgs::AtlasSimInterfaceStateConstPtr & msg )
//        {
//            if(   m_footStepResponse
//               && m_planStepIndex < m_footStepResponse->footsteps.size() )
//            {
//                atlas_msgs::AtlasSimInterfaceCommandPtr bdiStepCmd = buildDefaultStepCommand();
//                int index = 0;
//                const humanoid_nav_msgs::StepTarget & stepTarget = m_footStepResponse->footsteps[m_planStepIndex];
//                bdiStepCmd->step_params.desired_step = *buildDefaultStepData();
//
//                Eigen::Affine3d footStepPose = m_aggPose;
//                footStepPose.translation().x() += stepTarget.pose.x;
//                footStepPose.translation().y() += stepTarget.pose.y;
//                footStepPose.rotate( Eigen::AngleAxisd( stepTarget.pose.theta, Eigen::ZAxis3d ) );
//                tf::poseEigenToMsg( footStepPose, bdiStepCmd->step_params.desired_step.pose );
//
//                bdiStepCmd->step_params.desired_step.step_index = 1; // apparently this is always 1 for static stepping m_nextStepIndex;
//                bdiStepCmd->step_params.desired_step.foot_index = m_nextFootIndex;
//
//                m_bdiAtlasCommandPub.publish( bdiStepCmd );
//            }
//
//
//            if(      msg->behavior_feedback.step_feedback.status_flags == 1 // this apparently means swaying  according to: http://gazebosim.org/wiki/Tutorials/drcsim/2.5/atlas_sim_interface
//                && ! m_isSwaying
//                &&   m_footStepResponse )
//            {
//                ROS_INFO_STREAM( "Not sway to sway transition" );
//                m_isSwaying = true;
//                m_nextFootIndex = ! m_nextFootIndex;
//                ++m_nextStepIndex;
//                ++m_planStepIndex;
//            }
//            else
//            if(      msg->behavior_feedback.step_feedback.status_flags == 2 )
//            {
//                m_isSwaying = false;
//            }
//        }

void
BdiStepCommander::
publishStepCluster( const atlas_msgs::AtlasSimInterfaceStateConstPtr & msg )
{
    atlas_msgs::AtlasSimInterfaceCommandPtr bdiWalkCmd;
    enum BdiFoot { BDI_LEFT=0, BDI_RIGHT=1 };

    int nextStepIndexNeeded = msg->walk_feedback.next_step_index_needed;
    if( nextStepIndexNeeded != m_lastStepIndexNeeded )
    {
        ++m_planStepIndex;
        m_nextFootIndex = m_planStepIndex  % 2; // ignored
    }
    else
    {
        return;
    }

    if( msg->step_feedback.status_flags & 0x001 ) //swaying
    {
        return;
    }

    std::size_t index = 0;
    std::size_t planStepIndex = m_planStepIndex;
    std::size_t nextFootIndex = m_nextFootIndex;
    while(   m_plannedSteps
          && planStepIndex < m_plannedSteps->size()
          && index         < m_plannedSteps->size()
          && index         < 4
          && ros::ok()
          && !m_shutdown )
    {
        if( !bdiWalkCmd )
            bdiWalkCmd = buildDefaultWalkCommand();

        StepPose::Ptr stepTarget;
        size_t numSteps = m_plannedSteps->size();


        if( numSteps == 1 )
        {
            BdiFoot whichFoot;
            if( (*m_plannedSteps)[0]->swingFoot == LEFT_FOOT ) // no enum in AtlasBehviorStepData.h
                whichFoot = BDI_LEFT;
            else
                whichFoot = BDI_RIGHT;

            ROS_WARN( "Not enough steps (i%) to walk, bailing out", numSteps );
            bdiWalkCmd->behavior = atlas_msgs::AtlasSimInterfaceCommand::STEP;
            bdiWalkCmd->step_params.desired_step.step_index   = 1;
            bdiWalkCmd->step_params.desired_step.foot_index   = (int)whichFoot;
            bdiWalkCmd->step_params.desired_step.duration     = 0.63;
            bdiWalkCmd->step_params.desired_step.swing_height = 0.1;

            Eigen::Affine3d footStepPose = (*m_plannedSteps)[0]->swingFootPose();
            tf::poseEigenToMsg( footStepPose, bdiWalkCmd->step_params.desired_step.pose );
        }
        else
        {
            if( planStepIndex < numSteps && !m_shutdown ) //FIXME the shutdown part of the condition needs to be tested
            {
                stepTarget = (*m_plannedSteps)[planStepIndex];
            }
            else
            if( numSteps > 1 )
            {
                int overshootIndex = (planStepIndex - numSteps - 1)  % 2;
                stepTarget = (*m_plannedSteps)[ numSteps - 1 - overshootIndex ];
                ROS_WARN( "Overshoot index is: %i, using index %lu", overshootIndex, numSteps- 1 - overshootIndex  );
                ROS_WARN( "out of %lu ", numSteps  );

                bdiWalkCmd->behavior = atlas_msgs::AtlasSimInterfaceCommand::STAND;
            }

            bdiWalkCmd->walk_params.step_queue[index] = *buildDefaultStepData();

            Eigen::Affine3d footStepPose = stepTarget->swingFootPose();
            tf::poseEigenToMsg( footStepPose, bdiWalkCmd->walk_params.step_queue[index].pose );

            BdiFoot whichFoot;
            if( stepTarget->swingFoot == LEFT_FOOT ) // no enum in AtlasBehviorStepData.h
                whichFoot = BDI_LEFT;
            else
                whichFoot = BDI_RIGHT;

            bdiWalkCmd->walk_params.step_queue[index].step_index = nextStepIndexNeeded + index;
            bdiWalkCmd->walk_params.step_queue[index].foot_index = (int)whichFoot;
        }

        ++planStepIndex;
        ++index;
        nextFootIndex = planStepIndex  % 2; // ignored
    }


    if( bdiWalkCmd )
    {
//                if( m_planStepIndex > m_footStepResponse->footsteps.size() )
//                {
//                    m_footStepResponse.reset();
//                }
        ROS_INFO( "publishing walk: psi: %i, nsin: %i ", m_planStepIndex, nextStepIndexNeeded );
        bdiWalkCmd->k_effort = m_kEfforts;

        m_bdiAtlasCommandPub.publish( bdiWalkCmd );

        m_lastStepIndexNeeded = nextStepIndexNeeded;
    }
}


atlas_msgs::AtlasSimInterfaceCommandPtr
BdiStepCommander::
buildDefaultWalkCommand()
{
    atlas_msgs::AtlasSimInterfaceCommandPtr bdiWalkCommand( new atlas_msgs::AtlasSimInterfaceCommand );

    bdiWalkCommand->behavior = atlas_msgs::AtlasSimInterfaceCommand::WALK;
    bdiWalkCommand->k_effort = std::vector<uint8_t>( m_atlasLookup->getNumCmdJoints(), 0 );
    bdiWalkCommand->k_effort[m_atlasLookup->segmentNameToJointIndex( m_atlasLookup->lookupString( "head" ) )] = 255;

//            bdiWalkCommand->walk_params;

    return bdiWalkCommand;
}

atlas_msgs::AtlasSimInterfaceCommandPtr
BdiStepCommander::
buildDefaultStepCommand()
{
    atlas_msgs::AtlasSimInterfaceCommandPtr bdiStepCommand( new atlas_msgs::AtlasSimInterfaceCommand );

    bdiStepCommand->behavior = atlas_msgs::AtlasSimInterfaceCommand::STEP;
    bdiStepCommand->k_effort = std::vector<uint8_t>( m_atlasLookup->getNumCmdJoints(), 0 );
    bdiStepCommand->step_params.use_demo_walk = false;

    return bdiStepCommand;
}

atlas_msgs::AtlasBehaviorStepDataPtr
BdiStepCommander::
buildDefaultStepData()
{
    atlas_msgs::AtlasBehaviorStepDataPtr bdiStepData( new atlas_msgs::AtlasBehaviorStepData );

    bdiStepData->duration     = 0.63;
//    bdiStepCmd->foot_index
//    bdiStepCmd->pose
//    bdiStepCmd->step_index
    bdiStepData->swing_height = 0.05;

    return bdiStepData;
}



//atlascommander::Result
//BdiStepCommander::
//stepTo( const Eigen::Affine3d & destPose )
//{
//
//}

Eigen::Affine3d
BdiStepCommander::
lookupTransform( const std::string & fromFrame, const std::string & toFrame )
{
    tf::StampedTransform tfTransform;
    m_tfListener.waitForTransform( toFrame, fromFrame, ros::Time(0), ros::Duration(2) );
    m_tfListener.lookupTransform( toFrame, fromFrame, ros::Time(0), tfTransform );

    Eigen::Matrix4d homoTransform = re2::convertTfToEigen4x4( tfTransform );

    return Eigen::Affine3d( homoTransform );
}



void
BdiStepCommander::
walkTo( const WalkGoal::Ptr & walkGoal )
{
//    ROS_ASSERT_MSG( walkGoal->goalFrameId == m_odomFrameId, "Goal is not in odom frame" );
    ROS_INFO( "BDI got single goal walkto" );
    boost::atomic_store( &m_latestWalkGoal, walkGoal );
}


void
BdiStepCommander::
walkTo( const atlascommander::WalkGoalVectorPtr& walkGoals )
{
    ROS_INFO( "BDI got multi goal walkto" );
    boost::atomic_store( &m_latestWalkGoalVector, walkGoals );
}


StepPosePtrVectorPtr
BdiStepCommander::
planStepsTo( const WalkGoal::Ptr & walkGoal )
{
    if( m_stepPlannerType == MANUAL_PLANNER )
        return manualPlanStepsTo( walkGoal );
    else
    if( m_stepPlannerType == CIRCLE_PLANNER )
        return circlePlanStepsTo( walkGoal->goalPose );
    else
    if( m_stepPlannerType == NAO_PLANNER )
        return naoPlanStepsTo( walkGoal->goalPose );
    else
        return naoPlanStepsTo( walkGoal->goalPose );
}



void
BdiStepCommander::
setStepPlannerType( StepPlannerType stepPlannerType )
{
    m_stepPlannerType = stepPlannerType;
}





StepPosePtrVectorPtr
BdiStepCommander::
manualPlanStepsTo( const WalkGoal::Ptr & walkGoal )
{
    visualization_msgs::MarkerArray markerArrayMsg;

    visualization_msgs::Marker      markerMsg;
    markerMsg.action  = visualization_msgs::Marker::MODIFY;
    markerMsg.type    = visualization_msgs::Marker::CUBE;
    markerMsg.scale.x = 0.2;
    markerMsg.scale.y = 0.1;
    markerMsg.scale.z = 0.001;
    markerMsg.header.frame_id = "/odom";
    markerMsg.frame_locked    = true;

    StepPosePtrVectorPtr stepPoses( new StepPosePtrVector );

    double halfStepSize     = 0.2;
    double footSpreadOffset = 0.15;
    Foot   baseFoot         = otherFoot( walkGoal->swingLegSuggestion );


    StepPose::Ptr previousStepPose;
    StepPose::Ptr currentStepPose( new StepPose( baseFoot ) );
    currentStepPose->lFootPose = m_leftFootPose;
    currentStepPose->rFootPose = m_rightFootPose;

    ManualStepGenerator::Ptr pathGenerator;
    pathGenerator.reset( new ManualStepGenerator( currentStepPose, walkGoal->goalPose ) );

    StepPose::Ptr nextStepPose( new StepPose( *currentStepPose ) );
    nextStepPose->swingFoot = otherFoot( nextStepPose->swingFoot );// will be negated in the loop

    double epsilon   = 0.02;
    int    stepIndex = 0;
    while( ros::ok() && !m_shutdown &&
          (    previousStepPose == NULL
           || !previousStepPose->lFootPose.isApprox( nextStepPose->lFootPose, epsilon )     // while our current and next arn't the same
           || !previousStepPose->rFootPose.isApprox( nextStepPose->rFootPose, epsilon ) ) ) // FIXME is this epsilon good enough?
    {
        previousStepPose = nextStepPose;

        ROS_INFO_STREAM( "planning step : "<< stepIndex );
        Foot baseFoot = previousStepPose->swingFoot;
        nextStepPose.reset( new StepPose( baseFoot ) );

        // generateNextStep always gives a valid step, but may be the same location as last;
        nextStepPose->swingFootPose() = pathGenerator->generateNextStep( previousStepPose->swingFootPose(), // current base foot location
                                                                         previousStepPose->baseFootPose(),  // current swing foot location
                                                                         stepIndex );

        nextStepPose->baseFootPose()  = previousStepPose->swingFootPose();
        nextStepPose->comPose         = Eigen::Vector3d::Zero(); // NOTE, this is not used

        stepPoses->push_back( nextStepPose );
        ++stepIndex;

        tf::poseEigenToMsg( nextStepPose->swingFootPose(), markerMsg.pose );
        markerMsg.id = stepIndex;

        if( baseFoot == LEFT_FOOT )
            Eigen::Map<Eigen::Vector4f>( &markerMsg.color.r ) = Eigen::Vector4f( 1, 0, 0, 1 );
        else
            Eigen::Map<Eigen::Vector4f>( &markerMsg.color.r ) = Eigen::Vector4f( 0, 0, 1, 1 );

        markerArrayMsg.markers.push_back( markerMsg );
    }

    ROS_INFO_STREAM( "donePlanning " );


    m_footstepMarkerPub.publish( markerArrayMsg );
    return stepPoses;
}


StepPosePtrVectorPtr
BdiStepCommander::
circlePlanStepsTo( const Eigen::Affine3d & destPose )
{
    visualization_msgs::MarkerArray markerArrayMsg;

    visualization_msgs::Marker      markerMsg;
    markerMsg.action  = visualization_msgs::Marker::MODIFY;
    markerMsg.type    = visualization_msgs::Marker::CUBE;
    markerMsg.scale.x = 0.2;
    markerMsg.scale.y = 0.1;
    markerMsg.scale.z = 0.001;
    markerMsg.header.frame_id = "/odom";
    markerMsg.frame_locked    = true;

    StepPosePtrVectorPtr stepPoses( new StepPosePtrVector );

    double halfStepSize     = 0.2;
    double footSpreadOffset = 0.15;
    Foot   baseFoot         = re2uta::LEFT_FOOT;

    CirclePathStepGenerator::Ptr circlePathGenerator;
    circlePathGenerator.reset( new CirclePathStepGenerator( m_aggPose, destPose, halfStepSize, footSpreadOffset, otherFoot(baseFoot) ) );

    StepPose::Ptr previousStepPose;
    StepPose::Ptr nextStepPose( new StepPose( otherFoot( baseFoot ) ) ); // will be negated in the loop
    nextStepPose->lFootPose = m_leftFootPose;
    nextStepPose->rFootPose = m_rightFootPose;

    double epsilon   = 0.02;
    int    stepIndex = 0;
    while( ros::ok() && !m_shutdown &&
          (    previousStepPose == NULL
           || !previousStepPose->lFootPose.isApprox( nextStepPose->lFootPose, epsilon )     // while our current and next arn't the same
           || !previousStepPose->rFootPose.isApprox( nextStepPose->rFootPose, epsilon ) ) ) // FIXME is this epsilon good enough?
    {
        previousStepPose = nextStepPose;

        ROS_INFO_STREAM( "planning step : "<< stepIndex );
        Foot baseFoot = previousStepPose->swingFoot;
        nextStepPose.reset( new StepPose( baseFoot ) );

        // generateNextStep always gives a valid step, but may be the same location as last;
        nextStepPose->swingFootPose() = circlePathGenerator->generateNextStep( previousStepPose->swingFootPose(), // current base foot location
                                                                               previousStepPose->baseFootPose(),  // current swing foot location
                                                                               stepIndex );
        nextStepPose->baseFootPose()  = previousStepPose->swingFootPose();
        nextStepPose->comPose         = Eigen::Vector3d::Zero(); // NOTE, this is not used

        stepPoses->push_back( nextStepPose );
        ++stepIndex;


        tf::poseEigenToMsg( nextStepPose->swingFootPose(), markerMsg.pose );
        markerMsg.id = stepIndex;

        if( baseFoot == LEFT_FOOT )
            Eigen::Map<Eigen::Vector4f>( &markerMsg.color.r ) = Eigen::Vector4f( 1, 0, 0, 1 );
        else
            Eigen::Map<Eigen::Vector4f>( &markerMsg.color.r ) = Eigen::Vector4f( 0, 0, 1, 1 );

        markerArrayMsg.markers.push_back( markerMsg );
    }

    ROS_INFO_STREAM( "donePlanning " );


    m_footstepMarkerPub.publish( markerArrayMsg );
    return stepPoses;
}


StepPosePtrVectorPtr
BdiStepCommander::
naoPlanStepsTo( const Eigen::Affine3d & destPose )
{
    //FIXME needs code to switch to particular walking method controller
    humanoid_nav_msgs::PlanFootstepsResponsePtr footStepResponse;

    if( m_footStepPlannerClient.exists() )
    {
        humanoid_nav_msgs::PlanFootstepsRequest  request;

        request.start.x     = m_aggPose.translation().x();
        request.start.y     = m_aggPose.translation().y();
        request.start.theta = m_aggPose.rotation().eulerAngles(0,1,2).z();

        request.goal.x      = destPose.translation().x();
        request.goal.y      = destPose.translation().y();
        request.goal.theta  = destPose.rotation().eulerAngles(0,1,2).z();

        ROS_WARN( "About to call to service" );
        bool success = false;
        ros::Time t0 = ros::Time::now();
        footStepResponse.reset( new humanoid_nav_msgs::PlanFootstepsResponse );
        success = m_footStepPlannerClient.call( request, *footStepResponse );
        ros::Time t1 = ros::Time::now();
        ROS_WARN( "Service call took: %2.3f seconds", (t1-t0).toSec() );

        if( success && footStepResponse->result && footStepResponse->footsteps.size() > 0 )
        {
            ROS_WARN( "service success: %i", footStepResponse->result );
        }
        else
        {
            ROS_ERROR( "call to foot step planner failed : %i", footStepResponse->result );
            if( !m_footStepPlannerClient.exists() )
                ROS_ERROR( "foot step planner service doesn't exist" );

            footStepResponse.reset(); //FIXME causes crash!!!!!!!!!!!!
        }
    }
    else
    {
        ROS_ERROR( "call to foot step planner failed" );
    }

    if( !footStepResponse )
        return StepPosePtrVectorPtr();

    StepPosePtrVectorPtr stepPoses( new StepPosePtrVector );

    Foot baseFoot = re2uta::LEFT_FOOT;
    StepPose::Ptr previousStepPose( new StepPose( otherFoot( baseFoot ) ) ); // will be negated in the loop
    previousStepPose->lFootPose = m_leftFootPose;
    previousStepPose->rFootPose = m_rightFootPose;

    BOOST_FOREACH( const humanoid_nav_msgs::StepTarget & stepTarget, footStepResponse->footsteps )
    {
        Foot swingFoot;

        if( stepTarget.leg == humanoid_nav_msgs::StepTarget::left ) // according to StepTarget.h
            swingFoot = LEFT_FOOT;
        if( stepTarget.leg == humanoid_nav_msgs::StepTarget::right ) // according to StepTarget.h
            swingFoot = RIGHT_FOOT;

        Foot baseFoot = otherFoot( swingFoot );

        StepPose::Ptr stepPose( new StepPose( baseFoot ) );

        Eigen::Affine3d swingFootPose = Eigen::Affine3d::Identity(); //m_aggPoseAtCommandTime;
        swingFootPose.translation().x() += stepTarget.pose.x;
        swingFootPose.translation().y() += stepTarget.pose.y;
        swingFootPose.rotate( Eigen::AngleAxisd( stepTarget.pose.theta, Eigen::ZAxis3d ) );

        stepPose->swingFootPose() = swingFootPose;
        stepPose->baseFootPose()  = previousStepPose->swingFootPose();

        stepPoses->push_back( stepPose );
    }

    return stepPoses;
}
}

}

/*
 * BalanceCommander.cpp
 *
 *  Created on: Jun 14, 2013
 *      Author: andrew.somerville
 *              Isura Ranatunga
 */

#include <re2uta/AtlasCommander/BalanceCommander.hpp>
#include <re2uta/AtlasCommander/FullBodyControllerInterface.hpp>
#include <re2uta/walking/WalkPatternGenerator.hpp>
#include <re2uta/walking/StepTrajectoryGenerator.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <re2uta/CalculateCapturePoint.hpp>
#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <boost/shared_ptr.hpp>
#include <string>


namespace
{
    template <typename Type>
    Type exponentialWeightedAverage( Type oldVal, Type newVal, double newWeight )
    {
        return oldVal*(1-newWeight) + newWeight*(newVal);
    }

    template <typename Type>
    Type lowPassFilter( Type oldVal, Type newVal, double newWeight )
    {
        return exponentialWeightedAverage( oldVal, newVal, newWeight );
    }
}


namespace re2uta
{
namespace atlascommander
{


BalanceCommander::
BalanceCommander( const FullBodyControllerInterface::Ptr & fullBodyController,
                  const urdf::Model & model,
                  AtlasLookup::Ptr const & atlasLookup,
                  const ros::Duration & dt )
    : m_atlasLookup( atlasLookup )
{
    m_fullBodyController = fullBodyController;
    m_solver.reset( new FullBodyPoseSolver( model, m_atlasLookup->lookupString( "l_foot" ),
                                                   m_atlasLookup->lookupString( "r_foot" ),
                                                   m_atlasLookup->lookupString( "utorso" ) ) );
    m_atlasCaptureNode.reset( new AtlasCaptureNode(   model ) );

    m_instantaneousCapturePointPublisher  = m_node.advertise<visualization_msgs::Marker>   ( "/atlas/instantaneousCapturePoint", 1 );
    m_propagatedCapturePointPublisher     = m_node.advertise<visualization_msgs::Marker>   ( "/atlas/propagatedCapturePoint",    1 );
    m_supportPolygonPublisher               = m_node.advertise<geometry_msgs::PolygonStamped>( "/atlas/support_polygon",           1 );
    m_supportPolygonCenterPublisher       = m_node.advertise<visualization_msgs::Marker>(    "/atlas/support_polygon_center",    1 );
    m_CoPPublisher                          = m_node.advertise<visualization_msgs::Marker>(    "/atlas/CoP",                       1 );
    m_CoMPublisher                          = m_node.advertise<visualization_msgs::Marker>(    "/atlas/CoM",                       1 );

    m_halfStepSize = 0.2;
    m_dt = dt;

    // Fg = 9.81 * 90 = 883N
    // Fg*distToEdge = Fd*stepLength
    // 883N * 0.13 = Fd*0.5
    // Fd = 182N
    m_forceThresh  = 50; // to be conservative // FIXME we should reduce this even more if possible



    m_controlLoopThread = boost::thread( &BalanceCommander::commandLoop, this );

    m_shutdown    = false;
}


BalanceCommander::
~BalanceCommander()
{
    m_shutdown    = true;
    //m_controlLoopThread.join();
}

void
BalanceCommander::
shutdown()
{
    m_shutdown = true;
}

void
BalanceCommander::
setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg )
{
    ROS_INFO_STREAM_THROTTLE( 5, "Setting local state" );
    boost::atomic_store( &m_lastStateMsg, latestStateMsg );
}



void
BalanceCommander::
walkTo( const WalkGoal::Ptr & walkGoal )
{
    //FIXME we're not handling the frame id!
    setDest( walkGoal->goalPose );
}



void
BalanceCommander::
setDest( const Eigen::Affine3d & dest )
{
    Eigen::Affine3dPtr destPtr( new Eigen::Affine3d );
    *destPtr = dest;

    boost::atomic_store( &m_walkDestCmd, destPtr );
}




void
BalanceCommander::
publishOdomToRoot( const Eigen::Affine3d & baseFootInOdom,
                   const std::string &      baseFootFrameId,
                   const Eigen::VectorXd &  jointPositions  )
{
//    Eigen::Affine3d baseFootToTreeRoot;
//    baseFootToTreeRoot = m_solver->getTransformFromTo( baseFootFrameId, "root", jointPositions );
//
//    Eigen::Affine3d odomInBaseFoot;
//    odomInBaseFoot = baseFootInOdom.inverse();
//
//    Eigen::Affine3d odomInTreeRootXform;
//    odomInTreeRootXform = baseFootToTreeRoot * odomInBaseFoot;

    tf::Transform odomToTreeRoot;
    tf::transformEigenToTF( baseFootInOdom.inverse(), odomToTreeRoot );
    m_tfBroadcaster.sendTransform( tf::StampedTransform( odomToTreeRoot,
                                                         ros::Time::now(),
                                                         baseFootFrameId,
                                                         "odom"  ));
}


StepPose::Ptr
BalanceCommander::
moveStepPoseToNewOdom( const StepPose::Ptr & currentStepPose )
{
    StepPose::Ptr newStepPose( new StepPose( *currentStepPose ) );

    Eigen::Affine3d odomInBaseFoot;
    odomInBaseFoot.translation() = (currentStepPose->baseFootPose().translation() + currentStepPose->swingFootPose().translation())/2;

    Eigen::Quaterniond baseQuat(  currentStepPose->baseFootPose().rotation()  );
    Eigen::Quaterniond swingQuat( currentStepPose->swingFootPose().rotation() );

    odomInBaseFoot.linear() = baseQuat.slerp( 0.5, swingQuat ).toRotationMatrix();

    newStepPose->baseFootPose()  = odomInBaseFoot.inverse() * currentStepPose->baseFootPose() ;
    newStepPose->swingFootPose() = odomInBaseFoot.inverse() * currentStepPose->swingFootPose();
    newStepPose->comPose         = odomInBaseFoot.inverse() * currentStepPose->comPose        ;

    return newStepPose;
}


void
BalanceCommander::
setStepPlannerType( StepPlannerType stepPlanner )
{
    ROS_INFO_STREAM_THROTTLE(1, "Balance commander doesn't do walking" );
}


void
BalanceCommander::
commandLoop()
{
    std::string swingFootFrameId;
    std::string baseFootFrameId ;
    atlas_msgs::AtlasState::ConstPtr lastStateMsg;
    Eigen::VectorXd jointPositions;
    Foot            swingFoot = LEFT_FOOT;


    std::string lFootStr = m_atlasLookup->lookupString( "l_foot" );
    std::string rFootStr = m_atlasLookup->lookupString( "r_foot" );
    ros::NodeHandle node;
    ros::Publisher lFootTrajPub = node.advertise<nav_msgs::Path>( std::string( "/walkTrajectory/" ) + lFootStr, 5, true );
    ros::Publisher rFootTrajPub = node.advertise<nav_msgs::Path>( std::string( "/walkTrajectory/" ) + rFootStr, 5, true );
    ros::Publisher comTrajPub   = node.advertise<nav_msgs::Path>( "/walkTrajectory/com",    5, true );


    nav_msgs::Path lFootTraj;
    nav_msgs::Path rFootTraj;
    nav_msgs::Path comTraj;


    lFootTraj.header.frame_id = "/odom";
    rFootTraj.header.frame_id = "/odom";
    comTraj.header.frame_id   = "/odom";

    re2::VisualDebugPublisher vizDebug( "full_body_controler_viz_debug", "odom" );

    while( !(lastStateMsg = boost::atomic_load( &m_lastStateMsg )) && !m_shutdown && ros::ok() )
    {
        ROS_WARN_STREAM_THROTTLE( 2, "No atlas state available, waiting to start BalanceCommander command loop until seen" );
        m_dt.sleep();
    }

    StepPose::Ptr currentStepPose( new StepPose( otherFoot(swingFoot) ) );

    swingFootFrameId = footToFrameId( swingFoot,            m_atlasLookup );
    baseFootFrameId  = footToFrameId( otherFoot(swingFoot), m_atlasLookup );

    jointPositions   = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );

    //FIXME need IMU rotation correction
    currentStepPose->baseFootPose()  = Eigen::Affine3d::Identity();
    currentStepPose->swingFootPose() = m_solver->getPose(         baseFootFrameId, swingFootFrameId, jointPositions );
    currentStepPose->comPose         = m_solver->getCenterOfMass( baseFootFrameId,                   jointPositions );
    currentStepPose = moveStepPoseToNewOdom( currentStepPose );

    // Iterations to be used to throttle publishing
    int pubIterNo = 0;

    while( ros::ok() && !m_shutdown )
    {
        //FIXME verify
        Eigen::Affine3d walkSource;
        walkSource = Eigen::Affine3d::Identity(); // start at 0,0,0 for now

        Eigen::Affine3dPtr walkDest;
        walkDest      = boost::atomic_load( &m_walkDestCmd ); //, walkDest ); // represents transform between start and dest between feet
        lastStateMsg  = boost::atomic_load( &m_lastStateMsg );


        if( !lastStateMsg )
        {
            m_dt.sleep();
            ROS_WARN_THROTTLE( 2, "NO STATE MESSAGE!" );
            continue;
        }

        swingFootFrameId = footToFrameId( swingFoot,            m_atlasLookup );
        baseFootFrameId  = footToFrameId( otherFoot(swingFoot), m_atlasLookup );
        jointPositions   = m_atlasLookup->cmdToTree( parseJointPositions(  lastStateMsg ) );

        publishOdomToRoot( currentStepPose->baseFootPose(), baseFootFrameId, jointPositions );


        StepPose::Ptr currentStepPose( new StepPose( otherFoot(swingFoot) ) );
        //FIXME need IMU rotation correction
        currentStepPose->baseFootPose()  = Eigen::Affine3d::Identity();
        currentStepPose->swingFootPose() = m_solver->getPose(         baseFootFrameId, swingFootFrameId, jointPositions );
        currentStepPose->comPose         = m_solver->getCenterOfMass( baseFootFrameId,                   jointPositions );

        currentStepPose = moveStepPoseToNewOdom( currentStepPose );

        publishOdomToRoot( currentStepPose->baseFootPose(), baseFootFrameId, jointPositions );


        // Step if capture point outside support polygon

        m_atlasCaptureNode->updateMemberData( lastStateMsg );

        if( !m_atlasCaptureNode->isStateCaptured(  lastStateMsg, m_instantaneousCapturePoint ) )
        {
//            ROS_INFO_STREAM( "Taking a step ###" );
        }



        std::string     baseName;

        if( m_atlasCaptureNode->getBaseName() == lFootStr )
        {
              baseName            = lFootStr;
              m_l_footPos = Eigen::Affine3d::Identity();
              m_r_footPos = m_solver->getTransformFromTo( rFootStr , lFootStr, jointPositions );
        }
        else
        {
              baseName            = rFootStr;
              m_r_footPos = Eigen::Affine3d::Identity();
              m_l_footPos = m_solver->getTransformFromTo( lFootStr, rFootStr, jointPositions );
        }


        // Base can be changed now
        m_fullBodyController->switchBaseFoot( baseName );

        /*
         * This function will calculate the propagated Capture Point (CP)
         */
        m_atlasCaptureNode->updateInstantaneousCapturePoint( lastStateMsg    );
        m_atlasCaptureNode->updateCOM( baseName,  jointPositions );
        m_atlasCaptureNode->updatePropagatedCapturePoint(    lastStateMsg, 1 );
        m_atlasCaptureNode->updateCoP(lastStateMsg->l_foot, lastStateMsg->r_foot, m_l_footPos.translation(), m_r_footPos.translation() );
//        m_centerOfSupport = m_atlasCaptureNode->getSupportPolygonCenter();
        if( m_atlasCaptureNode->m_supportState == SUPPORT_DOUBLE )
        {
            //FIXME Hack to avoid pcl crash
            Eigen::Affine3d supportCenter = interpolate( m_l_footPos, m_r_footPos, 0.5 );
            supportCenter = Eigen::Translation3d( 0.05, 0, 0 ) * supportCenter;
            m_centerOfSupport = supportCenter.translation();
        }
        else
            m_centerOfSupport = m_atlasCaptureNode->getSupportPolygonCenter();



        m_COM = m_atlasCaptureNode->getComProjection( lastStateMsg->orientation );
        m_CoP = m_atlasCaptureNode->getCoP();

//        ROS_WARN_STREAM( m_atlasCaptureNode->getComProjection( lastStateMsg->orientation ) );



        visualization_msgs::Marker centroid_marker;

        centroid_marker.header.stamp = ros::Time::now();
        centroid_marker.header.frame_id = baseName;
        centroid_marker.type = visualization_msgs::Marker::SPHERE;
        centroid_marker.pose.position.x = m_centerOfSupport(0);
        centroid_marker.pose.position.y = m_centerOfSupport(1);
        centroid_marker.pose.position.z = m_centerOfSupport(2);
        //centroid_marker.pose.orientation.
            //tf::quaternionTFToMsg(tf_to_support_.getRotation(), com_marker.pose.orientation);
        centroid_marker.scale.x = 0.03;
        centroid_marker.scale.y = 0.03;
        centroid_marker.scale.z = 0.005;
        centroid_marker.color.a = 1.0;
        centroid_marker.color.g = 1.0;
        centroid_marker.color.r = 0.0;
        centroid_marker.color.b = 0.0;




        if( pubIterNo%10 == 0 )
        {
            // These will publish the capture points and support polygon
    //        m_instantaneousCapturePointPublisher.publish( m_atlasCaptureNode->getInstantaneousCapturePointMarker() );
    //        m_propagatedCapturePointPublisher.publish(    m_atlasCaptureNode->getPropagatedCapturePointMarker() );
            m_supportPolygonPublisher.publish(            m_atlasCaptureNode->getSupportPolygonMarker() );
            m_supportPolygonCenterPublisher.publish(      m_atlasCaptureNode->getSupportPolygonCenterMarker() );
            m_CoPPublisher.publish(                        m_atlasCaptureNode->getCoPMarker( ) /*m_fullBodyController->publishOppFootInBase()*/ );
            m_CoMPublisher.publish(                       m_atlasCaptureNode->getComProjectedMarker( lastStateMsg->orientation ) );
        }


        // COM position command generator
        Eigen::Vector3d desiredComPose = Eigen::Vector3d::Zero();

        Eigen::Vector3d comWeights( 1, 1, 0 );



        // COM z height
        double zCOM = 0.84;

        // Controller using COM reference
        desiredComPose      = m_centerOfSupport;
        desiredComPose(2)    = zCOM;



//        // Controller using CoP-COM offset
//
//        desiredComPose      = m_centerOfSupport - ( m_CoP - m_COM );
//        desiredComPose(2)    = zCOM;



        // Setting pelvis orientation constraint
        // FIXME does not seem to work
        FrameGoal::Ptr frameGoal( new FrameGoal );

        frameGoal->baseFrameId  = baseName;
        frameGoal->goalFrameId  = baseName; // TODO should this be in base or swing
        frameGoal->tipFrameId   = m_atlasLookup->lookupString( "pelvis" );

        frameGoal->goalPose     = Eigen::Affine3d::Identity();

        Eigen::VectorXd pelvisConstraint = Eigen::VectorXd::Zero( 6 );
        pelvisConstraint.head(3) = Eigen::Vector3d::Zero();
        pelvisConstraint.tail(3) = Eigen::Vector3d( 0.01, 0.05, 0.01 );

        frameGoal->weightMatrix = pelvisConstraint.asDiagonal();

        m_fullBodyController->setFrameGoal( frameGoal );





//        // Setting opposite foot constraint
//        // FIXME does not seem to work
//        FrameGoal::Ptr frameGoal( new FrameGoal );
//
//        frameGoal->baseFrameId  = baseName;
//        frameGoal->goalFrameId  = baseName; // TODO should this be in base or swing
//        frameGoal->tipFrameId   = "pelvis";
//
//        frameGoal->goalPose     = Eigen::Affine3d::Identity();
//
//        Eigen::VectorXd pelvisConstraint = Eigen::VectorXd::Zero( 6 );
//        pelvisConstraint.head(3) = Eigen::Vector3d::Zero();
//        pelvisConstraint.tail(3) = Eigen::Vector3d( 0.01, 0.05, 0.01 );
//
//        frameGoal->weightMatrix = pelvisConstraint.asDiagonal();
//
//        m_fullBodyController->setFrameGoal( frameGoal );






//        ROS_WARN_STREAM( "COM Position : " << m_centerOfSupport.transpose() << std::endl );

        m_fullBodyController->setCenterOfMassGoal( desiredComPose /*Eigen::Vector3d( 0.04, -0.08, 0 )*/, comWeights ); //FIXME pick better comz weight







    }
}


FrameGoalGains::Ptr
BalanceCommander::
buildActuationGoal( Foot foot, double factor )
{
    const int rollIndex = 3;

    FrameGoalGains::Ptr footGoalGains( new FrameGoalGains );
    footGoalGains->tipFrameId()                    = footToFrameId( foot, m_atlasLookup );
    *footGoalGains = *m_fullBodyController->defaultFrameGains();
    footGoalGains->posKp().segment( rollIndex, 2 ) *= factor;
    return footGoalGains;
}

bool
BalanceCommander::
highFootForce( const atlas_msgs::AtlasState::ConstPtr & atlasStateMsg, Foot foot, ros::Duration dt )
{
    if( !atlasStateMsg )
        return false;

    Eigen::Vector3d force;
    if( foot == LEFT_FOOT )
        force =  Eigen::Vector3dConstMap( &atlasStateMsg->l_foot.force.x );
    else
        force =  Eigen::Vector3dConstMap( &atlasStateMsg->r_foot.force.x );

    //FIXME Filter with more samples
    m_filteredForce = lowPassFilter( m_filteredForce, force, dt.toSec()/m_dt.toSec() );

    if( m_filteredForce.norm() > m_forceThresh )
        return true;

    return false;
}



}
}

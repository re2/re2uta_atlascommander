/*
 * NeuralNetworkController.cpp
 *
 *  Created on: Jun 4, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/AtlasCommander/NeuralNetworkJointController.hpp>
#include <ros/ros.h>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/foreach.hpp>



namespace re2uta
{
namespace atlascommander
{



//typedef boost::tuple<std::string, std::string>    StringPair;
typedef std::list<boost::shared_ptr<urdf::Link>>  LinkChain;
typedef std::list<LinkChain>                     ChainList;

//std::list<boost::tuple<std::string,std::string>>
void
findEndpoints( boost::shared_ptr<urdf::Link> & link, LinkChain * currentChain, ChainList & chainList  )
{
    typedef boost::shared_ptr<urdf::Link>   LinkPtr;
    typedef boost::shared_ptr<urdf::Joint>  JointPtr;
    typedef std::map<std::string, LinkPtr>  LinkMap;

    if( currentChain != NULL )
        currentChain->push_back( link );

    bool isBranch = false;
    BOOST_FOREACH( LinkPtr & childLink, link->child_links )
    {
        if( link->child_joints.size() < 2 || childLink->parent_joint->type == urdf::Joint::FIXED )
        {
            findEndpoints( childLink, currentChain, chainList );
        }
        else
        {
            LinkChain chain;
            chainList.push_back( chain );

            findEndpoints( childLink, &chainList.back(), chainList );
        }
    }
}


ChainList
findEndpoints( urdf::Model & urdfModel )
{
    ChainList chainList;
    findEndpoints( urdfModel.root_link_, NULL, chainList );

    return chainList;
}



void guessRanges( boost::array<boost::tuple<int,int>,5> & ranges, urdf::Model & urdfModel, re2uta::AtlasLookup::Ptr atlasLookup )
{
    typedef std::multimap<double,LinkChain> MassLinkChainMap;

    ChainList chainList = findEndpoints( urdfModel );
    MassLinkChainMap massChainMap;

    // Order by mass for guessing
    BOOST_FOREACH( LinkChain & chain, chainList )
    {
        double mass = 0;
        if( chain.back()->parent_joint->type != urdf::Joint::FIXED )
        {
            BOOST_FOREACH( boost::shared_ptr<urdf::Link> link, chain )
            {
                mass += link->inertial->mass;
            }

            massChainMap.insert( make_pair( mass, chain ) );
        }
    }

    ROS_WARN( "Guessing chains for neural network controllers, order matters" );
    int index = 0;
    BOOST_FOREACH( MassLinkChainMap::value_type & entry, massChainMap )
    {
        LinkChain & chain = entry.second;

        boost::tuple<int,int> range;
        range.get<0>() = atlasLookup->jointNameToJointIndex( chain.front()->parent_joint->name.c_str() );
        range.get<1>() = atlasLookup->jointNameToJointIndex( chain.back()->parent_joint->name.c_str() );

        //if( range != ranges[index] )
        {
//            ROS_WARN( "Changing range from: %i, %i to...", ranges[index].get<0>(), ranges[index].get<1>() );
            ROS_WARN( "Guessing Chain: %i, as from %s to %s", index, chain.front()->parent_joint->name.c_str(), chain.back()->parent_joint->name.c_str() );
            ROS_INFO( "indices: %i, %i, len: %lu, mass: %f", range.get<0>(), range.get<1>(), chain.size(), entry.first );
        }

        ranges[index] = range;
        ++index;
    }
}


NeuralNetworkJointController::
NeuralNetworkJointController( const ros::Duration & commandLoopPeriod, boost::condition_variable & syncVar, AtlasLookup::Ptr const & atlasLookup )
    : JointController( syncVar )
    , m_atlasLookup( atlasLookup )
{
    std::string urdfString;
    m_node.getParam( "/robot_description", urdfString );

    urdf::Model urdfModel;
    urdfModel.initString( urdfString );

    typedef atlas_msgs::AtlasState Atlas;

    typedef boost::array<boost::tuple<int,int>,5> Ranges;
    Ranges ranges;
    ranges[0] = boost::tuple<int,int>( Atlas::l_arm_shy, Atlas::l_arm_wrx ); // warning implicit order!
    ranges[1] = boost::tuple<int,int>( Atlas::r_arm_shy, Atlas::r_arm_wrx );
    ranges[2] = boost::tuple<int,int>( Atlas::l_leg_hpz, Atlas::l_leg_akx );
    ranges[3] = boost::tuple<int,int>( Atlas::r_leg_hpz, Atlas::r_leg_akx );
    ranges[4] = boost::tuple<int,int>( Atlas::back_bkz,  Atlas::back_bkx  );


    XmlRpc::XmlRpcValue paramValueList;
    if( m_node.getParam( "/drc/nn_ranges", paramValueList ) )
    {
//        BOOST_FOREACH( XmlRpc::XmlRpcValue::ValueStruct::value_type const & pair, paramValueList )
        for( XmlRpc::XmlRpcValue::ValueStruct::iterator itor = paramValueList.begin(); itor != paramValueList.end(); ++itor )
        {
            XmlRpc::XmlRpcValue::ValueStruct::value_type & pair = *itor;
            std::string const & key    = pair.first;
            XmlRpc::XmlRpcValue joints = pair.second;
            boost::tuple<int,int> range;

            ROS_INFO( "Chain: %s", key.c_str() );
            if( joints.size() == 2 )
            {
                range.get<0>() = m_atlasLookup->jointNameToJointIndex( (std::string &)(joints[0]) );
                range.get<1>() = m_atlasLookup->jointNameToJointIndex( (std::string &)(joints[1]) );
                if( key == "left_arm"  ) ranges[0] = range;
                if( key == "right_arm" ) ranges[1] = range;
                if( key == "left_leg"  ) ranges[2] = range;
                if( key == "right_leg" ) ranges[3] = range;
                if( key == "torso"     ) ranges[4] = range;

                ROS_INFO( "Start: %i, %s", range.get<0>(), ((std::string &)(joints[0])).c_str() );
                ROS_INFO( "End:   %i, %s", range.get<1>(), ((std::string &)(joints[1])).c_str() );
            }
        }
    }
    else
    {
        ROS_INFO( "Didn't find: /drc/nn_ranges, attempting guess instead" );
        guessRanges( ranges, urdfModel, m_atlasLookup );
    }



    double armKappa           =  0.09;
    double armKp              =  0;
    double armKd              = 10;
    double armHiddenLayers    =  8;
    double armInLearningRate  = 25;
    double armOutLearningRate = 15;
    m_node.getParam( "/drc/nn/arm/kappa",             armKappa );
    m_node.getParam( "/drc/nn/arm/kp",                armKp    );
    m_node.getParam( "/drc/nn/arm/kd",                armKd    );
    m_node.getParam( "/drc/nn/arm/hidden_layers",     armHiddenLayers );
    m_node.getParam( "/drc/nn/arm/learning_rate_in",  armInLearningRate   );
    m_node.getParam( "/drc/nn/arm/learning_rate_out", armOutLearningRate );

    double legKappa           =  0.10;
    double legKp              =  0;
    double legKd              = 20;
    double legHiddenLayers    =  8;
    double legInLearningRate  = 25;
    double legOutLearningRate = 25;
    m_node.getParam( "/drc/nn/leg/kappa",             legKappa );
    m_node.getParam( "/drc/nn/leg/kp",                legKp    );
    m_node.getParam( "/drc/nn/leg/kd",                legKd    );
    m_node.getParam( "/drc/nn/leg/hidden_layers",     legHiddenLayers );
    m_node.getParam( "/drc/nn/leg/learning_rate_in",  legInLearningRate   );
    m_node.getParam( "/drc/nn/leg/learning_rate_out", legOutLearningRate  );

    double torsoKappa           =  0.30;
    double torsoKp              =  0;
    double torsoKd              = 50;
    double torsoHiddenLayers    =  8;
    double torsoInLearningRate  = 25;
    double torsoOutLearningRate = 25;
    m_node.getParam( "/drc/nn/torso/kappa",             torsoKappa );
    m_node.getParam( "/drc/nn/torso/kp",                torsoKp    );
    m_node.getParam( "/drc/nn/torso/kd",                torsoKd    );
    m_node.getParam( "/drc/nn/torso/hidden_layers",     torsoHiddenLayers );
    m_node.getParam( "/drc/nn/torso/learning_rate_in",  torsoInLearningRate   );
    m_node.getParam( "/drc/nn/torso/learning_rate_out", torsoOutLearningRate  );


    double posFilterGain = 30;
    double velFilterGain = 30;
    m_node.getParam( "/drc/nn/pos_filter_gain", posFilterGain );
    m_node.getParam( "/drc/nn/vel_filter_gain", velFilterGain );


    m_nnController.reset( new MultiNetworkController( m_atlasLookup->getNumCmdJoints(), posFilterGain, velFilterGain ) );
    m_nnController->addController( ranges[0],   armKappa,    armKp,   armKd, armHiddenLayers,   armInLearningRate,   armOutLearningRate );
    m_nnController->addController( ranges[1],   armKappa,    armKp,   armKd, armHiddenLayers,   armInLearningRate,   armOutLearningRate );
    m_nnController->addController( ranges[2],   legKappa,    legKp,   legKd, legHiddenLayers,   legInLearningRate,   legOutLearningRate );
    m_nnController->addController( ranges[3],   legKappa,    legKp,   legKd, legHiddenLayers,   legInLearningRate,   legOutLearningRate );
    m_nnController->addController( ranges[4], torsoKappa,  torsoKp, torsoKd, torsoHiddenLayers, torsoInLearningRate, torsoOutLearningRate );

    m_networkDamping = 50;
    m_node.getParam( "/drc/nn/damping", m_networkDamping );

    m_shutdown          = false;
    m_iteration         = 0;
    m_commandLoopPeriod = commandLoopPeriod;
    m_commandLoopThread = boost::thread( &NeuralNetworkJointController::commandLoop, this );
    m_jointCommandPub   = m_node.advertise<atlas_msgs::AtlasCommand>( "/atlas/atlas_command", 1 );
    m_kEfforts          = std::vector<uint8_t>( m_atlasLookup->getNumCmdJoints(), 255 );


    std::vector<double>      defaultDamping;
    std::vector<std::string> standardJointOrder;

    standardJointOrder = m_atlasLookup->getJointNameVector();

    BOOST_FOREACH( std::string const & jointName, standardJointOrder )
    {
        boost::shared_ptr<urdf::Joint> joint;
        joint = urdfModel.joints_[jointName];

        defaultDamping.push_back( joint->dynamics->damping );
    }

    m_defaultDampingTerms = defaultDamping;
}

NeuralNetworkJointController::
~NeuralNetworkJointController()
{
    m_shutdown = true;
//    m_commandLoopThread.join();
}


void
NeuralNetworkJointController::
setJointCommand( const atlas_msgs::AtlasCommandConstPtr & msg, ros::Time commandTime )
{
    boost::atomic_store( &m_currentCommand, msg );
    ROS_ERROR_STREAM_THROTTLE( 20, "NeuralNetworkJointController::setJointCommand currently ignoring commandTime" );
}

void
NeuralNetworkJointController::
setKEfforts( const std::vector<uint8_t> & kefforts )
{
    ROS_WARN( "Neural Network Joint Controller go kefforts, sending them and adjusting damping" );
    ROS_ASSERT_MSG( kefforts.size() == m_kEfforts.size(), "NeuralNetworkJointController::  Bad k effort size" );
    m_kEfforts = kefforts;

    atlas_msgs::SetJointDamping transaction;
    transaction.request.damping_coefficients.fill( m_networkDamping ); // FIXME this should be parameterized

    int index = 0;
    BOOST_FOREACH( const uint8_t & intScale, m_kEfforts )
    {
        double scale = intScale/255.0;
        transaction.request.damping_coefficients[ index ] =   (scale)   * transaction.request.damping_coefficients[ index ]
                                                            + (1-scale) * m_defaultDampingTerms[index];
        ++index;
    }


    m_setDampingSrv.call( transaction );
}


void
NeuralNetworkJointController::
commandLoop()
{
    ROS_INFO( "Starting command loop, period is: %2.3f", m_commandLoopPeriod.toSec() );

    m_setDampingSrv     = m_node.serviceClient<atlas_msgs::SetJointDamping>( "/atlas/set_joint_damping" );
    m_setDampingSrv.waitForExistence();

    atlas_msgs::SetJointDamping transaction;
    transaction.request.damping_coefficients.fill( m_networkDamping );

    m_setDampingSrv.call( transaction );


    using boost::format;
    atlas_msgs::AtlasCommand::ConstPtr currentCommand;
    atlas_msgs::AtlasState::ConstPtr   currentState;

    ros::Duration margin( 0.0001 );
    m_nextCommandTime = ros::Time::now() + m_commandLoopPeriod;

    atlas_msgs::AtlasCommand::Ptr premadeCmdMsg( new atlas_msgs::AtlasCommand() );
    {
        atlas_msgs::AtlasCommand::Ptr emptyCmdMsg   = m_atlasLookup->createEmptyJointCmdMsg();
        atlas_msgs::AtlasCommand::Ptr defaultCmdMsg = m_atlasLookup->createDefaultJointCmdMsg( m_node );

        *premadeCmdMsg = *emptyCmdMsg;
    }

    boost::mutex                     mutex;
    boost::unique_lock<boost::mutex> lock( mutex );

    while( ros::ok() && !m_shutdown )
    {
        m_syncVar.wait( lock ); //FIXME this should be timed wait, but how to do it with ros time?
        ros::Duration( m_commandLoopPeriod.toSec() - std::fmod( ros::Time::now().toSec(), m_commandLoopPeriod.toSec() ) ).sleep();

        currentState   = boost::atomic_load( &m_latestStateMsg );
        currentCommand = boost::atomic_load( &m_currentCommand );
        if( !currentState || !currentCommand )
        {
            ROS_WARN_STREAM_THROTTLE( 5, "No current state continuing" );
            continue;
        }

        Eigen::VectorXd torques;

//        ROS_WARN_STREAM( "State in NN: " << *currentState );
//        ROS_WARN_STREAM( "State in Cmd: " << *currentCommand );

        m_nnController->setStateMsg(   currentState   );
        m_nnController->setCommandMsg( currentCommand );
        torques = m_nnController->calcTorques( m_commandLoopPeriod.toSec() );

        int  i        = 0;
        bool foundNan = false;
        BOOST_FOREACH( double torque, re2::toStd(torques) )
        {
            if( isnan( torque ) )
                foundNan = true;
            ++i;
        }
        if( foundNan )
        {
            ROS_ERROR( "Torque %i is nan!" );
            continue;
        }

        if( torques.size() == 0 )
        {
            ROS_ERROR( "Neural Network Controller returned torques of size 0" );
            continue;
        }

        atlas_msgs::AtlasCommand::Ptr atlasCommandMsg( new atlas_msgs::AtlasCommand );

        *atlasCommandMsg = *premadeCmdMsg;
        atlasCommandMsg->effort   = re2::toStd( torques );
        atlasCommandMsg->desired_controller_period_ms = 5;
        atlasCommandMsg->k_effort = m_kEfforts;

        m_jointCommandPub.publish( atlasCommandMsg );

        ++m_iteration;
    }
}



}
}

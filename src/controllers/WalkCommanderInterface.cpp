/*
 * WalkCommanderInterface.cpp
 *
 *  Created on: Jun 19, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/AtlasCommander/WalkCommanderInterface.hpp>

namespace re2uta
{
namespace atlascommander
{


void
WalkCommanderInterface::
walkTo( const atlascommander::WalkGoalVectorPtr& walkGoals ) //FIXME violating concept of interface
{
    if( walkGoals && !walkGoals->empty() )
    {
        walkTo( walkGoals->back() );
    }
    else
    {
        ROS_ERROR( "Empty or missing walk Goals" );
    }
}


}
}

/*
 * DefaultJointController.cpp
 *
 *  Created on: May 13, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/AtlasCommander/DefaultJointController.hpp>
#include <atlas_msgs/SetJointDamping.h>
#include <ros/ros.h>



namespace re2uta
{
namespace atlascommander
{


DefaultJointController::
DefaultJointController( const ros::Duration & commandLoopPeriod, boost::condition_variable & syncVar, AtlasLookup::Ptr const & atlasLookup  )
    : JointController( syncVar )
    , m_atlasLookup( atlasLookup )
{
    m_shutdown          = false;
    m_iteration         = 0;
    m_commandLoopPeriod = commandLoopPeriod;
    m_commandLoopThread = boost::thread( &DefaultJointController::commandLoop, this );
    m_jointCommandPub   = m_node.advertise<atlas_msgs::AtlasCommand>( "/atlas/atlas_command", 1 );
    m_kEfforts          = std::vector<uint8_t>( m_atlasLookup->getNumCmdJoints(), 255 );
}

DefaultJointController::
~DefaultJointController()
{
    m_shutdown = true;
//    m_commandLoopThread.join();
}


void
DefaultJointController::
setJointCommand( const atlas_msgs::AtlasCommandConstPtr & msg, ros::Time commandTime )
{
    boost::atomic_store( &m_currentCommand, msg );
//    ROS_ERROR_STREAM_THROTTLE( 20, "setJointCommand currently ignoring commandTime (throttled message)" );
}


void
DefaultJointController::
setKEfforts( const std::vector<uint8_t> & kefforts )
{
    ROS_ASSERT_MSG( kefforts.size() == m_kEfforts.size(), "DefaultJointController::  Bad k effort size" );
    m_kEfforts = kefforts;
}


void
DefaultJointController::
commandLoop()
{
    ROS_INFO( "Starting command loop, period is: %2.3f", m_commandLoopPeriod.toSec() );


    m_setDampingSrv     = m_node.serviceClient<atlas_msgs::SetJointDamping>( "/atlas/set_joint_damping" );
    m_setDampingSrv.waitForExistence();

    atlas_msgs::SetJointDamping transaction;
    transaction.request.damping_coefficients.fill( 30 );

    BOOST_FOREACH( double value, transaction.request.damping_coefficients )
    {
        ROS_WARN_STREAM( "Damping : " << value );
    }

    m_setDampingSrv.call( transaction );


    using boost::format;
    atlas_msgs::AtlasCommand::ConstPtr currentCommand;
    atlas_msgs::AtlasState::ConstPtr   currentState;

    ros::Duration margin( 0.0001 );
    m_nextCommandTime = ros::Time::now() + m_commandLoopPeriod;

    boost::mutex                     mutex;
    boost::unique_lock<boost::mutex> lock( mutex );

    while( ros::ok() && !m_shutdown )
    {
        m_syncVar.wait( lock ); //FIXME this should be timed wait, but how to do it with ros time?

//        currentState = boost::atomic_load( &m_latestStateMsg );
//        if( !currentState )
//        {
//            ROS_WARN_STREAM_THROTTLE( 5, "No current state continuting" );
//            continue;
//        }
//        m_nextCommandTime = currentState->header.stamp + m_commandLoopPeriod - margin;
//        ros::Time::sleepUntil( m_nextCommandTime );

        currentCommand = boost::atomic_load( &m_currentCommand );
        if( currentCommand )
        {
//            ROS_INFO_STREAM_THROTTLE( 5, "Incomplete AtlasCommander::lowLevelCommandLoop()" );
            atlas_msgs::AtlasCommand modCurrentCommand;
            modCurrentCommand = *currentCommand;

            modCurrentCommand.k_effort = m_kEfforts;

            m_jointCommandPub.publish( modCurrentCommand );
        }

//        ROS_INFO_STREAM_THROTTLE( 5, "Delay was: " << (ros::Time::now() - m_nextCommandTime).toSec() << " vs margin of: " << margin.toSec() );
        // FIXME should reset margin here based on actually delay

        ros::Duration( std::fmod( ros::Time::now().toSec(), m_commandLoopPeriod.toSec() ) ).sleep();
        ++m_iteration;
    }
}



}
}

/*
 * ros_utils.hpp
 *
 *  Created on: Apr 22, 2013
 *      Author: andrew.somerville
 */

#ifndef ROS_UTILS_HPP_
#define ROS_UTILS_HPP_

#include <geometry_msgs/Vector3Stamped.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/Int8.h>
#include <std_msgs/Int16.h>
#include <Eigen/Core>
#include <string>

namespace re2uta // fixme this should be a completely separate package
{

geometry_msgs::Vector3StampedPtr
buildVec3Stamped( const std::string & frameId, const Eigen::Vector3d & vec );

std_msgs::UInt8Ptr
buildUint8( unsigned int value );

std_msgs::Int8Ptr
buildInt8( unsigned int value );

std_msgs::UInt16Ptr
buildUint16( unsigned int value );

std_msgs::Int16Ptr
buildInt16( unsigned int value );

}

#endif /* ROS_UTILS_HPP_ */

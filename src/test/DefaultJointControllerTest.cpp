/*
 * DefaultJointControllerTest.cpp
 *
 *  Created on: May 14, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/AtlasCommander/DefaultJointController.hpp>
#include <boost/random.hpp>
#include <ros/ros.h>
#include <boost/foreach.hpp>

using namespace re2uta::atlascommander;

class DefaultJointControllerTest
{
    public:
        ros::NodeHandle m_node;

    public:
        void go()
        {
            boost::condition_variable foo;

            DefaultJointController defaultJointController( ros::Duration(0.1), foo );

            atlas_msgs::AtlasState::Ptr   atlasState(   new atlas_msgs::AtlasState   );
            atlas_msgs::AtlasCommand::Ptr atlasCommand( new atlas_msgs::AtlasCommand );

            atlasState->position.resize( 10 );
            atlasState->velocity.resize( 10 );

            atlasCommand->position.resize( 10 );
            atlasCommand->velocity.resize( 10 );

            BOOST_FOREACH( float & position, atlasState->position )
            {
                position = 0.1;
            }

            BOOST_FOREACH( float & velocity, atlasState->velocity )
            {
                velocity = 0.1;
            }

            BOOST_FOREACH( double & position, atlasCommand->position )
            {
                position = 0.1;
            }

            BOOST_FOREACH( double & velocity, atlasCommand->velocity )
            {
                velocity = 0.1;
            }

            defaultJointController.setJointCommand( atlasCommand, ros::Time::now() );

            boost::mt19937 rng;
            boost::uniform_real<double> u(0, 0.5);
            boost::variate_generator<boost::mt19937&, boost::uniform_real<double> > gen(rng, u);

            while( ros::ok() )
            {
                ros::spinOnce();

                ros::Duration( gen() ).sleep();
                ROS_INFO_STREAM( "sleep done" );

                atlasState->header.stamp = ros::Time::now();
                defaultJointController.setLatestStateMsg( atlasState );
                foo.notify_all();
            }
        }
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "default_joint_controller_test" );
    ROS_INFO( "Starting default_joint_controller_test" );

    DefaultJointControllerTest defaultJointControllerTest;

    defaultJointControllerTest.go();

    return 0;
}

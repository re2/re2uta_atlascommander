/*
 * PreviewWalkCommanderTest.cpp
 *
 *  Created on: Jun 5, 2013
 *      Author: andrew.somerville, Gus Atmeh
 */

#include <re2uta/AtlasCommander/FullBodyControllerInterface.hpp>
#include <re2uta/AtlasCommander/PreviewWalkCommander.hpp>
#include <re2uta/walking/StepGenerator.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <robot_state_publisher/robot_state_publisher.h>
#include <re2/visutils/marker_util.h>

#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>

#include <eigen_conversions/eigen_msg.h>

#include <ros/ros.h>

#include <boost/foreach.hpp>

#include <string>
#include <iostream>


using namespace re2uta::atlascommander;
using namespace re2uta;

namespace
{
    double nonNaN( const double & value )
    {
        if( std::isinf(value) || std::isnan(value) )
            return 0;
        else
            return value;
    }
}

class TestFullBodyController : public FullBodyControllerInterface
{
        typedef std::map<std::string, double> JointPositionMap;

        ros::NodeHandle                 m_node;
        re2::VisualDebugPublisher::Ptr  m_vizDebug;
        re2uta::FullBodyPoseSolver::Ptr m_poseSolver;
        atlas_msgs::AtlasState::Ptr     m_outputState;
        ros::Publisher                  m_atlasStatePub;
        JointPositionMap                m_jointPositionMap;
        re2uta::AtlasLookup::Ptr        m_atlasLookup;
        Eigen::VectorXd                 m_jointPositions;
        boost::shared_ptr<robot_state_publisher::RobotStatePublisher> m_robotStatePublisher;
        ros::Timer                      m_updateTimer;



    public:
        TestFullBodyController()
        {
            m_vizDebug.reset( new re2::VisualDebugPublisher( "preview_walk_controller_test" ) );
            std::string urdfString;
            m_node.getParam( "/robot_description", urdfString );

            urdf::Model model;
            model.initString( urdfString );
            m_atlasLookup.reset( new re2uta::AtlasLookup( model ) );
            m_poseSolver.reset(  new FullBodyPoseSolver(  model, m_atlasLookup->lookupString( "l_foot" ),
                                                                 m_atlasLookup->lookupString( "r_foot" ),
                                                                 m_atlasLookup->lookupString( "utorso" ) ) );


            KdlTreeConstPtr tree = m_atlasLookup->getTree();

            m_robotStatePublisher.reset( new robot_state_publisher::RobotStatePublisher( *tree ) );
            m_atlasStatePub          = m_node.advertise<atlas_msgs::AtlasState>( "/atlas/atlas_state", 1 );

            double legBend           = 0.4;
            m_jointPositions = m_atlasLookup->calcDefaultTreeJointAngles( legBend );
            ROS_INFO_STREAM( "Default joint angles\n" << m_jointPositions );

            BOOST_FOREACH( const SolverConstraint::Ptr & constraint,  m_poseSolver->constraints() )
            {
                constraint->setGoal( constraint->getCurrentOutput( m_jointPositions ) );
                ROS_INFO_STREAM( "Setting Constraint output: " << constraint->name()  );
                ROS_INFO_STREAM( "  current values: " << constraint->getCurrentOutput( m_jointPositions ).transpose() );
                ROS_INFO_STREAM( "  goal output:    " << constraint->goal().transpose() );

                CartesianConstraint::Ptr cartesianConstraint;
                cartesianConstraint = boost::dynamic_pointer_cast<CartesianConstraint>( constraint );
                if( cartesianConstraint )
                {
                    cartesianConstraint->posKp()  = defaultFrameGains()->posKp().head(  constraint->dimensions() );
                    cartesianConstraint->posKd()  = defaultFrameGains()->posKd().head(  constraint->dimensions() );
                    cartesianConstraint->velKp()  = defaultFrameGains()->velKp().head(  constraint->dimensions() );
                    cartesianConstraint->velMax() = defaultFrameGains()->velMax().head( constraint->dimensions() );
                }
            }

            m_poseSolver->comConstraint()->    setBaseElement( "l_foot" );
            m_poseSolver->postureConstraint()->setBaseElement( "l_foot" );
            m_poseSolver->oppFootConstraint()->setBaseElement( "l_foot" );
            m_poseSolver->oppFootConstraint()->setTipElement(  "r_foot" );


            m_poseSolver->postureConstraint()->goalWeightVector()       = Eigen::VectorXd::Zero(6);
            m_poseSolver->postureConstraint()->goalWeightVector()(3)    = 0.0001; // rot x
            m_poseSolver->postureConstraint()->goalWeightVector()(4)    = 0.001;  // rot y
            m_poseSolver->postureConstraint()->goalWeightVector()(5)    = 0.0001; // rot z
            m_poseSolver->optimalAngleConstraint()->goalWeightVector()  = Eigen::VectorXd::Ones(m_poseSolver->optimalAngleConstraint()->dimensions()) * 0.01;
//
//            m_poseSolver->comConstraint()->         goalWeightVector() = Eigen::VectorXd::Zero(3);
//            m_poseSolver->oppFootConstraint()->     goalWeightVector() = Eigen::VectorXd::Zero(6);
//            m_poseSolver->postureConstraint()->     goalWeightVector() = Eigen::VectorXd::Zero(6);
//            m_poseSolver->optimalAngleConstraint()->goalWeightVector() = Eigen::VectorXd::Zero(m_poseSolver->optimalAngleConstraint()->dimensions());

            m_updateTimer = m_node.createTimer( ros::Duration( 0.5 ), &TestFullBodyController::handleUpdateTimer, this );

            Eigen::VectorXd prevJointPositions = m_jointPositions;
            updateState();

            ROS_INFO_STREAM( "Diff = \n" << (m_jointPositions - prevJointPositions) );
        }

        virtual ~TestFullBodyController()
        {

        }


        void handleUpdateTimer( const ros::TimerEvent & timerEvent )
        {
            updateState();
        }

        void updateState()
        {
            atlas_msgs::AtlasState::Ptr outputState; //( new atlas_msgs::AtlasState ); // FIXME, we need to fill this out
            outputState.reset( new atlas_msgs::AtlasState );

            Eigen::VectorXd solution;
            solution = m_poseSolver->solvePose( m_jointPositions, 100 );

            if( solution.size() != 0 )
            {
                m_jointPositions = solution.unaryExpr( std::ptr_fun( nonNaN ) );
//                ROS_INFO_STREAM( "Solution = \n" << (m_jointPositions) );
            }

            outputState->position = re2::toStd( (Eigen::VectorXf)m_atlasLookup->treeToCmd( m_jointPositions ).cast<float>() );
            outputState->velocity = std::vector<float>( outputState->position.size(), 0 );

            int cmdJointId = 0;
            BOOST_FOREACH( const float & position,  outputState->position )
            {
                std::string jointName = m_atlasLookup->jointIndexToJointName( cmdJointId );
                m_jointPositionMap[ jointName ] = (double)position;
                ++cmdJointId;
            }

            m_jointPositionMap[ "hokuyo_joint" ] = 0;

//            outputState->l_foot.force.z = m_staticZForce;
//            outputState->r_foot.force.z = m_staticZForce;
//            outputState->header.stamp = ros::Time::now();

            m_robotStatePublisher->publishFixedTransforms();
            m_robotStatePublisher->publishTransforms( m_jointPositionMap, ros::Time::now() );

            if( outputState )
            {
//                ROS_INFO_STREAM_THROTTLE( 5, "Sending state!" );
                outputState->header.stamp = ros::Time::now();
                m_atlasStatePub.publish( outputState );
            }
        }


//        void setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg );
        void setCenterOfMassGoal( const Eigen::Vector3d & comLocation, const Eigen::Vector3d & comWeights = Eigen::Vector3d::Ones() )
        {
            m_poseSolver->comConstraint()->setGoal( comLocation );
            m_poseSolver->comConstraint()->setGoalWeightVector( comWeights );

            ROS_INFO_STREAM( "COM Goal set to:    " << comLocation.transpose() );
            ROS_INFO_STREAM( "COM weights set to: " << comWeights.transpose()  );

            Eigen::Vector4d color0( 1,1,0,1 );
            m_vizDebug->publishSegment( m_poseSolver->comConstraint()->getCurrentOutput( m_jointPositions ),
                                        comLocation, color0, 784999, m_poseSolver->comConstraint()->baseFrameId() );
            Eigen::Vector4d color1( 1,0,0,1 );
            m_vizDebug->publishTextAt( m_poseSolver->comConstraint()->name(), 0.05,
                                       comLocation, color1, 788999, m_poseSolver->comConstraint()->baseFrameId() );
            updateState();
        }

        virtual void setOppositeFootGoal( const Eigen::Affine3d & frame,  const Eigen::Vector6d & weights )
        {
            m_poseSolver->oppFootConstraint()->setGoal( re2uta::affineToTwist( frame ) );
            m_poseSolver->oppFootConstraint()->setGoalWeightVector( weights );

            ROS_INFO_STREAM( "Oppfoot constraint name:            " << m_poseSolver->oppFootConstraint()->name() );
            ROS_INFO_STREAM( "Oppfoot Goal translation set to:    " << frame.translation().transpose() );
            ROS_INFO_STREAM( "Oppfoot Goal roated x axis is:      " << (frame.rotation() * Eigen::XAxis3d).transpose() );
            ROS_INFO_STREAM( "Oppfoot Goal weight diagonal is:    " << weights.transpose() );


            Eigen::Vector4d color0( 1,1,0,1 );
            m_vizDebug->publishSegment( m_poseSolver->oppFootConstraint()->getCurrentOutput( m_jointPositions ),
                                        frame.translation(), color0, 745459, m_poseSolver->comConstraint()->baseFrameId() );
            Eigen::Vector4d color( 1,0,0,1 );
            m_vizDebug->publishTextAt( m_poseSolver->oppFootConstraint()->name(), 0.05,
                                       frame.translation(), color, 123123, m_poseSolver->oppFootConstraint()->baseFrameId() );

            updateState();
        }

        virtual void setFrameGoal(        const FrameGoal::Ptr &      frameGoal      )
        {
            ROS_ERROR_STREAM( "setFrameGoal shouldn't be being set, but rather setOppositeFootGoal" );
            ROS_INFO_STREAM( "Frame Goal baseFrameId:           " << frameGoal->baseFrameId );
            ROS_INFO_STREAM( "Frame Goal tipFrameId:            " << frameGoal->tipFrameId  );
            ROS_INFO_STREAM( "Frame Goal goalFrameId:           " << frameGoal->goalFrameId );
            ROS_INFO_STREAM( "Frame Goal translation set to:    " << frameGoal->goalPose.translation().transpose() );
            ROS_INFO_STREAM( "Frame Goal roated x axis is:      " << (frameGoal->goalPose.rotation() * Eigen::XAxis3d).transpose() );
            ROS_INFO_STREAM( "Frame Goal weight diagonal is:    " << frameGoal->weightMatrix.diagonal().transpose() );
            ROS_INFO_STREAM( "Frame Goal weight matrix is:    \n" << frameGoal->weightMatrix );

            Eigen::Vector4d color( 1,0,0,1 );
            m_vizDebug->publishTextAt( frameGoal->tipFrameId, 0.05, frameGoal->goalPose.translation(), color, 234234, frameGoal->baseFrameId );

//            m_poseSolver->comConstraint()->setGoal( comLocation );
//            m_poseSolver->comConstraint()->setGoalWeightVector( comWeights );
            updateState();
        }

        virtual void setFrameGoalGains(   const FrameGoalGains::Ptr & frameGoalGains )
        {
            ROS_WARN_STREAM( "set frame goal gains currently doesn't work correctly" );
            ROS_INFO_STREAM( "Frame Goal id      " << frameGoalGains->tipFrameId() );
            ROS_INFO_STREAM( "Frame Goal posKp   " << frameGoalGains->posKp() .transpose()  );
            ROS_INFO_STREAM( "Frame Goal posKd   " << frameGoalGains->posKd() .transpose()  );
            ROS_INFO_STREAM( "Frame Goal velKp   " << frameGoalGains->velKp() .transpose()  );
            ROS_INFO_STREAM( "Frame Goal velMax  " << frameGoalGains->velMax().transpose() );
            updateState();
        }

        virtual FrameGoalGains::Ptr  defaultFrameGains()
        {
            FrameGoalGains::Ptr frameGains( new FrameGoalGains );
            frameGains->posKp()  = Eigen::Vector6d::Ones() * 10;
            frameGains->posKd()  = Eigen::Vector6d::Ones() * 10;
            frameGains->velKp()  = Eigen::Vector6d::Ones() * 10;
            frameGains->velMax() = Eigen::Vector6d::Ones();

//            updateState();
            return frameGains;
        }

        void switchBaseFoot(      const std::string & baseFootFrameId )
        {
            ROS_INFO_STREAM( "New base foot:  " << baseFootFrameId );

            m_poseSolver->comConstraint()->setBaseElement( baseFootFrameId );
            m_poseSolver->oppFootConstraint()->setBaseElement( baseFootFrameId );

            if( baseFootFrameId == "l_foot" )
                m_poseSolver->oppFootConstraint()->setTipElement( "r_foot" );
            else
            if( baseFootFrameId == "r_foot" )
                m_poseSolver->oppFootConstraint()->setTipElement( "l_foot" );


        }


//        Eigen::Affine3d getConstraintState( const std::string & constraintName );

};


using namespace re2uta::atlascommander;

class PreviewWalkCommanderTest
{
    public:
        ros::NodeHandle           m_node;
        Eigen::Affine3d           m_dest;
        PreviewWalkCommander::Ptr m_previewWalkCommander;
        ros::Subscriber           m_stateSub;

    public:
        PreviewWalkCommanderTest()
        {
            std::string urdfString;
            m_node.getParam( "/robot_description", urdfString );

            urdf::Model urdfModel;
            urdfModel.initString( urdfString );

            m_previewWalkCommander.reset( new PreviewWalkCommander( TestFullBodyController::Ptr( new TestFullBodyController ),
                                                                    urdfModel,
                                                                    ros::Duration( 0.05 ),
                                                                    WalkCommanderInterface::NAO_PLANNER) );

            m_stateSub = m_node.subscribe( "/atlas/atlas_state", 1, &PreviewWalkCommanderTest::handleAtlasState, this );
        }


        void handleAtlasState( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg )
        {
//            ROS_INFO_STREAM_THROTTLE( 5,  "Received state!" );
            atlas_msgs::AtlasStatePtr modStateMsg( new atlas_msgs::AtlasState );
            *modStateMsg = *atlasStateMsg;
/*            modStateMsg->l_foot.force.z = 60;
            modStateMsg->r_foot.force.z = 60;*/

            m_previewWalkCommander->setLatestStateMsg( modStateMsg );
        }

        void go()
        {
            ros::Subscriber goalSub;
            goalSub = m_node.subscribe( "/commander/walkto", 1, &PreviewWalkCommanderTest::handleGoal, this );
            while( ros::ok() )
            {
                ros::spinOnce();
            }
        }

        void handleGoal( const geometry_msgs::PoseStampedConstPtr & poseMsg )
        {
            tf::poseMsgToEigen( poseMsg->pose, m_dest );

            m_previewWalkCommander->setDest( m_dest );
        }

};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "preview_walk_controller_test" );
    ROS_INFO( "Starting preview_walk_controller_test" );

    PreviewWalkCommanderTest previewWalkCommanderTest;

    previewWalkCommanderTest.go();

    return 0;
}

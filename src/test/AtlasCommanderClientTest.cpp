/*
 * AtlasCommanderClientTest.cpp
 *
 *  Created on: Apr 22, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/AtlasCommanderClient.hpp>
#include <re2/eigen/eigen_util.h>
#include <Eigen/Geometry>

using namespace re2uta;
using namespace re2uta::atlascommander;

int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "atlas_commander_node" );

    AtlasCommanderClient atlasCommanderClient;

    while( ros::ok() )
    {
        std::string planFrame( "l_foot" );
        std::string tipFrame(  "r_hand" );
        Eigen::Affine3d dest    = Eigen::Affine3d::Identity();
        Eigen::Vector6d weights = Eigen::Vector6d::Ones();

        atlasCommanderClient.walkto( Eigen::Vector3d(1,2,3) );
        atlasCommanderClient.switchToController( Test_Controller );
        atlasCommanderClient.commandFramePose( planFrame, tipFrame, dest, weights );
        ros::spinOnce();
        ros::Duration(0.2).sleep();
    }

    return 0;
}

/*
 * AtlasCommander.cpp
 *
 *  Created on: May 16, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/AtlasCommander/FullBodyController.hpp>
#include <re2uta/AtlasCommander.hpp>

#include <re2/eigen/eigen_util.h>
#include <boost/random.hpp>
#include <ros/ros.h>
#include <boost/foreach.hpp>

using namespace re2uta::atlascommander;
using namespace re2uta;


class AtlasCommanderTest
{
    public:
        ros::NodeHandle m_node;

    public:
        void go()
        {
            std::string urdfString;
            m_node.getParam( "/robot_description", urdfString );

            urdf::Model urdfModel;
            urdfModel.initString( urdfString );

//            bool geometricDebug = false;
//            m_node.getParam( "geometric_sim", geometricDebug );
//
//            bool publishState   = false;
//            m_node.getParam( "publish_state", publishState );
//
//            publishState = publishState && (!geometricDebug);

            AtlasCommander commander( urdfModel );

            atlas_msgs::AtlasState::Ptr atlasState( new atlas_msgs::AtlasState );

            atlasState->position.resize( 28 );
            atlasState->velocity.resize( 28 );

            BOOST_FOREACH( float & position, atlasState->position )
                position = 0.1;

            BOOST_FOREACH( float & velocity, atlasState->velocity )
                velocity = 0.1;

//            fullBodyController.setHandPositionConstraint();

            boost::mt19937 rng;
            boost::uniform_real<double> u(0.1, 0.2);
            boost::variate_generator<boost::mt19937&, boost::uniform_real<double> > gen(rng, u);

            Eigen::Affine3d originalHandPose = Eigen::Affine3d::Identity();
            originalHandPose.translation() = Eigen::Vector3d( 0.3, 0.2, 1.0 );

            Eigen::Vector6d handWeights;
            //originalHandPose = fullBodyController.getHandPose(); //FIXME this is a problem
            handWeights << 1, 1, 1, 0, 0, 0;

            double radius = 0.05;


//            ros::Publisher statePublisher;
//            if( publishState )
//                statePublisher = m_node.advertise<atlas_msgs::AtlasState>( "/atlas/atlas_state", 1 );

            while( ros::ok() )
            {
                ros::spinOnce();

                double angleRad = std::fmod( ros::Time::now().toSec(), 4 ) / 4  * 2 * M_PI;
                Eigen::Affine3d handPose = originalHandPose;
                handPose.translation().x() += std::cos( angleRad ) * radius;
                handPose.translation().z() += std::sin( angleRad ) * radius;

                FrameGoal::Ptr frameGoal( new FrameGoal );

                frameGoal->baseFrameId = "l_foot";
                frameGoal->goalFrameId = "l_foot";
                frameGoal->tipFrameId  = "l_hand";
                frameGoal->goalPose    = handPose;
                frameGoal->weightMatrix= handWeights.asDiagonal();

                commander.commandFramePose( frameGoal );

                ros::Duration( gen() ).sleep();
                ROS_INFO_STREAM( "sleep done" );
                ROS_INFO_STREAM( "angle: " << angleRad << "  y: " << handPose.translation().y() << "  z: " << handPose.translation().z() );

//                if( publishState )
//                {
//                    atlasState->header.stamp = ros::Time::now();
//                    statePublisher.publish( atlasState );
//                }
            }
        }
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "atlas_commander_test" );
    ROS_INFO( "Starting atlas_commander_test" );

    AtlasCommanderTest atlasCommanderTest;

    atlasCommanderTest.go();

    return 0;
}

/*
 * AtlastStateRateTest.cpp
 *
 *  Created on: May 26, 2013
 *      Author: andrew.somerville
 */


#include <atlas_msgs/AtlasState.h>
#include <atlas_msgs/AtlasCommand.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Int32.h>
#include <boost/thread.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

bool stop = false;

void siginthandler(int param)
{
    printf("User pressed Ctrl+C\n");
    stop = true;
}

template <typename MessageT>
class RateTest
{
    public:
        ros::NodeHandle m_node;
        boost::posix_time::ptime m_lastStateMsgPTime;
        ros::Time       m_lastStateMsgRosTime;
        ros::Time       m_lastStateMsgStampRosTime;
        boost::posix_time::time_duration m_maxCallbackDelay;
        int             m_maxCalled;
        std::string     m_topicName;
        boost::thread   m_workThread;

    public:

        RateTest( const std::string & topicName )
        {
            m_topicName = topicName;
            m_maxCalled = 0;
            m_workThread = boost::thread( &RateTest<MessageT>::work, this );
        }

        void handleMessage( const typename MessageT::ConstPtr & msg )
        {
//            ROS_INFO_STREAM( "Stamp diff us: " << (msg->header.stamp - m_lastStateMsgStampRosTime).toNSec()/1000 );
            m_lastStateMsgStampRosTime = msg->header.stamp;
        }

        void work()
        {
            ros::Publisher pubAtlasCommand = m_node.advertise<atlas_msgs::AtlasCommand>( "/atlas/atlas_command", 1 );
            atlas_msgs::AtlasCommand ac;


            ac.position.resize(28);
            ac.k_effort.resize(28);

              // default values for AtlasCommand
            for (unsigned int i = 0; i < 28; i++)
                ac.k_effort[i]     = 255;
            // simulated worker thread
            while(true)
            {
                // simulate working
                usleep(1000);
                // Let AtlasPlugin driver know that a response over /atlas/atlas_command
                // is expected every 5ms; and to wait for AtlasCommand if none has been
                // received yet. Use up the delay budget if wait is needed.
                ac.desired_controller_period_ms = 5;

                pubAtlasCommand.publish(ac);
            }
        }

        void go()
        {
            ros::Publisher  sleepDelayPub     = m_node.advertise<std_msgs::Int32>( "/sleep_delay",        50 );
            ros::Publisher  callbackDelayPub  = m_node.advertise<std_msgs::Int32>( "/callback_delay",     50 );
            ros::Publisher  sleepRDelayPub    = m_node.advertise<std_msgs::Float32>( "/sleep_ros_delay",    50 );
            ros::Publisher  callbackRDelayPub = m_node.advertise<std_msgs::Float32>( "/callback_ros_delay", 50 );
            ros::Subscriber stateSub          = m_node.subscribe( m_topicName, 10, &RateTest::handleMessage, this, ros::TransportHints().udp() );

            ros::Time                now              = ros::Time::now();
            boost::posix_time::ptime pnow             = boost::posix_time::microsec_clock::local_time();
            ros::Time                lastWakeupTime   = now;
            ros::Time                lastSleepTime    = now;
            boost::posix_time::ptime lastWakeupPtime  = pnow;
            boost::posix_time::ptime lastSleepPtime   = pnow;
            boost::posix_time::time_duration maxSleepDelay;
            boost::posix_time::time_duration sleepDelay;
            int called    = 0;
            std_msgs::Int32  intMsg;
            std_msgs::Float32  floatMsg;

            m_lastStateMsgStampRosTime = now;
            m_lastStateMsgRosTime      = now;
            m_lastStateMsgPTime        = pnow;
            boost::posix_time::ptime  begin = pnow;


            ROS_WARN_STREAM( "Max called in one go was:   " << m_maxCalled );
            ROS_WARN_STREAM( "Max callback delay was us:  " << m_maxCallbackDelay.total_microseconds() );
            ROS_WARN_STREAM( "Max sleep    delay was us:  " << maxSleepDelay.total_microseconds() );

            while( !stop && ros::ok() )
            {
                sleepDelay = lastWakeupPtime - lastSleepPtime;
                called = 0;

                pnow = boost::posix_time::microsec_clock::local_time();
                now  = ros::Time::now();

                if( !ros::getGlobalCallbackQueue()->empty() )
                {
                    boost::posix_time::time_duration callbackDelay  = (pnow - m_lastStateMsgPTime);
                    ros::Duration                    callbackRDelay = (now - m_lastStateMsgRosTime);
                    intMsg.data = callbackDelay.total_microseconds();
                    callbackDelayPub.publish( intMsg );
                    floatMsg.data = callbackRDelay.toSec();
                    callbackRDelayPub.publish( floatMsg );

                    if( (pnow - begin).total_milliseconds() > 500 )
                    {
                        m_maxCallbackDelay = std::max( m_maxCallbackDelay, callbackDelay );

                        if( m_maxCallbackDelay == callbackDelay )
                            ROS_ERROR_STREAM( "New max callback delay" );

                        if( callbackDelay > boost::posix_time::microsec( 1200 ) )
                        {
                            ROS_WARN_STREAM( "high callback delay " << callbackDelay.total_microseconds() );
                            ROS_WARN_STREAM( "last sleep delay " << sleepDelay.total_microseconds() );
                        }
                    }
                }

                while( ros::ok() && ros::getGlobalCallbackQueue()->callOne() == ros::CallbackQueue::Called )
                {
//                    ROS_INFO_STREAM( "Message: " << called );
                    boost::posix_time::ptime pnow = boost::posix_time::microsec_clock::local_time();
                    m_lastStateMsgRosTime = ros::Time::now();
                    m_lastStateMsgPTime = pnow;
                    ++called;
                }

                m_maxCalled = std::max( m_maxCalled, called );
                if( called > 0 )
                    ROS_WARN_STREAM( "high msg count " << called );


                now  = ros::Time::now();
                pnow = boost::posix_time::microsec_clock::local_time();

                intMsg.data = sleepDelay.total_microseconds();
                sleepDelayPub.publish( intMsg );

                if( (pnow - begin).total_milliseconds() > 500 )
                {
                    maxSleepDelay = std::max( sleepDelay, maxSleepDelay );

                    if( sleepDelay == maxSleepDelay )
                        ROS_ERROR_STREAM( "New Max Sleep delay " );

                    if( sleepDelay > boost::posix_time::microsec( 700 ) )
                    {
                        ROS_WARN_STREAM( "High Sleep delay " << sleepDelay.total_microseconds() );
                        ROS_WARN_STREAM( "with msg count:  " << called );
                        ROS_WARN_STREAM( "" );
                    }
                }

                lastSleepTime  = ros::Time::now();
                lastSleepPtime = boost::posix_time::microsec_clock::local_time();
                usleep( 100 );
                pnow = boost::posix_time::microsec_clock::local_time();
                now = ros::Time::now();

//                ROS_INFO_STREAM( "Sleep delay us:  " << (now - lastSleepTime).toNSec()/1000 );
//                ROS_INFO_STREAM( "Sleep pdelay us: " << (pnow - lastSleepPtime).total_microseconds() );
//                ROS_WARN_STREAM( "called was:      " << called );
//                ROS_INFO_STREAM( "" );

                lastWakeupTime  = now;
                lastWakeupPtime = pnow;
            }

            ROS_WARN_STREAM( "Max called in one go was:   " << m_maxCalled );
            ROS_WARN_STREAM( "Max callback delay was us:  " << m_maxCallbackDelay.total_microseconds() );
            ROS_WARN_STREAM( "Max sleep    delay was us:  " << maxSleepDelay.total_microseconds() );
        }
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "stateRateTest", ros::init_options::NoSigintHandler );
    ROS_INFO( "Starting stateRateTest" );

    signal(SIGINT, siginthandler);
    RateTest<atlas_msgs::AtlasState> stateRateTest( "/atlas/atlas_state" );

    stateRateTest.go();

    return 0;
}




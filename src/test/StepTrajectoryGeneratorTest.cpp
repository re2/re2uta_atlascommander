/*
 * WalkPatternGeneratorTest.cpp
 *
 *  Created on: May 29, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/walking/StepGenerator.hpp>
#include <re2uta/walking/CirclePathStepGenerator.hpp>
#include <re2uta/walking/StepTrajectoryGenerator.hpp>
#include <re2/visutils/marker_util.h>
#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <boost/foreach.hpp>
#include <string>
#include <iostream>




Eigen::Affine3d toAffine( const re2uta::Ray & ray0 )
{
    Eigen::Affine3d affine;

    affine.translation() = ray0.origin();
    affine.linear()      = Eigen::Quaterniond().setFromTwoVectors( Eigen::XAxis3d, ray0.direction() ).toRotationMatrix();

    return affine;
}




class StepTrajectoryGeneratorTest
{
    public:
        ros::NodeHandle m_node;
        Eigen::Affine3d m_dest;

        ros::Publisher m_lFootTrajPub;
        ros::Publisher m_rFootTrajPub;
        ros::Publisher m_lFootPosePub;
        ros::Publisher m_rFootPosePub;
        ros::Publisher m_comTrajPub  ;
        ros::Publisher m_zmpTrajPub  ;
        ros::Publisher m_simMarkerPub;

        StepTrajectoryGeneratorTest()
        {
            m_lFootTrajPub = m_node.advertise<nav_msgs::Path>(            "/walkTrajectory/l_foot", 5, true);
            m_rFootTrajPub = m_node.advertise<nav_msgs::Path>(            "/walkTrajectory/r_foot", 5, true);
            m_lFootPosePub = m_node.advertise<geometry_msgs::PoseStamped>("/walkTrajectory/l_foot_pose", 5, true);
            m_rFootPosePub = m_node.advertise<geometry_msgs::PoseStamped>("/walkTrajectory/r_foot_pose", 5, true);
            m_comTrajPub   = m_node.advertise<nav_msgs::Path>(            "/walkTrajectory/com",    5, true);
            m_simMarkerPub = m_node.advertise<visualization_msgs::Marker>("/walkTrajectory/sim",    5, true);
            m_zmpTrajPub   = m_node.advertise<nav_msgs::Path>(            "/walkTrajectory/zmp",    5, true);
        }

        void go()
        {
            ros::Subscriber goalSub;
            goalSub = m_node.subscribe( "/commander/walkto_dest", 1, &StepTrajectoryGeneratorTest::handleGoal, this );
            while( ros::ok() )
            {
                ros::spinOnce();
            }
        }

        void handleGoal( const geometry_msgs::PoseStampedConstPtr & poseMsg )
        {
            tf::poseMsgToEigen( poseMsg->pose, m_dest );

            runTest();
        }

        void runTest()
        {
            double destDist = 2;
            int index = 0;

            m_node.getParam( "dist" , destDist );



            nav_msgs::Path lFootTraj;
            nav_msgs::Path rFootTraj;
            nav_msgs::Path comTraj;
            nav_msgs::Path zmpTraj;

            geometry_msgs::PoseStamped poseStamped;

            geometry_msgs::PoseStamped zmpPoseStamped;


            poseStamped.header.frame_id = "/odom";
            zmpPoseStamped.header.frame_id = "/odom";

            ros::Duration( 0.5 ).sleep();


            std::cout << "Running circles" << "\n";
            re2uta::Ray ray0( Eigen::Vector3d( 0, 0, 0 ), Eigen::Vector3d( 1, 0, 0) );
            re2uta::Ray ray1( m_dest.translation(), m_dest.rotation()*Eigen::Vector3d( 1, 0, 0) );
            re2uta::CircleVector circles = re2uta::findCircles(ray0,ray1);



            lFootTraj.poses.clear();
            rFootTraj.poses.clear();

            std::cout << "starting" << std::endl;
            std::cout << "0" << std::endl;
////            plan = m_walkPatternGenerator.buildStraightLineWalkPlan( Eigen::Affine3d( Eigen::Translation3d(       0,0,0) ),
////                                                                     Eigen::Affine3d( Eigen::Translation3d(destDist,0,0) ), 0.2 );

            re2uta::StepPose::Ptr originalStepPose( new re2uta::StepPose( otherFoot( re2uta::LEFT_FOOT ) ) );
            originalStepPose->comPose   = Eigen::Vector3d( 0, 0, 0.814 );
            originalStepPose->lFootPose = Eigen::Translation3d( 0,  0.15, 0 );
            originalStepPose->rFootPose = Eigen::Translation3d( 0, -0.15, 0 );

            re2uta::StepTrajectoryGenerator stepTrajectoryGenerator( originalStepPose, m_dest, 0.15, 0.15 );

            re2uta::StepPlan::Ptr stepPlan;
            while( stepPlan = stepTrajectoryGenerator.generateNextStep( originalStepPose ) )
            {

                std::cout << "swing begin  " << stepPlan->stepPoses().front()->swingFootPose().translation().transpose() << "\n";
                std::cout << "swing end    " << stepPlan->stepPoses().back() ->swingFootPose().translation().transpose() << "\n";




                BOOST_FOREACH( const Eigen::Vector3d & zmpPose, *stepTrajectoryGenerator.m_zmpRefPlan )
                {
                    zmpPoseStamped.pose.position.x = zmpPose.x() ;
                    zmpPoseStamped.pose.position.y = zmpPose.y() ;
                    zmpPoseStamped.pose.position.z = zmpPose.z() ;
                    zmpTraj.poses.push_back( zmpPoseStamped );

                }

                BOOST_FOREACH( const re2uta::StepPose::Ptr & stepPose, stepPlan->stepPoses() )
                {
                    tf::poseEigenToMsg( stepPose->lFootPose, poseStamped.pose );
                    lFootTraj.poses.push_back( poseStamped );

                    tf::poseEigenToMsg( stepPose->rFootPose, poseStamped.pose );
                    rFootTraj.poses.push_back( poseStamped );

                    poseStamped.pose.position.x = stepPose->comPose.x();
                    poseStamped.pose.position.y = stepPose->comPose.y();
                    poseStamped.pose.position.z = stepPose->comPose.z();

                    comTraj.poses.push_back( poseStamped );
                }
            }



            lFootTraj.header.frame_id = "/odom";
            rFootTraj.header.frame_id = "/odom";
            comTraj.header.frame_id   = "/odom";
            zmpTraj.header.frame_id   = "/odom";

            m_lFootTrajPub.publish( lFootTraj );
            m_rFootTrajPub.publish( rFootTraj );
            m_comTrajPub.publish(   comTraj   );
            m_zmpTrajPub.publish(   zmpTraj   );



            using re2::visualization_msgs_util::createMarkerMsg;
            visualization_msgs::Marker::Ptr simMarker;
            Eigen::Vector4d color( 1,1,0,1 );
            simMarker = createMarkerMsg( 1,
                                         visualization_msgs::Marker::LINE_STRIP,
                                         color,
                                         "test",
                                         "/odom" );

            for( int i = 0; i < lFootTraj.poses.size() && ros::ok(); ++i )
            {
                simMarker->header.frame_id = "/odom";
                simMarker->points.clear();
                simMarker->points.push_back( lFootTraj.poses[i].pose.position );
                simMarker->points.push_back( rFootTraj.poses[i].pose.position );
                simMarker->points.push_back(   comTraj.poses[i].pose.position );
                simMarker->points.push_back( lFootTraj.poses[i].pose.position );

//                std::cout << "left  " << Eigen::Vector3dMap( &lFootTraj.poses[i].pose.position.x ).transpose() << "\n";
//                std::cout << "right " << Eigen::Vector3dMap( &rFootTraj.poses[i].pose.position.x ).transpose() << "\n";
//                std::cout << "com   " << Eigen::Vector3dMap( &comTraj.poses[i].pose.position.x   ).transpose() << "\n";

                m_simMarkerPub.publish( simMarker );
                m_lFootPosePub.publish( lFootTraj.poses[i] );
                m_rFootPosePub.publish( rFootTraj.poses[i] );
                ros::Duration(0.005).sleep();
            }

            ROS_INFO( "End" );


        }
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "step_trajectory_generator_test" );
    ROS_INFO( "Starting step_trajectory_generator_test" );

    StepTrajectoryGeneratorTest stepTrajectoryGeneratorTest;

    stepTrajectoryGeneratorTest.go();

    return 0;
}

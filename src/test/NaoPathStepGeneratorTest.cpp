/*
 * NaoPathStepGenerator.cpp
 *
 *  Created on: Jun 19, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/walking/StepGenerator.hpp>
#include <re2uta/walking/NaoPathStepGenerator.hpp>
#include <re2/visutils/marker_util.h>

#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>
#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <boost/foreach.hpp>
#include <string>
#include <iostream>


using namespace re2uta;

class NaoPathStepGeneratorTest
{
    public:
        ros::NodeHandle m_node;
        Eigen::Affine3d m_dest;
        NaoPathStepGenerator::Ptr m_stepGenerator;


        ros::Publisher m_footstepMarkerPub;
        ros::Publisher m_simMarkerPub;

        NaoPathStepGeneratorTest()
        {
            m_simMarkerPub      = m_node.advertise<visualization_msgs::Marker>(      "/walkTrajectory/sim", 5, true);
            m_footstepMarkerPub = m_node.advertise<visualization_msgs::MarkerArray>( "/footstep_planner/footsteps_array", 1 );
        }

        void go()
        {
            ros::Subscriber goalSub;
            goalSub = m_node.subscribe( "/commander/walkto_dest", 1, &NaoPathStepGeneratorTest::handleGoal, this );
            while( ros::ok() )
            {
                ros::spinOnce();
            }
        }

        void handleGoal( const geometry_msgs::PoseStampedConstPtr & poseMsg )
        {
            ROS_INFO( "Got Goal" );
            tf::poseMsgToEigen( poseMsg->pose, m_dest );

            runTest();
        }

        void runTest()
        {
            m_stepGenerator.reset( new NaoPathStepGenerator( Eigen::Affine3d::Identity(), m_dest, 0.15 ) );

            Eigen::Affine3d base  = Eigen::Affine3d::Identity();
            Eigen::Affine3d swing = Eigen::Affine3d::Identity();


            visualization_msgs::MarkerArray markerArrayMsg;
            visualization_msgs::Marker      markerMsg;
            markerMsg.action  = visualization_msgs::Marker::MODIFY;
            markerMsg.type    = visualization_msgs::Marker::CUBE;
            markerMsg.scale.x = 0.2;
            markerMsg.scale.y = 0.1;
            markerMsg.scale.z = 0.001;
            markerMsg.header.frame_id = "/odom";
            markerMsg.frame_locked    = true;

            int index = 0;
            Eigen::Affine3d nextPose = Eigen::Affine3d::Identity();
            while( ! m_stepGenerator->hasArrived(nextPose) && (index < 200) )
            {
                ROS_INFO_STREAM( "Got step: " << index );
                nextPose = m_stepGenerator->generateNextStep( base, swing, index );

                tf::poseEigenToMsg( nextPose, markerMsg.pose );
                markerMsg.id = index;

                if( index % 2 == 0 )
                    Eigen::Map<Eigen::Vector4f>( &markerMsg.color.r ) = Eigen::Vector4f( 1, 0, 0, 1 );
                else
                    Eigen::Map<Eigen::Vector4f>( &markerMsg.color.r ) = Eigen::Vector4f( 0, 0, 1, 1 );

                markerArrayMsg.markers.push_back( markerMsg );

                ++index;
            }

            m_footstepMarkerPub.publish( markerArrayMsg );

            ROS_INFO_STREAM( "Done." );
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "nao_path_step_generator_test" );
    ROS_INFO( "Starting nao_path_step_generator_test" );

    NaoPathStepGeneratorTest naoPathStepGeneratorTest;

    naoPathStepGeneratorTest.go();

    return 0;
}

/*
 * InteractiveMarkerTest.cpp
 *
 *  Created on: Jun 11, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/interactive_marker_utils.hpp>
#include <interactive_markers/interactive_marker_server.h>
#include <eigen_conversions/eigen_msg.h>
#include <re2/eigen/eigen_util.h>
#include <ros/ros.h>



class InteractiveMarkerTest
{
    public:
        ros::NodeHandle m_node;
        interactive_markers::InteractiveMarkerServer m_interactiveMarkerServer;


    public:
        InteractiveMarkerTest()
            : m_interactiveMarkerServer( "simple" )
        {
            // create an interactive marker for our server

            visualization_msgs::InteractiveMarker::Ptr marker0;
            marker0 = re2uta::create6dBoxControl( "l_foot", "test_marker", "lorum ipsum" );

            // add the interactive marker to our collection &
            // tell the server to call processFeedback() when feedback arrives for it
            m_interactiveMarkerServer.insert(*marker0, boost::bind( &InteractiveMarkerTest::processFeedback, this, _1 ) );

            // 'commit' changes and send to all clients
            m_interactiveMarkerServer.applyChanges();
        }

        void processFeedback( const visualization_msgs::InteractiveMarkerFeedbackConstPtr & msg )
        {
            ROS_INFO_STREAM( "Feedback" );
            ROS_INFO_STREAM( "Msg : " << *msg );
        }

        void go()
        {
            ROS_INFO( "GO!" );
            ros::spin();
        }

};

int main (int argc, char *argv[])
{

    ros::init( argc, argv, "interactive_marker_test" );

    InteractiveMarkerTest interactiveMarkerTest;

    interactiveMarkerTest.go();

    return 0;

}

/*
 * GeometricSimulatorTest.cpp
 *
 *  Created on: Apr 26, 2013
 *      Author: andrew.somerville
 */



#include <re2uta/AtlasLookup.hpp>
#include <re2/eigen/eigen_util.h>
#include <atlas_msgs/AtlasCommand.h>
#include <atlas_msgs/AtlasState.h>
#include <kdl_parser/kdl_parser.hpp>
#include <urdf_model/model.h>
#include <ros/ros.h>
#include <boost/foreach.hpp>

using namespace re2uta;


class GeometricSimulationTest
{
    private:
        ros::NodeHandle  m_node;
        urdf::Model      m_urdfModel;
        ros::Subscriber  m_jointStateSub;
        ros::Publisher   m_jointCommandPub;
        AtlasLookup::Ptr m_atlasLookup;
        Eigen::VectorXd  m_currentCmdJointAngles;



    public:
        GeometricSimulationTest()
        {
            std::string urdfString;
            m_node.getParam( "/robot_description", urdfString );
            m_urdfModel.initString( urdfString );
            m_atlasLookup.reset( new AtlasLookup( m_urdfModel ) );

            m_currentCmdJointAngles.resize( m_atlasLookup->getNumCmdJoints() );
            m_jointCommandPub = m_node.advertise<atlas_msgs::AtlasCommand>( "/atlas/atlas_command", 1 );
            m_jointStateSub   = m_node.subscribe( "/atlas/atlas_state", 1, &GeometricSimulationTest::handleJointStates, this );
        }


        void handleJointStates( const atlas_msgs::AtlasStateConstPtr & msg )
        {
//            assert( msg->position.size()  == m_currentCmdJointAngles.size() );
            m_currentCmdJointAngles = re2::toEigen( msg->position ).cast<double>();
        }


        void publishJointCommand( const Eigen::VectorXd & jointPositionCmd )
        {
            atlas_msgs::AtlasCommand::Ptr msg( new atlas_msgs::AtlasCommand );

            msg = m_atlasLookup->createEmptyJointCmdMsg();
            msg->position = re2::toStd( jointPositionCmd );

            m_jointCommandPub.publish( msg );
        }


        void go()
        {
            Eigen::VectorXd defaultJointAngles = m_atlasLookup->calcDefaultCmdJointAngles( 0.4 );
            Eigen::VectorXd cmdAngles          = m_atlasLookup->calcDefaultCmdJointAngles( 0.4 );

            int period = 100;
            int count  = 0;
            while( ros::ok() )
            {
                int    step    = count % period;
                double portion = step/(double)period;
                double sin     = std::sin( M_PI * portion );

                cmdAngles = defaultJointAngles + Eigen::VectorXd::Ones(cmdAngles.size()) * sin * 0.1;

                ros::spinOnce();
                publishJointCommand( cmdAngles );
                ros::Duration(0.05).sleep();
                ++count;
            }
        }
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "geometric_simulator_test" );

    GeometricSimulationTest geometricSimulationTest;

    geometricSimulationTest.go();

    return 0;
}

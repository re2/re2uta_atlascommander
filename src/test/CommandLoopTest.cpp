/*
 * CommandLoopTest.cpp
 *
 *  Created on: May 3, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/AtlasCommander/CommandLoop.hpp>
#include <boost/random.hpp>
#include <ros/ros.h>

class CommandLoopTest
{
    public:
        ros::NodeHandle m_node;

    public:
        void go()
        {
            re2uta::atlascommander::CommandLoop commandLoop( ros::Duration( 0.01 ) );
            boost::mt19937 rng;
            boost::uniform_real<double> u(0, 5);
            boost::variate_generator<boost::mt19937&, boost::uniform_real<double> > gen(rng, u);

            while( ros::ok() )
            {
                ros::spinOnce();

                double rand = gen();
                ROS_INFO_STREAM( "sleep: " << rand );
                ros::Duration(rand).sleep();
                ROS_INFO_STREAM( "sleep done" );

                ROS_INFO_STREAM( "Current iteration"   << commandLoop.iteration() );
                ROS_INFO_STREAM( "Next command time: " << commandLoop.nextCommandTime() );
            }
        }
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "command_loop_test" );
    ROS_INFO( "Starting command_loop_test" );

    CommandLoopTest commandLoopTest;

    commandLoopTest.go();

    return 0;
}

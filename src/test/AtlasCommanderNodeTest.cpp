/*
 * Author: Andrew Somerville
 * date:   2013-04-18
 */

#include "../utils.hpp"
#include "../ros_utils.hpp"

#include <re2/eigen/eigen_util.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <ros/ros.h>
#include <Eigen/Core>


using namespace re2uta;

class AtlasCommanderNodeTest
{
    public:
        void go()
        {
            ros::NodeHandle node;

            ros::Publisher walktoPub = node.advertise<geometry_msgs::Vector3Stamped>( "walkto", 1 );

            while( ros::ok() )
            {
                walktoPub.publish( buildVec3Stamped("/",Eigen::Vector3d(1,2,3)) );
                ros::spinOnce();
                ros::Duration(0.5).sleep();
            }
        }
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "atlas_commander_node_test" );

    AtlasCommanderNodeTest atlasCommanderNodeTest;

    atlasCommanderNodeTest.go();

    return 0;
}

/**
 *
 * CalculateSwingTrajectoryTest.cpp: ...
 *
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created Apr 24, 2013
 * @modified Apr 24, 2013
 *
 */

#include <ros/ros.h>

#include <atlas_msgs/AtlasCommand.h>
#include <atlas_msgs/AtlasState.h>

#include <std_msgs/Int32.h>

#include <std_msgs/Empty.h>
#include <tf/transform_listener.h>

#include <kdl_parser/kdl_parser.hpp>
#include <urdf_model/model.h>

#include <re2uta/CalculateCapturePoint.hpp>

#include <re2uta/CalculateSwingTraj.hpp>

#include <re2uta/AtlasCommander/FullBodyController.hpp>

#include <re2uta/AtlasCommander/NeuralNetworkJointController.hpp>

#include <re2uta/AtlasCommander/DefaultJointController.hpp>

#include <boost/random.hpp>

using namespace re2uta;
using namespace re2uta::atlascommander;


class AtlasSwingNodeTest
{
    private:

        ros::NodeHandle         m_node;

        boost::
        condition_variable      m_foo;

        AtlasCaptureNode::Ptr   m_atlasCaptureNode;
        AtlasSwingNode::Ptr     m_atlasSwingNode;

        JointController::Ptr    m_neuralNetworkJointController;

        FullBodyController::Ptr m_fullBodyController;

        FrameGoal::Ptr          m_frameGoal;

        ros::Subscriber         m_atlasStateSubscriber;

        ros::Publisher          m_instantaneousCapturePointPublisher;
        ros::Publisher          m_propagatedCapturePointPublisher;
        ros::Publisher          m_stopTrajectoryPublisher;
        ros::Publisher          m_supportPolygonPublisher;
        ros::Publisher          m_swingTrajPublisher;

        Eigen::Vector3d         m_instantaneousCapturePoint;

        double                  m_ankleOffset;


    public:

        /*
         * This is the constructor for the AtlasCaptureNode
         */
        AtlasSwingNodeTest()
        {

            std::string urdfString;

            m_node.getParam( "/robot_description", urdfString );

            urdf::Model urdfModel;
            urdfModel.initString( urdfString );

            m_atlasCaptureNode.reset( new AtlasCaptureNode( urdfModel ) );
            m_atlasSwingNode.reset( new AtlasSwingNode );

//            m_neuralNetworkJointController.reset( new NeuralNetworkJointController( ros::Duration(0.1), m_foo ) );
//            m_neuralNetworkJointController.reset( new NeuralNetworkJointController( ros::Duration(0.001), m_foo ) );
//            m_fullBodyController.reset( new FullBodyController(urdfModel, m_foo, m_neuralNetworkJointController ) );

//            m_frameGoal.reset( new FrameGoal );

            m_atlasStateSubscriber = m_node.subscribe( "/atlas/atlas_state" , 1, &AtlasSwingNodeTest::handleAtlasState , this );

            m_instantaneousCapturePointPublisher  = m_node.advertise<visualization_msgs::Marker>("/atlas/instantaneousCapturePoint", 1);
            m_propagatedCapturePointPublisher     = m_node.advertise<visualization_msgs::Marker>("/atlas/propagatedCapturePoint", 1);
            m_swingTrajPublisher                  = m_node.advertise<nav_msgs::Path>("/atlas/swingTraj", 1);

            m_stopTrajectoryPublisher = m_node.advertise<std_msgs::Int32>("joint_trajectory_startStop", 1);
            m_supportPolygonPublisher = m_node.advertise<geometry_msgs::PolygonStamped>("/atlas/support_polygon", 1);

            m_ankleOffset  = -0.079342f;

        }


        /*
         * Here we set the KDL tree joint states using the Atlas state message
         * This is where the main processing is done                 *
         */
        void handleAtlasState( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg )
        {

            double stepPeriod = 1;

            // Preempt if capture point outside support polygon
            if( !m_atlasCaptureNode->isStateCaptured(  atlasStateMsg, m_instantaneousCapturePoint ) )
            {
                std_msgs::Int32 trajStartStop;
                trajStartStop.data = StopPublishing;

                m_stopTrajectoryPublisher.publish( trajStartStop );
            }

            /*
             * This function will calculate the propagated Capture Point (CP)
             */
            m_atlasCaptureNode->updateInstantaneousCapturePoint( atlasStateMsg );
            m_atlasCaptureNode->updatePropagatedCapturePoint( atlasStateMsg, stepPeriod );

            Eigen::Vector3d propagatedCapturePoint = m_atlasCaptureNode->getPropagatedCapturePoint();
            Eigen::Vector3d swingFootStart         = m_atlasCaptureNode->getSwingFootPose();

            propagatedCapturePoint(2) += m_ankleOffset;

            m_atlasSwingNode->calcSwingTrajectory( swingFootStart, propagatedCapturePoint, stepPeriod );


            // FIXME add the controller/trigger part
            if( false )
            {

//              // ##### IK and CONTROLLER PART
//
//              Eigen::Vector6d swingLegWeights;
//              //originalHandPose = fullBodyController.getHandPose(); //FIXME this is a problem
//              swingLegWeights << 1, 1, 0.1, 0, 0, 0;
//
//              Eigen::Affine3d swingLegPose = Eigen::Affine3d::Identity();
//
//              swingLegPose.translation() = m_atlasSwingNode->getSwingPosPoint( 0.005 );
//
//              std::cout << swingLegPose.data();
//
//              FrameGoal::Ptr frameGoal( new FrameGoal );
//
//              frameGoal->baseFrameId  = m_atlasCaptureNode->getBaseName();
//              frameGoal->goalFrameId  = m_atlasCaptureNode->getBaseName(); // TODO should this be in base or swing
//              frameGoal->tipFrameId   = m_atlasCaptureNode->getSwingName();
//
//              frameGoal->goalPose     = swingLegPose;
//              frameGoal->weightMatrix = swingLegWeights.asDiagonal();
//
//              m_fullBodyController->setFrameGoal( frameGoal );
//
////              ros::Duration( 0.001 ).sleep();
//              ROS_INFO_STREAM( "sleep done" );
//              ROS_INFO_STREAM( /*"angle: " << angleRad <<*/ "  y: " << swingLegPose.translation().y() << "  z: " << swingLegPose.translation().z() );
//
//              // FIXME this will not compile since atlasStateMsg is const
//              // Not sure why we need to set the time like this
//    //            atlasStateMsg->header.stamp = ros::Time::now();
//              m_neuralNetworkJointController->setLatestStateMsg(   atlasStateMsg );
//              m_fullBodyController->setLatestStateMsg( atlasStateMsg );
//              m_foo.notify_all();
//
//              // ##### IK and CONTROLLER PART END

            }

            // These will publish the capture points and support polygon
            m_instantaneousCapturePointPublisher.publish( m_atlasCaptureNode->getInstantaneousCapturePointMarker() );
            m_propagatedCapturePointPublisher.publish( m_atlasCaptureNode->getPropagatedCapturePointMarker() );
            m_supportPolygonPublisher.publish( m_atlasCaptureNode->getSupportPolygonMarker() );
            m_swingTrajPublisher.publish( m_atlasSwingNode->getSwingPosTrajMsg( stepPeriod /*step period*/, m_atlasCaptureNode->getBaseName() ) );

        }

        void go()
        {
            ROS_INFO( "GO!" );
            ros::spin();
        }

};

int main (int argc, char *argv[])
{

    ros::init( argc, argv, "AtlasSwingNodeTest" );

    AtlasSwingNodeTest atlasSwingNodeTest;

    atlasSwingNodeTest.go();

    return 0;

}

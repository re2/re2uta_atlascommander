/*
 * WalkPatternGeneratorTest.cpp
 *
 *  Created on: May 29, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/walking/StepGenerator.hpp>
#include <re2uta/walking/WalkPatternGenerator.hpp>
#include <re2/visutils/marker_util.h>
#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <boost/foreach.hpp>
#include <string>
#include <iostream>




Eigen::Affine3d toAffine( const re2uta::Ray & ray0 )
{
    Eigen::Affine3d affine;

    affine.translation() = ray0.origin();
    affine.linear()      = Eigen::Quaterniond().setFromTwoVectors( Eigen::XAxis3d, ray0.direction() ).toRotationMatrix();

    return affine;
}




class WalkPatternGeneratorTest
{
    public:
        ros::NodeHandle m_node;
        re2uta::WalkPatternGenerator m_walkPatternGenerator;
        Eigen::Affine3d m_dest;

        void go()
        {
            ros::Subscriber goalSub;
            goalSub = m_node.subscribe( "/move_base_simple/goal", 1, &WalkPatternGeneratorTest::handleGoal, this );
            while( ros::ok() )
            {
                ros::spinOnce();
            }
        }

        void handleGoal( const geometry_msgs::PoseStampedConstPtr & poseMsg )
        {
            tf::poseMsgToEigen( poseMsg->pose, m_dest );

            runTest();
        }

        void runTest()
        {
            double destDist = 2;
            int index = 0;

            m_node.getParam( "dist" , destDist );

            ros::Publisher lFootTrajPub = m_node.advertise<nav_msgs::Path>(            "/walkTrajectory/l_foot", 5, true);
            ros::Publisher rFootTrajPub = m_node.advertise<nav_msgs::Path>(            "/walkTrajectory/r_foot", 5, true);
            ros::Publisher lFootPosePub = m_node.advertise<geometry_msgs::PoseStamped>("/walkTrajectory/l_foot_pose", 5, true);
            ros::Publisher rFootPosePub = m_node.advertise<geometry_msgs::PoseStamped>("/walkTrajectory/r_foot_pose", 5, true);
            ros::Publisher comTrajPub   = m_node.advertise<nav_msgs::Path>(            "/walkTrajectory/com",    5, true);
            ros::Publisher simMarkerPub = m_node.advertise<visualization_msgs::Marker>("/walkTrajectory/sim",    5, true);

            nav_msgs::Path lFootTraj;
            nav_msgs::Path rFootTraj;
            nav_msgs::Path comTraj;

            geometry_msgs::PoseStamped poseStamped;
            poseStamped.header.frame_id = "/world";


            std::cout << "Running circles" << "\n";
            re2uta::Ray ray0( Eigen::Vector3d( 0, 0, 0 ), Eigen::Vector3d( 1, 0, 0) );
            re2uta::Ray ray1( m_dest.translation(), m_dest.rotation()*Eigen::Vector3d( 1, 0, 0) );
            re2uta::CircleVector circles = re2uta::findCircles(ray0,ray1);



            lFootTraj.poses.clear();
            rFootTraj.poses.clear();

            std::cout << "starting" << std::endl;
            re2uta::WalkPlan plan;
            std::cout << "0" << std::endl;
////            plan = m_walkPatternGenerator.buildStraightLineWalkPlan( Eigen::Affine3d( Eigen::Translation3d(       0,0,0) ),
////                                                                     Eigen::Affine3d( Eigen::Translation3d(destDist,0,0) ), 0.2 );
            plan = m_walkPatternGenerator.buildCircleBasedWalkPlan( toAffine( re2uta::Ray( Eigen::Vector3d( 0,0,0), Eigen::Vector3d( 1, 0,0) ) ),
                                                                    m_dest, 0.3 );
//                                                                    toAffine( re2uta::Ray( Eigen::Vector3d( 2,1,0), Eigen::Vector3d( 0,-1,0) ) ), 0.2 );
            std::cout << "0.5" << std::endl;

            index = 0;
            BOOST_FOREACH( const Eigen::Affine3d & planPose, *plan.leftFootPoses )
            {
                tf::poseEigenToMsg( planPose, poseStamped.pose );
                lFootTraj.poses.push_back( poseStamped );
                ++index;
            }
            std::cout << "1" << std::endl;

            index = 0;
            BOOST_FOREACH( const Eigen::Affine3d & planPose, *plan.rightFootPoses )
            {
                tf::poseEigenToMsg( planPose, poseStamped.pose );
                rFootTraj.poses.push_back( poseStamped );
                ++index;
            }

            std::cout << "2" << std::endl;
            index = 0;
            BOOST_FOREACH( const Eigen::Vector3d & comPoint, *plan.comPoints )
            {
                poseStamped.pose.position.x = comPoint.x();
                poseStamped.pose.position.y = comPoint.y();
                poseStamped.pose.position.z = comPoint.z();

                comTraj.poses.push_back( poseStamped );
                ++index;
            }
            std::cout << "3" << std::endl;

            lFootTraj.header.frame_id = "/world";
            rFootTraj.header.frame_id = "/world";
            comTraj.header.frame_id   = "/world";
            lFootTrajPub.publish( lFootTraj );
            rFootTrajPub.publish( rFootTraj );
            comTrajPub.publish(   comTraj );




            using re2::visualization_msgs_util::createMarkerMsg;
            visualization_msgs::Marker::Ptr simMarker;
            Eigen::Vector4d color( 1,1,0,1 );
            simMarker = createMarkerMsg( 1,
                                         visualization_msgs::Marker::LINE_STRIP,
                                         color,
                                         "test",
                                         "/world" );

            for( int i = 0; i < lFootTraj.poses.size() && ros::ok(); ++i )
            {
                simMarker->header.frame_id = "/world";
                simMarker->points.clear();
                simMarker->points.push_back( lFootTraj.poses[i].pose.position );
                simMarker->points.push_back( rFootTraj.poses[i].pose.position );
                simMarker->points.push_back(   comTraj.poses[i].pose.position );
                simMarker->points.push_back( lFootTraj.poses[i].pose.position );

//                std::cout << "left  " << Eigen::Vector3dMap( &lFootTraj.poses[i].pose.position.x ).transpose() << "\n";
//                std::cout << "right " << Eigen::Vector3dMap( &rFootTraj.poses[i].pose.position.x ).transpose() << "\n";
//                std::cout << "com   " << Eigen::Vector3dMap( &comTraj.poses[i].pose.position.x   ).transpose() << "\n";

                simMarkerPub.publish( simMarker );
                lFootPosePub.publish( lFootTraj.poses[i] );
                rFootPosePub.publish( rFootTraj.poses[i] );
                ros::Duration(0.005).sleep();
            }







//            Eigen::Affine3d lCurrentPose( Eigen::Translation3d( 0, -0.05, 0 ) );
//            Eigen::Affine3d rCurrentPose( Eigen::Translation3d( 0,  0.05, 0 ) );
//
//            re2uta::CirclePathStepGenerator generator( toAffine( re2uta::Ray( Eigen::Vector3d( 0,0,0), Eigen::Vector3d( 1, 0,0) ) ),
//                                                       toAffine( re2uta::Ray( Eigen::Vector3d( 2,1,0), Eigen::Vector3d( 0,-1,0) ) ), 0.2, 0.15 );
//            for( int i = 0 ; (!generator.hasArrived(rCurrentPose) || !generator.hasArrived(lCurrentPose)) && ros::ok(); ++i )
//            {
//                if( i % 2 )
//                {
//                    lCurrentPose = generator.generateNextStep( rCurrentPose, lCurrentPose, i );
//                    tf::poseEigenToMsg( lCurrentPose, poseStamped.pose );
//                    lFootTraj.poses.push_back( poseStamped );
//                    lFootPosePub.publish( poseStamped );
//                    std::cout << "Translation: " << lCurrentPose.translation() << "\n";
//                }
//                else
//                {
//                    rCurrentPose = generator.generateNextStep( lCurrentPose, rCurrentPose, i );
//                    tf::poseEigenToMsg( rCurrentPose, poseStamped.pose );
//                    rFootTraj.poses.push_back( poseStamped );
//                    rFootPosePub.publish( poseStamped );
//                    std::cout << "Translation: " << rCurrentPose.translation() << "\n";
//                }
//
//                lFootTrajPub.publish( lFootTraj );
//                rFootTrajPub.publish( rFootTraj );
//                ros::Duration(0.05).sleep();
//            }


//            re2uta::Ray ray0( Eigen::Vector3d( 0, 0, 0 ), Eigen::Vector3d( 1, 0, 0) );
//            re2uta::Ray ray1( Eigen::Vector3d( 2, 1, 0 ), Eigen::Vector3d( 0, -1, 0) );
//            re2uta::CircleVector circles = re2uta::findCircles(ray0,ray1);
//
//            Eigen::Quaterniond orientation;
//
//            simMarker->points.clear();
//
//            orientation = Eigen::Quaterniond().setFromTwoVectors( Eigen::XAxis3d, ray0.direction() );
//            simMarker->type = visualization_msgs::Marker::ARROW;
//            simMarker->id   = 20;
//            simMarker->scale.x = 0.1;
//            simMarker->scale.y = 0.1;
//            simMarker->scale.z = 0.10;
//            simMarker->pose.position.x = ray0.origin().x();
//            simMarker->pose.position.y = ray0.origin().y();
//            simMarker->pose.position.z = 0;
//            simMarker->pose.orientation.w = orientation.w();
//            simMarker->pose.orientation.x = orientation.x();
//            simMarker->pose.orientation.y = orientation.y();
//            simMarker->pose.orientation.z = orientation.z();
//            simMarkerPub.publish( simMarker );
//
//            orientation = Eigen::Quaterniond().setFromTwoVectors( Eigen::XAxis3d, ray1.direction() );
//            simMarker->type = visualization_msgs::Marker::ARROW;
//            simMarker->id   = 21;
//            simMarker->scale.x = 0.1;
//            simMarker->scale.y = 0.1;
//            simMarker->scale.z = 0.10;
//            simMarker->pose.position.x = ray1.origin().x();
//            simMarker->pose.position.y = ray1.origin().y();
//            simMarker->pose.position.z = 0;
//            simMarker->pose.orientation.w = orientation.w();
//            simMarker->pose.orientation.x = orientation.x();
//            simMarker->pose.orientation.y = orientation.y();
//            simMarker->pose.orientation.z = orientation.z();
//            simMarkerPub.publish( simMarker );
//
//            index = 22;
//            BOOST_FOREACH( const re2uta::Circle & circle, circles )
//            {
//                simMarker->type = visualization_msgs::Marker::CYLINDER;
//                simMarker->id   = index;
//                simMarker->scale.x = circle.radius() * 2;
//                simMarker->scale.y = circle.radius() * 2;
//                simMarker->scale.z = 0.005;
//                simMarker->color.a = 0.1;
//                simMarker->pose.position.x = circle.origin().x();
//                simMarker->pose.position.y = circle.origin().y();
//                simMarker->pose.position.z = 0;
//                simMarkerPub.publish( simMarker );
//                ++index;
//            }
//
//
//
//
//            tf::poseEigenToMsg( lCurrentPose, poseStamped.pose );
//            lFootTraj.poses.push_back( poseStamped );
//
//            tf::poseEigenToMsg( rCurrentPose, poseStamped.pose );
//            rFootTraj.poses.push_back( poseStamped );


        }
};


int main(int argCount, char** argVec)
{
    ros::init( argCount, argVec, "walk_pattern_generator_test" );
    ROS_INFO( "Starting walk_pattern_generator_test" );

    WalkPatternGeneratorTest walkPatternGeneratorTest;

    walkPatternGeneratorTest.go();

    return 0;
}

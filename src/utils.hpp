/*
 * utils.hpp
 *
 *  Created on: Apr 22, 2013
 *      Author: andrew.somerville
 */

#ifndef UTILS_HPP_
#define UTILS_HPP_

#include <re2uta/AtlasCommander/types.hpp>

#include <string>


namespace re2uta
{ namespace atlascommander
{

template <typename Type>
Type exponentialWeightedAverage( Type oldVal, Type newVal, double newWeight )
{
    return oldVal*(1-newWeight) + newWeight*(newVal);
}

template <typename Type>
Type lowPassFilter( Type oldVal, Type newVal, double newWeight )
{
    return exponentialWeightedAverage( oldVal, newVal, newWeight );
}


}
}





#endif /* UTILS_HPP_ */

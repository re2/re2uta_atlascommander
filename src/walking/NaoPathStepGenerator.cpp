/*
 * NaoStepGenerator.cpp
 *
 *  Created on: Jun 19, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/AtlasCommander/BdiStepCommander.hpp>
#include <re2uta/walking/NaoPathStepGenerator.hpp>
#include <re2uta/walking/StepGenerator.hpp>

#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>

#include <urdf/model.h>

#include <humanoid_nav_msgs/PlanFootsteps.h>
#include <atlas_msgs/AtlasSimInterfaceCommand.h>
#include <atlas_msgs/AtlasSimInterfaceState.h>
//#include <atlas_msgs/AtlasControlTypes.h>
#include <re2uta/AtlasCommander/types.hpp>
#include <re2/eigen/eigen_util.h>
#include <re2/matrix_conversions.h>

#include <eigen_conversions/eigen_msg.h>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_broadcaster.h>
#include <eigen_conversions/eigen_kdl.h>
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <boost/foreach.hpp>


namespace re2uta
{


NaoPathStepGenerator::
NaoPathStepGenerator( const StepPose::ConstPtr srcStepPose,
                      const Eigen::Affine3d & destPose )
    : StepGenerator( ros::Duration( std::numeric_limits<double>::quiet_NaN() ) )
{
    Eigen::Affine3d srcPose;
    srcPose = interpolate( srcStepPose->baseFootPose(), srcStepPose->swingFootPose(), 0.5 );
    init( srcStepPose, srcPose, destPose );
}

NaoPathStepGenerator::
NaoPathStepGenerator( const  Eigen::Affine3d & srcPose,
                      const  Eigen::Affine3d & destPose,
                      double footSpreadOffset )
    : StepGenerator( ros::Duration( std::numeric_limits<double>::quiet_NaN() ) )
{
//    Eigen::Affine3d srcPose;
//    assert(false);
    m_srcStepPose.reset( new StepPose( LEFT_FOOT ) ); // NAO Always starts with LEFT?
    m_srcStepPose->lFootPose = Eigen::Translation3d( 0,  footSpreadOffset, 0 ) * srcPose;
    m_srcStepPose->rFootPose = Eigen::Translation3d( 0, -footSpreadOffset, 0 ) * srcPose;
    m_srcStepPose->comPose   = srcPose.translation() + Eigen::Vector3d( 0, 0, 0.83 );

    init( m_srcStepPose, srcPose, destPose );
}



void
NaoPathStepGenerator::
init( const StepPose::ConstPtr srcStepPose,  const  Eigen::Affine3d & srcPose, const  Eigen::Affine3d & destPose )
{

    m_footStepPlannerClient  = m_node.serviceClient<humanoid_nav_msgs::PlanFootsteps>( "/plan_footsteps", true );

    humanoid_nav_msgs::PlanFootstepsResponsePtr footStepResponse;

    if( m_footStepPlannerClient.exists() )
    {
        humanoid_nav_msgs::PlanFootstepsRequest  request;

        request.start.x     = srcPose.translation().x();
        request.start.y     = srcPose.translation().y();
        request.start.theta = srcPose.rotation().eulerAngles(0,1,2).z();

        request.goal.x      = destPose.translation().x();
        request.goal.y      = destPose.translation().y();
        request.goal.theta  = destPose.rotation().eulerAngles(0,1,2).z();

        ROS_WARN( "About to call to service" );
        bool success = false;
        ros::Time t0 = ros::Time::now();
        footStepResponse.reset( new humanoid_nav_msgs::PlanFootstepsResponse );
        success = m_footStepPlannerClient.call( request, *footStepResponse );
        ros::Time t1 = ros::Time::now();
        ROS_WARN( "Service call took: %2.3f seconds", (t1-t0).toSec() );

        if( success && footStepResponse->result && footStepResponse->footsteps.size() > 0 )
        {
            ROS_WARN( "service success: %i", footStepResponse->result );
        }
        else
        {
            ROS_ERROR( "call to foot step planner failed : %i", footStepResponse->result );
            if( !m_footStepPlannerClient.exists() )
                ROS_ERROR( "foot step planner service doesn't exist" );

            footStepResponse.reset(); //FIXME causes crash!!!!!!!!!!!!
        }
    }
    else
    {
        ROS_ERROR( "call to foot step planner failed" );
    }

    StepPosePtrVectorPtr stepPoses;
    if( footStepResponse )
    {
        stepPoses.reset( new StepPosePtrVector );

        Foot baseFoot = m_srcStepPose->baseFoot();
        StepPose::Ptr previousStepPose( new StepPose( *m_srcStepPose ) ); // will be negated in the loop

        int index = 0;
        BOOST_FOREACH( const humanoid_nav_msgs::StepTarget & stepTarget, footStepResponse->footsteps )
        {
            if( index == 0 || index == 1 )
            {
                ++index;
                continue;
            }

            Foot swingFoot;

            if( stepTarget.leg == humanoid_nav_msgs::StepTarget::left ) // according to StepTarget.h
                swingFoot = LEFT_FOOT;
            if( stepTarget.leg == humanoid_nav_msgs::StepTarget::right ) // according to StepTarget.h
                swingFoot = RIGHT_FOOT;

            Foot baseFoot = otherFoot( swingFoot );

            StepPose::Ptr stepPose( new StepPose( baseFoot ) );

            Eigen::Affine3d swingFootPose = Eigen::Affine3d::Identity(); //m_aggPoseAtCommandTime;
            swingFootPose.translation().x() += stepTarget.pose.x;
            swingFootPose.translation().y() += stepTarget.pose.y;
            swingFootPose.rotate( Eigen::AngleAxisd( stepTarget.pose.theta, Eigen::ZAxis3d ) );

            stepPose->swingFootPose() = swingFootPose;
            stepPose->baseFootPose()  = previousStepPose->swingFootPose();

            stepPoses->push_back( stepPose );
        }
    }

    m_stepPoses = stepPoses;
    if( m_stepPoses )
        m_stepPoseItor = m_stepPoses->begin();
}


bool
NaoPathStepGenerator::
hasArrived( const Eigen::Affine3d & current ) const
{
    if(  !m_stepPoses
       || m_stepPoseItor == m_stepPoses->end() )
    {
        return true;
    }
    else
    {
        return false;
    }
}


Eigen::Affine3d
NaoPathStepGenerator::
generateNextStep( const Eigen::Affine3d & last,
                  const Eigen::Affine3d & current,
                  int  stepIndex )
{
    Eigen::Affine3d nextStep = current;
    if(    m_stepPoses
        && m_stepPoseItor != m_stepPoses->end() )
    {
        if( *m_stepPoseItor )
            nextStep = (*m_stepPoseItor)->swingFootPose();

//        if( boost::next( m_stepPoseItor ) != m_stepPoses->end() )
//        {
            ++ m_stepPoseItor;
//        }
    }

    return nextStep;
}

StepPose::Ptr
NaoPathStepGenerator::
getBeginStepPose()
{
    return m_srcStepPose;
}




}

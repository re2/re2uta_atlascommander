/*
 * ManualStepGenerator.cpp
 *
 *  Created on: Jun 19, 2013
 *      Author: andrew.somerville
 */



#include <re2uta/walking/ManualStepGenerator.hpp>
//#include <re2uta/walking/StepGenerator.hpp>
//
//#include <visualization_msgs/MarkerArray.h>
//#include <visualization_msgs/Marker.h>
//
//#include <urdf/model.h>
//
//#include <humanoid_nav_msgs/PlanFootsteps.h>
//#include <atlas_msgs/AtlasSimInterfaceCommand.h>
//#include <atlas_msgs/AtlasSimInterfaceState.h>
////#include <atlas_msgs/AtlasControlTypes.h>
//#include <re2uta/AtlasCommander/types.hpp>
//#include <re2/eigen/eigen_util.h>
//#include <re2/matrix_conversions.h>
//
//#include <eigen_conversions/eigen_msg.h>
//#include <tf_conversions/tf_eigen.h>
//#include <tf/transform_broadcaster.h>
//#include <eigen_conversions/eigen_kdl.h>
//#include <ros/ros.h>
//#include <Eigen/Geometry>
//#include <boost/foreach.hpp>


namespace re2uta
{

ManualStepGenerator::
ManualStepGenerator( const StepPose::Ptr & currentStepPose, const StepPosePtrVectorPtr & stepPoses )
    : StepGenerator( ros::Duration( std::numeric_limits<double>::quiet_NaN() ) )
{
    m_srcStepPose  = currentStepPose;
    m_stepPoses    = stepPoses;

    if( m_stepPoses )
        m_stepPoseItor = m_stepPoses->begin();
    else
        ROS_ERROR( "Step Poses empty" );
}


ManualStepGenerator::
ManualStepGenerator( const StepPose::Ptr & currentStepPose, const Eigen::Affine3d & destPose )
    : StepGenerator( ros::Duration( std::numeric_limits<double>::quiet_NaN() ) )
{
    m_srcStepPose = currentStepPose;
    m_stepPoses.reset( new StepPosePtrVector );

    StepPose::Ptr nextStepPose( new StepPose( *currentStepPose ) );
    nextStepPose->swingFootPose() = destPose;

    m_stepPoses->push_back( nextStepPose );

    if( m_stepPoses )
        m_stepPoseItor = m_stepPoses->begin();
    else
        ROS_ERROR( "Step Poses empty" );
}


ManualStepGenerator::
~ManualStepGenerator()
{

}


bool
ManualStepGenerator::
hasArrived(   const Eigen::Affine3d & current ) const
{
    if(    !m_stepPoses
        || m_stepPoseItor == m_stepPoses->end() )
    {
        return true;
    }
    return false;
}

Eigen::Affine3d
ManualStepGenerator::
generateNextStep( const Eigen::Affine3d & last,
                  const Eigen::Affine3d & current,
                  int  stepIndex )
{
    if( m_stepPoses && m_stepPoseItor != m_stepPoses->end() )
    {
        Eigen::Affine3d pose = (*m_stepPoseItor)->swingFootPose();
        ++m_stepPoseItor;
        return pose;
    }
    else
    {
        return current;
    }
}

StepPose::Ptr
ManualStepGenerator::
getBeginStepPose()
{
    return m_srcStepPose;
}



}

/*
 * CirclePathStepGenerator.cpp
 *
 *  Created on: Jun 17, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/walking/CirclePathStepGenerator.hpp>
#include <re2/visutils/marker_util.h>

#include <ros/ros.h>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <boost/math/special_functions/sign.hpp>
#include <iostream>

namespace re2uta
{



bool sameSide( const Ray & ray0, const Ray & ray1 )
{
    Eigen::Vector3d r0r1 = ray1.origin() - ray0.origin();
    Eigen::Vector3d r1r0 = ray0.origin() - ray1.origin();
    double projection0   = r0r1.dot( ray0.normal(Eigen::ZAxis3d) );
    double projection1   = r1r0.dot( ray1.normal(Eigen::ZAxis3d) );

    return boost::math::sign( projection0 ) == boost::math::sign( projection1 );
}



CircleVector findCircles( const Ray & ray0, const Ray & ray1 )
{
    Circle circle0;
    Circle circle1;

    ros::NodeHandle node;
    ros::Publisher simMarkerPub = node.advertise<visualization_msgs::Marker>("/walkTrajectory/sim",    5, true);
    using re2::visualization_msgs_util::createMarkerMsg;
    visualization_msgs::Marker::Ptr simMarker;
    Eigen::Vector4d color( 1,1,0,1 );
    simMarker = createMarkerMsg( 1,
                                 visualization_msgs::Marker::LINE_STRIP,
                                 color,
                                 "test",
                                 "odom" );

//    re2::VisualDebugPublisher pub( "circles" );

    Eigen::Vector3d p0p1 = ray1.origin() - ray0.origin();

    circle0.radius() = p0p1.norm() / 4;
    circle1.radius() = p0p1.norm() / 4;

    //Project the vector between the ray origins onto their normals to get
    //the "side" they're oin.

    Eigen::Vector3d r0r1 = ray1.origin() - ray0.origin();
    Eigen::Vector3d r1r0 = ray0.origin() - ray1.origin();

    double projection0   = r0r1.dot( ray0.normal(Eigen::ZAxis3d) );
    double projection1   = r1r0.dot( ray1.normal(Eigen::ZAxis3d) );

    Eigen::Vector3d circle0direction = (projection0 * ray0.normal(Eigen::ZAxis3d)).normalized();
    Eigen::Vector3d circle1direction = (projection1 * ray1.normal(Eigen::ZAxis3d)).normalized();

    if( isnan( circle0direction.norm() )  )
        circle0direction = ray0.normal(Eigen::ZAxis3d).normalized();

    if( isnan( circle1direction.norm() ) )
        circle1direction = ray1.normal(Eigen::ZAxis3d).normalized();


    circle0.origin() = ray0.origin() + circle0direction * circle0.radius();
    circle1.origin() = ray1.origin() + circle1direction * circle1.radius();


    double upperBound0 = std::numeric_limits<double>::infinity();
    double lowerBound0 = 0;

    double upperBound1 = std::numeric_limits<double>::infinity();
    double lowerBound1 = 0;

    Eigen::Vector3d edgePoint0;
    Eigen::Vector3d edgePoint1;

    double * upperBound = NULL;
    double * lowerBound = NULL;
    Circle * circle     = NULL;
    const Ray * ray        = NULL;
    Eigen::Vector3d * circleDirection = NULL;

    double diff      = 10000;
    double tolerance = 0.02;

    int count = 0;
    while( std::abs( diff ) > tolerance && ros::ok() )
    {
        if( count % 2 == 0 )
        {
//            std::cout << "\n\nCircle0 \n";
            upperBound  = &upperBound0;
            lowerBound  = &lowerBound0;
            circle      = &circle0;
            ray         = &ray0;
            circleDirection = &circle0direction;
        }
        else
        {
//            std::cout << "\n\nCircle1 \n";
            upperBound  = &upperBound1;
            lowerBound  = &lowerBound1;
            circle      = &circle1;
            ray         = &ray1;
            circleDirection = &circle1direction;
        }


        if( isinf( *upperBound ) )
            circle->radius() = circle->radius() * 2;
        else
            circle->radius() = (*upperBound + *lowerBound)/2;

        circle->origin() = ray->origin() + *circleDirection * circle->radius();

        edgePoint0 = circle0.origin() + (circle1.origin() - circle0.origin()).normalized() * circle0.radius();
        edgePoint1 = circle1.origin() + (circle0.origin() - circle1.origin()).normalized() * circle1.radius();

        diff = (edgePoint0 - circle0.origin()).norm() - (edgePoint1 - circle0.origin()).norm();

        if( diff < 0 ) // edge0 is closer to circle0 than edge1, need to grow
            *lowerBound = circle->radius();
        else
            *upperBound = circle->radius();

        simMarker->type = visualization_msgs::Marker::CYLINDER;
        simMarker->id   = 22;
        simMarker->scale.x = circle0.radius() * 2;
        simMarker->scale.y = circle0.radius() * 2;
        simMarker->scale.z = 0.005;
        simMarker->color.a = 0.1;
        simMarker->pose.position.x = circle0.origin().x();
        simMarker->pose.position.y = circle0.origin().y();
        simMarker->pose.position.z = 0;
        simMarkerPub.publish( simMarker );

        simMarker->type = visualization_msgs::Marker::CYLINDER;
        simMarker->id   = 23;
        simMarker->scale.x = circle1.radius() * 2;
        simMarker->scale.y = circle1.radius() * 2;
        simMarker->scale.z = 0.005;
        simMarker->color.a = 0.1;
        simMarker->pose.position.x = circle1.origin().x();
        simMarker->pose.position.y = circle1.origin().y();
        simMarker->pose.position.z = 0;
        simMarkerPub.publish( simMarker );
//
//        std::cout << "diff " << diff << "\n" ;
//        std::cout << "circleDirection " << *circleDirection << "\n" ;
//        std::cout << "upperBound " << upperBound << "\n" ;
//        std::cout << "lowerBound0 " << lowerBound0 << "\n" ;
//        std::cout << "circle0.radius()\t" << circle0.radius() << "\n" ;
//        std::cout << "circle0.origin() "  << circle0.origin().transpose() << "\n" ;
//        std::cout << "circle1.radius()\t" << circle1.radius() << "\n" ;
//        std::cout << "circle1.origin() "  << circle1.origin().transpose() << "\n" ;
//        std::cout << "edgePoint0 " << edgePoint0.transpose() << "\n" ;
//        std::cout << "edgePoint1 " << edgePoint1.transpose() << "\n" ;
//        std::cout << "edgediff1  " << (edgePoint1 - circle0.origin()).transpose() << "\n";
//        std::cout << "edgediff0  " << (edgePoint0 - circle0.origin()).transpose() << "\n" ;
//        std::cout << "edgediff1  \t" << (edgePoint1 - circle0.origin()).norm() << "\n";
//        std::cout << "edgediff0  \t" << (edgePoint0 - circle0.origin()).norm() << "\n" ;
//
        ++count;
    }

    CircleVector circles;
    circles.push_back( circle0 );

    if( sameSide( ray0, ray1 ) )
    {
        std::cout << "Oh shit. They're on the same side." << std::endl;
        Circle extraCircle;
        extraCircle.radius() = (circle0.radius() + circle1.radius())/2; //FIXME how to pick this?

        double c0c2Dist = circle0.radius() + extraCircle.radius();
        double c0c1Dist = circle0.radius() + circle1.radius();
        double c1c2Dist = circle1.radius() + extraCircle.radius();

        Eigen::Vector3d c0c1 = circle1.origin() - circle0.origin();
        Eigen::Vector3d c0q;
        c0q = c0c1.normalized() * (c0c1Dist/2 + ((   pow( c0c2Dist, 2 )
                                                   - pow( c1c2Dist, 2 )
                                                 )  / (2*c0c1Dist)
                                                )
                                  );

        std::cout << "c0c1        " << c0c1.transpose() << "\n";
        std::cout << "c0c1.norm() " << c0c1.norm()      << "\n";
        std::cout << "c0c1.normalized() " << c0c1.normalized().transpose()      << "\n";
        std::cout << "c0c1Dist    " << c0c1Dist         << "\n";
        std::cout << "c0c2Dist    " << c0c2Dist         << "\n";
        std::cout << "c1c2Dist    " << c1c2Dist         << "\n";
        std::cout << "pow( c0c2Dist, 2 ) " << pow( c0c2Dist, 2 ) << "\n";
        std::cout << "pow( c1c2Dist, 2 ) " << pow( c1c2Dist, 2 ) << "\n";
        std::cout << "pow( c0c2Dist, 2 ) - pow( c1c2Dist, 2 ) " << pow( c0c2Dist, 2 ) - pow( c1c2Dist, 2 ) << "\n";
        std::cout << "(pow( c0c2Dist, 2 ) - pow( c1c2Dist, 2 )) / 2*c0c1Dist " << (pow( c0c2Dist, 2 ) - pow( c1c2Dist, 2 )) / (2*c0c1Dist) << "\n";
        std::cout << "pow( c1c2Dist, 2 ) " << pow( c1c2Dist, 2 ) << "\n";
        std::cout << "c0q         " << c0q.transpose() << "\n";
        std::cout << "c0c1        " << c0q.norm()      << "\n";


        Eigen::Vector3d c0r0            = ray0.origin()    - circle0.origin();
        Eigen::Vector3d rotationVector0 = c0r0.cross( ray0.direction() );
        Eigen::Vector3d c0c1Normal      = c0c1.cross( rotationVector0 ).normalized();

        Eigen::Vector3d qc2;
        qc2 = c0c1Normal * sqrt( pow( c0c2Dist, 2 ) - pow( c0q.norm(), 2 ) );

        extraCircle.origin() = circle0.origin() + c0q + qc2;

        circles.push_back( extraCircle );

        simMarker->type = visualization_msgs::Marker::CYLINDER;
        simMarker->id   = 24;
        simMarker->scale.x = extraCircle.radius() * 2;
        simMarker->scale.y = extraCircle.radius() * 2;
        simMarker->scale.z = 0.005;
        simMarker->color.a = 0.1;
        simMarker->pose.position.x = extraCircle.origin().x();
        simMarker->pose.position.y = extraCircle.origin().y();
        simMarker->pose.position.z = 0;
        simMarkerPub.publish( simMarker );
    }

    circles.push_back( circle1 );

    return circles;
}


CirclePathStepGenerator::
CirclePathStepGenerator( const StepPose::ConstPtr srcPose,
                         const Eigen::Affine3d & dest,
                         double halfStepLength,
                         double footSpreadOffset,
                         Foot   swingFoot )
    : StepGenerator( ros::Duration( std::numeric_limits<double>::quiet_NaN() ) )
{
    m_source           = Eigen::Affine3d::Identity();
    m_source.
    translation()       = srcPose->comPose;
    m_dest             = dest;
    m_halfStepLength   = halfStepLength;
    m_footSpreadOffset = footSpreadOffset;
    m_srcPose.reset( new StepPose (*srcPose) );

    Ray ray0( m_source.translation(), m_source.rotation() * Eigen::XAxis3d );
    Ray ray1( m_dest.translation(),   m_dest.rotation()   * Eigen::XAxis3d );

    m_circles        = findCircles( ray0, ray1 );
    m_circleIterator = m_circles.begin();
    m_swingFoot      = swingFoot;
}


CirclePathStepGenerator::
CirclePathStepGenerator( const Eigen::Affine3d & source,
                         const Eigen::Affine3d & dest,
                         double halfStepLength,
                         double footSpreadOffset,
                         Foot   swingFoot )
    : StepGenerator( ros::Duration( std::numeric_limits<double>::quiet_NaN() ) )
{
    m_source           = source;
    m_dest             = dest;
    m_halfStepLength   = halfStepLength;
    m_footSpreadOffset = footSpreadOffset;

    Ray ray0( m_source.translation(), m_source.rotation() * Eigen::XAxis3d );
    Ray ray1( m_dest.translation(),   m_dest.rotation()   * Eigen::XAxis3d );

    m_circles        = findCircles( ray0, ray1 );
    m_circleIterator = m_circles.begin();
    m_swingFoot      = swingFoot;

    StepPose::Ptr beginStepPose( new StepPose( otherFoot(m_swingFoot) ) );

    // We need the locations of the feet instead of just getting a pose
    Eigen::Translation3d lFootPose( 0,  m_footSpreadOffset, 0    );
    Eigen::Translation3d rFootPose( 0, -m_footSpreadOffset, 0    );
    Eigen::Translation3d   comPose( 0,                0.03, 0.83 );

    beginStepPose->lFootPose = m_source * lFootPose;
    beginStepPose->rFootPose = m_source * rFootPose;
    beginStepPose->comPose   = comPose.translation();

    m_srcPose = beginStepPose;
}


StepPose::Ptr
CirclePathStepGenerator::
getBeginStepPose()
{
    return m_srcPose;
}


bool
CirclePathStepGenerator::
hasArrived( const Eigen::Affine3d & current ) const
{
    return withinDistance( current, m_footSpreadOffset + 0.01 );
}

bool
CirclePathStepGenerator::
withinDistance( const Eigen::Affine3d & current, double distance ) const
{
    Circle & circle = *m_circleIterator;

    Eigen::Vector3d currentRadiusVector = (current.translation() - circle.origin()).normalized();
    Eigen::Vector3d currentDirection    = (current.rotation() * Eigen::XAxis3d);
//    Eigen::Vector3d destDirection       = (m_dest.rotation() * Eigen::XAxis3d);
    Eigen::Vector3d nextLocation        = current.translation() + currentDirection.normalized() * distance;
    Eigen::Vector3d nextInDestFrame     = nextLocation          - m_dest.translation();
    Eigen::Vector3d currentInDestFrame  = current.translation() - m_dest.translation();

//    double projectionOntoDestVec = nextInDestFrame.dot( destDirection );
    double nextDistFromDest             = (nextInDestFrame).head(2).norm();
    double currentDistFromDest          = (currentInDestFrame).head(2).norm();
//
    std::cout << "\n";
//    std::cout << "current: "    << current.translation().transpose() << "\n";
    std::cout << "Next:    "    << nextLocation.transpose() << "\n";
//    std::cout << "Dest:    "    << m_dest.translation().transpose() << "\n";
//    std::cout << "NDistance:  " << nextDistFromDest    << "\n";
//    std::cout << "CDistance:  " << currentDistFromDest << "\n";
//    std::cout << "compare:    " << distance << "\n";

    if(    nextDistFromDest    < distance
        || currentDistFromDest < distance )
        return true;
    else
        return false;
}


CircleVector::iterator
CirclePathStepGenerator::
determineCircle( const Eigen::Affine3d & current )
{
    CircleVector::iterator  circle0 = m_circleIterator;
    CircleVector::iterator  circle1 = boost::next( circle0 );

    if( circle1 != m_circles.end() )
    {
        double distToCircleCenter0 = (current.translation() - circle0->origin()).norm();
        double distToCircleCenter1 = (current.translation() - circle1->origin()).norm();
        double diffFromCircleEdge0 = distToCircleCenter0 - circle0->radius();
        double diffFromCircleEdge1 = distToCircleCenter1 - circle1->radius();

        if(   diffFromCircleEdge0
            > diffFromCircleEdge1 )
        {
            return boost::next( m_circleIterator );
        }
    }

    return m_circleIterator;
}



Eigen::Affine3d
CirclePathStepGenerator::
generateNextStep( const Eigen::Affine3d & lastBase, const Eigen::Affine3d & currentSwing, int stepIndex )
{
    bool isInsideTrack = false;
    double effectiveRadius = (m_circleIterator->origin() - currentSwing.translation()).norm();

    if( effectiveRadius < m_circleIterator->radius() )
        isInsideTrack = true;

    CircleVector::iterator lastCircle = m_circleIterator;
    m_circleIterator = determineCircle( currentSwing );
    Circle & circle = *m_circleIterator;

    if( lastCircle != m_circleIterator )
        isInsideTrack = !isInsideTrack;


    double stepLength = 2*m_halfStepLength;

    double insideRadius    = circle.radius() - m_footSpreadOffset;
    double outsideRadius   = circle.radius() + m_footSpreadOffset;
    double maxArcAngle     = (stepLength) / outsideRadius; //r*theta = arclength, so arclength/radius = theta
    double arcAngle        = maxArcAngle/2;

//    if( stepIndex == 0 )
//        arcAngle = maxArcAngle / 2;


    double desiredRadius;

    if( isInsideTrack )
        desiredRadius = insideRadius;
    else
        desiredRadius = outsideRadius;

    Eigen::Affine3d swingStep = Eigen::Affine3d::Identity();

//    Eigen::Vector3d currentRadiusVector = (current.translation() - circle.origin()).normalized() * desiredRadius;
    Eigen::Vector3d lastRadiusVector    = (lastBase.translation() - circle.origin()).normalized() * desiredRadius;
    Eigen::Vector3d currentDirection    = currentSwing.rotation() * Eigen::XAxis3d;
    Eigen::Vector3d rotationVector      = lastRadiusVector.cross( currentDirection ).normalized();

    Eigen::Vector3d destinationRadiusVector = (m_dest.translation() - circle.origin()).normalized() * desiredRadius;
    Eigen::Vector3d nextRadiusVector        = Eigen::AngleAxisd( arcAngle, rotationVector.normalized() ) * lastRadiusVector;

    if( (lastRadiusVector - destinationRadiusVector).norm() < (lastRadiusVector - nextRadiusVector).norm() )
        nextRadiusVector = destinationRadiusVector;

    Eigen::Vector3d nextTangent = rotationVector.cross( nextRadiusVector ).normalized();

    swingStep.translation() = (nextRadiusVector + circle.origin()) - (currentSwing.translation());
    swingStep.linear()      = Eigen::Quaterniond().setFromTwoVectors( currentDirection, nextTangent ).toRotationMatrix();

    swingStep.translation() += currentSwing.translation();
    swingStep.linear()       = swingStep.rotation() * currentSwing.rotation();
    return swingStep;

}




}

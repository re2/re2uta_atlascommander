/*
 * interactive_marker_util.cpp
 *
 *  Created on: Jun 11, 2013
 *      Author: andrew.somerville
 */

#include "eigen_utils.hpp"

#include <re2uta/interactive_marker_utils.hpp>
#include <interactive_markers/interactive_marker_server.h>
#include <eigen_conversions/eigen_msg.h>
#include <re2/eigen/eigen_util.h>
#include <Eigen/Geometry>

namespace re2uta
{



visualization_msgs::InteractiveMarker::Ptr
create6dBoxControl( const std::string & frameId, const std::string & name, const std::string & description )
{
    visualization_msgs::InteractiveMarker::Ptr marker0( new visualization_msgs::InteractiveMarker );
    marker0->header.frame_id = frameId;
    marker0->name            = name;
    marker0->description     = description;
    marker0->scale           = 0.1;

    // create a grey box marker
    visualization_msgs::Marker boxMarker;
    boxMarker.type = visualization_msgs::Marker::CUBE;
    boxMarker.scale.x = 0.05;
    boxMarker.scale.y = 0.05;
    boxMarker.scale.z = 0.05;
    boxMarker.color.r = 0.5;
    boxMarker.color.g = 0.5;
    boxMarker.color.b = 0.5;
    boxMarker.color.a = 1.0;

    // create a non-interactive control which contains the box
    visualization_msgs::InteractiveMarkerControl boxControl;
    boxControl.always_visible = true;
    boxControl.markers.push_back( boxMarker );

    marker0->controls.push_back( boxControl );

    visualization_msgs::InteractiveMarkerControl rotateControl;

    rotateControl.name = "rotate_x";
    rotateControl.interaction_mode = visualization_msgs::InteractiveMarkerControl::ROTATE_AXIS;
    marker0->controls.push_back(rotateControl);

    rotateControl.name = "move_x";
    rotateControl.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
    marker0->controls.push_back(rotateControl);


    rotateControl.orientation = toMsg( Eigen::Quaterniond().setFromTwoVectors(Eigen::XAxis3d, Eigen::YAxis3d) ).orientation;
    rotateControl.name = "rotate_y";
    rotateControl.interaction_mode = visualization_msgs::InteractiveMarkerControl::ROTATE_AXIS;
    marker0->controls.push_back(rotateControl);

    rotateControl.orientation = toMsg( Eigen::Quaterniond().setFromTwoVectors(Eigen::XAxis3d, Eigen::YAxis3d) ).orientation;
    rotateControl.name = "move_y";
    rotateControl.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
    marker0->controls.push_back(rotateControl);


    rotateControl.orientation = toMsg( Eigen::Quaterniond().setFromTwoVectors(Eigen::XAxis3d, Eigen::ZAxis3d) ).orientation;
    rotateControl.name = "rotate_z";
    rotateControl.interaction_mode = visualization_msgs::InteractiveMarkerControl::ROTATE_AXIS;
    marker0->controls.push_back(rotateControl);

    rotateControl.orientation = toMsg( Eigen::Quaterniond().setFromTwoVectors(Eigen::XAxis3d, Eigen::ZAxis3d) ).orientation;
    rotateControl.name = "rotate_z";
    rotateControl.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
    marker0->controls.push_back(rotateControl);

    return marker0;
}




}

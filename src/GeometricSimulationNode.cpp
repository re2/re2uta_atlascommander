/*
 * GeometricSimulationNode.cpp
 *
 *  Created on: Apr 26, 2013
 *      Author: andrew.somerville
 */



#include <re2uta/AtlasLookup.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <re2/kdltools/kdl_tools.h>
#include <atlas_msgs/AtlasCommand.h>
#include <atlas_msgs/AtlasState.h>
#include <re2/eigen/eigen_util.h>
#include <sensor_msgs/JointState.h>
#include <robot_state_publisher/robot_state_publisher.h>
#include <tf/transform_broadcaster.h>
#include <kdl_parser/kdl_parser.hpp>
#include <atlas_msgs/SetJointDamping.h>
#include <geometry_msgs/Quaternion.h>
#include <urdf_model/model.h>
#include <ros/ros.h>
#include <boost/foreach.hpp>
#include <iostream>
#include <fstream>


using namespace re2uta;

class GeometricSimulationNode
{
    private:
        typedef std::map<std::string, double> JointPositionMap;

        ros::NodeHandle  m_node;
        KdlTreeConstPtr  m_tree;
        urdf::Model      m_urdfModel;
        Eigen::VectorXd  m_defaultTreeJointAngles;
        JointPositionMap m_jointPositionMap;
        atlas_msgs::AtlasState::Ptr m_outputState;
        AtlasLookup::Ptr m_atlasLookup;
        ros::Duration    m_publishPeriod;
        ros::Subscriber  m_atlasCommandSub;
        ros::Publisher   m_atlasStatePub;
        double           m_staticZForce;
        geometry_msgs::
        Quaternion       m_imuOrientation;
        ros::ServiceServer m_jointDampingSrv;

        tf::TransformBroadcaster m_broadcaster;


    public:
        GeometricSimulationNode()
        {
            m_publishPeriod.fromSec( 0.01 );
            std::string urdfString;

            m_staticZForce = 0;
            m_node.getParam( "static_force_value_z", m_staticZForce );

            m_node.getParam( "/robot_description", urdfString );
            m_urdfModel.initString( urdfString );
            m_atlasLookup.reset( new AtlasLookup( m_urdfModel ) );
            m_tree = m_atlasLookup->getTree();

            double legBend           = 0.4;
            m_defaultTreeJointAngles = m_atlasLookup->calcDefaultTreeJointAngles( legBend );
            m_atlasCommandSub        = m_node.subscribe( "/atlas/atlas_command", 1, &GeometricSimulationNode::handleAtlasJointCommand, this );
            m_atlasStatePub          = m_node.advertise<atlas_msgs::AtlasState>( "/atlas/atlas_state", 1 );
            m_jointDampingSrv        = m_node.advertiseService( "/atlas/set_joint_damping", &GeometricSimulationNode::handleJointDampingSrv, this );

            atlas_msgs::AtlasState::Ptr outputState; //( new atlas_msgs::AtlasState ); // FIXME, we need to fill this out
            outputState.reset( new atlas_msgs::AtlasState );
            outputState->position = re2::toStd( (Eigen::VectorXf)m_atlasLookup->calcDefaultCmdJointAngles( legBend ).cast<float>() );
            outputState->velocity = std::vector<float>( outputState->position.size(), 0 );
            outputState->l_foot.force.z = m_staticZForce;
            outputState->r_foot.force.z = m_staticZForce;
            m_imuOrientation.w            = 1;
            m_imuOrientation.x            = 0;
            m_imuOrientation.y            = 0;
            m_imuOrientation.z            = 0;
            outputState->orientation    = m_imuOrientation;

            boost::atomic_store( &m_outputState, atlas_msgs::AtlasState::Ptr( outputState ) );
            BOOST_FOREACH( KDL::SegmentMap::value_type const & entry, m_tree->getSegments() )
            {
                const KDL::Segment  & segment      = entry.second.segment;

                if( segment.getJoint().getType() != KDL::Joint::None )
                {
                    int qnr = entry.second.q_nr;
                    m_jointPositionMap[ segment.getJoint().getName() ] = m_defaultTreeJointAngles(qnr);
                    ROS_INFO_STREAM( "Joint at " << segment.getJoint().getName() << " starting at " <<  m_defaultTreeJointAngles(qnr) );
                }
            }

            ROS_INFO_STREAM( "GeometricSimulationNode setup" );
        }


        bool handleJointDampingSrv( atlas_msgs::SetJointDamping::Request & req, atlas_msgs::SetJointDamping::Response & res )
        {
            ROS_INFO( "Fake set joint damping" );
            return true;
        }


        void handleAtlasJointCommand( const atlas_msgs::AtlasCommandConstPtr & msg )
        {
            atlas_msgs::AtlasState::Ptr outputState; //( new atlas_msgs::AtlasState ); // FIXME, we need to fill this out
            outputState.reset( new atlas_msgs::AtlasState );

            int cmdJointId = 0;
            BOOST_FOREACH( const float & position,  msg->position )
            {
                if( msg->k_effort[cmdJointId] > 0 )
                {
                    outputState->position.push_back(    m_outputState->position[cmdJointId]
                                                      + msg->kp_position[cmdJointId] * (msg->position[cmdJointId]
                                                                                         - m_outputState->position[cmdJointId])
                                                      +  msg->velocity[cmdJointId] * m_publishPeriod.toSec() );
                }
                else
                {
                    outputState->position.push_back( m_outputState->position[cmdJointId] );
                }

                outputState->velocity.push_back( msg->velocity[cmdJointId] );
                std::string jointName = m_atlasLookup->jointIndexToJointName( cmdJointId );
                m_jointPositionMap[ jointName ] = (double)outputState->position.back();
                ++cmdJointId;
            }

            outputState->l_foot.force.z = m_staticZForce;
            outputState->r_foot.force.z = m_staticZForce;
            outputState->header.stamp = ros::Time::now();

            boost::atomic_store( &m_outputState, atlas_msgs::AtlasState::Ptr( outputState ) );
        }


        void go()
        {
            ROS_INFO_STREAM( "GeometricSimulationNode starting" );
            robot_state_publisher::RobotStatePublisher robotStatePublisher( *m_tree );

            atlas_msgs::AtlasState::Ptr outputState;

            while( ros::ok() )
            {
                m_publishPeriod.sleep();
                ros::spinOnce();

                robotStatePublisher.publishFixedTransforms();
                robotStatePublisher.publishTransforms( m_jointPositionMap, ros::Time::now() );

                outputState = boost::atomic_load( &m_outputState );
                if( outputState )
                {
                    outputState->header.stamp = ros::Time::now();
                    m_atlasStatePub.publish( outputState );
                }
            }
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};


int main( int argc, char ** argv )
{
    ros::init( argc, argv, "re2uta_atlasCommander_geometricSimulationNode" );
    ros::NodeHandle node;

    GeometricSimulationNode geometricSimulationNode;

    geometricSimulationNode.go();

    return 0;
}

/*
 * eigen_utils.hpp
 *
 *  Created on: Jun 11, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PointStamped.h>
#include <eigen_conversions/eigen_msg.h>
#include <re2/eigen/eigen_util.h>
#include <Eigen/Geometry>


namespace re2uta
{

template< typename T >
geometry_msgs::Pose toMsg( const T& anyPose )
{
    Eigen::Affine3d eigPose( anyPose );
    geometry_msgs::Pose pose;
    tf::poseEigenToMsg( eigPose, pose );

    return pose;
}

inline
geometry_msgs::Point toMsg( const Eigen::Vector3d & point )
{
    geometry_msgs::Point geoPoint;
    Eigen::Vector3dMap( &geoPoint.x ) = point;

    return geoPoint;
}

inline
geometry_msgs::PointStamped toMsgStamped( const Eigen::Vector3d & point )
{
    geometry_msgs::PointStamped geoPoint;

    Eigen::Vector3dMap( &geoPoint.point.x ) = point;

    return geoPoint;
}


inline
Eigen::Affine3d
toEigen( const geometry_msgs::Pose& anyPose )
{
    Eigen::Affine3d eigPose;
    tf::poseMsgToEigen( anyPose, eigPose );

    return eigPose;
}

inline
Eigen::Affine3d
toEigen( const geometry_msgs::PoseStamped& anyPose )
{
    Eigen::Affine3d eigPose;
    tf::poseMsgToEigen( anyPose.pose, eigPose );

    return eigPose;
}

}

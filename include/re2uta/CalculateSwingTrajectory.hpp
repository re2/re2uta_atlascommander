/**
 *
 * CalculateCapturePoint.hpp: ...
 *
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created Apr 24, 2013
 * @modified Apr 24, 2013
 *
 */

#pragma once

#include <ros/ros.h>
#include <ros/subscribe_options.h>

#include <sensor_msgs/JointState.h>
#include <osrf_msgs/JointCommands.h>
#include <atlas_msgs/AtlasCommand.h>
#include <atlas_msgs/AtlasState.h>
#include <std_msgs/Int32.h>

#include <std_msgs/Empty.h>
#include <tf/transform_listener.h>

#include <re2uta/debug.hpp>

#include <re2uta/AtlasLookup.hpp>
#include <re2uta/calc_point_mass.hpp>
#include <re2uta/center_of_mass_jacobian.hpp>
#include <re2uta/capture_point.hpp>

#include <re2/eigen/eigen_util.h>
#include <re2/matrix_conversions.h>

#include <kdl_parser/kdl_parser.hpp>
#include <urdf_model/model.h>

#include <Eigen/Core>

#include <boost/foreach.hpp>

#include <iostream>
#include <fstream>

#include <re2uta/SupportPolygon.hpp>

// KDL stuff TODO maybe already included??
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>


// TODO maybe move these enums to a global place

enum PreemptFlag
{
    KeepPublishing    = 0, // 0 - keep publishing (default)
    StopPublishing    = 1, // 1 - stop trajectory pub
    RestartPublishing = 2, // 2 - start/restart trajectory pub
    ResetPublishing   = 3  // 3 - Reset trajectory
};

namespace re2uta
{

class AtlasCaptureNode
{
    private:

        ros::NodeHandle  m_node;
        KDL::Tree        m_tree;

        Eigen::VectorXd  m_treeJointPositions;
        Eigen::VectorXd  m_atlasJointPositions;

        Eigen::VectorXd  m_atlasR2LjointPos;
        Eigen::VectorXd  m_atlasL2RjointPos;

        Eigen::VectorXd  m_jointMins;
        Eigen::VectorXd  m_jointMaxs;

        Eigen::Vector3d  m_comPosition;
        Eigen::Vector3d  m_lastCOMPosition;
        Eigen::Vector3d  m_comVelocity;

        Eigen::Vector3d  m_instantaneousCapturePoint;
        Eigen::Vector3d  m_propagatedCapturePoint;

        AtlasLookup::Ptr m_atlasLookup;

        re2uta::AtlasSupportPolygon::Ptr m_supportPolygon;

        ros::Subscriber       m_atlasStateSubscriber;

        ros::Publisher        m_capturePointPublisher;
        ros::Publisher        m_stopTrajectoryPublisher;
        ros::Publisher        m_supportPolygonPublisher;

        tf::TransformListener m_tfListener;

        ros::Time             m_lastTime;;
        ros::Time             m_sampleTime;

        // Maintains the sate of Atlas support
        FootSupport           m_supportState;

        // Maintains base frame
        std::string           m_baseName;

        KDL::Frame            m_R2Ltransform;

        /*
         * This will perform the forward kinematics from root to tip of a chain using KDL solvers
         * TODO change this to Tree FK
         */
        KDL::Frame ChainFK( KDL::Tree & in_tree, std::string root, std::string tip, Eigen::VectorXd & atlasJointPos );

        /*
         * This function sets the internal class state that maintains the support state of Atlas
         * TODO add more sophisticated way to calculate this, maybe include torques etc
         */
        void setSupportState( double lForceZ, double rForceZ );

    public:

        typedef boost::shared_ptr<AtlasCaptureNode> Ptr;

        AtlasCaptureNode( const urdf::Model & urdfModel );

        Eigen::Vector3d calculateInstantaneousCapturePoint( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg );

        Eigen::Vector3d calculatePropagatedCapturePoint( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg, double delT );

        /*
         * This function will update the Instantaneous Capture Point member data
         */
        void updateInstantaneousCapturePoint( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg );

        /*
         * This function will update the propagated Capture Point (CP) member data
         */
        void updatePropagatedCapturePoint( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg, double delT );

        /*
         * Here we set the KDL tree joint states using the Atlas state message
         * This is where the main processing is done                 *
         */
        bool checkCaptureState( const atlas_msgs::AtlasStateConstPtr & atlasStateMsg, Eigen::Vector3d & capturePoint );

        Eigen::Vector3d getInstantaneousCapturePoint();

        /*
         * This will get the time propagated Capture Point (CP) from Instantaneous Capture Point (ICP)
         * This will also perform a calculation to get the Instantaneous Capture Point (ICP)
         */
        Eigen::Vector3d getPropagatedCapturePoint();

        visualization_msgs::Marker getInstantaneousCapturePointMarker();

        visualization_msgs::Marker getPropagatedCapturePointMarker();

        geometry_msgs::PolygonStamped getSupportPolygonMarker();

};

}

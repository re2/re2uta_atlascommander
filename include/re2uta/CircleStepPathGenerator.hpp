/*
 * CircleStepPathGenerator.hpp
 *
 *  Created on: May 31, 2013
 *      Author: andrew.somerville
 */


#pragma once

#include <re2/eigen/eigen_util.h>
#include <re2/eigen/eigen_util.h>
#include <re2/visutils/VisualDebugPublisher.h>
#include <Eigen/Geometry>
#include <boost/utility.hpp>
#include <vector>




//
//std::cout << "diff " << nextTangent - step.linear() * currentDirection << std::endl;
//
//Eigen::Vector4d blue(   0,0,1,1 );
//Eigen::Vector4d white(  1,0,0,1 );
//Eigen::Vector4d purple( 1,0,1,1 );
//Eigen::Vector4d yellow( 1,1,0,1 );
//m_visDebug.publishVectorFrom( circle.origin(),     currentRadiusVector, blue, 1 );
//m_visDebug.publishVectorFrom( circle.origin(),     nextRadiusVector,    white, 2 );
//m_visDebug.publishVectorFrom( circle.origin() + currentRadiusVector, currentTangent,  purple, 3 );
//m_visDebug.publishVectorFrom( circle.origin() + nextRadiusVector,    nextTangent,     yellow, 4 );
//
//std::cout << "currentRadiusVector:  " << currentRadiusVector.transpose()     << "\n" ;
//std::cout << "currentRadiusVector:  " << currentRadiusVector.norm()     << "\n" ;
//std::cout << "nextRadiusVector:     " << nextRadiusVector.transpose()        << "\n" ;
//std::cout << "nextRadiusVector:     " << nextRadiusVector.norm()        << "\n" ;
//std::cout << "radius diff:     " << nextRadiusVector - currentRadiusVector        << "\n" ;
//std::cout << "radius diff:     " << (nextRadiusVector - currentRadiusVector).norm()        << "\n" ;
//std::cout << "rotationVector:       " << rotationVector.transpose()          << "\n" ;
//std::cout << "rotationVector:       " << rotationVector.norm()          << "\n" ;
//std::cout << "currentDirection:     " << currentDirection.transpose()        << "\n" ;
//std::cout << "currentDirection:     " << currentDirection.norm()        << "\n" ;
//std::cout << "currentTangent:       " << currentTangent.transpose()        << "\n" ;
//std::cout << "nextTangent:          " << nextTangent.transpose()        << "\n" ;
//std::cout << "arc Angle:       " << arcAngle                        << "\n" ;
//std::cout << "deflection:      " << deflection                      << "\n" ;
//std::cout << "circle  origin:  " << circle.origin().transpose()     << "\n" ;
//std::cout << "circle  radius:  " << circle.radius()                 << "\n" ;
//std::cout << "effectiveRadius: " << effectiveRadius                 << "\n" ;
//std::cout << "desiredRadius:   " << desiredRadius                   << "\n" ;
//std::cout << "circle  radius:  " << circle.radius()                 << "\n" ;
//std::cout << "current radvec:  " << currentRadiusVector.transpose() << "\n" ;

/*
 * interactive_marker_util.hpp
 *
 *  Created on: Jun 11, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <visualization_msgs/InteractiveMarker.h>
#include <string>

namespace re2uta
{

visualization_msgs::InteractiveMarker::Ptr
create6dBoxControl( const std::string & frameId, const std::string & name, const std::string & description = std::string() );


}

/*
 * AtlasCommanderClient.hpp
 *
 *  Created on: Apr 22, 2013
 *      Author: andrew.somerville
 */

#ifndef ATLASCOMMANDERCLIENT_HPP_
#define ATLASCOMMANDERCLIENT_HPP_

#include <re2uta/AtlasCommander/types.hpp>
#include <re2/eigen/eigen_util.h>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <ros/ros.h>


namespace re2uta
{

class AtlasCommanderClient
{
    public:
        AtlasCommanderClient();

        void walkto( const Eigen::Vector3d & dest );
        void switchToController( atlascommander::ControllerType controllerType );

        void
        commandFramePose( const std::string &     planFrameName,
                          const std::string &     tipFrameName,
                          const Eigen::Affine3d & dest,
                          const Eigen::Vector6d & weights );

    private:
        ros::NodeHandle m_node;
        ros::Publisher m_walktoPub;
        ros::Publisher m_selectControllerPub;
        ros::Publisher m_commandFramePub;
};

}
#endif /* ATLASCOMMANDERCLIENT_HPP_ */

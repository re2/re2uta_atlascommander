/*
 * RosHalfstepTrajectoryGenerator.cpp
 *
 *  Created on: Jun 17, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2uta/walking/StepTrajectoryGenerator.hpp>
#include <re2uta/AtlasCommander/WalkCommanderInterface.hpp>

namespace re2uta
{

class RosHalfstepTrajectoryGenerator
{
    public:
        typedef std::vector<walk_msgs::Footprint2d> Footprint2dVector;
        typedef std::vector<halfsteps_pattern_generator::Footprint> FootprintVector;
        typedef boost::shared_ptr<FootprintVector>  FootprintVectorPtr;

    public:
        RosHalfstepTrajectoryGenerator( const StepPose::ConstPtr & stepPose,
                                        const Eigen::Affine3d & dest,
                                        double halfStepSize,
                                        double footSpreadOffset,
                                        ros::Duration halfStepDt,
                                        atlascommander::WalkCommanderInterface::StepPlannerType plannerType,
                                        const AtlasLookup::Ptr & atlasLookup
                                        );

        StepPlan::Ptr generateNextStep( const StepPose::ConstPtr & stepPose );


    protected:
        FootprintVectorPtr
        generateAllStepsFootprints();



    protected:
        ros::ServiceClient m_halfStepClient;
        ros::NodeHandle    m_node;

        halfsteps_pattern_generator::
        GetPath           m_pathTransaction;
        StepGeneratorPtr  m_stepGenerator;
        size_t            m_stepIndex;
        size_t            m_pointIndex;
        Foot              m_swingFoot;
        ros::Duration     m_dt;
        double            m_footSpreadOffset;
        AtlasLookup::Ptr  m_atlasLookup;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};


}

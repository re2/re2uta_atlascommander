/*
 * ManualStepGenerator.hpp
 *
 *  Created on: Jun 19, 2013
 *      Author: andrew.somerville
 */

#pragma once


#include <re2uta/walking/StepGenerator.hpp>


namespace re2uta
{


class ManualStepGenerator : public StepGenerator
{
    public:
        ManualStepGenerator( const StepPose::Ptr & currentStepPose, const StepPosePtrVectorPtr & stepPoses  );
        ManualStepGenerator( const StepPose::Ptr & currentStepPose, const Eigen::Affine3d & destPose );

        virtual ~ManualStepGenerator();


        virtual bool            hasArrived(   const Eigen::Affine3d & current ) const;
        virtual Eigen::Affine3d generateNextStep( const Eigen::Affine3d & last,
                                                  const Eigen::Affine3d & current,
                                                  int  stepIndex );


    protected:
        StepPose::Ptr getBeginStepPose();

        void
        init( const StepPose::ConstPtr srcStepPose,  const  Eigen::Affine3d & srcPose, const  Eigen::Affine3d & destPose );

    protected:
        ros::NodeHandle              m_node;

        StepPose::Ptr                m_srcStepPose;

        StepPosePtrVectorPtr         m_stepPoses;
        StepPosePtrVector::iterator  m_stepPoseItor;


    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}

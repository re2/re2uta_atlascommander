/*
 * NaoStepGenerator.hpp
 *
 *  Created on: Jun 19, 2013
 *      Author: andrew.somerville
 */


#pragma once

#include <re2uta/walking/StepGenerator.hpp>


namespace re2uta
{


class NaoPathStepGenerator : public StepGenerator
{
    public:
        NaoPathStepGenerator( const StepPose::ConstPtr srcPose,
                              const Eigen::Affine3d &  dest   );

        NaoPathStepGenerator( const  Eigen::Affine3d & srcPose,
                              const  Eigen::Affine3d & destPose,
                              double footSpreadOffset );

        virtual ~NaoPathStepGenerator() {}


        virtual bool            hasArrived(   const Eigen::Affine3d & current ) const;
        virtual Eigen::Affine3d generateNextStep( const Eigen::Affine3d & last,
                                                  const Eigen::Affine3d & current,
                                                  int  stepIndex );


    protected:
        StepPose::Ptr getBeginStepPose();

        void
        init( const StepPose::ConstPtr srcStepPose,  const  Eigen::Affine3d & srcPose, const  Eigen::Affine3d & destPose );

    protected:
        ros::NodeHandle              m_node;
        ros::ServiceClient           m_footStepPlannerClient;

        double                       m_halfStepLength;
        double                       m_footSpreadOffset;
        Foot                         m_swingFoot;
        StepPose::Ptr                m_srcStepPose;

        StepPosePtrVectorPtr         m_stepPoses;
        StepPosePtrVector::iterator  m_stepPoseItor;


    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}

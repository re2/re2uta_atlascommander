/*
 * WalkPatternGenerator.hpp
 *
 *  Created on: May 29, 2013
 *      Author: Ghassan Atmeh, andrew.somerville
 */

#pragma once

#include <re2uta/walking/StepGenerator.hpp>
#include <re2uta/splines/Spline.hpp>
#include <re2/eigen/eigen_util.h>
#include <Eigen/Core>



namespace re2uta
{


class StepGenerator;
typedef boost::shared_ptr<StepGenerator> StepGeneratorPtr;

class WalkPatternGenerator
{
    public:
        WalkPatternGenerator();

//        WalkPlan
//        buildStraightLineWalkPlan( const Eigen::Affine3d & source, const Eigen::Affine3d & dest, double stepLength );

        WalkPlan
        buildCircleBasedWalkPlan( const Eigen::Affine3d & source, const Eigen::Affine3d & dest, double stepLength );

        WalkPlan
        buildTrajectories( StepGenerator & stepGenerator );

        WalkPlan::Ptr
        buildSingleStepTrajectory( StepGenerator &         stepGenerator,
                                   Foot                    baseFoot,
                                   const Eigen::Affine3d & baseFootPose,
                                   const Eigen::Affine3d & swingFootPose );

    public:
        Eigen::Vector3dListPtr m_zmpRefPlan;


    protected:

        void
        planSingleStep( StepGenerator &          stepGenerator,
                        Eigen::Affine3dListPtr & swingFootPoses,
                        Eigen::Affine3dListPtr & baseFootPoses,
                        Eigen::Vector3dVector &  comRef,
                        int                      stepIndex,
                        double                   defaultStepLength,
                        double                   defaultStepPeriod,
                        double                   dt,
                        double                   time );

        Spline createTranslationSpline( double stepLength, double stepPeriod );
//        Spline createYSpline();
        Spline createZSpline( double stepDist, double stepPeriod );
        Spline createAnkleRotSpline( double stepPeriod );
        Spline createGroundContactSpline( double stepPeriod );


        Eigen::VectorXd calcTranslationTimeKeypoints( double stepPeriod );
        Eigen::VectorXd calcRotationTimeKeypoints( double stepPeriod );

    protected:
        double m_defaultStepLength;
        double m_ankleHeight;
        double m_foreFootLen;
        double m_aftFootLen;
        double m_apexX;
        double m_liftoffAnklePitch;
        double m_touchdownAnklePitch;
        double m_startAnklePitch;
        double m_endAnklePitch;
        double m_maxZ;
        double m_stepPeriod;
        double m_dt;
        bool   m_leftFirst;
};



}

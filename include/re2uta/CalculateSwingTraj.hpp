/**
 *
 * CalculateSwingTrajectory.hpp: ...
 *
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created Jun 2, 2013
 * @modified Jun 3, 2013
 *
 */

#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <re2uta/splines/Spline.hpp>

namespace re2uta
{

class AtlasSwingNode
{

    private:

        double m_defaultStepLength;
        double m_ankleHeight;
        double m_foreFootLen;
        double m_aftFootLen;
        double m_apexX;
        double m_liftoffAnklePitch;
        double m_touchdownAnklePitch;
        double m_startAnklePitch;
        double m_endAnklePitch;
        double m_maxZ;
        double m_stepPeriod;
        double m_dt;
        bool   m_leftFirst;

        // TODO check if this is the way to go
        // Maybe use a smart pointer
        Spline::Ptr m_xSpline;
        Spline::Ptr m_ySpline;
        Spline::Ptr m_zSpline;


        Spline createXSpline( double stepStart, double stepEnd, double stepPeriod );
        Spline createYSpline( double stepStart, double stepEnd, double stepPeriod );
        Spline createZSpline( double stepStart, double stepEnd, double stepPeriod );
        Spline createAnkleRotSpline( double stepPeriod );
        Spline createGroundContactSpline( double stepPeriod );

        Eigen::VectorXd calcTranslationTimeKeypoints( double stepPeriod );
        Eigen::VectorXd calcRotationTimeKeypoints( double stepPeriod );

    public:

        typedef boost::shared_ptr<AtlasSwingNode> Ptr;

        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        AtlasSwingNode();

        void calcSwingTrajectory( Eigen::Vector3d & stepStart, Eigen::Vector3d & stepEnd, double stepPeriod );

        Eigen::Vector3d getSwingPosPoint( double timeVal );
        Eigen::Vector3d getSwingVelPoint( double timeVal );
        Eigen::Vector3d getSwingAccPoint( double timeVal );

        nav_msgs::Path getSwingPosTrajMsg( double stepPeriod, std::string supportName = "r_foot" );

};

}

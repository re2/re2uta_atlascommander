/**
 *
 * SupportPolygon.hpp: this class was heavily inspired by hrl_kinematics
 *
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created May 28, 2013
 * @modified May 28, 2013
 *
 */

#include <pcl/surface/convex_hull.h>
#include <pcl/point_types.h>

#include <geometry_msgs/PolygonStamped.h>
#include <visualization_msgs/Marker.h>

#include <tf/transform_listener.h>

#include <tf_conversions/tf_eigen.h>
#include <tf_conversions/tf_kdl.h>

#include <Eigen/Core>

enum FootSupport {SUPPORT_DOUBLE, SUPPORT_SINGLE_RIGHT, SUPPORT_SINGLE_LEFT};

namespace re2uta
{

class AtlasSupportPolygon
{

    private:

        /// Support polygon (x,y) for the right foot
        std::vector<tf::Point> m_rFootSupPoly_;
        /// Support polygon (x,y) for the left foot (mirrored from right)
        std::vector<tf::Point> m_lFootSupPoly_;
        std::vector<tf::Point> m_bothSupPoly_;

        std::vector<tf::Point> m_support_polygon_;

        tf::Transform tf_to_support_;
        tf::Transform tf_right_to_left;

        // Maintains the state of Atlas support
        FootSupport     m_support_mode;

        // Maintains the base link name
        std::string     m_baseLinkName;
        double          m_ankleOffset;
        Eigen::Vector4f m_rCentroid;
        Eigen::Vector4f m_lCentroid;
        Eigen::Vector4f m_bothCentroid;
        Eigen::Vector4f m_supportCentroid;

    private:

        /*
         * This creates a convex hull from a set of points
         * from hrl_kinematics/TestStability.cpp
         */
        std::vector<tf::Point> convexHull( const std::vector<tf::Point>& points ) const;

        /*
         * This initializes the foot polygon
         * from hrl_kinematics/TestStability.cpp
         */
        void initFootPolygon();

        /*
         * This will check if the given point inside the given polygon
         * This will return a bool true or false
         * from hrl_kinematics/TestStability.cpp
         */
        bool pointInConvexHull(const tf::Point& point, const std::vector<tf::Point>& polygon) const;

    public:

        typedef boost::shared_ptr<AtlasSupportPolygon> Ptr;

        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        AtlasSupportPolygon()
        {

            // TODO maybe move this to initFootPolygon to remove weird errors by not initializing it
            m_ankleOffset  = -0.079342f;

            m_support_mode = SUPPORT_DOUBLE;

            // This is set to identity since the ankle is the base frame
            tf_to_support_.setIdentity();

            // This initializes the left and right support polygons
            initFootPolygon();

        }

        ~AtlasSupportPolygon()
        {

        }

        void setSupportPolygon();

        /*
         * This allows to set the support mode externally
         */
        void setSupportMode( FootSupport & supMode );

        /*
         * This allows to set the support mode externally
         */
        void setBaseLink( std::string & name );

        /*
         * This allows to set the transform from right to left leg
         */
        void setR2Ltransform( Eigen::Affine3d & r2lTrans );

        /*
         * This will return a geometry message Polygon which will contain the support polygon
         */
        geometry_msgs::PolygonStamped getSupportPolygon() const;

        /*
         * This function wraps the pointInConvexHull function and
         * will return false if the point is not inside the support polygon
         */
        bool isPoseStable( const Eigen::Vector3d & vector3 ) const;

        /*
         * This will output the center of the support polygon
         */
        Eigen::Vector3d getSupportPolygonCenter() const;

        /*
         * The following publishes a visualization marker for the centroid of the support polygon
         */
        visualization_msgs::Marker getSupportPolygonCenterMarker() const;

};

}


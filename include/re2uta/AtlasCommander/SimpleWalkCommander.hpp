/*
 * SimpleWalkCommander.hpp
 *
 *  Created on: Jun 5, 2013
 *      Author: andrew.somerville
 */

#pragma once


#include <re2uta/walking/WalkPatternGenerator.hpp>
#include <re2uta/AtlasCommander/WalkCommanderInterface.hpp>
#include <re2uta/AtlasCommander/FullBodyControllerInterface.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <atlas_msgs/AtlasState.h>
#include <tf/transform_broadcaster.h>
#include <urdf/model.h>
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <string>


namespace Eigen
{
    typedef boost::shared_ptr<Affine3d> Affine3dPtr;
}

namespace re2uta
{
namespace atlascommander
{



class SimpleWalkCommander : public WalkCommanderInterface
{
    public:
        typedef boost::shared_ptr<SimpleWalkCommander> Ptr;


    public:
        SimpleWalkCommander( const FullBodyControllerInterface::Ptr & fullBodyController,
                              const urdf::Model & model,
                              const ros::Duration & dt  );
        virtual ~SimpleWalkCommander();


        //WalkCommanderInterface requirements
        virtual void walkTo( const WalkGoal::Ptr & walkGoal );

        virtual void setStepPlannerType( StepPlannerType stepPlannerType );

        virtual void shutdown();

        virtual void setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg );

        void                setDest( const Eigen::Affine3d & dest );


    protected:
        bool                highFootForce( const atlas_msgs::AtlasState::ConstPtr & atlasStateMsg, Foot foot, ros::Duration dt );
        void                commandLoop();

        StepPose::Ptr
        getCurrentStepPoseInBaseFoot( Foot baseFoot );

        void
        publishOdomToRoot( const Eigen::Affine3d & baseFootInOdom,
                           const std::string &      baseFootFrameId,
                           const Eigen::VectorXd &  jointPositions );

        StepPose::Ptr
        moveStepPoseToNewOdom( const StepPose::Ptr & currentStepPose );

        Eigen::Affine3dPtr               m_walkDestCmd;
        FullBodyControllerInterface::Ptr m_fullBodyController;
        FullBodyPoseSolver::Ptr          m_solver;
        AtlasLookup::Ptr                 m_atlasLookup;
        atlas_msgs::AtlasState::ConstPtr m_lastStateMsg;
        ros::Duration                    m_dt;
        double                           m_halfStepSize;
        Eigen::Vector3d                  m_filteredForce;
        double                           m_forceThresh;
        boost::thread                    m_controlLoopThread;
        tf::TransformBroadcaster         m_tfBroadcaster;
        bool                             m_shutdown;

        StepPlannerType                  m_stepPlannerType;


    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


}
}

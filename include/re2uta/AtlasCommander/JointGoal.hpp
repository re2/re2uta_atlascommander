/*
 * JointGoal.hpp
 *
 *  Created on: Jun 5, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2/eigen/eigen_util.h>
#include <boost/shared_ptr.hpp>
#include <string>
#include <list>

namespace re2uta
{
namespace atlascommander
{

class JointGoalGains
{
    public:
        typedef boost::shared_ptr<FrameGoalGains> Ptr;

    public:
        Eigen::VectorXd & posKp()      { return m_posKp;      }
        Eigen::VectorXd & posKd()      { return m_posKd;      }
        Eigen::VectorXd & velKp()      { return m_velKp;      }
        Eigen::VectorXd & velMax()     { return m_velMax;     }

    protected:
        Eigen::VectorXd m_posKp;
        Eigen::VectorXd m_posKd;
        Eigen::VectorXd m_velKp;
        Eigen::VectorXd m_velMax;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


typedef std::list<JointGoalGains::Ptr>                     JointGoalGainsList;
typedef boost::shared_ptr<std::list<JointGoalGains::Ptr> > JointGoalGainsListPtr;

class JointGoal
{
    public:
        typedef boost::shared_ptr<JointGoal> Ptr;

    public:
        JointGoal() {}
        Eigen::VectorXd jointGoal;
        Eigen::MatrixXd weightMatrix;

        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


typedef std::list<JointGoal::Ptr>                     JointGoalList;
typedef boost::shared_ptr<std::list<JointGoal::Ptr> > JointGoalListPtr;


}
}

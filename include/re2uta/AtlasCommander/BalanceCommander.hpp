/*
 * BalanceCommander.hpp
 *
 *  Created on: Jun 14, 2013
 *      Author: andrew.somerville
 *              Isura Ranatunga
 */

#pragma once

#include <re2uta/walking/WalkPatternGenerator.hpp>
#include <re2uta/walking/StepGenerator.hpp>
#include <re2uta/AtlasCommander/WalkCommanderInterface.hpp>
#include <re2uta/AtlasCommander/FullBodyControllerInterface.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <re2uta/CalculateCapturePoint.hpp>

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/PolygonStamped.h>
#include <atlas_msgs/AtlasState.h>
#include <tf/transform_broadcaster.h>
#include <urdf/model.h>
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <string>


namespace Eigen
{
    typedef boost::shared_ptr<Affine3d> Affine3dPtr;
}

namespace re2uta
{
namespace atlascommander
{



class BalanceCommander : public WalkCommanderInterface
{
    public:
        typedef boost::shared_ptr<BalanceCommander> Ptr;


    public:
        BalanceCommander( const FullBodyControllerInterface::Ptr & fullBodyController, const urdf::Model & model, AtlasLookup::Ptr const & atlasLookup, const ros::Duration & dt );
        virtual ~BalanceCommander();

        //WalkCommanderInterface requirements
        virtual void walkTo( const WalkGoal::Ptr & walkGoal );


        void setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg );

        FrameGoalGains::Ptr buildActuationGoal( Foot foot, double factor );
        void                setDest( const Eigen::Affine3d & dest );
        bool                highFootForce( const atlas_msgs::AtlasState::ConstPtr & atlasStateMsg, Foot foot, ros::Duration dt );
        void                shutdown();

        void                 setStepPlannerType( StepPlannerType stepPlanner );

    protected:
        void                commandLoop();

        void
        publishOdomToRoot( const Eigen::Affine3d & baseFootInOdom,
                           const std::string &      baseFootFrameId,
                           const Eigen::VectorXd &  jointPositions );

        StepPose::Ptr
        moveStepPoseToNewOdom( const StepPose::Ptr & currentStepPose );

        ros::NodeHandle                     m_node;

        Eigen::Affine3dPtr               m_walkDestCmd;
        FullBodyControllerInterface::Ptr m_fullBodyController;
        FullBodyPoseSolver::Ptr          m_solver;
        AtlasLookup::Ptr                 m_atlasLookup;
        AtlasCaptureNode::Ptr            m_atlasCaptureNode;

        Eigen::Vector3d                  m_instantaneousCapturePoint;
        Eigen::Vector3d                  m_centerOfSupport;
        Eigen::Vector3d                  m_COM;
        Eigen::Vector3d                  m_CoP;


        // Need to use FK depending on supportFoot to get this
        Eigen::Affine3d                  m_l_footPos;
        Eigen::Affine3d                  m_r_footPos;

        ros::Publisher                   m_instantaneousCapturePointPublisher;
        ros::Publisher                   m_propagatedCapturePointPublisher   ;
        ros::Publisher                   m_supportPolygonPublisher              ;
        ros::Publisher                   m_supportPolygonCenterPublisher     ;
        ros::Publisher                   m_CoPPublisher                         ;
        ros::Publisher                   m_CoMPublisher                         ;

        atlas_msgs::AtlasState::ConstPtr m_lastStateMsg;
        ros::Duration                    m_dt;
        double                           m_halfStepSize;
        Eigen::Vector3d                  m_filteredForce;
        double                           m_forceThresh;
        boost::thread                    m_controlLoopThread;
        tf::TransformBroadcaster         m_tfBroadcaster;
        bool                             m_shutdown;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


}
}

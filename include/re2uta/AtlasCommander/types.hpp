/*
 * types.hpp
 *
 *  Created on: Apr 22, 2013
 *      Author: andrew.somerville
 */

#ifndef TYPES_HPP_
#define TYPES_HPP_

namespace re2uta
{
namespace atlascommander
{

enum Result
{
    Failure_Result = 0,
    Success_Result,
};

}
}


#endif /* TYPES_HPP_ */

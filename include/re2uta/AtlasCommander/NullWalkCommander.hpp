/*
 * NullWalkCommander.hpp
 *
 *  Created on: Jun 14, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2uta/AtlasCommander/WalkCommanderInterface.hpp>
#include <re2uta/walking/StepGenerator.hpp>
#include <atlas_msgs/AtlasState.h>
#include <Eigen/Geometry>
#include <boost/shared_ptr.hpp>


namespace re2uta
{
namespace atlascommander
{


class NullWalkCommander : public WalkCommanderInterface
{
    public:
        typedef boost::shared_ptr<WalkCommanderInterface> Ptr;

    public:
        NullWalkCommander() {}

        virtual void walkTo( const WalkGoal::Ptr & walkGoal ){}
        virtual void setStepPlannerType( StepPlannerType stepPlanner ){}
        virtual void setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg ){}
        virtual void shutdown(){}

//        virtual void setDest( const Eigen::Affine3d & dest ) = 0;
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


}
}

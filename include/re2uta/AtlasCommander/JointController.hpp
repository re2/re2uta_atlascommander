/*
 * JointController.hpp
 *
 *  Created on: May 13, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <atlas_msgs/AtlasCommand.h>
#include <atlas_msgs/AtlasState.h>
#include <boost/shared_ptr.hpp>
#include <boost/thread/condition_variable.hpp>

namespace re2uta
{
namespace atlascommander
{


class JointController
{
    public:
        typedef boost::shared_ptr<JointController>       Ptr;
        typedef boost::shared_ptr<JointController const> ConstPtr;

    public:
        JointController( boost::condition_variable & syncVar );
        virtual ~JointController();
        virtual void setLatestStateMsg( const atlas_msgs::AtlasStateConstPtr & msg );
        virtual void setJointCommand( const atlas_msgs::AtlasCommandConstPtr & msg, ros::Time commandTime ) = 0;
        virtual void setKEfforts( const std::vector<uint8_t> & kefforts ) = 0;

    protected:
        atlas_msgs::AtlasState::ConstPtr m_latestStateMsg;
        boost::condition_variable &      m_syncVar;
};


}
}


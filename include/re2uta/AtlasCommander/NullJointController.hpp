/*
 * NullJointController.hpp
 *
 *  Created on: Jun 10, 2013
 *      Author: andrew.somerville
 */


#pragma once

#include <re2uta/AtlasCommander/JointController.hpp>
#include <boost/thread/condition_variable.hpp>
#include <atlas_msgs/AtlasState.h>
#include <atlas_msgs/AtlasCommand.h>
#include <boost/thread.hpp>
#include <ros/ros.h>

namespace re2uta
{
namespace atlascommander
{

class NullJointController : public JointController
{
    public:
        NullJointController( boost::condition_variable & syncVar );
        virtual ~NullJointController();
        virtual void setJointCommand( const atlas_msgs::AtlasCommandConstPtr & msg, ros::Time commandTime );
        virtual void setKEfforts( const std::vector<uint8_t> & kefforts );


};

}
}


/*
 * BdiWalkController.hpp
 *
 *  Created on: May 20, 2013
 *      Author: andrew.somerville
 */

#pragma once


#include <re2uta/walking/StepGenerator.hpp>
//#include <atlas_msgs/AtlasControlTypes.h>
#include <re2uta/AtlasCommander/types.hpp>
//#include <re2uta/AtlasCommander/JointController.hpp>
#include <re2uta/AtlasCommander/FullBodyControllerInterface.hpp>
#include <re2uta/AtlasCommander/WalkCommanderInterface.hpp>

#include <humanoid_nav_msgs/PlanFootsteps.h>
#include <atlas_msgs/AtlasSimInterfaceCommand.h>
#include <atlas_msgs/AtlasSimInterfaceState.h>

#include <urdf/model.h>
#include <re2uta/AtlasLookup.hpp>


#include <eigen_conversions/eigen_msg.h>
#include <re2/eigen/eigen_util.h>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <eigen_conversions/eigen_kdl.h>
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <boost/foreach.hpp>
#include <string>

namespace re2uta
{

namespace atlascommander
{

class BdiStepCommander : public WalkCommanderInterface
{
    public:
        typedef boost::shared_ptr<BdiStepCommander> Ptr;

    public:
        BdiStepCommander( const FullBodyControllerInterface::Ptr & fullBodyController,
//                          const JointController::Ptr & jointController,
                          const urdf::Model & model,
                          AtlasLookup::Ptr const & atlasLookup,
                          StepPlannerType stepPlannerType = NAO_PLANNER );
//        BdiStepCommander( const urdf::Model & model, StepPlannerType stepPlannerType );


        virtual ~BdiStepCommander();

//        atlascommander::Result

        virtual
        void
        walkTo( const WalkGoal::Ptr & walkGoal );

        virtual
        void
        walkTo( const atlascommander::WalkGoalVectorPtr& walkGoals ); //FIXME violating concept of interface


        virtual
        void
        setStepPlannerType( StepPlannerType stepPlannerType );

        virtual
        void
        setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg );

        void
        shutdown();


//        static Ptr make( const urdf::Model & model,
////                          const JointController::Ptr & jointController,
//                          const FullBodyControllerInterface::Ptr & fullBodyController,
//                          StepPlannerType stepPlannerType );




    protected:

        void
        handleBdiStateMessage( const atlas_msgs::AtlasSimInterfaceStateConstPtr & msg );

//        void
//        publishStep( const atlas_msgs::AtlasSimInterfaceStateConstPtr & msg );

        void
        publishStepCluster( const atlas_msgs::AtlasSimInterfaceStateConstPtr & msg );

        atlas_msgs::AtlasSimInterfaceCommandPtr
        buildDefaultWalkCommand();

        atlas_msgs::AtlasSimInterfaceCommandPtr
        buildDefaultStepCommand();

        atlas_msgs::AtlasBehaviorStepDataPtr
        buildDefaultStepData();

//        atlascommander::Result
//        stepTo( const Eigen::Affine3d & destPose )

        StepPosePtrVectorPtr
        planStepsTo( const WalkGoal::Ptr & walkGoal );

        StepPosePtrVectorPtr
        circlePlanStepsTo( const Eigen::Affine3d & destPose );

        StepPosePtrVectorPtr
        naoPlanStepsTo( const Eigen::Affine3d & destPose );

        StepPosePtrVectorPtr
        manualPlanStepsTo( const WalkGoal::Ptr & walkGoal );

        Eigen::Affine3d
        lookupTransform( const std::string & fromFrame, const std::string & toFrame );

        void stateUpdateCallbackLoop();


    protected:
        ros::NodeHandle         m_node;
        urdf::Model             m_model;
        ros::Subscriber         m_bdiAtlasStateSub;
        ros::Publisher          m_bdiAtlasCommandPub;
        ros::Publisher          m_footstepMarkerPub;
        ros::ServiceClient      m_footStepPlannerClient;
        std::string             m_odomFrameId;
//        humanoid_nav_msgs::PlanFootstepsResponsePtr m_footStepResponse;
        StepPosePtrVectorPtr    m_plannedSteps;
        atlas_msgs::AtlasSimInterfaceStateConstPtr  m_lastBdiStateMsg;
        int                     m_nextStepIndex;
        int                     m_lastStepIndexNeeded;
        int                     m_nextFootIndex;
        int                     m_planStepIndex;
        bool                    m_isSwaying;

        Eigen::Affine3d         m_leftFootPose;
        Eigen::Affine3d         m_rightFootPose;

        Eigen::Affine3d         m_aggPose;
        Eigen::Affine3d         m_aggPoseAtCommandTime;

        tf::TransformBroadcaster m_tfBroadcaster;
        tf::TransformListener    m_tfListener;
        int                      m_bdiStateIndex;

        re2uta::AtlasLookup::Ptr m_atlasLookup;
//        JointController::Ptr     m_jointController;
        FullBodyControllerInterface::Ptr m_fullBodyController;
        std::vector<uint8_t>     m_kEfforts;

        boost::thread            m_bdiUpdateThread;
        ros::CallbackQueue       m_stateUpdateThreadCallbackQueue;

        WalkGoal::Ptr            m_latestWalkGoal;
        WalkGoalVectorPtr        m_latestWalkGoalVector;

//        PathPlanner::Ptr         m_plannerType;

        StepPlannerType          m_stepPlannerType;
        bool                     m_shutdown;

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};

}
}




/*
 * CrawlCommander.hpp
 *
 *  Created on: Jun 17, 2013
 *      Author: Ghassan Atmeh & Adrian Rodriguez
 */

#pragma once


#include <re2uta/walking/WalkPatternGenerator.hpp>
#include <re2uta/AtlasCommander/WalkCommanderInterface.hpp>
#include <re2uta/AtlasCommander/FullBodyControllerInterface.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <re2uta/AtlasCommander/JointController.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <atlas_msgs/AtlasState.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <urdf/model.h>
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <string>


namespace Eigen
{
    typedef boost::shared_ptr<Affine3d> Affine3dPtr;
}

namespace re2uta
{
namespace atlascommander
{



class CrawlCommander : public WalkCommanderInterface
{
    public:
        typedef boost::shared_ptr<CrawlCommander> Ptr;


    public:
        CrawlCommander( const FullBodyControllerInterface::Ptr & fullBodyController,
                        const urdf::Model & model,
                        const AtlasLookup::Ptr & atlasLookup,
                        const ros::Duration & dt,
                        StepPlannerType       stepPlannerType );
        virtual ~CrawlCommander();

        //CrawlCommander requirements
        virtual void walkTo( const WalkGoal::Ptr & walkGoal );
        void         setDest( const Eigen::Affine3d & dest );
        virtual void setStepPlannerType( StepPlannerType stepPlannerType );
        virtual void shutdown();
        virtual void setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg );

    protected:
        // Primitives
        void handX(               const std::string & baseFrameId, const Eigen::VectorXd & treeJointPositions );
        void handDrop(            const std::string & baseFrameId, const Eigen::VectorXd & treeJointPositions );
        void limbsDownFromPelvis( const std::string & baseFrameId, const Eigen::VectorXd & treeJointPositions );
        void dropPelvis(          const std::string & baseFrameId, const Eigen::VectorXd & treeJointPositions );
        void unconstrainPosture(  const std::string & baseFrameId, const Eigen::VectorXd & treeJointPositions );
        void getToGround();
        void changeComInSquat( const std::string & baseFrameId );

        Eigen::Affine3d           getFrameToInertial( const std::string & frameId );
        Eigen::Affine3d           getImuToInertial();

        void orientOnGround();
        void sitUp();
        void spreadArmsOnGround();
        void extendLegs();

        bool isForwardBackward(    const Eigen::Affine3dPtr & walkDest );
        bool isToSide(             const Eigen::Affine3dPtr & walkDest );
        void turn(                 const Eigen::Affine3dPtr & walkDest );
        void scootForwardBackward( const Eigen::Affine3dPtr & walkDest );


        FrameGoalGains::Ptr buildActuationGoal( Limb limb, double factor );
        bool                highLimbForce( const atlas_msgs::AtlasState::ConstPtr & atlasStateMsg, Limb limb, ros::Duration dt );
        void                commandLoop();


        CrawlPose::Ptr
        getCurrentPosesInBase( Limb baseLimb, const atlas_msgs::AtlasStateConstPtr & stateMsg );

        void
        publishInertialFrameToOdom( const std::string & frameId,  const Eigen::Affine3d & frameToOdom );

        void
        sleepWithTfPublish( const std::string & baseFrameId, const Eigen::Affine3d & baseFootInOdom, double seconds );


        CrawlPose::Ptr
        moveCrawlPoseToNewOdom( const CrawlPose::Ptr & currentCrawlPose );


    protected:
        ros::NodeHandle                  m_node;
        ros::ServiceClient               m_leftGripperSrv;
        ros::ServiceClient               m_rightGripperSrv;

        Eigen::Affine3dPtr               m_walkDestCmd;
        FullBodyControllerInterface::Ptr m_fullBodyController;
        FullBodyPoseSolver::Ptr          m_solver;
        AtlasLookup::Ptr                 m_atlasLookup;
        atlas_msgs::AtlasState::ConstPtr m_lastStateMsg;
        ros::Duration                    m_dt;
        double                           m_halfStepSize;
        Eigen::Vector3d                  m_filteredForce;
        boost::thread                    m_controlLoopThread;
        tf::TransformBroadcaster         m_tfBroadcaster;
        bool                             m_shutdown;
        tf::TransformListener            m_tfListener;
        double                           m_forceThresh;
        StepPlannerType                  m_stepPlannerType;
        ros::Time                        m_nextCommandTime;
        JointController::Ptr             m_jointController;
        Eigen::Vector3d                  m_swingLimbStartPos;
        Eigen::Vector3d                     m_swingPosition;


    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


}
}

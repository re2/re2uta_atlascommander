/*
 * NeuralNetworkJointController.hpp
 *
 *  Created on: Jun 4, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2uta/AtlasCommander/JointController.hpp>
#include <re2uta/MultiNetworkController.hpp>
#include <atlas_msgs/SetJointDamping.h>
#include <boost/thread.hpp>
#include <vector>

namespace re2uta
{
namespace atlascommander
{


class NeuralNetworkJointController : public JointController
{
    public:
        NeuralNetworkJointController( const ros::Duration & commandLoopPeriod, boost::condition_variable & syncVar, AtlasLookup::Ptr const & atlasLookup );
        virtual ~NeuralNetworkJointController();
        virtual void setJointCommand( const atlas_msgs::AtlasCommandConstPtr & msg, ros::Time commandTime );
        virtual void setKEfforts( const std::vector<uint8_t> & kefforts );



    protected:
        void commandLoop();

    protected:
        ros::NodeHandle                    m_node;
        ros::Duration                      m_commandLoopPeriod;
        atlas_msgs::AtlasCommand::ConstPtr m_currentCommand;
        long unsigned int                  m_iteration;
        ros::Time                          m_nextCommandTime;
        bool                               m_shutdown;
        boost::thread                      m_commandLoopThread;
        ros::Publisher                     m_jointCommandPub;
        MultiNetworkController::Ptr        m_nnController;
        re2uta::AtlasLookup::Ptr           m_atlasLookup;
        ros::ServiceClient                 m_setDampingSrv;
//        boost::array<double,28>            m_defaultDampingTerms;
        std::vector<double>                m_defaultDampingTerms;
        double                             m_networkDamping;

        std::vector<uint8_t>               m_kEfforts;

};



}
}

/*
 * PreviewWalkCommander.hpp
 *
 *  Created on: Jun 5, 2013
 *      Author: andrew.somerville
 */

#pragma once


#include <re2uta/walking/WalkPatternGenerator.hpp>
#include <re2uta/AtlasCommander/WalkCommanderInterface.hpp>
#include <re2uta/AtlasCommander/FullBodyControllerInterface.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <atlas_msgs/AtlasState.h>
#include <tf/transform_broadcaster.h>
#include <urdf/model.h>
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <string>


namespace Eigen
{
    typedef boost::shared_ptr<Affine3d> Affine3dPtr;
}

namespace re2uta
{
namespace atlascommander
{



class PreviewWalkCommander : public WalkCommanderInterface
{
    public:
        typedef boost::shared_ptr<PreviewWalkCommander> Ptr;


    public:
        PreviewWalkCommander( const FullBodyControllerInterface::Ptr & fullBodyController,
                              const urdf::Model & model,
                              const AtlasLookup::Ptr & atlasLookup,
                              const ros::Duration & dt,
                              StepPlannerType       stepPlannerType );
        virtual ~PreviewWalkCommander();


        //WalkCommanderInterface requirements
        virtual void walkTo( const WalkGoal::Ptr & walkGoal );

        virtual void setStepPlannerType( StepPlannerType stepPlannerType );

        virtual void shutdown();

        virtual void setLatestStateMsg( const atlas_msgs::AtlasState::ConstPtr & latestStateMsg );

        void                setDest( const Eigen::Affine3d & dest );


    protected:
        FrameGoalGains::Ptr buildActuationGoal( Foot foot, double factor );
        bool                highFootForce( const atlas_msgs::AtlasState::ConstPtr & atlasStateMsg, Foot foot, ros::Duration dt );
        void                commandLoop();

        void
        publishBaseFootToOdom( Foot baseFoot,
                               const atlas_msgs::AtlasState::ConstPtr & lastStateMsg,
                               const Eigen::Affine3d & baseFootInOdom );

        StepPose::Ptr
        getCurrentStepPoseInBase( Foot baseFoot, const atlas_msgs::AtlasStateConstPtr & stateMsg );

        StepPose::Ptr
        odomToBase( StepPose::Ptr poseInOdom );

//        Eigen::Affine3d
//        getImuToInertial();
//
//        Eigen::Affine3d
//        getFrameToInertial( const std::string & frameId );

        StepPose::Ptr
        moveStepPoseToNewOdom( const StepPose::Ptr & currentStepPose );

    protected:
        ros::NodeHandle                  m_node;
        Eigen::Affine3dPtr               m_walkDestCmd;
        FullBodyControllerInterface::Ptr m_fullBodyController;
        FullBodyPoseSolver::Ptr          m_solver;
        AtlasLookup::Ptr                 m_atlasLookup;
        atlas_msgs::AtlasState::ConstPtr m_lastStateMsg;
        ros::Duration                    m_dt;
        double                           m_halfStepSize;
        Eigen::Vector3d                  m_filteredForce;
        double                           m_forceThresh;
        boost::thread                    m_controlLoopThread;
        tf::TransformBroadcaster         m_tfBroadcaster;
        bool                             m_shutdown;
        ros::Duration                    m_halfStepGeneratorDuration;
        std::vector<uint8_t>             m_kEfforts;



        StepPlannerType                  m_stepPlannerType;


    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


}
}

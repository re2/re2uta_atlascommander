/*
 * AtlasController.hpp
 *
 *  Created on: Apr 22, 2013
 *      Author: andrew.somerville
 */

#ifndef ATLASCONTROLLER_HPP_
#define ATLASCONTROLLER_HPP_


#include <boost/shared_ptr.hpp>

namespace re2uta
{

class AtlasController
{
    public:
        boost::shared_ptr<AtlasController> Ptr;

    public:
        AtlasController();
        virtual ~AtlasController();

        bool Accepts( int commandType );

        std::string name();


};

}


#endif /* ATLASCONTROLLER_HPP_ */

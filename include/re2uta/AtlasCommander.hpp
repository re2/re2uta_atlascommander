/*
 * AtlasCommander.hpp
 *
 *  Created on: Apr 22, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2uta/AtlasCommander/WalkCommanderInterface.hpp>
#include <re2uta/AtlasCommander/DefaultJointController.hpp>
#include <re2uta/AtlasCommander/FullBodyController.hpp>
//#include <re2uta/AtlasCommander/WalkController.hpp>
#include <re2uta/AtlasCommander/BalanceCommander.hpp>
#include <re2uta/AtlasCommander/types.hpp>
#include <re2uta/FullBodyPoseSolver.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <re2/visutils/VisualDebugPublisher.h>
#include <tf/transform_listener.h>
#include <atlas_msgs/AtlasSimInterfaceState.h>
#include <atlas_msgs/AtlasState.h>
#include <re2/eigen/eigen_util.h>
#include <urdf/model.h>
#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <map>
#include <typeinfo>


namespace re2uta
{

class AtlasCommander
{
    public:
        typedef boost::shared_ptr<AtlasCommander> Ptr;
//        typedef std::map<atlascommander::ControllerType, AtlasController::Ptr> ControllerMap;

        enum ResultType
        {
            Failure_Result = 0,
            Success_Result,
        };

        enum WalkCommanderType
        {
            BDI_WALK_COMMANDER = 0,
            RE2UTA_WALK_COMMMANDER,
            BALANCE_WALK_COMMMANDER,
            CAPTUREPOINT_WALK_COMMMANDER,
            CRAWL_WALK_COMMMANDER,
            OFF
        };

    public:
        AtlasCommander( const urdf::Model & mode, AtlasLookup::Ptr const & atlasLookup );
        virtual ~AtlasCommander();


        atlascommander::Result
        walkTo( const atlascommander::WalkGoal::Ptr & walkGoal );

        atlascommander::Result
        walkTo( const atlascommander::WalkGoalVectorPtr & walkGoals );

        atlascommander::Result
        commandFramePose( const atlascommander::FrameGoal::Ptr frameGoal );

        atlascommander::Result
        setWalkCommanderType( WalkCommanderType commanderType );

        atlascommander::Result
        setStepPlannerType( atlascommander::WalkCommanderInterface::StepPlannerType stepPlannerType );


    protected:
        void
        handleBdiStateMessage( const atlas_msgs::AtlasSimInterfaceStateConstPtr & msg );

        void
        handleAtlasStates( const atlas_msgs::AtlasState::ConstPtr & msg );

        void stimulate( const ros::Time & sampleTime );
        void stateUpdateCallbackLoop();

        Eigen::Affine3d
        lookupTransform( const std::string & fromFrame, const std::string & toFrame );

    private:
        ros::NodeHandle                         m_node;
        ros::Duration                           m_commandLoopPeriod;
        bool                                    m_shutdown;

        urdf::Model                             m_urdfModel;
        AtlasLookup::Ptr                        m_atlasLookup;
        ros::Subscriber                         m_atlasStateSub;
        ros::Publisher                          m_stateUpdateLoopTestPub;
        ros::Publisher                          m_walkCommanderChangedPub;
        ros::CallbackQueue                      m_stateUpdateThreadCallbackQueue;
        boost::thread                           m_stateUpdateThread;
        boost::mutex                            m_stateMsgReadyMutex;
        boost::condition_variable               m_stateMsgReadyCondition;
        ros::Time                               m_lastNotifyTime;
        atlas_msgs::AtlasState::ConstPtr        m_lastStateMsg;

        atlascommander::JointController::Ptr    m_jointController;
        atlascommander::FullBodyController::Ptr m_fullBodyController;
//        atlascommander::WalkController::Ptr     m_walkController;
        atlascommander::WalkCommanderInterface::Ptr m_walkCommander;

        atlascommander::WalkCommanderInterface::
        StepPlannerType                         m_stepPlannerType;


        re2::VisualDebugPublisher::Ptr          m_vizDebugPublisher;
        tf::TransformListener                   m_tfListener;
        WalkCommanderType                       m_walkCommanderType;

};

}



cmake_minimum_required(VERSION 2.4.6)
include($ENV{ROS_ROOT}/core/rosbuild/rosbuild.cmake)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
#set(ROS_BUILD_TYPE RelWithDebInfo)
#set(ROS_BUILD_TYPE Debug)

rosbuild_init()

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

#SET (CMAKE_CXX_COMPILER             "/usr/bin/clang++")

find_package(PCL 1.6 REQUIRED)
#find_package(re2uta_splines REQUIRED)
rosbuild_genmsg()

find_package(catkin REQUIRED COMPONENTS
  re2uta_splines
)

include_directories(include
#  ${catkin_INCLUDE_DIRS}
  ${PCL_INCLUDE_DIRS}
  ${re2uta_splines_INCLUDE_DIRS}
)

rosbuild_add_library( ${PROJECT_NAME}_1
									  src/AtlasCommander.cpp
									  )

rosbuild_add_library( ${PROJECT_NAME}_2
                                      src/SupportPolygon.cpp
 								      src/CalculateCapturePoint.cpp
# 								      src/CalculateSwingTraj.cpp
									  src/AtlasCommander.cpp
#                                      src/CommandLoop.cpp
                                      src/interactive_marker_utils.cpp
                                      src/utils.cpp
                                      src/ros_utils.cpp
                                      src/walking/WalkPatternGenerator.cpp
                                      src/walking/StepGenerator.cpp
                                      src/walking/CirclePathStepGenerator.cpp
                                      src/walking/ManualStepGenerator.cpp
                                      src/walking/NaoPathStepGenerator.cpp
                                      src/walking/StepTrajectoryGenerator.cpp
                                      src/walking/RosHalfstepTrajectoryGenerator.cpp
                                      src/splines/splines.cpp
                                      src/splines/Spline.cpp
                                      src/controllers/BalanceCommander.cpp
                                      src/controllers/BdiStepCommander.cpp
                                      src/controllers/CapturePointCommander.cpp
                                      src/controllers/CrawlCommander.cpp
                                      src/controllers/DefaultJointController.cpp
                                      src/controllers/FullBodyController.cpp
                                      src/controllers/JointController.cpp
                                      src/controllers/NeuralNetworkJointController.cpp
#                                      src/controllers/CompliantModelJointController.cpp
                                      src/controllers/NullJointController.cpp
                                      src/controllers/NullWalkCommander.cpp
                                      src/controllers/PreviewWalkCommander.cpp
                                      src/controllers/WalkCommanderInterface.cpp
#                                      src/controllers/SimpleWalkCommander.cpp
                                      )

rosbuild_add_compile_flags(${PROJECT_NAME}_2
#                                           -Werror
                                            -Wall
                                            -Werror=return-type
#                                            -Wno-error=delete-non-virtual-dtor # clang only
#                                            -Wno-error=self-assign             # clang only
                                            -Wno-error=unused-variable
                                            -Wno-error=unused
                                            -std=c++0x                          # gcc only
#                                            -std=c++11                          # clang only
                                            -Wno-error=unused-but-set-variable  # gcc only
#                                            -Wno-error=c++11-extensions        # clang only
                                            )

#rosbuild_add_library( ${PROJECT_NAME}Client src/AtlasCommanderClient.cpp )



rosbuild_add_executable( AtlasCommanderNode           src/AtlasCommanderNode.cpp              )
rosbuild_add_executable( GeometricSimulationNode      src/GeometricSimulationNode.cpp         )
#rosbuild_add_executable( OdomPublisherNode            src/OdomPublisherNode.cpp               )
#rosbuild_add_compile_flags( AtlasCommander_node       -std=c++0x )
#
#rosbuild_add_executable( AtlasCommander_test          src/test/AtlasCommanderTest.cpp         )
#rosbuild_add_executable( AtlasCommanderNode_test      src/test/AtlasCommanderNodeTest.cpp     )
#rosbuild_add_executable( AtlasStateRate_test          src/test/AtlasStateRateTest.cpp         )
#rosbuild_add_executable( AtlasCommanderClient_test   src/test/AtlasCommanderClientTest.cpp   )
#rosbuild_add_executable( BdiStepCommander_test        src/test/BdiStepCommanderTest.cpp       )
#rosbuild_add_executable( CalculateCapturePoint_test   src/test/CalculateCapturePointTest.cpp  )
#rosbuild_add_executable( CalculateSwingTraj_test 	  src/test/CalculateSwingTrajTest.cpp     )
#rosbuild_add_executable( CirclePathStepGenerator      src/test/CirclePathStepGeneratorTest.cpp  )
#rosbuild_add_executable( CommandLoop_test            src/test/CommandLoopTest.cpp            )
#rosbuild_add_executable( DefaultJointController_test  src/test/DefaultJointControllerTest.cpp )
rosbuild_add_executable( GeometricSimulation_test     src/test/GeometricSimulationTest.cpp    )
#rosbuild_add_executable( FullBodyController_test      src/test/FullBodyControllerTest.cpp     )
#rosbuild_add_executable( InteractiveMarker_test       src/test/InteractiveMarkerTest.cpp      )
#rosbuild_add_executable( JointStateRate_test          src/test/JointStateRateTest.cpp         )
#rosbuild_add_executable( NaoPathStepGenerator_test     src/test/NaoPathStepGeneratorTest.cpp  )
#rosbuild_add_executable( PathGeneration_test          src/test/PathGenerationTest.cpp         )
#rosbuild_add_executable( PreviewWalkCommander_test    src/test/PreviewWalkCommanderTest.cpp   )
#rosbuild_add_executable( Synch_test                   src/test/SynchTest.cpp                  )
#rosbuild_add_executable( StepTrajectoryGenerator_test    src/test/StepTrajectoryGeneratorTest.cpp   )
#rosbuild_add_executable( WalkPatternGenerator_test    src/test/WalkPatternGeneratorTest.cpp   )
#rosbuild_add_executable( ComProjection_test           src/test/ComProjectionTest.cpp		  )
#rosbuild_add_compile_flags(NaoPathStepGenerator_test -ggdb)

rosbuild_add_compile_flags( AtlasCommanderNode -std=c++0x )
#rosbuild_add_compile_flags( AtlasCommanderNode -std=c++11 )

target_link_libraries( AtlasCommanderNode          ${PROJECT_NAME}_1 )
target_link_libraries( AtlasCommanderNode          ${PROJECT_NAME}_2 )
target_link_libraries( AtlasCommanderNode          ${PCL_LIBRARIES} )
#target_link_libraries( GeometricSimulationNode     ${PROJECT_NAME} )
#target_link_libraries( OdomPublisherNode           ${PROJECT_NAME} )
#
#target_link_libraries( AtlasCommander_test         ${PROJECT_NAME} )
#target_link_libraries( AtlasCommanderNode_test     ${PROJECT_NAME} )
#target_link_libraries( AtlasCommanderClient_test   ${PROJECT_NAME} )
#target_link_libraries( AtlasCommanderClient_test   ${PROJECT_NAME}Client )
#target_link_libraries( BdiStepCommander_test       ${PROJECT_NAME} )
#target_link_libraries( CalculateCapturePoint_test  ${PROJECT_NAME} )
#target_link_libraries( CalculateSwingTraj_test 	${PROJECT_NAME} )
#target_link_libraries( CommandLoop_test            ${PROJECT_NAME} )
#target_link_libraries( DefaultJointController_test ${PROJECT_NAME} )
#target_link_libraries( FullBodyController_test     ${PROJECT_NAME} )
#target_link_libraries( InteractiveMarker_test      ${PROJECT_NAME} )
#target_link_libraries( NaoPathStepGenerator_test         ${PROJECT_NAME} )
#target_link_libraries( PathGeneration_test          ${PROJECT_NAME} )
#target_link_libraries( PreviewWalkCommander_test    ${PROJECT_NAME} )
#target_link_libraries( StepTrajectoryGenerator_test ${PROJECT_NAME} )
#target_link_libraries( WalkPatternGenerator_test    ${PROJECT_NAME} )
#target_link_libraries( ComProjection_test           ${PROJECT_NAME} )

